/**
 * Represents a component.
 */
export enum MyGeneratorComponent
{
    /**
     * Indicates the Woodpecker CI configuration component.
     */
    WoodpeckerCI = "woodpecker-configuration"
}
