import { ok } from "node:assert";
import { BaseGeneratorFactory, ComponentCollection, FileMapping, FileMappingCollectionEditor, GeneratorConstructor, GeneratorExtensionConstructor, GeneratorOptions, IBaseGeneratorOptions, IComponentCollection } from "@manuth/extended-yo-generator";
import { JSONCConverter, JSONCCreatorMapping, TSConfigFileMapping, TSProjectGenerator, TSProjectPackageFileMapping } from "@manuth/generator-ts-project";
// eslint-disable-next-line node/no-unpublished-import
import type { TSConfigJSON } from "types-tsconfig";
import { MarkdownFileProcessor } from "./MarkdownFileProcessor.js";
import { MyGeneratorComponent } from "./MyGeneratorComponent.js";
import { MyTSProjectPackageFileMapping } from "./MyTSProjectPackageFileMapping.js";
import { WoodpeckerFileMapping } from "./WoodpeckerFileMapping.js";

/**
 * Provides the functionality to create base-constructors.
 *
 * @template T
 * The type of the constructor of the base generator.
 */
export class MyTSProjectGenerator<T extends GeneratorConstructor<TSProjectGenerator<any, any, any>>> extends BaseGeneratorFactory<T>
{
    /**
     * Initializes a new instance of the {@linkcode MyTSProjectGenerator} class.
     */
    protected constructor()
    {
        super();
    }

    /**
     * Gets the default instance of the {@linkcode MyTSProjectGenerator} class.
     */
    protected static override get Default(): MyTSProjectGenerator<any>
    {
        return new MyTSProjectGenerator();
    }

    /**
     * @inheritdoc
     *
     * @template TBase
     * The type of the constructor of the base generator.
     *
     * @param base
     * The constructor the generated constructor should be based on.
     *
     * @param options
     * An object containing options for creating the base generator.
     *
     * @returns
     * The generated constructor.
     */
    public static override Create<TBase extends GeneratorConstructor>(base: TBase, options?: IBaseGeneratorOptions): GeneratorExtensionConstructor<TBase>
    {
        return this.Default.Create(base, options);
    }

    /**
     * Creates a new base-constructor.
     *
     * @param base
     * The constructor the generated constructor should be based on.
     *
     * @param options
     * An object containing options for creating the base generator.
     *
     * @returns
     * The generated constructor.
     */
    protected override CreateConstructor(base: T, options?: IBaseGeneratorOptions): GeneratorExtensionConstructor<T>
    {
        let baseClass = super.CreateConstructor(base, options);
        let self = this;

        /**
         * Represents a base-generator inheriting the specified base.
         */
        class BaseGenerator extends baseClass
        {
            /**
             * Initializes a new instance of the {@linkcode BaseGenerator} class.
             *
             * @param args
             * The arguments for creating the base generator.
             *
             * @param options
             * A set of options for the generator.
             */
            public constructor(args: string | string[], options: GeneratorOptions)
            {
                super(args, options);
            }

            /**
             * @inheritdoc
             */
            protected override get BaseComponents(): ComponentCollection<any, any>
            {
                let components = super.BaseComponents;
                ok(components);

                components.Categories.Replace(
                    () => true,
                    (item) =>
                    {
                        item.Components.Replace(
                            () => true,
                            (item) =>
                            {
                                self.ProcessFileMappings(this, item.FileMappings);
                                return item;
                            });

                        return item;
                    });

                return components;
            }

            /**
             * @inheritdoc
             */
            protected override get BaseFileMappings(): FileMappingCollectionEditor
            {
                let result = super.BaseFileMappings;
                self.ProcessFileMappings(this, result);
                return result;
            }

            /**
             * @inheritdoc
             */
            public override get Components(): IComponentCollection<any, any>
            {
                let components = super.Components;

                for (let category of components.Categories)
                {
                    if (category.DisplayName === "General")
                    {
                        category.Components.push(
                            {
                                ID: MyGeneratorComponent.WoodpeckerCI,
                                DisplayName: "Woodpecker CI Configuration",
                                DefaultEnabled: true,
                                FileMappings: ["check", "deploy"].map(
                                    (stage) =>
                                    {
                                        let result = new WoodpeckerFileMapping(this);
                                        result.PipelineStage = stage;
                                        return result;
                                    })
                            });
                    }
                }

                return components;
            }

            /**
             * @inheritdoc
             */
            public override async cleanup(): Promise<void>
            {
                return this.Base.cleanup();
            }
        }

        return BaseGenerator as any;
    }

    /**
     * Processes the file-mappings.
     *
     * @param generator
     * The generator to process the file-mappings for.
     *
     * @param fileMappings
     * The file-mappings to process.
     *
     * @returns
     * The processed file-mappings.
     */
    protected ProcessFileMappings(generator: TSProjectGenerator<any, any, any>, fileMappings: FileMappingCollectionEditor): void
    {
        fileMappings.ReplaceContext(
            (fileMapping: FileMapping<any, any>) => fileMapping.Destination.endsWith(".md"),
            (fileMapping) => new MarkdownFileProcessor(generator, fileMapping.Result));

        fileMappings.ReplaceContext(
            TSProjectPackageFileMapping,
            (fileMapping) =>
            {
                return new MyTSProjectPackageFileMapping(
                    generator,
                    fileMapping.ResolverContext as TSProjectPackageFileMapping<any, any, any>);
            });

        fileMappings.ReplaceContext(
            (fileMapping: FileMapping<any, any>) => fileMapping.Destination === generator.destinationPath(TSConfigFileMapping.GetFileName("base")),
            (fileMapping) =>
            {
                return {
                    Source: fileMapping.Source,
                    Destination: fileMapping.Destination,
                    Processor: async (target, generator) =>
                    {
                        ok(target.Source);
                        let originalConfig: TSConfigJSON = new JSONCConverter().Parse(generator.fs.read(target.Source)) as TSConfigJSON;
                        originalConfig.compilerOptions ??= {};
                        await fileMapping.Processor();
                        let tsConfig: TSConfigJSON = new JSONCConverter().Parse(generator.fs.read(target.Destination)) as TSConfigJSON;
                        tsConfig.compilerOptions ??= {};
                        tsConfig.compilerOptions.plugins = originalConfig.compilerOptions.plugins;
                        return new JSONCCreatorMapping(generator, target.Destination, tsConfig).Processor();
                    }
                };
            });
    }
}
