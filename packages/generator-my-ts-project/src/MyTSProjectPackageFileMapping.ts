import { GeneratorOptions } from "@manuth/extended-yo-generator";
import { IScriptMapping, ITSProjectPackage, ITSProjectSettings, ScriptMapping, TSProjectGenerator, TSProjectPackageFileMapping } from "@manuth/generator-ts-project";
import { Package, PackageDependencyCollection } from "@manuth/package-json-editor";
import { Constants } from "./Constants.js";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import type { MyTSProjectGenerator } from "./MyTSProjectGenerator.js";

/**
 * Represents a file-mapping for the `package.json` file of {@linkcode MyTSProjectGenerator}s.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 *
 * @template TPackage
 * The type of the packages of the generator.
 */
export class MyTSProjectPackageFileMapping<TSettings extends ITSProjectSettings, TOptions extends GeneratorOptions, TPackage extends ITSProjectPackage> extends TSProjectPackageFileMapping<TSettings, TOptions, TPackage>
{
    /**
     * The base of the file-mapping.
     */
    private baseFileMapping: TSProjectPackageFileMapping<TSettings, TOptions, TPackage>;

    /**
     * Initializes a new instance of the {@linkcode MyTSProjectPackageFileMapping} class.
     *
     * @param generator
     * The generator of the file-mapping.
     *
     * @param baseFileMapping
     * The base of the file-mapping.
     */
    public constructor(generator: TSProjectGenerator<TSettings, TOptions, TPackage>, baseFileMapping: TSProjectPackageFileMapping<TSettings, TOptions, TPackage>)
    {
        super(generator, baseFileMapping.CreationContext);
        this.baseFileMapping = baseFileMapping;
    }

    /**
     * @inheritdoc
     */
    public override get Destination(): string
    {
        return this.Base.Destination;
    }

    /**
     * @inheritdoc
     *
     * @returns
     * The object to dump.
     */
    public override GetSourceObject(): Promise<Package>
    {
        return this.Base.GetPackage();
    }

    /**
     * @inheritdoc
     */
    public override get TypeScriptScripts(): Array<IScriptMapping<TSettings, TOptions> | string>
    {
        return [
            ...(
                this.CreationContext.IsWorkspaceRoot ?
                [
                    {
                        Destination: "prepare",
                        Processor: async () =>
                        {
                            return this.Base.ScriptSource.Scripts.Get("initialize");
                        }
                    }
                ] as Array<IScriptMapping<TSettings, TOptions> | string> :
                []),
            ...this.GetBaseScripts(this.Base.TypeScriptScripts)
        ];
    }

    /**
     * @inheritdoc
     */
    public override get LintScripts(): Array<IScriptMapping<TSettings, TOptions> | string>
    {
        return this.GetBaseScripts(this.Base.LintScripts);
    }

    /**
     * @inheritdoc
     */
    public override get MiscScripts(): Array<IScriptMapping<TSettings, TOptions> | string>
    {
        return this.GetBaseScripts(this.Base.MiscScripts);
    }

    /**
     * @inheritdoc
     */
    public override get ScriptSource(): Package
    {
        return Constants.Package;
    }

    /**
     * Gets the base of the file-mapping.
     */
    protected get Base(): TSProjectPackageFileMapping<TSettings, TOptions, TPackage>
    {
        return this.baseFileMapping;
    }

    /**
     * Gets scripts from the base file-mapping.
     *
     * @param scriptMappings
     * The scripts to retrieve from the base file-mapping.
     *
     * @returns
     * The scripts retrieved from the base file-mapping.
     */
    protected GetBaseScripts(scriptMappings: Array<IScriptMapping<TSettings, TOptions> | string>): Array<IScriptMapping<TSettings, TOptions>>
    {
        return scriptMappings.map(
            (script) =>
            {
                return this.GetBaseScript(script);
            });
    }

    /**
     * Gets a script from the base file-mapping.
     *
     * @param scriptMapping
     * The script to retrieve from the base file-mapping.
     *
     * @returns
     * The script retrieved from the base file-mapping.
     */
    protected GetBaseScript(scriptMapping: IScriptMapping<TSettings, TOptions> | string): IScriptMapping<TSettings, TOptions>
    {
        let result: IScriptMapping<TSettings, TOptions>;

        if (typeof scriptMapping === "string")
        {
            result = {
                Destination: scriptMapping
            };
        }
        else
        {
            result = {
                ID: scriptMapping.ID,
                Destination: scriptMapping.Destination
            };
        }

        result.Processor = async (script, scriptMapping) =>
        {
            return this.Base.ScriptMappingCollection.Get(
                (baseScriptMapping: ScriptMapping<TSettings, TOptions>) =>
                {
                    return baseScriptMapping.Destination === result.Destination;
                }).Processor();
        };

        return result;
    }

    /**
     * @inheritdoc
     *
     * @returns
     * The loaded package.
     */
    protected override async LoadPackage(): Promise<Package>
    {
        let result = await this.Base.GetPackage();

        if (this.CreationContext.IsWorkspaceRoot)
        {
            result.Register(
                new PackageDependencyCollection(
                    this.Base.ScriptSource,
                    {
                        devDependencies: [
                            "@types/nameof",
                            "@typescript-nameof/nameof",
                            "ts-patch"
                        ]
                    }),
                true);
        }

        return result;
    }
}
