import { GeneratorOptions, GeneratorSettingKey, IGenerator } from "@manuth/extended-yo-generator";
import { ITSProjectSettings, TSProjectComponent, YAMLTransformMapping } from "@manuth/generator-ts-project";
import path from "upath";
import { Document } from "yaml";
import { toJS } from "yaml/util";

const { join } = path;

/**
 * Provides the functionality to create a Woodpecker CI config files.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 */
export class WoodpeckerFileMapping<TSettings extends ITSProjectSettings, TOptions extends GeneratorOptions> extends YAMLTransformMapping<TSettings, TOptions>
{
    /**
     * The name of the pipeline stage of the file.
     */
    private pipelineStage: string | null = null;

    /**
     * Initializes a new instance of the {@linkcode WoodpeckerFileMapping} class.
     *
     * @param generator
     * The generator of the file-mapping.
     */
    public constructor(generator: IGenerator<TSettings, TOptions>)
    {
        super(generator);
    }

    /**
     * Gets the base name of the file.
     */
    public get BaseName(): string
    {
        if (this.PipelineStage !== null)
        {
            return join(".woodpecker", `.${this.PipelineStage}.yml`);
        }
        else
        {
            return ".woodpecker.yml";
        }
    }

    /**
     * Gets or sets the name of the pipeline stage of the file.
     */
    public get PipelineStage(): string | null
    {
        return this.pipelineStage;
    }

    /**
     * @inheritdoc
     */
    public set PipelineStage(value: string | null)
    {
        this.pipelineStage = value;
    }

    /**
     * @inheritdoc
     */
    public override get Source(): string
    {
        return this.Generator.modulePath(this.BaseName);
    }

    /**
     * @inheritdoc
     */
    public get Destination(): string
    {
        return this.BaseName;
    }

    /**
     * @inheritdoc
     *
     * @param documents
     * The documents to transform.
     *
     * @returns
     * The transformed documents.
     */
    public override async Transform(documents: Document.Parsed[]): Promise<Document.Parsed[]>
    {
        let stepsKey = "steps";
        let imageKey = "image";
        let commandsKey = "commands";
        let document = documents[0];
        let steps: Record<string, any> = toJS(document.get(stepsKey), null);

        for (let name in steps)
        {
            let step = steps[name];

            if (imageKey in step)
            {
                let image = step[imageKey];

                if (typeof image === "string")
                {
                    document.setIn(
                        [stepsKey, name, imageKey],
                        image.replace(/^(node)(:.+)?$/, "$1"));
                }
            }

            if (
                !this.Generator.Settings[GeneratorSettingKey.Components]?.includes(TSProjectComponent.MonoRepo) &&
                commandsKey in step)
            {
                let commands: string[] = step[commandsKey];

                for (let i = 0; i < commands.length; i++)
                {
                    let command = commands[i];

                    if (/\s+--workspaces\b/.test(command))
                    {
                        document.setIn(
                            [stepsKey, name, commandsKey, i],
                            command.replace(/\s+--workspaces\b/, ""));
                    }
                }
            }
        }

        return [document];
    }
}
