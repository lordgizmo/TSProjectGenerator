import { deepStrictEqual, ok, strictEqual } from "node:assert";
import { GeneratorOptions } from "@manuth/extended-yo-generator";
import { FileMappingTester, TestContext } from "@manuth/extended-yo-generator-test";
import { ITSProjectPackage, ITSProjectSettings, TSProjectPackageFileMapping } from "@manuth/generator-ts-project";
import { Package } from "@manuth/package-json-editor";
import { TestTSModuleGenerator } from "./TestTSModuleGenerator.js";
import { MyTSProjectPackageFileMapping } from "../MyTSProjectPackageFileMapping.js";

/**
 * Registers tests for the {@linkcode MyTSProjectPackageFileMapping} class.
 *
 * @param context
 * The test-context.
 */
export function MyTSProjectPackageFileMappingTests(context: TestContext<TestTSModuleGenerator>): void
{
    suite(
        nameof(MyTSProjectPackageFileMapping),
        () =>
        {
            let transformPlugin = "@typescript-nameof/nameof";
            let patchPackageName = "ts-patch";
            let generator: TestTSModuleGenerator;
            let tester: FileMappingTester<TestTSModuleGenerator, ITSProjectSettings, GeneratorOptions, TestMyTSProjectPackageFileMapping>;
            let npmPackage: Package;

            /**
             * Provides an implementation of the {@linkcode MyTSProjectPackageFileMapping} class for testing.
             */
            class TestMyTSProjectPackageFileMapping extends MyTSProjectPackageFileMapping<ITSProjectSettings, GeneratorOptions, ITSProjectPackage>
            {
                /**
                 * @inheritdoc
                 */
                public override get Base(): TSProjectPackageFileMapping<ITSProjectSettings, GeneratorOptions, ITSProjectPackage>
                {
                    return super.Base;
                }

                /**
                 * @inheritdoc
                 *
                 * @returns
                 * The loaded package.
                 */
                public override async LoadPackage(): Promise<Package>
                {
                    return super.LoadPackage();
                }
            }

            suiteSetup(
                async function()
                {
                    this.timeout(5 * 60 * 1000);
                    generator = await context.Generator;

                    tester = new FileMappingTester(
                        generator,
                        new TestMyTSProjectPackageFileMapping(
                            generator,
                            new TSProjectPackageFileMapping(generator.Base, generator.PackageCreationContext)));
                });

            setup(
                async () =>
                {
                    await tester.Run();
                    npmPackage = new Package(tester.FileMapping.Destination);
                });

            suite(
                "General",
                () =>
                {
                    test(
                        "Checking whether all scripts from the base file-mapping are included…",
                        () =>
                        {
                            ok(
                                tester.FileMappingOptions.Base.ScriptMappingCollection.Items.every(
                                    (scriptMapping) =>
                                    {
                                        return tester.FileMappingOptions.ScriptMappingCollection.Items.some(
                                            (script) =>
                                            {
                                                return script.Destination === scriptMapping.Destination;
                                            });
                                    }));
                        });
                });

            suite(
                nameof<TestMyTSProjectPackageFileMapping>((fileMapping) => fileMapping.MiscScripts),
                () =>
                {
                    test(
                        `Checking whether all scripts for using \`${patchPackageName}\` are present…`,
                        () =>
                        {
                            strictEqual(npmPackage.Scripts.Get("prepare"), "ts-patch install");
                        });
                });

            suite(
                nameof<TestMyTSProjectPackageFileMapping>((fileMapping) => fileMapping.LoadPackage),
                () =>
                {
                    let dependencies: string[];

                    suiteSetup(
                        () =>
                        {
                            dependencies = [
                                transformPlugin,
                                patchPackageName
                            ];
                        });

                    test(
                        "Checking whether properties from the base package are applied properly…",
                        async () =>
                        {
                            let exports = context.RandomObject;

                            let testFileMapping = new TestMyTSProjectPackageFileMapping(
                                generator,
                                new class extends TSProjectPackageFileMapping<ITSProjectSettings, GeneratorOptions, ITSProjectPackage>
                                {
                                    /**
                                     * @inheritdoc
                                     */
                                    protected override async LoadPackage(): Promise<Package>
                                    {
                                        let result = await super.LoadPackage();
                                        result.Exports = exports;
                                        return result;
                                    }
                                }(generator, generator.PackageCreationContext));

                            deepStrictEqual((await testFileMapping.LoadPackage()).Exports, exports);
                        });

                    test(
                        `Checking whether all required dependencies for using \`${transformPlugin}\` are present…`,
                        () =>
                        {
                            for (let dependency of dependencies)
                            {
                                ok(npmPackage.AllDependencies.Has(dependency));

                                ok(
                                    npmPackage.AllDependencies.Entries.some(
                                        (entry) =>
                                        {
                                            return entry[0].startsWith("@types/") &&
                                                entry[1].startsWith("npm:@typescript-nameof/types@");
                                        }));
                            }
                        });
                });
        });
}
