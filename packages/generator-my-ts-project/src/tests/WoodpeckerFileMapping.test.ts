import { ok } from "node:assert";
import { GeneratorOptions, GeneratorSettingKey } from "@manuth/extended-yo-generator";
import { TestContext } from "@manuth/extended-yo-generator-test";
import { ITSProjectSettings, TSProjectComponent } from "@manuth/generator-ts-project";
import { YAMLFileMappingTester } from "@manuth/generator-ts-project-test";
import { Document } from "yaml";
import { MyTSModuleGenerator } from "../generators/module/MyTSModuleGenerator.js";
import { WoodpeckerFileMapping } from "../WoodpeckerFileMapping.js";

/**
 * Registers tests for the {@linkcode WoodpeckerFileMapping} class.
 *
 * @param context
 * The test-context.
 */
export function WoodpeckerFileMappingTests(context: TestContext<MyTSModuleGenerator>): void
{
    suite(
        nameof(WoodpeckerFileMapping),
        () =>
        {
            let generator: MyTSModuleGenerator;
            let testers: Array<YAMLFileMappingTester<MyTSModuleGenerator, ITSProjectSettings, GeneratorOptions, WoodpeckerFileMapping<ITSProjectSettings, GeneratorOptions>>> = [];

            /**
             * Represents a condition for commands.
             */
            type CommandCondition = (command: string) => boolean;

            /**
             * Gets the emitted documents.
             *
             * @returns
             * The emitted documents.
             */
            async function GetDocuments(): Promise<Document.Parsed[]>
            {
                return (
                    await Promise.all(testers.map(
                        async (tester) =>
                        {
                            return tester.ParseOutput();
                        }))).flatMap((documents) => documents);
            }

            suiteSetup(
                async function()
                {
                    try
                    {
                        this.timeout(5 * 60 * 1000);
                        generator = await context.Generator;

                        for (let stage of ["check", "deploy"])
                        {
                            let fileMapping = new WoodpeckerFileMapping(generator);
                            fileMapping.PipelineStage = stage;
                            testers.push(new YAMLFileMappingTester(generator, fileMapping));
                        }
                    }
                    catch (e)
                    {
                        console.log(e);
                    }
                });

            setup(
                async () =>
                {
                    try
                    {
                        await Promise.all(testers.map((tester) => tester.Run()));
                    }
                    catch (e)
                    {
                        console.log(e);
                    }
                });

            /**
             * Asserts the truthiness of the specified {@linkcode condition}.
             *
             * @param condition
             * The condition to assert.
             *
             * @param all
             * A value indicating whether the assertion should apply to all commands.
             *
             * @returns
             * A value indicating whether the assertion is true.
             */
            async function AssertCommand(condition: CommandCondition, all = false): Promise<boolean>
            {
                let filter = <T>(array: T[]) => (condition: (item: T) => boolean) => (all ? array.every(condition) : array.some(condition));

                return filter(await GetDocuments())(
                    (document) =>
                    {
                        let steps: any[] = Object.values(document.toJSON().steps);

                        return filter(steps)(
                            (step) =>
                            {
                                let commands: string[] = step.commands ?? [];

                                return filter(commands)(
                                    (command) =>
                                    {
                                        return condition(command);
                                    });
                            });
                    });
            }

            suite(
                nameof<WoodpeckerFileMapping<any, any>>((fileMapping) => fileMapping.Transform),
                () =>
                {
                    let imageName = "node";
                    let imageKey = "image";
                    let workspaceArg = "--workspaces";

                    test(
                        `Checking whether \`${imageName}\`-steps are executed without a specific version…`,
                        async function()
                        {
                            this.timeout(2 * 1000);
                            this.slow(1 * 1000);

                            ok(
                                (await GetDocuments()).every(
                                    (document) =>
                                    {
                                        return (Object.values(document.toJSON().steps as any[]).every(
                                            (step) =>
                                            {
                                                if (imageKey in step)
                                                {
                                                    let image = step[imageKey];

                                                    return typeof image !== "string" ||
                                                        !image.startsWith(imageName) ||
                                                        image === imageName;
                                                }
                                                else
                                                {
                                                    return true;
                                                }
                                            }));
                                    }));
                        });

                    test(
                        `Checking whether \`${workspaceArg}\` arguments of commands are stripped away…`,
                        async function()
                        {
                            this.timeout(2 * 1000);
                            this.slow(1 * 1000);
                            ok(await AssertCommand((command) => !command.includes(workspaceArg), true));
                        });

                    test(
                        `Checking whether the \`${workspaceArg}\` remains for mono repo environments…`,
                        async function()
                        {
                            this.timeout(4 * 1000);
                            this.slow(2 * 1000);
                            generator.Settings[GeneratorSettingKey.Components] = [TSProjectComponent.MonoRepo];

                            for (let tester of testers)
                            {
                                await tester.Run();
                            }

                            ok(await AssertCommand((command) => command.includes(workspaceArg)));
                        });

                    test(
                        "Checking whether gitea-releases are adjusted correctly…",
                        async function()
                        {
                            this.timeout(2 * 1000);
                            this.slow(1 * 1000);

                            ok(
                                (await GetDocuments()).every(
                                    (document) =>
                                    {
                                        let steps: any[] = Object.values(document.toJSON().steps);

                                        return steps.every(
                                            (step) =>
                                            {
                                                if (step.image === "woodpeckerci/plugin-gitea-release")
                                                {
                                                    let files = step.settings.files;

                                                    return (files.length === 1) &&
                                                        (files[0] === "*.tgz");
                                                }
                                                else
                                                {
                                                    return true;
                                                }
                                            });
                                    }));
                        });
                });
        });
}
