import { fileURLToPath } from "node:url";
import { TestContext } from "@manuth/extended-yo-generator-test";
import { GeneratorName } from "@manuth/generator-ts-project";
import upath from "upath";
import { MarkdownFileProcessorTests } from "./MarkdownFileProcessor.test.js";
import { MyTSProjectGeneratorTests } from "./MyTSProjectGenerator.test.js";
import { MyTSProjectPackageFileMappingTests } from "./MyTSProjectPackageFileMapping.test.js";
import { TestTSModuleGenerator } from "./TestTSModuleGenerator.js";
import { WoodpeckerFileMappingTests } from "./WoodpeckerFileMapping.test.js";

const { join } = upath;

/**
 * Registers the main tests.
 */
function MainTests(): void
{
    suite(
        "MyTSProjectGenerator",
        () =>
        {
            let workingDir: string;
            let context = TestContext.Default;

            let projectContext = new TestContext<TestTSModuleGenerator>(join(fileURLToPath(new URL(".", import.meta.url)), "generators", GeneratorName.Main));

            suiteSetup(
                () =>
                {
                    workingDir = process.cwd();
                });

            suiteTeardown(
                () =>
                {
                    process.chdir(workingDir);

                    for (let testContext of [context, projectContext])
                    {
                        testContext.Dispose();
                    }
                });

            MyTSProjectGeneratorTests(projectContext);
            MarkdownFileProcessorTests();
            WoodpeckerFileMappingTests(projectContext);
            MyTSProjectPackageFileMappingTests(projectContext);
        });
}

MainTests();
