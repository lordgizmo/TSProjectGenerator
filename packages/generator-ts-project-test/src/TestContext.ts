import { Answers, DistinctQuestion, PromptModule, PromptModuleBase, QuestionTypeName } from "inquirer";
import { MockSTDIN, stdin } from "mock-stdin";
import { IMockedAnswer } from "./Inquiry/IMockedAnswer.js";
import { TestPrompt } from "./Inquiry/TestPrompt.js";

/**
 * Represents a context for testing.
 */
export class TestContext
{
    /**
     * Initializes a new instance of the {@linkcode TestContext} class.
     */
    public constructor()
    { }

    /**
     * @inheritdoc
     */
    public static get Default(): TestContext
    {
        return new TestContext();
    }

    /**
     * Prompts the specified {@linkcode questions} and mocks the specified {@linkcode answers} to the {@linkcode process.stdin}.
     *
     * @param promptModule
     * The component for prompting the questions.
     *
     * @param questions
     * The questions to prompt.
     *
     * @param answers
     * The answers to mock.
     *
     * @param mockedStdin
     * The {@linkcode MockSTDIN}-instance to use.
     *
     * @returns
     * The result of the prompts.
     */
    public async MockPrompts<T extends Answers>(promptModule: PromptModule, questions: Array<DistinctQuestion<T>>, answers: Array<string[] | IMockedAnswer>, mockedStdin?: MockSTDIN): Promise<T>
    {
        let generatedMock = null;

        if (!mockedStdin)
        {
            generatedMock = stdin();
            mockedStdin = generatedMock;
        }

        /**
         * Sends the specified {@linkcode input} to the {@linkcode process.stdin}.
         *
         * @param input
         * The input to send to the {@linkcode process.stdin}.
         */
        function SendInput(input: string[]): void
        {
            for (let line of input)
            {
                mockedStdin?.send(line);
            }
        }

        /**
         * Processes the specified {@linkcode mockedAnswer}.
         *
         * @param mockedAnswer
         * The mocked answer to process.
         */
        function ProcessMockedAnswer(mockedAnswer: IMockedAnswer | string[]): void
        {
            let answer: IMockedAnswer;

            if (Array.isArray(mockedAnswer))
            {
                answer = {
                    input: mockedAnswer
                };
            }
            else
            {
                answer = mockedAnswer;
            }

            process.nextTick(
                () =>
                {
                    if (mockedStdin)
                    {
                        answer?.preprocess?.(mockedStdin);
                    }

                    SendInput(answer?.input ?? []);

                    if (mockedStdin)
                    {
                        answer?.callback?.(mockedStdin);
                    }
                });
        }

        try
        {
            let index = 0;
            let result = promptModule(questions);
            ProcessMockedAnswer(answers[index++]);

            result.ui.process.subscribe(
                {
                    next: (answerHash) =>
                    {
                        ProcessMockedAnswer(answers[index++]);
                    }
                });

            await result;
            return result;
        }
        catch (exception)
        {
            throw exception;
        }
        finally
        {
            generatedMock?.restore();
        }
    }

    /**
     * Registers mocha tasks for restoring the working directory.
     */
    public RegisterWorkingDirRestorer(): void
    {
        let workingDir: string;

        setup(
            () =>
            {
                workingDir = process.cwd();
            });

        teardown(
            () =>
            {
                process.chdir(workingDir);
            });
    }

    /**
     * Registers the {@linkcode TestPrompt}.
     *
     * @param promptModule
     * The prompt-module to register the {@linkcode TestPrompt}.
     *
     * @param type
     * The name of the type to register the {@linkcode TestPrompt}.
     */
    public RegisterTestPrompt(promptModule: PromptModuleBase, type: QuestionTypeName = "input"): void
    {
        promptModule.registerPrompt(type, TestPrompt);
    }
}
