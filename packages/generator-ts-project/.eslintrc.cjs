const { join } = require("node:path");
const { PluginName, PresetName } = require("@manuth/eslint-plugin-typescript");

module.exports = {
    extends: [
        `plugin:${PluginName}/${PresetName.RecommendedWithTypeChecking}`
    ],
    root: true,
    env: {
        node: true,
        es6: true
    },
    parserOptions: {
        project: [
            join(__dirname, "tsconfig.app.json"),
            join(__dirname, "tsconfig.editor.json"),
            join(__dirname, "tsconfig.eslint.json"),
            join(__dirname, "src", "tests", "tsconfig.json"),
            join(__dirname, "type-tests", "tsconfig.json")
        ]
    },
    overrides: [
        {
            files: [
                "./scripts/**"
            ],
            rules: {
                "import/no-extraneous-dependencies": "off",
                "node/no-unpublished-import": "off"
            }
        }
    ]
};
