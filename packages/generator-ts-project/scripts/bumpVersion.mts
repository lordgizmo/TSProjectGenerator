import { join } from "node:path";
import { fileURLToPath } from "node:url";
import { copy } from "fs-extra";

(
    async () =>
    {
        let dirUrl = new URL(".", import.meta.url);
        let packageUrl = new URL("..", dirUrl);
        let workspaceUrl = new URL(join("..", ".."), packageUrl);
        let baseName = "scripts/realBumpVersion";
        let sourceName = `${baseName}.ts`;
        let outName = `${baseName}.js`;
        await copy(join(fileURLToPath(packageUrl), sourceName), join(fileURLToPath(workspaceUrl), sourceName));
        await import(`${new URL(outName, workspaceUrl)}`);
    })();

export { };
