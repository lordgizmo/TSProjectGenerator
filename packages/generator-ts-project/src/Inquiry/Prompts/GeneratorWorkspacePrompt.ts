import { ReadLine } from "node:readline";
import { GeneratorSettingKey, Question } from "@manuth/extended-yo-generator";
import { Answers } from "inquirer";
import { IWorkspaceQuestion } from "./IWorkspaceQuestion.js";
import { WorkspacePrompt } from "./WorkspacePrompt.js";
import { TSGeneratorCategory } from "../../generators/generator/Components/TSGeneratorCategory.js";
import { ITSGeneratorPackage } from "../../generators/generator/Settings/ITSGeneratorPackage.js";
import { ITSGeneratorSettings } from "../../generators/generator/Settings/ITSGeneratorSettings.js";
import { TSGeneratorComponent } from "../../generators/generator/Settings/TSGeneratorComponent.js";

declare module "inquirer"
{
    /**
     * @inheritdoc
     */
    // eslint-disable-next-line @typescript-eslint/naming-convention
    interface QuestionMap<T>
    {
        /**
         * Represents the workspace prompt.
         */
        [WorkspacePrompt.TypeName]: IWorkspaceQuestion<T>;
    }
}

/**
 * Provides a prompt for asking for generator workspace details.
 *
 * @template T
 * The type of the prompt options.
 */
export class GeneratorWorkspacePrompt<T extends IWorkspaceQuestion> extends WorkspacePrompt<T, ITSGeneratorPackage>
{
    /**
     * Initializes a new instance of the {@linkcode GeneratorWorkspacePrompt} class.
     *
     * @param question
     * The options for the prompt.
     *
     * @param readLine
     * An object for performing read from and write to the console.
     *
     * @param answers
     * The answer-object.
     */
    public constructor(question: T, readLine: ReadLine, answers: Answers)
    {
        super(question, readLine, answers);
    }

    /**
     * @inheritdoc
     *
     * @param index
     * The index of the workspace to ask for.
     *
     * @param generatorSettings
     * The generator settings.
     *
     * @returns
     * The questions for the workspace with the specified {@linkcode index}.
     */
    protected override async GetQuestions(index: number, generatorSettings: ITSGeneratorSettings): Promise<Array<Question<Answers & ITSGeneratorPackage>>>
    {
        return [
            ...await super.GetQuestions(index, generatorSettings),
            ...(
                generatorSettings[GeneratorSettingKey.Components]?.includes(TSGeneratorComponent.SubGeneratorExample) ?
                [
                    new TSGeneratorCategory(this.opt.generator as any).SubGeneratorQuestion
                ] :
                []) as Array<Question<Answers & ITSGeneratorPackage>>
        ];
    }
}
