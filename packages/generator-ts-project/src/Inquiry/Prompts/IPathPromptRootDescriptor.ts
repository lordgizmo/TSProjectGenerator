/**
 * Provides information about a root-directory.
 */
export interface IPathPromptRootDescriptor
{
    /**
     * The path to the root-directory.
     */
    path: string;

    /**
     * A value indicating whether paths outside the {@linkcode path} are allowed.
     */
    allowOutside?: boolean;
}
