import { PlatformPath } from "node:path";
import { Answers, AsyncDynamicQuestionProperty, InputQuestionOptions } from "inquirer";
import { IPathPromptRootDescriptor } from "./IPathPromptRootDescriptor.js";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import type { PathPrompt } from "./PathPrompt.js";

/**
 * Provides options for the {@linkcode PathPrompt}.
 *
 * @template T
 * The type of the answers.
 */
export interface IPathQuestionOptions<T extends Answers = Answers> extends InputQuestionOptions<T>
{
    /**
     * A component for handling file-system paths.
     */
    path?: PlatformPath;

    /**
     * The directory to use for resolving relative paths for the {@linkcode default} value and the answer.
     */
    prefixedRoot?: AsyncDynamicQuestionProperty<IPathPromptRootDescriptor | string | undefined, T>;
}
