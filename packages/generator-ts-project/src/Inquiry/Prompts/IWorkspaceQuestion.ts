import { Answers } from "inquirer";
import { IWorkspaceQuestionOptions } from "./IWorkspaceQuestionOptions.js";
import { WorkspacePrompt } from "./WorkspacePrompt.js";

/**
 * Provides options for the {@linkcode WorkspacePrompt}.
 *
 * @template T
 * The type of the answers.
 */
export interface IWorkspaceQuestion<T extends Answers = Answers> extends IWorkspaceQuestionOptions<T>
{
    /**
     * @inheritdoc
     */
    type: typeof WorkspacePrompt.TypeName;
}
