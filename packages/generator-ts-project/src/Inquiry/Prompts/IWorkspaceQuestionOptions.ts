import { Answers } from "inquirer";
import { IArrayQuestionOptions } from "./IArrayQuestionOptions.js";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import type { WorkspacePrompt } from "./WorkspacePrompt.js";
import { ITSProjectSettings } from "../../Project/Settings/ITSProjectSettings.js";
import { TSProjectGenerator } from "../../Project/TSProjectGenerator.js";

/**
 * Provides options for the {@linkcode WorkspacePrompt}.
 *
 * @template T
 * The type of the answers.
 */
export interface IWorkspaceQuestionOptions<T extends Answers = Answers> extends IArrayQuestionOptions<T>
{
    /**
     * The generator related to the question.
     */
    generator: TSProjectGenerator<T & ITSProjectSettings, any, any>;
}
