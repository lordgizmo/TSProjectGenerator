import { ReadLine } from "readline";
import { Question } from "@manuth/extended-yo-generator";
import { Answers, DistinctQuestion } from "inquirer";
import { ArrayPrompt } from "./ArrayPrompt.js";
import { IArrayPromptHash } from "./IArrayPromptHash.js";
import { IWorkspaceQuestion } from "./IWorkspaceQuestion.js";
import { ITSProjectPackage } from "../../Project/Settings/ITSProjectPackage.js";
import { RunningWorkspaceSettings } from "../../Project/Workspace/RunningWorkspaceSettings.js";

declare module "inquirer"
{
    /**
     * @inheritdoc
     */
    // eslint-disable-next-line @typescript-eslint/naming-convention
    interface QuestionMap<T>
    {
        /**
         * Represents the workspace prompt.
         */
        [WorkspacePrompt.TypeName]: IWorkspaceQuestion<T>;
    }
}

/**
 * Provides a prompt for asking for workspace details.
 *
 * @template T
 * The type of the prompt options.
 *
 * @template TWorkspace
 * The type of the workspace to ask for.
 */
export class WorkspacePrompt<T extends IWorkspaceQuestion, TWorkspace extends ITSProjectPackage> extends ArrayPrompt<T, TWorkspace>
{
    /**
     * The name of the prompt-type.
     */
    public static readonly TypeName = "workspace";

    /**
     * Initializes a new instance of the {@linkcode WorkspacePrompt} class.
     *
     * @param question
     * The options for the prompt.
     *
     * @param readLine
     * An object for performing read from and write to the console.
     *
     * @param answers
     * The answer-object.
     */
    public constructor(question: T, readLine: ReadLine, answers: Answers)
    {
        super(
            {
                ...question,
                promptTypes: {
                    ...question.generator.env.adapter.promptModule.prompts,
                    ...question.promptTypes
                }
            }, readLine, answers);
    }

    /**
     * @inheritdoc
     *
     * @param items
     * The currently stored items.
     *
     * @returns
     * The new item to add to the result.
     */
    protected override async PromptItem(items: readonly TWorkspace[]): Promise<TWorkspace>
    {
        return this.Inquirer.prompt(
            await this.GetQuestions(
                items.length,
                {
                    ...this.answers as any,
                    [this.opt.name as string]: items
                }));
    }

    /**
     * Gets the questions to ask for the workspace with the specified {@linkcode index}.
     *
     * @param index
     * The index of the workspace to ask for.
     *
     * @param generatorSettings
     * The generator settings.
     *
     * @returns
     * The questions for the workspace with the specified {@linkcode index}.
     */
    protected async GetQuestions(index: number, generatorSettings: any): Promise<Array<Question<Answers & TWorkspace>>>
    {
        return this.opt.generator.GetCreationContext(
            new RunningWorkspaceSettings(
                this.opt.generator.GetWorkspaceContext(index).Settings,
                generatorSettings)).Questions as Array<Question<Answers & TWorkspace>>;
    }

    /**
     * @inheritdoc
     *
     * @param items
     * The currently stored items.
     *
     * @returns
     * A question for asking the user whether another item should be added.
     */
    protected override async GetRepetitionQuestion(items: readonly TWorkspace[]): Promise<DistinctQuestion<IArrayPromptHash>>
    {
        return {
            ...await super.GetRepetitionQuestion(items),
            message: "Do you want to add another workspace?"
        };
    }
}
