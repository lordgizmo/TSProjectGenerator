import { DependencyCollection, Dictionary } from "@manuth/package-json-editor";
import { PickKeys } from "ts-essentials";

/**
 * Provides the functionality to merge multiple dependency collections.
 */
export class MergedDependencyCollection extends DependencyCollection
{
    /**
     * Initializes a new instance of the {@linkcode MergedDependencyCollection} class.
     */
    public constructor()
    {
        super({});
    }

    /**
     * Gets the merged collections.
     */
    protected get MergedCollections(): DependencyCollection[]
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public override get Dependencies(): Dictionary<string, string>
    {
        return this.MergeDependencies(nameof(this.Dependencies) as any);
    }

    /**
     * @inheritdoc
     */
    public override get DevelopmentDependencies(): Dictionary<string, string>
    {
        return this.MergeDependencies(nameof(this.DevelopmentDependencies) as any);
    }

    /**
     * @inheritdoc
     */
    public override get PeerDependencies(): Dictionary<string, string>
    {
        return this.MergeDependencies(nameof(this.PeerDependencies) as any);
    }

    /**
     * @inheritdoc
     */
    public override get OptionalDependencies(): Dictionary<string, string>
    {
        return this.MergeDependencies(nameof(this.OptionalDependencies) as any);
    }

    /**
     * Merges the dependencies with the specified {@linkcode key}.
     *
     * @param key
     * The key to load the dependencies from.
     *
     * @returns
     * The merged dependencies.
     */
    protected MergeDependencies(key: PickKeys<DependencyCollection, Dictionary<string, string>>): Dictionary<string, string>
    {
        let result = new Dictionary<string, string>();
        result.AddRange(super[key]);

        for (let dependencyCollection of this.MergedCollections)
        {
            result.AddRange(dependencyCollection[key]);
        }

        return result;
    }
}
