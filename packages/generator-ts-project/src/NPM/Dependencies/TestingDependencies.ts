import { MyPackageDependencyCollection } from "./MyPackageDependencyCollection.js";

/**
 * Provides all dependencies that are required for testing.
 */
export class TestingDependencies extends MyPackageDependencyCollection
{
    /**
     * Initializes a new instance of the {@linkcode TestingDependencies} class.
     */
    public constructor()
    {
        super(
            {
                devDependencies: [
                    "@types/mocha",
                    "mocha",
                    "source-map-support"
                ]
            });
    }
}
