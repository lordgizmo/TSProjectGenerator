import { DependencyCollection } from "@manuth/package-json-editor";
import { BuildDependencies } from "./BuildDependencies.js";
import { MergedDependencyCollection } from "./MergedDependencyCollection.js";
import { MyPackageDependencyCollection } from "./MyPackageDependencyCollection.js";
import { TestingDependencies } from "./TestingDependencies.js";

/**
 * Provides all common dependencies.
 */
export class WorkspaceRootDependencies extends MergedDependencyCollection
{
    /**
     * Initializes a new instance of the {@linkcode WorkspaceRootDependencies} class.
     */
    public constructor()
    {
        super();
    }

    /**
     * @inheritdoc
     */
    protected override get MergedCollections(): DependencyCollection[]
    {
        return [
            new BuildDependencies(),
            new TestingDependencies(),
            new MyPackageDependencyCollection(
                {
                    devDependencies: [
                        "@manuth/package-json-editor",
                        "@types/fs-extra",
                        "@types/git-branch",
                        "@types/node",
                        "@types/npm-which",
                        "fs-extra",
                        "git-branch",
                        "globby",
                        "npm-which",
                        "ts-node"
                    ]
                })
        ];
    }
}
