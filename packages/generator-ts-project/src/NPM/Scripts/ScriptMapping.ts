import { ok } from "node:assert";
import { ContextResolver, GeneratorOptions, IGenerator, IGeneratorSettings, Resolvable } from "@manuth/extended-yo-generator";
import { Package } from "@manuth/package-json-editor";
import { IScriptMapping } from "./IScriptMapping.js";
import { ScriptProcessor } from "./ScriptProcessor.js";

/**
 * Represents a script-mapping for copying npm-scripts.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 */
export class ScriptMapping<TSettings extends IGeneratorSettings, TOptions extends GeneratorOptions> extends ContextResolver<IScriptMapping<TSettings, TOptions>, ScriptMapping<TSettings, TOptions>, TSettings, TOptions> implements IScriptMapping<TSettings, TOptions>
{
    /**
     * The package to load the script-source from.
     */
    private sourcePackage: Package;

    /**
     * Initializes a new instance of the {@linkcode ScriptMapping} class.
     *
     * @param generator
     * The generator of the script-mapping
     *
     * @param sourcePackage
     * The package to load the script-source from.
     *
     * @param scriptInfo
     * A component which provides information about the script.
     */
    public constructor(generator: IGenerator<TSettings, TOptions>, sourcePackage: Package, scriptInfo: string | IScriptMapping<TSettings, TOptions>)
    {
        super(
            generator,
            typeof scriptInfo === "string" ?
                {
                    Source: scriptInfo,
                    Destination: scriptInfo
                } :
                scriptInfo);

        this.sourcePackage = sourcePackage;
    }

    /**
     * Gets the package to load the script-source from.
     */
    public get SourcePackage(): Package
    {
        return this.sourcePackage;
    }

    /**
     * Gets the name of the source-script.
     */
    public get Source(): string | undefined
    {
        if (this.ResolverContext.Source)
        {
            return this.ResolveProperty(this, this.ResolverContext.Source);
        }
        else
        {
            return undefined;
        }
    }

    /**
     * @inheritdoc
     */
    public set Source(value: Resolvable<ScriptMapping<TSettings, TOptions>, TSettings, TOptions, string> | undefined)
    {
        this.ResolverContext.Source = value;
    }

    /**
     * Gets the name of the destination-script.
     */
    public get Destination(): string
    {
        return this.ResolveProperty(this, this.ResolverContext.Destination);
    }

    /**
     * @inheritdoc
     */
    public set Destination(value: Resolvable<ScriptMapping<TSettings, TOptions>, TSettings, TOptions, string>)
    {
        this.ResolverContext.Destination = value;
    }

    /**
     * Gets a component for manipulating the script.
     */
    public get Processor(): () => Promise<string>
    {
        return async () =>
        {
            let script: string | undefined;

            if (this.Source)
            {
                script = this.SourcePackage.Scripts.Get(this.Source);
            }

            let result = await this.ResolverContext.Processor?.(script, this, this.Generator) ?? script;
            ok(result);
            return result;
        };
    }

    /**
     * @inheritdoc
     */
    public set Processor(value: ScriptProcessor<TSettings, TOptions>)
    {
        this.ResolverContext.Processor = value;
    }

    /**
     * @inheritdoc
     */
    public get Result(): IScriptMapping<TSettings, TOptions>
    {
        return this;
    }
}
