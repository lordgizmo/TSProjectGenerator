import { join } from "node:path";
import { ComponentBase, GeneratorOptions, IFileMapping, Question } from "@manuth/extended-yo-generator";
import { LintingQuestion } from "../../Inquiry/LintingQuestion.js";
import { ESLintRCFileMapping } from "../../Project/FileMappings/ESLintRCFileMapping.js";
import { ITSProjectSettings } from "../../Project/Settings/ITSProjectSettings.js";
import { TSProjectComponent } from "../../Project/Settings/TSProjectComponent.js";
import { TSConfigFileMapping } from "../../Serialization/TSConfigFileMapping.js";
import { ITSProjectPackage } from "../Settings/ITSProjectPackage.js";
import { TSProjectGenerator } from "../TSProjectGenerator.js";
import { ICreationContext } from "../Workspace/ICreationContext.js";

/**
 * Provides a component which allows creating files for linting the workspace.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 *
 * @template TPackage
 * The type of the packages of the generator.
 */
export class LintingComponent<TSettings extends ITSProjectSettings, TOptions extends GeneratorOptions, TPackage extends ITSProjectPackage> extends ComponentBase<TSettings, TOptions>
{
    /**
     * The generator of the project.
     */
    private projectGenerator: TSProjectGenerator<TSettings, TOptions, TPackage>;

    /**
     * Initializes a new instance of the {@linkcode LintingComponent} class.
     *
     * @param generator
     * The generator of the file-mapping.
     */
    public constructor(generator: TSProjectGenerator<TSettings, TOptions, TPackage>)
    {
        super(generator);
        this.projectGenerator = generator;
    }

    /**
     * @inheritdoc
     */
    public override get Generator(): TSProjectGenerator<TSettings, TOptions, TPackage>
    {
        return this.projectGenerator;
    }

    /**
     * @inheritdoc
     */
    public get ID(): string
    {
        return TSProjectComponent.Linting;
    }

    /**
     * @inheritdoc
     */
    public get DisplayName(): string
    {
        return "ESLint configurations";
    }

    /**
     * @inheritdoc
     */
    public override get DefaultEnabled(): boolean
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public override get Questions(): Array<Question<TSettings>>
    {
        return [
            new LintingQuestion(this.Generator)
        ] as Array<Question<TSettings>>;
    }

    /**
     * @inheritdoc
     */
    public override get FileMappings(): Array<IFileMapping<TSettings, TOptions>>
    {
        return [
            this.Generator.PackageCreationContext,
            ...this.Generator.WorkspaceCreationContexts
        ].flatMap((context) => this.GetFileMappings(context));
    }

    /**
     * Gets the file mappings for the specified {@linkcode context}.
     *
     * @param context
     * The context to get the file mappings for.
     *
     * @returns
     * The file mappings for the specified {@linkcode context}.
     */
    protected GetFileMappings(context: ICreationContext<TSettings, TPackage>): Array<IFileMapping<TSettings, TOptions>>
    {
        return [
            new ESLintRCFileMapping(this.Generator, context),
            new class extends TSConfigFileMapping<TSettings, TOptions>
            {
                /**
                 * @inheritdoc
                 */
                public override get MiddleExtension(): string
                {
                    return "eslint";
                }

                /**
                 * @inheritdoc
                 */
                public override get Destination(): string
                {
                    return join(context.Destination, super.Destination);
                }

                /**
                 * @inheritdoc
                 */
                public override get Source(): string
                {
                    return this.Generator.modulePath(super.Source);
                }
            }(this.Generator)
        ];
    }
}
