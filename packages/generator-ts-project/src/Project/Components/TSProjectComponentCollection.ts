import { ComponentCollectionBase, GeneratorOptions, IComponentCategory } from "@manuth/extended-yo-generator";
import { TSProjectGeneralCategory } from "./TSProjectGeneralCategory.js";
import { ITSProjectPackage } from "../Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../Settings/ITSProjectSettings.js";
import { TSProjectGenerator } from "../TSProjectGenerator.js";

/**
 * Provides components for {@linkcode TSProjectGenerator}s.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 *
 * @template TPackage
 * The type of the packages of the generator.
 */
export class TSProjectComponentCollection<TSettings extends ITSProjectSettings, TOptions extends GeneratorOptions, TPackage extends ITSProjectPackage> extends ComponentCollectionBase<TSettings, TOptions>
{
    /**
     * The generator of the project.
     */
    private projectGenerator: TSProjectGenerator<TSettings, TOptions, TPackage>;

    /**
     * Initializes a new instance of the {@linkcode TSProjectComponentCollection} class.
     *
     * @param generator
     * The generator of the collection.
     */
    public constructor(generator: TSProjectGenerator<TSettings, TOptions, TPackage>)
    {
        super(generator);
        this.projectGenerator = generator;
    }

    /**
     * @inheritdoc
     */
    public override get Generator(): TSProjectGenerator<TSettings, TOptions, TPackage>
    {
        return this.projectGenerator;
    }

    /**
     * @inheritdoc
     */
    public get Question(): string
    {
        return "What do you want to include in your project?";
    }

    /**
     * @inheritdoc
     */
    public get Categories(): Array<IComponentCategory<TSettings, TOptions>>
    {
        return [
            new TSProjectGeneralCategory(this.Generator)
        ];
    }
}
