import { ComponentCategoryBase, GeneratorOptions, IComponent } from "@manuth/extended-yo-generator";
import { LintingComponent } from "./LintingComponent.js";
import { TSProjectWorkspaceComponent } from "./TSProjectWorkspaceComponent.js";
import { WorkspaceComponent } from "./WorkspaceComponent.js";
import { ITSProjectPackage } from "../Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../Settings/ITSProjectSettings.js";
import { TSProjectGenerator } from "../TSProjectGenerator.js";

/**
 * Provides general components for {@linkcode TSProjectGenerator}s.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 *
 * @template TPackage
 * The type of the packages of the generator.
 */
export class TSProjectGeneralCategory<TSettings extends ITSProjectSettings, TOptions extends GeneratorOptions, TPackage extends ITSProjectPackage> extends ComponentCategoryBase<TSettings, TOptions>
{
    /**
     * The generator of the project.
     */
    private projectGenerator: TSProjectGenerator<TSettings, TOptions, TPackage>;

    /**
     * Initializes a new instance of the {@linkcode TSProjectGeneralCategory} class.
     *
     * @param generator
     * The generator of the category.
     */
    public constructor(generator: TSProjectGenerator<TSettings, TOptions, TPackage>)
    {
        super(generator);
        this.projectGenerator = generator;
    }

    /**
     * Gets the generator of the project.
     */
    public override get Generator(): TSProjectGenerator<TSettings, TOptions, TPackage>
    {
        return this.projectGenerator;
    }

    /**
     * @inheritdoc
     */
    public get DisplayName(): string
    {
        return "General";
    }

    /**
     * @inheritdoc
     */
    public get Components(): Array<IComponent<TSettings, TOptions>>
    {
        return [
            this.LintingComponent,
            this.WorkspaceComponent,
            this.MonoRepoComponent
        ];
    }

    /**
     * Provides a component for creating a linting-environment.
     */
    protected get LintingComponent(): IComponent<TSettings, TOptions>
    {
        return new LintingComponent(this.Generator);
    }

    /**
     * Provides a component for creating a vscode-environment.
     */
    protected get WorkspaceComponent(): IComponent<TSettings, TOptions>
    {
        return new TSProjectWorkspaceComponent(this.Generator);
    }

    /**
     * Provides a component for creating a mono repo.
     */
    protected get MonoRepoComponent(): IComponent<TSettings, TOptions>
    {
        return new WorkspaceComponent(this.Generator);
    }
}
