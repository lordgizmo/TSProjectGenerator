import { GeneratorOptions, GeneratorSettingKey, IGenerator } from "@manuth/extended-yo-generator";
import { JSONProcessor } from "../../Serialization/JSONProcessor.js";
import { CodeWorkspaceComponent } from "../../VSCode/Components/CodeWorkspaceComponent.js";
import { CodeFileMappingCreator } from "../../VSCode/FileMappings/CodeFileMappingCreator.js";
import { CodeWorkspaceProvider } from "../../VSCode/FileMappings/CodeWorkspaceProvider.js";
import { WorkspaceFileCreator } from "../../VSCode/FileMappings/WorkspaceFileCreator.js";
import { WorkspaceFileLoader } from "../../VSCode/FileMappings/WorkspaceFileLoader.js";
import { WorkspaceFolderCreator } from "../../VSCode/FileMappings/WorkspaceFolderCreator.js";
import { IWorkspaceMetadata } from "../../VSCode/IWorkspaceMetadata.js";
import { ITSProjectSettings } from "../Settings/ITSProjectSettings.js";
import { TSProjectComponent } from "../Settings/TSProjectComponent.js";
import { TSProjectPackageKey } from "../Settings/TSProjectPackageKey.js";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import type { TSProjectGenerator } from "../TSProjectGenerator.js";
import { TSProjectWorkspaceProcessor } from "../VSCode/TSProjectWorkspaceProcessor.js";

/**
 * Provides a component for creating a vscode-workspace folder for {@linkcode TSProjectGenerator}s.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 */
export class TSProjectWorkspaceComponent<TSettings extends ITSProjectSettings, TOptions extends GeneratorOptions> extends CodeWorkspaceComponent<TSettings, TOptions>
{
    /**
     * Initializes a new instance of the {@linkcode TSProjectWorkspaceComponent} class.
     *
     * @param generator
     * The generator of the component.
     */
    public constructor(generator: IGenerator<TSettings, TOptions>)
    {
        super(generator);
    }

    /**
     * @inheritdoc
     */
    protected override get WorkspaceProcessor(): JSONProcessor<TSettings, TOptions, IWorkspaceMetadata>
    {
        return new TSProjectWorkspaceProcessor(this);
    }

    /**
     * @inheritdoc
     */
    public override get Source(): CodeWorkspaceProvider<TSettings, TOptions>
    {
        return new WorkspaceFileLoader(this, this.Generator.modulePath("TSProjectGenerator.code-workspace"));
    }

    /**
     * @inheritdoc
     */
    protected override get FileMappingCreator(): CodeFileMappingCreator<TSettings, TOptions>
    {
        if (this.Generator.Settings[GeneratorSettingKey.Components]?.includes(TSProjectComponent.MonoRepo))
        {
            return new WorkspaceFileCreator(
                this,
                `${this.Generator.Settings[TSProjectPackageKey.DisplayName]}.code-workspace`);
        }
        else
        {
            return new WorkspaceFolderCreator(this);
        }
    }
}
