import { ComponentBase, GeneratorOptions, Question } from "@manuth/extended-yo-generator";
import { DistinctQuestion } from "inquirer";
import { WorkspacePrompt } from "../../Inquiry/Prompts/WorkspacePrompt.js";
import { ITSProjectPackage } from "../Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../Settings/ITSProjectSettings.js";
import { TSProjectComponent } from "../Settings/TSProjectComponent.js";
import { TSProjectSettingKey } from "../Settings/TSProjectSettingKey.js";
import { TSProjectGenerator } from "../TSProjectGenerator.js";

/**
 * Provides a component which allows the creation of a monorepo containing workspaces.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 *
 * @template TPackage
 * The type of the packages of the generator.
 */
export class WorkspaceComponent<TSettings extends ITSProjectSettings, TOptions extends GeneratorOptions, TPackage extends ITSProjectPackage> extends ComponentBase<TSettings, TOptions>
{
    /**
     * The generator of the project.
     */
    private projectGenerator: TSProjectGenerator<TSettings, TOptions, TPackage>;

    /**
     * Initializes a new instance of the {@linkcode WorkspaceComponent} class.
     *
     * @param generator
     * The generator of the file-mapping.
     */
    public constructor(generator: TSProjectGenerator<TSettings, TOptions, TPackage>)
    {
        super(generator);
        this.projectGenerator = generator;
    }

    /**
     * @inheritdoc
     */
    public override get Generator(): TSProjectGenerator<TSettings, TOptions, TPackage>
    {
        return this.projectGenerator;
    }

    /**
     * @inheritdoc
     */
    public override get ID(): string
    {
        return TSProjectComponent.MonoRepo;
    }

    /**
     * @inheritdoc
     */
    public override get DisplayName(): string
    {
        return "Mono Repo";
    }

    /**
     * @inheritdoc
     */
    public override get DefaultEnabled(): boolean
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public override get Questions(): Array<Question<TSettings>>
    {
        return [
            {
                type: WorkspacePrompt.TypeName,
                name: TSProjectSettingKey.Workspaces,
                generator: this.Generator,
                message: "Please specify the details of the workspace",
                defaultRepeat: false
            } as DistinctQuestion<TSettings> as any
        ];
    }
}
