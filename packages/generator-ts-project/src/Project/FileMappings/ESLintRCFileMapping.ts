import { ok } from "node:assert";
import { PresetName } from "@manuth/eslint-plugin-typescript";
import { GeneratorOptions } from "@manuth/extended-yo-generator";
// eslint-disable-next-line node/no-unpublished-import
import type { Linter } from "eslint";
import { ExportAssignment, Node, SourceFile, SyntaxKind } from "ts-morph";
import { fileName } from "types-eslintrc";
import upath from "upath";
import { LintRuleset } from "../../Linting/LintRuleset.js";
import { TSConfigFileMapping } from "../../Serialization/TSConfigFileMapping.js";
import { TypeScriptTransformMapping } from "../../Serialization/TypeScriptTransformMapping.js";
import { ITSProjectPackage } from "../Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../Settings/ITSProjectSettings.js";
import { TSProjectSettingKey } from "../Settings/TSProjectSettingKey.js";
import { TSProjectGenerator } from "../TSProjectGenerator.js";
import { ICreationContext } from "../Workspace/ICreationContext.js";

const { changeExt, join } = upath;

/**
 * Provides a file-mapping for the `.eslintrc.js` file.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 *
 * @template TPackage
 * The type of the packages of the generator.
 */
export class ESLintRCFileMapping<TSettings extends ITSProjectSettings, TOptions extends GeneratorOptions, TPackage extends ITSProjectPackage> extends TypeScriptTransformMapping<TSettings, TOptions>
{
    /**
     * The generator of the project.
     */
    private projectGenerator: TSProjectGenerator<TSettings, TOptions, TPackage>;

    /**
     * The context of the package creation.
     */
    private creationContext: ICreationContext<TSettings, TPackage>;

    /**
     * Initializes a new instance of the {@linkcode ESLintRCFileMapping} class.
     *
     * @param generator
     * The generator of the file-mapping.
     *
     * @param context
     * The context of the package creation.
     */
    public constructor(generator: TSProjectGenerator<TSettings, TOptions, TPackage>, context: ICreationContext<TSettings, TPackage>)
    {
        super(generator);
        this.projectGenerator = generator;
        this.creationContext = context;
    }

    /**
     * Gets the default name of the file.
     */
    public static get FileName(): string
    {
        return changeExt(fileName, ".cjs");
    }

    /**
     * @inheritdoc
     */
    public override get Generator(): TSProjectGenerator<TSettings, TOptions, TPackage>
    {
        return this.projectGenerator;
    }

    /**
     * Gets the default base-name of the file.
     */
    public get DefaultBaseName(): string
    {
        return ESLintRCFileMapping.FileName;
    }

    /**
     * Gets the base-name of the file.
     */
    public get BaseName(): string
    {
        return this.DefaultBaseName;
    }

    /**
     * @inheritdoc
     */
    public override get Source(): string
    {
        return this.Generator.modulePath(this.BaseName);
    }

    /**
     * @inheritdoc
     */
    public get Destination(): string
    {
        return join(this.CreationContext.Destination, this.BaseName);
    }

    /**
     * Gets the creation context of the package.
     */
    protected get CreationContext(): ICreationContext<TSettings, TPackage>
    {
        return this.creationContext;
    }

    /**
     * Processes the specified {@linkcode sourceFile}.
     *
     * @param sourceFile
     * The source-file to process.
     *
     * @returns
     * The processed source-file.
     */
    protected override async Transform(sourceFile: SourceFile): Promise<SourceFile>
    {
        if (this.CreationContext.IsMonoRepo && this.CreationContext.IsWorkspace)
        {
            sourceFile.getDescendantsOfKind(SyntaxKind.CallExpression)
                .filter((callExpression) => callExpression.getExpression().getText() === nameof(require))
                .map((callExpression) => callExpression.getFirstAncestorByKind(SyntaxKind.VariableDeclaration))
                .filter(
                    (declaration) =>
                    {
                        return declaration?.getNameNode().asKind(SyntaxKind.ObjectBindingPattern)?.getElements().some(
                            (element) =>
                            {
                                return element.getName() === nameof(PresetName);
                            }) ?? false;
                    })
                .forEach(
                    (declaration) =>
                    {
                        declaration?.remove();
                    });
        }

        let exports = sourceFile.getStatements().find(
            (statement): statement is ExportAssignment =>
            {
                if (Node.isExpressionStatement(statement))
                {
                    let expression = statement.getExpression();

                    if (Node.isBinaryExpression(expression))
                    {
                        return expression.getLeft().getText() === `${nameof(module)}.${nameof(module.exports)}`;
                    }
                }

                return false;
            });

        ok(exports);

        let exportsAssignment = exports.getExpression();

        if (Node.isBinaryExpression(exportsAssignment))
        {
            let eslintConfig = exportsAssignment.getRight();

            if (Node.isObjectLiteralExpression(eslintConfig))
            {
                let excludedFiles = ["type-tests"];

                let excludedProperties = [
                    nameof<Linter.Config>((config) => config.ignorePatterns),
                    nameof<Linter.Config>((config) => config.overrides)
                ];

                let extendsProperty = eslintConfig.getProperty(nameof<Linter.Config>((config) => config.extends));

                if (this.CreationContext.IsMonoRepo)
                {
                    if (this.CreationContext.IsWorkspace)
                    {
                        excludedProperties?.push(
                            nameof<Linter.Config>((config) => config.extends),
                            nameof<Linter.Config>((config) => config.env),
                            nameof<Linter.Config>((config) => config.root),
                            nameof<Linter.Config>((config) => config.rules));

                        excludedFiles.push(TSConfigFileMapping.GetFileName("editor"));
                    }
                    else
                    {
                        excludedFiles.push(
                            TSConfigFileMapping.GetFileName(),
                            TSConfigFileMapping.GetFileName("app"));
                    }
                }

                if (this.CreationContext.IsWorkspaceRoot)
                {
                    let preset: string;

                    switch (this.Generator.Settings[TSProjectSettingKey.LintRuleset])
                    {
                        case LintRuleset.Weak:
                            preset = nameof(PresetName.WeakWithTypeChecking);
                            break;
                        case LintRuleset.Recommended:
                        default:
                            preset = nameof(PresetName.RecommendedWithTypeChecking);
                            break;
                    }

                    if (Node.isPropertyAssignment(extendsProperty))
                    {
                        let extendsValue = extendsProperty.getInitializer();

                        if (Node.isArrayLiteralExpression(extendsValue))
                        {
                            for (let item of extendsValue.getElements())
                            {
                                if (Node.isTemplateExpression(item))
                                {
                                    for (let templateSpan of item.getTemplateSpans())
                                    {
                                        let presetNameProperty = templateSpan.getExpression();

                                        if (Node.isPropertyAccessExpression(presetNameProperty) &&
                                            presetNameProperty.getExpression().getText() === nameof(PresetName))
                                        {
                                            presetNameProperty.getNameNode().replaceWithText(preset);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                for (let property of excludedProperties)
                {
                    eslintConfig.getProperty(property)?.remove();
                }

                let projectArray = eslintConfig
                    .getProperty(nameof<Linter.Config>((config) => config.parserOptions))
                    ?.asKindOrThrow(SyntaxKind.PropertyAssignment).getInitializer()
                    ?.asKindOrThrow(SyntaxKind.ObjectLiteralExpression).getProperty("project")
                    ?.asKindOrThrow(SyntaxKind.PropertyAssignment).getInitializer()
                    ?.asKindOrThrow(SyntaxKind.ArrayLiteralExpression);

                projectArray?.getElements().filter(
                    (element) =>
                    {
                        return element.getDescendantsOfKind(SyntaxKind.StringLiteral).some(
                            (literal) =>
                            {
                                return excludedFiles.includes(literal.getLiteralValue()) ?? false;
                            });
                    }).forEach((element) => projectArray?.removeElement(element));
            }
        }

        return sourceFile;
    }
}
