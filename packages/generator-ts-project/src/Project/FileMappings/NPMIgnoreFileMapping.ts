import { join } from "node:path";
import { FileMappingBase, GeneratorOptions, IGenerator } from "@manuth/extended-yo-generator";
import { applyPatch, parsePatch } from "diff";
import { ITSProjectPackage } from "../Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../Settings/ITSProjectSettings.js";
import { ICreationContext } from "../Workspace/ICreationContext.js";

/**
 * Provides the functionality to copy the `.npmignore` file.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 */
export class NPMIgnoreFileMapping<TSettings extends ITSProjectSettings, TOptions extends GeneratorOptions> extends FileMappingBase<TSettings, TOptions>
{
    /**
     * The context of the package creation.
     */
    private creationContext: ICreationContext<TSettings, ITSProjectPackage> | undefined;

    /**
     * Initializes a new instance of the {@linkcode NPMIgnoreFileMapping} class.
     *
     * @param generator
     * The generator of the file-mapping.
     *
     * @param context
     * The context of the package creation.
     */
    public constructor(generator: IGenerator<TSettings, TOptions>, context?: ICreationContext<TSettings, ITSProjectPackage>)
    {
        super(generator);
        this.creationContext = context;
    }

    /**
     * Gets the default file-name of `.npmignore`-files.
     */
    public static get FileName(): string
    {
        return ".npmignore";
    }

    /**
     * Gets the default base-name of the file.
     */
    public get DefaultBaseName(): string
    {
        return NPMIgnoreFileMapping.FileName;
    }

    /**
     * Gets the base-name of the file.
     */
    public get BaseName(): string
    {
        return this.DefaultBaseName;
    }

    /**
     * @inheritdoc
     */
    public override get Source(): string
    {
        return this.Generator.modulePath(this.BaseName);
    }

    /**
     * @inheritdoc
     */
    public get Destination(): string
    {
        return join(
            this.creationContext?.Destination ?? "",
            this.BaseName);
    }

    /**
     * Gets the name of the patch file.
     */
    protected get PatchFileName(): string
    {
        return this.Generator.commonTemplatePath("npmignore.diff");
    }

    /**
     * @inheritdoc
     */
    public override async Processor(): Promise<void>
    {
        this.WriteOutput(
            applyPatch(
                await this.ReadSource(),
                parsePatch(
                    await this.ReadFile(this.PatchFileName)
                )[0]));
    }
}
