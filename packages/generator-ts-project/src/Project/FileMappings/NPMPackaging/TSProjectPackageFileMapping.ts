import { join } from "node:path";
import { GeneratorOptions, GeneratorSettingKey } from "@manuth/extended-yo-generator";
import { Package, PackageType } from "@manuth/package-json-editor";
import commonPathPrefix from "common-path-prefix";
import { minimatch } from "minimatch";
import path from "upath";
import { Constants } from "../../../Core/Constants.js";
import { LintEssentials } from "../../../NPM/Dependencies/LintEssentials.js";
import { WorkspaceRootDependencies } from "../../../NPM/Dependencies/WorkspaceRootDependencies.js";
import { PackageFileMapping } from "../../../NPM/FileMappings/PackageFileMapping.js";
import { IScriptMapping } from "../../../NPM/Scripts/IScriptMapping.js";
import { ITSProjectPackage } from "../../Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../../Settings/ITSProjectSettings.js";
import { TSProjectComponent } from "../../Settings/TSProjectComponent.js";
import { TSProjectSettingKey } from "../../Settings/TSProjectSettingKey.js";
import { TSProjectGenerator } from "../../TSProjectGenerator.js";
import { ICreationContext } from "../../Workspace/ICreationContext.js";

const { dirname, joinSafe, relative } = path;

/**
 * Represents a file-mapping for the `package.json` file of {@linkcode TSProjectGenerator}s.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 *
 * @template TPackage
 * The type of the packages of the generator.
 */
export class TSProjectPackageFileMapping<TSettings extends ITSProjectSettings, TOptions extends GeneratorOptions, TPackage extends ITSProjectPackage> extends PackageFileMapping<TSettings, TOptions>
{
    /**
     * The generator of the file mapping.
     */
    private projectGenerator: TSProjectGenerator<TSettings, TOptions, TPackage>;

    /**
     * The context of the package creation.
     */
    private creationContext: ICreationContext<TSettings, TPackage>;

    /**
     * Initializes a new instance of the {@linkcode TSProjectPackageFileMapping} class.
     *
     * @param generator
     * The generator of the file-mapping.
     *
     * @param context
     * The context of the package creation.
     */
    public constructor(generator: TSProjectGenerator<TSettings, TOptions, TPackage>, context: ICreationContext<TSettings, TPackage>)
    {
        super(generator);
        this.projectGenerator = generator;
        this.creationContext = context;
    }

    /**
     * @inheritdoc
     */
    public override get Generator(): TSProjectGenerator<TSettings, TOptions, TPackage>
    {
        return this.projectGenerator;
    }

    /**
     * Gets the context of the package creation.
     */
    public get CreationContext(): ICreationContext<TSettings, TPackage>
    {
        return this.creationContext;
    }

    /**
     * @inheritdoc
     */
    public override get Destination(): string
    {
        return join(this.CreationContext.Destination, super.Destination);
    }

    /**
     * Gets all `npm`-scripts which are related to `TypeScript`.
     */
    public get TypeScriptScripts(): Array<IScriptMapping<TSettings, TOptions> | string>
    {
        return this.CreationContext.IsWorkspaceRoot ?
            [
                "build",
                "rebuild",
                "watch",
                {
                    Source: "clean-base",
                    Destination: "clean",
                    ...(
                        this.CreationContext.IsMonoRepo ?
                            {
                                Processor: async (script) =>
                                {
                                    let patterns = this.WorkspacePatterns.map(
                                        (pattern) =>
                                        {
                                            return `"${joinSafe(pattern, "lib")}"`;
                                        }).join(" ");

                                    return script?.replace(/(rimraf) \.\/(lib)/, `$1 -g ${patterns}`) ?? "";
                                }
                            } :
                            {})
                }
            ] :
            [];
    }

    /**
     * Gets all `npm`-scripts which are related to linting.
     */
    public get LintScripts(): Array<IScriptMapping<TSettings, TOptions> | string>
    {
        return this.CreationContext.IsWorkspaceRoot ?
            [
                {
                    Source: "lint-local",
                    Destination: "lint",
                    ...(
                        this.CreationContext.IsMonoRepo ?
                            {
                                Processor: async (script) =>
                                {
                                    let sourcePath = "./src";
                                    script ??= "";
                                    script = script.replace(` ${sourcePath} `, " ");

                                    let workspacePatterns = ["src", ".eslintrc.cjs"].flatMap(
                                        (item) =>
                                        {
                                            return this.WorkspacePatterns.map(
                                                (workspacePattern) =>
                                                {
                                                    return joinSafe(workspacePattern, item);
                                                });
                                        }).join(" ");

                                    script += ` ${workspacePatterns}`;
                                    return script;
                                }
                            } :
                            {})
                },
                "lint-ide"
            ] :
            [];
    }

    /**
     * Gets additional `npm`-scripts.
     */
    public get MiscScripts(): Array<IScriptMapping<TSettings, TOptions> | string>
    {
        let testScriptName = "test";
        let prepackScriptName = "prepack";

        let result: Array<IScriptMapping<TSettings, TOptions> | string> = [];

        if (this.CreationContext.IsWorkspaceRoot)
        {
            if (this.CreationContext.IsMonoRepo)
            {
                result.push(
                    {
                        Destination: testScriptName,
                        Processor: async () => "npm exec --workspaces -- mocha && npm run --if-present --workspaces test"
                    });
            }
            else
            {
                result.push(
                    {
                        Destination: testScriptName,
                        Processor: async () => "mocha"
                    });
            }

            result.push("bump-version");

            if (!this.CreationContext.IsMonoRepo)
            {
                result.push(
                    {
                        Destination: prepackScriptName,
                        Processor: async () => "npm run build"
                    });
            }
        }
        else if (this.CreationContext.IsWorkspace)
        {
            let relativeRoot = relative(
                this.Generator.destinationPath(dirname(this.Destination)),
                this.Generator.destinationPath());

            result.push(
                {
                    Destination: prepackScriptName,
                    Processor: async () => `npm --prefix ${relativeRoot} run build`
                });
        }

        return result;
    }

    /**
     * @inheritdoc
     */
    public override get ScriptSource(): Package
    {
        return Constants.Package;
    }

    /**
     * @inheritdoc
     */
    protected override get ScriptMappings(): Array<IScriptMapping<TSettings, TOptions> | string>
    {
        return [
            ...this.TypeScriptScripts,
            ...(
                this.Generator.Settings[GeneratorSettingKey.Components]?.includes(TSProjectComponent.Linting) ?
                this.LintScripts :
                []),
            ...this.MiscScripts
        ];
    }

    /**
     * Gets the patterns for matching the workspaces.
     */
    protected get WorkspacePatterns(): string[]
    {
        let result: string[] = [];

        let packageRoots = this.Generator.WorkspaceCreationContexts.map(
            (workspaceContext) =>
            {
                return joinSafe(
                    "./",
                    relative(
                        this.Generator.destinationPath(),
                        this.Generator.destinationPath(workspaceContext.Destination)));
            });

        let workspacePattern = joinSafe(commonPathPrefix(packageRoots), "*");

        if (minimatch.match(packageRoots, workspacePattern).length > 1)
        {
            result.push(workspacePattern);
        }

        for (let packageRoot of packageRoots)
        {
            if (!result.some((pattern) => minimatch(packageRoot, pattern)))
            {
                result.push(packageRoot);
            }
        }

        return result;
    }

    /**
     * @inheritdoc
     *
     * @returns
     * The loaded package.
     */
    protected override async LoadPackage(): Promise<Package>
    {
        let result = await super.LoadPackage();
        result.Name = await this.CreationContext.ModuleName;
        result.Description = await this.CreationContext.Description;

        if (this.CreationContext.IsWorkspaceRoot)
        {
            if (this.CreationContext.IsMonoRepo)
            {
                let workspaceKey = "workspaces";

                result.Private = true;
                result.Files = [];

                if (result.AdditionalProperties.Has(workspaceKey))
                {
                    result.AdditionalProperties.Remove(workspaceKey);
                }

                result.AdditionalProperties.Add(
                    workspaceKey,
                    {
                        packages: this.WorkspacePatterns
                    });
            }

            result.Register(new WorkspaceRootDependencies(), true);

            if (this.Generator.Settings[GeneratorSettingKey.Components]?.includes(TSProjectComponent.Linting))
            {
                result.Register(new LintEssentials(), true);
            }
        }

        if (this.CreationContext.IsWorkspace)
        {
            result.Type = this.Generator.Settings[TSProjectSettingKey.ESModule] ? PackageType.ESModule : PackageType.CommonJS;

            result.Exports = {
                "./package.json": "./package.json"
            };

            result.PublishConfig = {
                ...result.PublishConfig,
                access: "public"
            };
        }

        return result;
    }
}
