/**
 * Provides information about a suite function.
 */
export interface ISuiteFunctionInfo
{
    /**
     * A value indicating whether the suite is the test entrypoint.
     */
    IsEntrypoint?: boolean;

    /**
     * The name of the suite function.
     */
    Name: string;

    /**
     * The description of the suite function.
     */
    Description: string;
}
