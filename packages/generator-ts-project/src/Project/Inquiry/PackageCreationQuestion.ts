import { QuestionBase } from "@manuth/extended-yo-generator";
import { ITSProjectPackage } from "../Settings/ITSProjectPackage.js";
import { ICreationPromptResolver } from "../Workspace/ICreationPromptResolver.js";

/**
 * Represents a question that is related to the creation of a package.
 *
 * @template TAnswers
 * The type of the answers to the question.
 */
export abstract class PackageCreationQuestion<TAnswers extends ITSProjectPackage> extends QuestionBase<TAnswers>
{
    /**
     * A component for resolving answers provided by the user.
     */
    private resolver: ICreationPromptResolver<TAnswers>;

    /**
     * Initializes a new instance of the {@linkcode PackageCreationQuestion} class.
     *
     * @param resolver
     * A component for resolving answers provided by the user.
     */
    public constructor(resolver: ICreationPromptResolver<TAnswers>)
    {
        super();
        this.resolver = resolver;
    }

    /**
     * Gets a component for resolving answers provided by the user.
     */
    protected get Resolver(): ICreationPromptResolver<TAnswers>
    {
        return this.resolver;
    }
}
