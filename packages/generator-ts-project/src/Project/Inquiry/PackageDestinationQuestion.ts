import { PackageCreationQuestion } from "./PackageCreationQuestion.js";
import { IPathPromptRootDescriptor } from "../../Inquiry/Prompts/IPathPromptRootDescriptor.js";
import { IPathQuestionOptions } from "../../Inquiry/Prompts/IPathQuestionOptions.js";
import { PathPrompt } from "../../Inquiry/Prompts/PathPrompt.js";
import { ITSProjectPackage } from "../Settings/ITSProjectPackage.js";
import { ICreationPromptResolver } from "../Workspace/ICreationPromptResolver.js";

/**
 * Provides a question for asking for the destination-path of a project.
 *
 * @template TAnswers
 * The type of the answers to the question.
 */
export class PackageDestinationQuestion<TAnswers extends ITSProjectPackage> extends PackageCreationQuestion<TAnswers> implements IPathQuestionOptions<TAnswers>
{
    /**
     * Initializes a new instance of the {@linkcode PackageDestinationQuestion} class.
     *
     * @param resolver
     * A component for resolving answers provided by the user.
     */
    public constructor(resolver: ICreationPromptResolver<TAnswers>)
    {
        super(resolver);
        this.type = PathPrompt.TypeName;
        this.name = this.Resolver.DestinationKey as string;
    }

    /**
     * Gets the root of the path to ask for.
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * The root of the path to ask for.
     */
    public prefixedRoot = async (answers: TAnswers): Promise<IPathPromptRootDescriptor | undefined> =>
    {
        return this.PrefixedRoot(answers);
    };

    /**
     * @inheritdoc
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * The message to show to the user.
     */
    public async Message(answers: TAnswers): Promise<string>
    {
        return this.Resolver.DestinationMessage;
    }

    /**
     * @inheritdoc
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * The default value.
     */
    public override async Default(answers: TAnswers): Promise<string>
    {
        return this.Resolver.GetSuggestedPackagePath(answers);
    }

    /**
     * Gets the root of the path to ask for.
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * The root of the path to ask for.
     */
    public async PrefixedRoot(answers: TAnswers): Promise<IPathPromptRootDescriptor | undefined>
    {
        return {
            path: await this.Resolver.GetPackageRoot(answers),
            allowOutside: this.Resolver.AllowOutside
        };
    }

    /**
     * @inheritdoc
     *
     * @param input
     * The answer provided by the user.
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * Either a value indicating whether the answer is valid or a {@linkcode String} which describes the error.
     */
    protected override Validate(input: any, answers: TAnswers): Promise<string | boolean>
    {
        return this.Resolver.ValidateDestination(input, answers);
    }
}
