import { InputQuestionOptions } from "inquirer";
import { PackageCreationQuestion } from "./PackageCreationQuestion.js";
import { ITSProjectPackage } from "../Settings/ITSProjectPackage.js";
import { ICreationPromptResolver } from "../Workspace/ICreationPromptResolver.js";

/**
 * Provides a question for asking for the module-name of a project.
 *
 * @template TAnswers
 * The type of the answers to the question.
 */
export class PackageModuleNameQuestion<TAnswers extends ITSProjectPackage> extends PackageCreationQuestion<TAnswers> implements InputQuestionOptions<TAnswers>
{
    /**
     * Initializes a new instance of the {@linkcode PackageModuleNameQuestion} class.
     *
     * @param context
     * A component for resolving answers provided by the user.
     */
    public constructor(context: ICreationPromptResolver<TAnswers>)
    {
        super(context);
        this.name = this.Resolver.ModuleNameKey as string;
    }

    /**
     * @inheritdoc
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * The message which is shown to the user.
     */
    public async Message(answers: TAnswers): Promise<string>
    {
        return this.Resolver.ModuleNameMessage;
    }

    /**
     * @inheritdoc
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * The default value for this question.
     */
    public override async Default(answers: TAnswers): Promise<string>
    {
        return this.Resolver.GetSuggestedModuleName(answers);
    }

    /**
     * @inheritdoc
     *
     * @param input
     * The input provided by the user.
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * Either a value indicating whether the input is valid or a string which contains an error-message.
     */
    public override async Validate(input: string, answers: TAnswers): Promise<string | boolean>
    {
        return this.Resolver.ValidateModuleName(input, answers);
    }
}
