import { TSProjectPackageKey } from "./TSProjectPackageKey.js";

/**
 * Provides settings for creating a package.
 */
export interface ITSProjectPackage
{
    /**
     * Gets or sets the path to save the package to.
     */
    [TSProjectPackageKey.Destination]: string;

    /**
     * Gets or sets the name of the project.
     */
    [TSProjectPackageKey.Name]: string;

    /**
     * Gets or sets the human-readable name.
     */
    [TSProjectPackageKey.DisplayName]: string;

    /**
     * Gets or sets the description of the project.
     */
    [TSProjectPackageKey.Description]: string;
}
