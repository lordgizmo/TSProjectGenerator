import { IGeneratorSettings } from "@manuth/extended-yo-generator";
import { ITSProjectPackage } from "./ITSProjectPackage.js";
import { TSProjectSettingKey } from "./TSProjectSettingKey.js";
import { LintRuleset } from "../../Linting/LintRuleset.js";

/**
 * Provides settings for project-generators.
 */
export interface ITSProjectSettings extends IGeneratorSettings, ITSProjectPackage
{
    /**
     * Gets or sets a value indicating whether the project should be created as an ESModule.
     */
    [TSProjectSettingKey.ESModule]: boolean;

    /**
     * Gets or sets the ruleset of the project.
     */
    [TSProjectSettingKey.LintRuleset]: LintRuleset;

    /**
     * Gets the workspaces of the project.
     */
    [TSProjectSettingKey.Workspaces]: ITSProjectPackage[];
}
