/**
 * Represents a component for a project.
 */
export enum TSProjectComponent
{
    /**
     * Indicates linting.
     */
    Linting = "linting",

    /**
     * Indicates a monorepo.
     */
    MonoRepo = "monorepo",

    /**
     * Indicates a VSCode workspace.
     */
    VSCode = "vscode"
}
