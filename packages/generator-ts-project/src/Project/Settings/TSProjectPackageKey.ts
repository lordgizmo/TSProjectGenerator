/**
 * Represents a setting for a project package.
 */
export enum TSProjectPackageKey
{
    /**
     * Indicates the {@linkcode Destination} setting.
     */
    Destination = "destination",

    /**
     * Indicates the {@linkcode DisplayName} setting.
     */
    DisplayName = "displayName",

    /**
     * Indicates the {@linkcode Name} setting.
     */
    Name = "name",

    /**
     * Indicates the {@linkcode Description} setting.
     */
    Description = "description"
}
