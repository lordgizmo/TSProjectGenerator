/**
 * Represents a setting for a project-generator.
 */
export enum TSProjectSettingKey
{
    /**
     * Indicates the {@linkcode ESModule} setting.
     */
    ESModule = "es-module",

    /**
     * Indicates the {@linkcode LintRuleset}-setting.
     */
    LintRuleset = "lintRuleset",

    /**
     * Indicates the {@linkcode Workspaces}-setting.
     */
    Workspaces = "package-workspaces"
}
