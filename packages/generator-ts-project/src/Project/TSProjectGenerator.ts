import { createRequire } from "node:module";
import { isAbsolute, relative, sep } from "node:path";
import { fileURLToPath } from "node:url";
import { Generator, IComponentCollection, IFileMapping, Question } from "@manuth/extended-yo-generator";
import { Package } from "@manuth/package-json-editor";
import { TempDirectory } from "@manuth/temp-files";
import chalk from "chalk";
// eslint-disable-next-line @typescript-eslint/tslint/config
import dedent from "dedent";
// eslint-disable-next-line node/no-unpublished-import
import type { ESLint } from "eslint";
import fs from "fs-extra";
import kebabCase from "lodash.kebabcase";
import npmWhich from "npm-which";
// eslint-disable-next-line node/no-unpublished-import
import type { Linter } from "tslint";
import { fileName, TSConfigJSON } from "types-tsconfig";
// eslint-disable-next-line node/no-unpublished-import
import type { Program } from "typescript";
import path from "upath";
import { TSProjectComponentCollection } from "./Components/TSProjectComponentCollection.js";
import { ESLintRCFileMapping } from "./FileMappings/ESLintRCFileMapping.js";
import { ITSProjectPackage } from "./Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "./Settings/ITSProjectSettings.js";
import { ITSProjectOptions } from "./Settings/TSProjectOptions.js";
import { TSProjectPackageKey } from "./Settings/TSProjectPackageKey.js";
import { ICreationContextSettings } from "./Workspace/ICreationContextSettings.js";
import { PackageCreationContext } from "./Workspace/PackageCreationContext.js";
import { ProjectCreationContext } from "./Workspace/ProjectCreationContext.js";
import { RootSettings } from "./Workspace/RootSettings.js";
import { WorkspaceSettings } from "./Workspace/WorkspaceSettings.js";
import { PathPrompt } from "../Inquiry/Prompts/PathPrompt.js";
import { WorkspacePrompt } from "../Inquiry/Prompts/WorkspacePrompt.js";
import { BuildDependencies } from "../NPM/Dependencies/BuildDependencies.js";
import { LintEssentials } from "../NPM/Dependencies/LintEssentials.js";
import { TSProjectSettingKey } from "../Project/Settings/TSProjectSettingKey.js";
import { TSConfigFileMapping } from "../Serialization/TSConfigFileMapping.js";

const { readFile, readJSON, writeFile, writeJSON } = fs;
const { join, resolve } = path;

/**
 * Provides the functionality to generate a project written in in TypeScript.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 *
 * @template TPackage
 * The type of the packages of the generator.
 */
export class TSProjectGenerator<TSettings extends ITSProjectSettings = ITSProjectSettings, TOptions extends ITSProjectOptions = ITSProjectOptions, TPackage extends ITSProjectPackage = ITSProjectPackage> extends Generator<TSettings, TOptions>
{
    /**
     * Initializes a new instance of the {@linkcode TSProjectGenerator} class.
     *
     * @param args
     * A set of arguments for the generator.
     *
     * @param options
     * A set of options for the generator.
     */
    public constructor(args: string | string[], options: TOptions)
    {
        super(
            args,
            {
                ...options,
                customPriorities: [
                    ...(options.customPriorities as any[] ?? []),
                    {
                        before: "end",
                        priorityName: "cleanup"
                    }
                ]
            });

        this.env.adapter.promptModule.registerPrompt(PathPrompt.TypeName, PathPrompt);
        this.env.adapter.promptModule.registerPrompt(WorkspacePrompt.TypeName, WorkspacePrompt);

        this.option(
            kebabCase(nameof<ITSProjectOptions>((options) => options.skipCleanup)),
            {
                type: Boolean,
                default: false,
                description: "Skips the process of cleaning the generated code using eslint"
            });
    }

    /**
     * Gets the path to the directory for the source-files.
     */
    public get SourceRoot(): string
    {
        return "src";
    }

    /**
     * Gets the package creation context.
     */
    public get PackageCreationContext(): PackageCreationContext<TSettings, TOptions, TPackage>
    {
        return this.GetCreationContext(new RootSettings());
    }

    /**
     * Gets the contexts for creating the workspaces.
     */
    public get WorkspaceCreationContexts(): Array<PackageCreationContext<TSettings, TOptions, TPackage>>
    {
        if (this.PackageCreationContext.IsMonoRepo)
        {
            return this.Settings[TSProjectSettingKey.Workspaces]?.map(
                (_, index) =>
                {
                    return this.GetWorkspaceContext(index);
                }) ?? [];
        }
        else
        {
            return [];
        }
    }

    /**
     * @inheritdoc
     */
    public override get Questions(): Array<Question<TSettings>>
    {
        return this.PackageCreationContext.Questions;
    }

    /**
     * @inheritdoc
     */
    public override get Components(): IComponentCollection<TSettings, TOptions>
    {
        return new TSProjectComponentCollection(this);
    }

    /**
     * @inheritdoc
     */
    public override get FileMappings(): Array<IFileMapping<TSettings, TOptions>>
    {
        return [
            ...super.FileMappings,
            ...this.PackageCreationContext.FileMappings,
            ...this.WorkspaceCreationContexts.flatMap((context) => context.FileMappings)
        ];
    }

    /**
     * Creates a new creation context according to the specified {@linkcode settings}.
     *
     * @param settings
     * The settings for the creation context.
     *
     * @returns
     * The newly created creation context.
     */
    public GetCreationContext(settings: ICreationContextSettings<TSettings, TPackage>): PackageCreationContext<TSettings, TOptions, TPackage>
    {
        return new ProjectCreationContext(this);
    }

    /**
     * Gets the creation context for the workspace with the specified {@linkcode index}.
     *
     * @param index
     * The index of the workspace to get the creation context for.
     *
     * @returns
     * The creation context for the workspace with the specified {@linkcode index}.
     */
    public GetWorkspaceContext(index: number): PackageCreationContext<TSettings, TOptions, TPackage>
    {
        return this.GetCreationContext(new WorkspaceSettings(this, index));
    }

    /**
     * @inheritdoc
     */
    public override async prompting(): Promise<void>
    {
        return super.prompting();
    }

    /**
     * @inheritdoc
     */
    public override async writing(): Promise<void>
    {
        this.log(chalk.whiteBright("Generating the Workspace"));
        this.destinationRoot(this.destinationPath(this.Settings[TSProjectPackageKey.Destination]));
        this.Settings[TSProjectPackageKey.Destination] = ".";
        return super.writing();
    }

    /**
     * @inheritdoc
     */
    public override async install(): Promise<void>
    {
        this.log("Your workspace has been generated!");
        super.install();
    }

    /**
     * Cleans the workspace.
     */
    public async cleanup(): Promise<void>
    {
        if (!this.options.skipCleanup)
        {
            await this.Cleanup();
        }
    }

    /**
     * @inheritdoc
     */
    public override async end(): Promise<void>
    {
        this.log("");

        let destinationPath = relative(process.cwd(), this.destinationPath());

        if (!isAbsolute(destinationPath) && !destinationPath.startsWith("."))
        {
            destinationPath = [".", destinationPath].join(sep);
        }

        this.log(
            dedent(
                `
                    ${chalk.whiteBright("Finished!")}
                    Your package "${this.Settings[TSProjectPackageKey.DisplayName]}" has been created!
                    To start editing with Visual Studio Code use the following command:

                        code "${destinationPath}"`));
    }

    /**
     * Cleans the workspace.
     */
    protected async Cleanup(): Promise<void>
    {
        let tempDir = new TempDirectory();
        let esLintJSFileName = new ESLintRCFileMapping(this, this.PackageCreationContext).DefaultBaseName;
        let projectPackage = new Package(this.destinationPath(Package.FileName));
        let lintPackage = new Package(tempDir.MakePath(Package.FileName), {});
        let workspaceRequire: NodeRequire;
        let linterConstructor: typeof Linter;
        let eslintConstructor: typeof ESLint;
        let program: Program;
        let linter: ESLint;
        let tsConfigFile = tempDir.MakePath(fileName);
        let tsConfig = await readJSON(this.modulePath(TSConfigFileMapping.GetFileName("base"))) as TSConfigJSON;
        this.log("");
        this.log(chalk.whiteBright("Cleaning up the TypeScript-Files…"));
        this.log(chalk.whiteBright("Creating a temporary linting-environment…"));
        tsConfig.compilerOptions ??= {};
        tsConfig.compilerOptions.allowJs = true;
        tsConfig.compilerOptions.checkJs = true;
        tsConfig.compilerOptions.rootDir = resolve(this.destinationPath());

        tsConfig.include = [
            resolve(this.destinationPath("scripts", "**", "*")),
            resolve(this.destinationPath("**", ESLintRCFileMapping.FileName))
        ];

        if (this.PackageCreationContext.IsMonoRepo)
        {
            let workspaces: any = projectPackage.AdditionalProperties.Get("workspaces");
            let workspacePatterns: string[] = [];

            if (Array.isArray(workspaces))
            {
                workspacePatterns = workspaces;
            }
            else if (Array.isArray(workspaces.packages))
            {
                workspacePatterns = workspaces.packages;
            }

            tsConfig.include.push(
                ...workspacePatterns.map(
                    (pattern) =>
                    {
                        return resolve(this.destinationPath(pattern, this.SourceRoot));
                    }));
        }
        else
        {
            tsConfig.include.push(resolve(this.destinationPath(this.SourceRoot, "**", "*")));
        }

        await writeJSON(tsConfigFile, tsConfig);
        await writeFile(tempDir.MakePath(esLintJSFileName), await readFile(this.modulePath(esLintJSFileName)));
        lintPackage.Register(new BuildDependencies());
        lintPackage.Register(new LintEssentials());
        await writeJSON(lintPackage.FileName, lintPackage.ToJSON());

        this.spawnCommandSync(
            npmWhich(fileURLToPath(new URL(".", import.meta.url))).sync("npm"),
            [
                "install",
                "--silent"
            ],
            {
                cwd: tempDir.FullName
            });

        workspaceRequire = createRequire(join(tempDir.FullName, ".js"));
        linterConstructor = workspaceRequire("tslint").Linter;
        eslintConstructor = workspaceRequire("eslint").ESLint;
        program = linterConstructor.createProgram(tsConfigFile);

        linter = new eslintConstructor(
            {
                cwd: tempDir.FullName,
                fix: true,
                useEslintrc: false,
                overrideConfigFile: tempDir.MakePath(esLintJSFileName),
                overrideConfig: {
                    parserOptions: {
                        project: tsConfigFile
                    }
                }
            });

        for (let fileName of program.getRootFileNames())
        {
            this.log(chalk.gray(`Cleaning up "${relative(this.destinationPath(), fileName)}"…`));
            await eslintConstructor.outputFixes(await linter.lintFiles(fileName));
        }

        tempDir.Dispose();
    }
}
