import { GeneratorOptions, GeneratorSettingKey } from "@manuth/extended-yo-generator";
import upath from "upath";
import { CodeWorkspaceComponent } from "../../VSCode/Components/CodeWorkspaceComponent.js";
import { IDebugConfiguration } from "../../VSCode/IDebugConfiguration.js";
import { ILaunchSettings } from "../../VSCode/ILaunchSettings.js";
import { LaunchSettingsProcessor } from "../../VSCode/LaunchSettingsProcessor.js";
import { ITSProjectSettings } from "../Settings/ITSProjectSettings.js";
import { TSProjectComponent } from "../Settings/TSProjectComponent.js";
import { TSProjectPackageKey } from "../Settings/TSProjectPackageKey.js";
import { TSProjectSettingKey } from "../Settings/TSProjectSettingKey.js";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import type { TSProjectGenerator } from "../TSProjectGenerator.js";

const { join, normalize } = upath;

/**
 * Provides the functionality to process debug-configurations for {@linkcode TSProjectGenerator}s.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 */
export class TSProjectLaunchSettingsProcessor<TSettings extends ITSProjectSettings, TOptions extends GeneratorOptions> extends LaunchSettingsProcessor<TSettings, TOptions>
{
    /**
     * Initializes a new instance of the {@linkcode TSProjectLaunchSettingsProcessor} class.
     *
     * @param component
     * The component of the processor.
     */
    public constructor(component: CodeWorkspaceComponent<TSettings, TOptions>)
    {
        super(component);
    }

    /**
     * Gets a value indicating whether the project is a mono repo.
     */
    protected get IsMonoRepo(): boolean
    {
        return this.Generator.Settings[GeneratorSettingKey.Components]?.includes(TSProjectComponent.MonoRepo);
    }

    /**
     * @inheritdoc
     *
     * @param debugSettings
     * The data to process.
     *
     * @returns
     * The processed data.
     */
    public override async Process(debugSettings: ILaunchSettings): Promise<ILaunchSettings>
    {
        let result: ILaunchSettings;
        delete debugSettings.compounds;
        result = await super.Process(debugSettings);

        if (this.IsMonoRepo)
        {
            let presentationKey = "presentation";
            let orderKey = "order";
            let index = 2;
            let configurations: IDebugConfiguration[] = [];
            let testConfigNames: string[] = [];

            for (let configuration of result.configurations ?? [])
            {
                if (configuration.program?.includes("mocha"))
                {
                    for (let workspace of this.Generator.Settings[TSProjectSettingKey.Workspaces] ?? [])
                    {
                        let displayName = workspace[TSProjectPackageKey.DisplayName];

                        configuration.name = `Launch ${displayName} Tests`;
                        testConfigNames.push(configuration.name);
                        this.ReplaceWorkspaceDirectives(configuration, displayName);

                        configurations.push(
                            {
                                ...configuration,
                                [presentationKey]: {
                                    ...configuration[presentationKey],
                                    [orderKey]: index++
                                }
                            });
                    }
                }
                else
                {
                    this.ReplaceWorkspaceDirectives(configuration, this.WorkspaceRootName);
                    configurations.push(configuration);
                }
            }

            result.configurations = configurations;

            result.compounds = [
                {
                    name: "Launch Tests",
                    configurations: testConfigNames,
                    [presentationKey]: {
                        [orderKey]: 1
                    }
                }
            ];
        }

        return result;
    }

    /**
     * Determines whether a debug-configuration should be included.
     *
     * @param debugConfig
     * The debug-configuration to filter.
     *
     * @returns
     * A value indicating whether the debug-configuration should be included.
     */
    protected override async FilterDebugConfig(debugConfig: IDebugConfiguration): Promise<boolean>
    {
        let workspaceDirective = this.GetWorkspaceFolderDirective("TSProjectGenerator");

        return (
            (debugConfig.args as string[])?.[0]?.includes(workspaceDirective) ||
            (debugConfig.cwd as string)?.includes(workspaceDirective)) &&
            !(normalize(debugConfig.program ?? "").toLowerCase().endsWith(join("node_modules", "yo", "lib", "cli.js")));
    }

    /**
     * @inheritdoc
     *
     * @param debugConfig
     * The debug-configuration to process.
     *
     * @returns
     * The processed debug-configuration.
     */
    protected override async ProcessDebugConfig(debugConfig: IDebugConfiguration): Promise<IDebugConfiguration>
    {
        debugConfig.name = debugConfig.name.replace(/(?<!\s)\s+TSProjectGenerator\s+/, " ");
        delete debugConfig.presentation;
        delete debugConfig.autoAttachChildProcesses;
        delete debugConfig.skipFiles;

        this.ReplaceWorkspaceDirectives(
            debugConfig,
            this.IsMonoRepo ? "Placeholder" : undefined);

        return debugConfig;
    }

    /**
     * @inheritdoc
     *
     * @param config
     * The configuration to replace the workspace directives in.
     *
     * @param newName
     * The new name of the workspace.
     */
    protected override ReplaceWorkspaceDirectives(config: IDebugConfiguration, newName?: string | undefined): void
    {
        super.ReplaceWorkspaceDirectives(config, newName);

        if (this.IsMonoRepo &&
            config.program?.includes("/node_modules/"))
        {
            config.program = this.ReplaceWorkspaceFolder(config.program, this.WorkspaceRootName);
        }
    }
}
