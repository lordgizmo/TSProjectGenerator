import { GeneratorOptions, GeneratorSettingKey } from "@manuth/extended-yo-generator";
import { CodeWorkspaceComponent } from "../../VSCode/Components/CodeWorkspaceComponent.js";
import { SettingsProcessor } from "../../VSCode/SettingsProcessor.js";
import { ITSProjectSettings } from "../Settings/ITSProjectSettings.js";
import { TSProjectComponent } from "../Settings/TSProjectComponent.js";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import type { TSProjectGenerator } from "../TSProjectGenerator.js";

/**
 * Provides the functionality to process settings for {@linkcode TSProjectGenerator}s.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 */
export class TSProjectSettingsProcessor<TSettings extends ITSProjectSettings, TOptions extends GeneratorOptions> extends SettingsProcessor<TSettings, TOptions>
{
    /**
     * Initializes a new instance of the {@linkcode TSProjectSettingsProcessor} class.
     *
     * @param component
     * The component of the processor.
     */
    public constructor(component: CodeWorkspaceComponent<TSettings, TOptions>)
    {
        super(component);
    }

    /**
     * @inheritdoc
     *
     * @param key
     * The key of the setting.
     *
     * @param value
     * The value of the setting to filter.
     *
     * @returns
     * A value indicating whether the setting with the specified key should be included.
     */
    protected override async FilterSetting(key: string, value: any): Promise<boolean>
    {
        return ![
            "files.associations",
            "mochaExplorer.debuggerConfig",
            "search.exclude",
            "typescript.tsdk",
            ...(
                this.Generator.Settings[GeneratorSettingKey.Components]?.includes(TSProjectComponent.MonoRepo) ?
                [] :
                [
                    "terminal.integrated.cwd"
                ])
        ].includes(key);
    }
}
