import { GeneratorOptions, GeneratorSettingKey } from "@manuth/extended-yo-generator";
import { CodeWorkspaceComponent } from "../../VSCode/Components/CodeWorkspaceComponent.js";
import { ITaskDefinition } from "../../VSCode/ITaskDefinition.js";
import { TasksProcessor } from "../../VSCode/TasksProcessor.js";
import { ITSProjectSettings } from "../Settings/ITSProjectSettings.js";
import { TSProjectComponent } from "../Settings/TSProjectComponent.js";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import type { TSProjectGenerator } from "../TSProjectGenerator.js";

/**
 * Provides the functionality to process tasks for {@linkcode TSProjectGenerator}s.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 */
export class TSProjectTasksProcessor<TSettings extends ITSProjectSettings, TOptions extends GeneratorOptions> extends TasksProcessor<TSettings, TOptions>
{
    /**
     * Initializes a new instance of the {@linkcode TSProjectTasksProcessor} class.
     *
     * @param component
     * The component of the processor.
     */
    public constructor(component: CodeWorkspaceComponent<TSettings, TOptions>)
    {
        super(component);
    }

    /**
     * Processes a task-configuration.
     *
     * @param task
     * The task to process.
     *
     * @returns
     * The processed task.
     */
    protected override async ProcessTask(task: ITaskDefinition): Promise<ITaskDefinition>
    {
        if (!this.Generator.Settings[GeneratorSettingKey.Components]?.includes(TSProjectComponent.MonoRepo))
        {
            let workspaceFolderDirective = this.GetWorkspaceFolderDirective();
            this.ReplaceWorkspaceDirectives(task);

            if (
                task.type === "shell" &&
                task.command === "npm" &&
                Array.isArray(task.args) &&
                task.args[0] === "run" &&
                task.args.length >= 2)
            {
                Object.assign(
                    task,
                    {
                        type: "npm"
                    });

                delete task.command;
                task.script = task.args[1];
                task.args.splice(0, 2);

                if (task.args.length === 0)
                {
                    delete task.args;
                }

                task = {
                    ...(task.label ? { label: task.label } : {}),
                    ...{
                        type: task.type,
                        script: task.script
                    },
                    ...task
                };
            }

            if (typeof task.options === "object")
            {
                if (typeof task.options.cwd === "string")
                {
                    task.options.cwd = this.ReplaceWorkspaceFolder(task.options.cwd);

                    if (task.options.cwd === workspaceFolderDirective)
                    {
                        delete task.options.cwd;
                    }
                }

                if (Object.keys(task.options).length === 0)
                {
                    delete task.options;
                }
            }

            if (task.problemMatcher)
            {
                task.problemMatcher = (Array.isArray(task.problemMatcher) ? task.problemMatcher : [task.problemMatcher]).map(
                    (problemMatcher) =>
                    {
                        if (typeof problemMatcher === "object")
                        {
                            if (
                                Array.isArray(problemMatcher.fileLocation) &&
                                problemMatcher.fileLocation[0] === "relative" &&
                                this.ReplaceWorkspaceFolder(problemMatcher.fileLocation[1]) === workspaceFolderDirective)
                            {
                                delete problemMatcher.fileLocation;
                            }

                            if (Object.keys(problemMatcher).length === 1 && problemMatcher.base)
                            {
                                return problemMatcher.base;
                            }
                        }

                        return problemMatcher;
                    });

                if (task.problemMatcher.length === 1)
                {
                    task.problemMatcher = task.problemMatcher[0];
                }
            }
        }
        else
        {
            this.ReplaceWorkspaceDirectives(task, this.WorkspaceRootName);
        }

        if (
            typeof task.label === "string" &&
            task.label.toLowerCase() === "lint")
        {
            if (
                typeof task.problemMatcher === "object" &&
                Array.isArray(task.problemMatcher))
            {
                task.problemMatcher = task.problemMatcher[0];
            }
        }

        return task;
    }
}
