import { GeneratorOptions, GeneratorSettingKey } from "@manuth/extended-yo-generator";
import path from "upath";
import { TSProjectExtensionsProcessor } from "./TSProjectExtensionsProcessor.js";
import { TSProjectLaunchSettingsProcessor } from "./TSProjectLaunchSettingsProcessor.js";
import { TSProjectSettingsProcessor } from "./TSProjectSettingsProcessor.js";
import { TSProjectTasksProcessor } from "./TSProjectTasksProcessor.js";
import { TSProjectSettingKey } from "../../Project/Settings/TSProjectSettingKey.js";
import { JSONProcessor } from "../../Serialization/JSONProcessor.js";
import { CodeWorkspaceComponent } from "../../VSCode/Components/CodeWorkspaceComponent.js";
import { IExtensionSettings } from "../../VSCode/IExtensionSettings.js";
import { ILaunchSettings } from "../../VSCode/ILaunchSettings.js";
import { ITaskSettings } from "../../VSCode/ITaskSettings.js";
import { IWorkspaceMetadata } from "../../VSCode/IWorkspaceMetadata.js";
import { WorkspaceFolder } from "../../VSCode/WorkspaceFolder.js";
import { WorkspaceProcessor } from "../../VSCode/WorkspaceProcessor.js";
import { ITSProjectSettings } from "../Settings/ITSProjectSettings.js";
import { TSProjectComponent } from "../Settings/TSProjectComponent.js";
import { TSProjectPackageKey } from "../Settings/TSProjectPackageKey.js";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import type { TSProjectGenerator } from "../TSProjectGenerator.js";

const { joinSafe } = path;

/**
 * Provides the functionality to process workspaces for {@linkcode TSProjectGenerator}s.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 */
export class TSProjectWorkspaceProcessor<TSettings extends ITSProjectSettings, TOptions extends GeneratorOptions> extends WorkspaceProcessor<TSettings, TOptions>
{
    /**
     * Initializes a new instance of the {@linkcode TSProjectWorkspaceProcessor} class.
     *
     * @param component
     * The component of the processor.
     */
    public constructor(component: CodeWorkspaceComponent<TSettings, TOptions>)
    {
        super(component);
    }

    /**
     * @inheritdoc
     */
    protected override get ExtensionsProcessor(): JSONProcessor<TSettings, TOptions, IExtensionSettings>
    {
        return new TSProjectExtensionsProcessor(this.Component);
    }

    /**
     * @inheritdoc
     */
    protected override get LaunchSettingsProcessor(): JSONProcessor<TSettings, TOptions, ILaunchSettings>
    {
        return new TSProjectLaunchSettingsProcessor(this.Component);
    }

    /**
     * @inheritdoc
     */
    protected override get SettingsProcessor(): JSONProcessor<TSettings, TOptions, Record<string, any>>
    {
        return new TSProjectSettingsProcessor(this.Component);
    }

    /**
     * @inheritdoc
     */
    protected override get TasksProcessor(): JSONProcessor<TSettings, TOptions, ITaskSettings>
    {
        return new TSProjectTasksProcessor(this.Component);
    }

    /**
     * @inheritdoc
     *
     * @param data
     * The data to process.
     *
     * @returns
     * The processed data.
     */
    public override async Process(data: IWorkspaceMetadata): Promise<IWorkspaceMetadata>
    {
        let result = await super.Process(data);

        if (this.Generator.Settings[GeneratorSettingKey.Components]?.includes(TSProjectComponent.MonoRepo))
        {
            result.folders = [
                ...(
                    (this.Generator.Settings[TSProjectSettingKey.Workspaces] ?? []).map(
                        (workspace): WorkspaceFolder =>
                        {
                            return {
                                name: workspace[TSProjectPackageKey.DisplayName],
                                path: joinSafe("./", workspace[TSProjectPackageKey.Destination])
                            };
                        })),
                {
                    name: this.WorkspaceRootName,
                    path: "."
                }
            ];
        }

        return result;
    }
}
