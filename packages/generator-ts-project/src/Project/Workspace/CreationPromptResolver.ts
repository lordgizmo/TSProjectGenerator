import { basename, isAbsolute, join } from "node:path";
import { IGenerator } from "@manuth/extended-yo-generator";
import { Package } from "@manuth/package-json-editor";
import fs from "fs-extra";
import path from "upath";
import { ICreationContext } from "./ICreationContext.js";
import { ICreationPromptResolver } from "./ICreationPromptResolver.js";
import { ITSProjectPackage } from "../Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../Settings/ITSProjectSettings.js";

const { pathExists } = fs;
const { normalize } = path;

/**
 * Provides the functionality to resolve answers to prompts answered by the user.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TAnswers
 * The type of the answers to resolve.
 */
export class CreationPromptResolver<TSettings extends ITSProjectSettings, TAnswers extends ITSProjectPackage> implements ICreationPromptResolver<TAnswers>
{
    /**
     * The generator of the resolver.
     */
    private generator: IGenerator<TSettings, any>;

    /**
     * The context of the resolver.
     */
    private context: ICreationContext<TSettings, TAnswers>;

    /**
     * Initializes a new instance of the {@linkcode CreationPromptResolver} class.
     *
     * @param generator
     * The generator of the resolver.
     *
     * @param context
     * The context of the resolver.
     */
    public constructor(generator: IGenerator<TSettings, any>, context: ICreationContext<TSettings, TAnswers>)
    {
        this.generator = generator;
        this.context = context;
    }

    /**
     * @inheritdoc
     */
    public get DestinationKey(): keyof TAnswers
    {
        return this.Context.Settings.DestinationKey;
    }

    /**
     * @inheritdoc
     */
    public get DisplayNameKey(): keyof TAnswers
    {
        return this.Context.Settings.DisplayNameKey;
    }

    /**
     * @inheritdoc
     */
    public get ModuleNameKey(): keyof TAnswers
    {
        return this.Context.Settings.ModuleNameKey;
    }

    /**
     * @inheritdoc
     */
    public get DescriptionKey(): keyof TAnswers
    {
        return this.Context.Settings.DescriptionKey;
    }

    /**
     * @inheritdoc
     */
    public get AllowOutside(): boolean
    {
        return this.Context.Settings.AllowOutside;
    }

    /**
     * @inheritdoc
     */
    public get DestinationMessage(): string
    {
        return this.Context.DestinationMessage;
    }

    /**
     * @inheritdoc
     */
    public get DisplayNameMessage(): string
    {
        return this.Context.DisplayNameMessage;
    }

    /**
     * @inheritdoc
     */
    public get ModuleNameMessage(): string
    {
        return this.Context.ModuleNameMessage;
    }

    /**
     * @inheritdoc
     */
    public get DescriptionMessage(): string
    {
        return this.Context.DescriptionMessage;
    }

    /**
     * Gets the generator of the resolver.
     */
    protected get Generator(): IGenerator<TSettings, any>
    {
        return this.generator;
    }

    /**
     * Gets the context of the resolver.
     */
    protected get Context(): ICreationContext<TSettings, TAnswers>
    {
        return this.context;
    }

    /**
     * @inheritdoc
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * The suggested path to the package to create.
     */
    public GetSuggestedPackagePath(answers: TAnswers): Promise<string>
    {
        return this.Context.GetSuggestedPackagePath(answers);
    }

    /**
     * @inheritdoc
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * The suggested human readable name for the package.
     */
    public async GetSuggestedDisplayName(answers: TAnswers): Promise<string>
    {
        return basename(this.GetPackagePath(answers));
    }

    /**
     * @inheritdoc
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * A suggested module name for the package.
     */
    public async GetSuggestedModuleName(answers: TAnswers): Promise<string>
    {
        return this.Context.GetSuggestedModuleName(answers);
    }

    /**
     * @inheritdoc
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * The suggested description for the package.
     */
    public async GetSuggestedDescription(answers: TAnswers): Promise<string | undefined>
    {
        let npmPackage = await this.GetPackage(answers);
        await npmPackage?.Normalize();
        return npmPackage?.Description;
    }

    /**
     * @inheritdoc
     *
     * @param answers
     * The answers provided by the user up to this point.
     *
     * @returns
     * The root of the package to create.
     */
    public async GetPackageRoot(answers: TAnswers): Promise<string>
    {
        return this.Context.Settings.GetPackageRoot(answers);
    }

    /**
     * @inheritdoc
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * The destination path of the generator.
     */
    public GetDestination(answers: TAnswers): string
    {
        return this.Context.Settings.GetDestination(answers);
    }

    /**
     * @inheritdoc
     *
     * @param answers
     * The answers provided by the user up to this point.
     *
     * @returns
     * The path to the package to create.
     */
    public GetPackagePath(answers: TAnswers): string
    {
        let destination = this.GetDestination(answers);

        if (isAbsolute(destination))
        {
            return destination;
        }
        else
        {
            return this.Generator.destinationPath(destination);
        }
    }

    /**
     * @inheritdoc
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * The path to the `package.json` file to create.
     */
    public async GetPackageFileName(answers: TAnswers): Promise<string>
    {
        return join(this.GetPackagePath(answers), Package.FileName);
    }

    /**
     * Gets the parsed package file.
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * The parsed package file.
     */
    public async GetPackage(answers: TAnswers): Promise<Package | undefined>
    {
        if (await pathExists(await this.GetPackageFileName(answers)))
        {
            return new Package(await this.GetPackageFileName(answers));
        }
        else
        {
            return new Package(await this.GetPackageFileName(answers), {});
        }
    }

    /**
     * @inheritdoc
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * The name of the package to create.
     */
    public async GetModuleName(answers: TAnswers): Promise<string>
    {
        return answers[this.ModuleNameKey] as string;
    }

    /**
     * @inheritdoc
     *
     * @param answers
     * The answers provided by the user up to this point.
     *
     * @returns
     * The human readable name of the package.
     */
    public GetDisplayName(answers: TAnswers): string
    {
        return answers[this.DisplayNameKey] as string;
    }

    /**
     * @inheritdoc
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * The description of the package.
     */
    public async GetDescription(answers: TAnswers): Promise<string>
    {
        return answers[this.DescriptionKey] as string;
    }

    /**
     * @inheritdoc
     *
     * @param input
     * The input provided by the user.
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * Either a value indicating whether the specified {@linkcode input} is valid or a text describing the issue.
     */
    public async ValidateDestination(input: string, answers: TAnswers): Promise<string | boolean>
    {
        let path = normalize(input);

        if (this.Context.Settings.BannedDirectories.some(
            (bannedDirectory) =>
            {
                return normalize(bannedDirectory) === path;
            }))
        {
            return `The directory \`${input}\` has already been used.`;
        }
        else
        {
            return true;
        }
    }

    /**
     * @inheritdoc
     *
     * @param input
     * The input provided by the user.
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * Either a value indicating whether the specified {@linkcode input} is valid or a text describing the issue.
     */
    public async ValidateDisplayName(input: string, answers: TAnswers): Promise<string | boolean>
    {
        if (this.Context.Settings.BannedDisplayNames.includes(input))
        {
            return `The name \`${input}\` is already in use.`;
        }
        else
        {
            return true;
        }
    }

    /**
     * @inheritdoc
     *
     * @param input
     * The input provided by the user.
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * Either a value indicating whether the specified {@linkcode input} is valid or a text describing the issue.
     */
    public async ValidateModuleName(input: string, answers: TAnswers): Promise<string | boolean>
    {
        return this.Context.ValidateModuleName(input, answers);
    }
}
