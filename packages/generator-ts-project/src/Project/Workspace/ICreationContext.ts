import { Package } from "@manuth/package-json-editor";
import { ICreationContextSettings } from "./ICreationContextSettings.js";
import { ICreationPromptResolver } from "./ICreationPromptResolver.js";
import { ITSProjectPackage } from "../Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../Settings/ITSProjectSettings.js";

/**
 * Represents a component which provides settings for creating packages.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TAnswers
 * The type of the answers of the context.
 */
export interface ICreationContext<TSettings extends ITSProjectSettings, TAnswers extends ITSProjectPackage>
{
    /**
     * Gets a message for asking for the package destination.
     */
    get DestinationMessage(): string;

    /**
     * Gets a message for asking for the human readable name of the package.
     */
    get DisplayNameMessage(): string;

    /**
     * Gets a message for asking for the module name of the package.
     */
    get ModuleNameMessage(): string;

    /**
     * Gets a message for asking for the package description.
     */
    get DescriptionMessage(): string;

    /**
     * Gets a value indicating whether packages are allowed outside of the package root.
     */
    get AllowOutside(): boolean;

    /**
     * Gets a value indicating whether the package to generate is a mono repo.
     */
    get IsMonoRepo(): boolean;

    /**
     * Gets a value indicating whether the package is the root of the workspace.
     */
    get IsWorkspaceRoot(): boolean;

    /**
     * Gets a value indicating whether the current package is a workspace.
     */
    get IsWorkspace(): boolean;

    /**
     * Gets a component for resolving the answers provided by the user.
     */
    get Resolver(): ICreationPromptResolver<TAnswers>;

    /**
     * Gets the settings of the creation context.
     */
    get Settings(): ICreationContextSettings<TSettings, TAnswers>;

    /**
     * Gets the metadata stored in the generator settings.
     */
    get Metadata(): TAnswers;

    /**
     * Gets the destination path of the package.
     */
    get Destination(): string;

    /**
     * Gets the path to the directory of the package to create.
     */
    get PackagePath(): string;

    /**
     * Gets the path to the `package.json` file to create.
     */
    get PackageFileName(): Promise<string>;

    /**
     * Gets the parsed representation of the existing package file.
     */
    get Package(): Promise<Package | undefined>;

    /**
     * The name of the package to create.
     */
    get ModuleName(): Promise<string>;

    /**
     * Gets the human readable name of the package.
     */
    get DisplayName(): string;

    /**
     * Gets the description of the package.
     */
    get Description(): Promise<string>;

    /**
     * Gets a suggested path to the package to create.
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * The suggested path to the package to create.
     */
    GetSuggestedPackagePath(answers: TAnswers): Promise<string>;

    /**
     * Gets a suggested module name for the package.
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * A suggested module name for the package.
     */
    GetSuggestedModuleName(answers: TAnswers): Promise<string>;

    /**
     * Determines whether the {@linkcode input} provided by the user is valid.
     *
     * @param input
     * The input provided by the user.
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * Either a value indicating whether the specified {@linkcode input} is valid or a string containing an error message.
     */
    ValidateModuleName(input: string, answers: TAnswers): Promise<string | boolean>;
}
