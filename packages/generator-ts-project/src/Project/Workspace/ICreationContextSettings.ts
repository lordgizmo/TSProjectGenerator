import { ITSProjectPackage } from "../Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../Settings/ITSProjectSettings.js";

/**
 * Provides settings for the creation of a package.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TAnswers
 * The type of the answers of the context.
 */
export interface ICreationContextSettings<TSettings extends ITSProjectSettings, TAnswers extends ITSProjectPackage>
{
    /**
     * Gets the key of the destination property.
     */
    get DestinationKey(): keyof TAnswers;

    /**
     * Gets the key for the human readable name of the package.
     */
    get DisplayNameKey(): keyof TAnswers;

    /**
     * Gets the key for the module name property.
     */
    get ModuleNameKey(): keyof TAnswers;

    /**
     * Gets the key for the description property.
     */
    get DescriptionKey(): keyof TAnswers;

    /**
     * Gets a value indicating whether packages are allowed outside of the {@linkcode GetPackageRoot}.
     */
    get AllowOutside(): boolean;

    /**
     * Gets a value indicating whether the package is the root of the workspace.
     */
    get IsWorkspaceRoot(): boolean;

    /**
     * Gets a set of banned directories.
     */
    get BannedDirectories(): string[];

    /**
     * Gets a set of banned display names.
     */
    get BannedDisplayNames(): string[];

    /**
     * Gets a set of banned module names.
     */
    get BannedModuleNames(): string[];

    /**
     * Determines the root of the package to create based on the {@linkcode answers} provided by the user.
     *
     * @param answers
     * The answers provided by the user up to this point.
     *
     * @returns
     * The root of the package to create.
     */
    GetPackageRoot(answers: TAnswers): Promise<string>;

    /**
     * Gets the destination path of the generator.
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * The destination path of the generator.
     */
    GetDestination(answers: TAnswers): string;

    /**
     * Extracts the metadata of the package from the generator settings.
     *
     * @param settings
     * The settings of the generator.
     *
     * @returns
     * The metadata of the package.
     */
    GetMetadata(settings: TSettings): TAnswers;
}
