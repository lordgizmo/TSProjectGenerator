import { Package } from "@manuth/package-json-editor";
import { ITSProjectPackage } from "../Settings/ITSProjectPackage.js";

/**
 * Provides the functionality to resolve prompts answered by the user.
 *
 * @template TAnswers
 * The type of the answers to resolve.
 */
export interface ICreationPromptResolver<TAnswers extends ITSProjectPackage>
{
    /**
     * Gets the key of the destination property.
     */
    get DestinationKey(): keyof TAnswers;

    /**
     * Gets the key for the human readable name of the package.
     */
    get DisplayNameKey(): keyof TAnswers;

    /**
     * Gets the key for the module name property.
     */
    get ModuleNameKey(): keyof TAnswers;

    /**
     * Gets the key of the description property.
     */
    get DescriptionKey(): keyof TAnswers;

    /**
     * Gets a value indicating whether packages are allowed outside of the {@linkcode GetPackageRoot}.
     */
    get AllowOutside(): boolean;

    /**
     * Gets a message for asking for the package destination.
     */
    get DestinationMessage(): string;

    /**
     * Gets a message for asking for the human readable name of the package.
     */
    get DisplayNameMessage(): string;

    /**
     * Gets a message for asking for the module name of the package.
     */
    get ModuleNameMessage(): string;

    /**
     * Gets a message for asking for the package description.
     */
    get DescriptionMessage(): string;

    /**
     * Gets a suggested path to the package to create.
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * The suggested path to the package to create.
     */
    GetSuggestedPackagePath(answers: TAnswers): Promise<string>;

    /**
     * Gets a suggested human readable name for the package to created.
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * The suggested human readable name for the package.
     */
    GetSuggestedDisplayName(answers: TAnswers): Promise<string>;

    /**
     * Gets a suggested module name for the package.
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * A suggested module name for the package.
     */
    GetSuggestedModuleName(answers: TAnswers): Promise<string>;

    /**
     * Gets a suggested description for the package.
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * The suggested description for the package.
     */
    GetSuggestedDescription(answers: TAnswers): Promise<string | undefined>;

    /**
     * Determines the root of the package to create based on the {@linkcode answers} provided by the user.
     *
     * @param answers
     * The answers provided by the user up to this point.
     *
     * @returns
     * The root of the package to create.
     */
    GetPackageRoot(answers: TAnswers): Promise<string>;

    /**
     * Determines the root of the package to create based on the {@linkcode answers} provided by the user.
     *
     * @param answers
     * The answers provided by the user up to this point.
     *
     * @returns
     * The root of the package to create.
     */
    GetPackageRoot(answers: TAnswers): Promise<string>;

    /**
     * Gets the destination path of the generator.
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * The destination path of the generator.
     */
    GetDestination(answers: TAnswers): string;

    /**
     * @inheritdoc
     *
     * @param answers
     * The answers provided by the user up to this point.
     *
     * @returns
     * The path to the package to create.
     */
    GetPackagePath(answers: TAnswers): string;

    /**
     * Gets the path to the `package.json` file to create.
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * The path to the `package.json` file to create.
     */
    GetPackageFileName(answers: TAnswers): Promise<string>;

    /**
     * Gets the parsed package file.
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * The parsed package file.
     */
    GetPackage(answers: TAnswers): Promise<Package | undefined>;

    /**
     * Gets the name of the package.
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * The name of the package to create.
     */
    GetModuleName(answers: TAnswers): Promise<string>;

    /**
     * Gets the human readable name of the package.
     *
     * @param answers
     * The answers provided by the user up to this point.
     *
     * @returns
     * The human readable name of the package.
     */
    GetDisplayName(answers: TAnswers): string;

    /**
     * Gets the description of the package.
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * The description of the package.
     */
    GetDescription(answers: TAnswers): Promise<string>;

    /**
     * Determines whether the {@linkcode input} provided by the user is valid.
     *
     * @param input
     * The input provided by the user.
     *
     * @param answers
     * The answers provided by the user.
     */
    ValidateDestination(input: string, answers: TAnswers): Promise<string | boolean>;

    /**
     * Determines whether the {@linkcode input} provided by the user is valid.
     *
     * @param input
     * The input provided by the user.
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * Either a value indicating whether the specified {@linkcode input} is valid or a text describing the issue.
     */
    ValidateDisplayName(input: string, answers: TAnswers): Promise<string | boolean>;

    /**
     * Determines whether the {@linkcode input} provided by the user is valid.
     *
     * @param input
     * The input provided by the user.
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * Either a value indicating whether the specified {@linkcode input} is valid or a string containing an error message.
     */
    ValidateModuleName(input: string, answers: TAnswers): Promise<string | boolean>;
}
