import { GeneratorOptions, GeneratorSettingKey, IFileMapping, Question } from "@manuth/extended-yo-generator";
import { Package, PackageType } from "@manuth/package-json-editor";
import kebabCase from "lodash.kebabcase";
import validate from "validate-npm-package-name";
import { CreationPromptResolver } from "./CreationPromptResolver.js";
import { ICreationContext } from "./ICreationContext.js";
import { ICreationContextSettings } from "./ICreationContextSettings.js";
import { ICreationPromptResolver } from "./ICreationPromptResolver.js";
import { RootSettings } from "./RootSettings.js";
import { JSONCCreatorMapping } from "../../Serialization/JSONCCreatorMapping.js";
import { PackageDescriptionQuestion } from "../Inquiry/PackageDescriptionQuestion.js";
import { PackageDestinationQuestion } from "../Inquiry/PackageDestinationQuestion.js";
import { PackageDisplayNameQuestion } from "../Inquiry/PackageDisplayNameQuestion.js";
import { PackageModuleNameQuestion } from "../Inquiry/PackageModuleNameQuestion.js";
import { ITSProjectPackage } from "../Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../Settings/ITSProjectSettings.js";
import { TSProjectComponent } from "../Settings/TSProjectComponent.js";
import { TSProjectSettingKey } from "../Settings/TSProjectSettingKey.js";
import { TSProjectGenerator } from "../TSProjectGenerator.js";

/**
 * Provides a context and settings for creating a package.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 *
 * @template TAnswers
 * The type of the answers of the context.
 */
export abstract class PackageCreationContext<TSettings extends ITSProjectSettings, TOptions extends GeneratorOptions, TAnswers extends ITSProjectPackage> implements ICreationContext<TSettings, TAnswers>
{
    /**
     * The generator this context belongs to.
     */
    private generator: TSProjectGenerator<TSettings, TOptions, TAnswers>;

    /**
     * A component for resolving answers to prompts provided by the user.
     */
    private resolver: ICreationPromptResolver<TAnswers>;

    /**
     * The settings for the creation context.
     */
    private settings: ICreationContextSettings<TSettings, TAnswers>;

    /**
     * Initializes a new instance of the {@linkcode PackageCreationContext} class.
     *
     * @param generator
     * The generator this context belongs to.
     *
     * @param settings
     * The settings for the creation context.
     */
    public constructor(generator: TSProjectGenerator<TSettings, TOptions, TAnswers>, settings?: ICreationContextSettings<TSettings, TAnswers>)
    {
        this.generator = generator;
        this.resolver = new CreationPromptResolver(generator, this);
        this.settings = settings ?? new RootSettings();
    }

    /**
     * Gets the key of the module kind property.
     */
    public get ModuleKindKey(): keyof TSettings
    {
        return TSProjectSettingKey.ESModule;
    }

    /**
     * Gets the generator this context belongs to.
     */
    public get Generator(): TSProjectGenerator<TSettings, TOptions, TAnswers>
    {
        return this.generator;
    }

    /**
     * @inheritdoc
     */
    public get AllowOutside(): boolean
    {
        return this.Settings.AllowOutside;
    }

    /**
     * @inheritdoc
     */
    public abstract get DestinationMessage(): string;

    /**
     * @inheritdoc
     */
    public abstract get DisplayNameMessage(): string;

    /**
     * @inheritdoc
     */
    public get ModuleNameMessage(): string
    {
        return "What's the name of the npm package?";
    }

    /**
     * Gets a message for asking for the module kind of the package.
     */
    public get ModuleKindMessage(): string
    {
        return "What kind of package do you wish to create?";
    }

    /**
     * @inheritdoc
     */
    public abstract get DescriptionMessage(): string;

    /**
     * @inheritdoc
     */
    public get Destination(): string
    {
        return this.Resolver.GetDestination(this.Metadata);
    }

    /**
     * @inheritdoc
     */
    public get PackagePath(): string
    {
        return this.Resolver.GetPackagePath(this.Metadata);
    }

    /**
     * @inheritdoc
     */
    public get PackageFileName(): Promise<string>
    {
        return this.Resolver.GetPackageFileName(this.Metadata);
    }

    /**
     * @inheritdoc
     */
    public get Package(): Promise<Package | undefined>
    {
        return this.Resolver.GetPackage(this.Metadata);
    }

    /**
     * @inheritdoc
     */
    public get ModuleName(): Promise<string>
    {
        return this.Resolver.GetModuleName(this.Metadata);
    }

    /**
     * @inheritdoc
     */
    public get DisplayName(): string
    {
        return this.Resolver.GetDisplayName(this.Metadata);
    }

    /**
     * @inheritdoc
     */
    public get Description(): Promise<string>
    {
        return this.Resolver.GetDescription(this.Metadata);
    }

    /**
     * @inheritdoc
     */
    public get IsMonoRepo(): boolean
    {
        return (this.Generator.Settings[GeneratorSettingKey.Components] ?? []).includes(TSProjectComponent.MonoRepo);
    }

    /**
     * @inheritdoc
     */
    public get IsWorkspaceRoot(): boolean
    {
        return this.Settings.IsWorkspaceRoot;
    }

    /**
     * @inheritdoc
     */
    public get IsWorkspace(): boolean
    {
        return !this.IsMonoRepo || !this.IsWorkspaceRoot;
    }

    /**
     * Gets a component for resolving answers to prompts provided by the user.
     */
    public get Resolver(): ICreationPromptResolver<TAnswers>
    {
        return this.resolver;
    }

    /**
     * @inheritdoc
     */
    public get Settings(): ICreationContextSettings<TSettings, TAnswers>
    {
        return this.settings;
    }

    /**
     * Gets the metadata stored in the generator settings.
     */
    public get Metadata(): TAnswers
    {
        return this.GetMetadata(this.Generator.Settings);
    }

    /**
     * Gets the questions in the proper order.
     */
    public get Questions(): Array<Question<TSettings>>
    {
        return [
            ...(
                [
                    this.DestinationQuestion,
                    this.DisplayNameQuestion,
                    this.ModuleNameQuestion
                ] as any[]),
            ...(
                this.IsWorkspaceRoot ?
                    [
                        this.ModuleKindQuestion
                    ] :
                    []),
            this.DescriptionQuestion
        ];
    }

    /**
     * Gets the file mappings for the package creation.
     */
    public get FileMappings(): Array<IFileMapping<TSettings, TOptions>>
    {
        return [
            ...this.CommonFileMappings,
            ...(
                this.IsWorkspaceRoot ?
                    this.WorkspaceRootFileMappings :
                    []),
            ...(
                this.IsWorkspace ?
                    this.WorkspaceFileMappings :
                    [])
        ];
    }

    /**
     * Gets the file mappings which are applied to all directories.
     */
    protected get CommonFileMappings(): Array<IFileMapping<TSettings, TOptions>>
    {
        return [];
    }

    /**
     * Gets the file mappings which are applied only to the workspace root.
     */
    protected get WorkspaceRootFileMappings(): Array<IFileMapping<TSettings, TOptions>>
    {
        if (this.IsMonoRepo)
        {
            return [
                new JSONCCreatorMapping(this.Generator, "package-lock.json", {})
            ];
        }
        else
        {
            return [];
        }
    }

    /**
     * Gets the file mappings which are only applied to actual package directories.
     */
    protected get WorkspaceFileMappings(): Array<IFileMapping<TSettings, TOptions>>
    {
        return [];
    }

    /**
     * Gets a question for asking for the destination of the package.
     */
    protected get DestinationQuestion(): Question<TAnswers>
    {
        return new PackageDestinationQuestion(this.Resolver) as Question<TAnswers>;
    }

    /**
     * Gets a question for asking for the human readable name of the package.
     */
    protected get DisplayNameQuestion(): Question<TAnswers>
    {
        return new PackageDisplayNameQuestion(this.Resolver) as Question<TAnswers>;
    }

    /**
     * Gets a question for asking for the name of the module.
     */
    protected get ModuleNameQuestion(): Question<TAnswers>
    {
        return new PackageModuleNameQuestion(this.Resolver) as Question<TAnswers>;
    }

    /**
     * Gets a question for asking for the kind of the package.
     */
    protected get ModuleKindQuestion(): Question<TSettings>
    {
        return {
            type: "list",
            name: this.ModuleKindKey as string,
            default: true,
            message: this.ModuleKindMessage,
            choices: [
                {
                    name: nameof(PackageType.CommonJS),
                    value: false
                },
                {
                    name: nameof(PackageType.ESModule),
                    value: true
                }
            ]
        };
    }

    /**
     * Gets a question for asking for the description of the package.
     */
    protected get DescriptionQuestion(): Question<TAnswers>
    {
        return new PackageDescriptionQuestion(this.Resolver) as Question<TAnswers>;
    }

    /**
     * @inheritdoc
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * The suggested path to the package to create.
     */
    public async GetSuggestedPackagePath(answers: TAnswers): Promise<string>
    {
        return "./";
    }

    /**
     * @inheritdoc
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * A suggested module name for the package.
     */
    public async GetSuggestedModuleName(answers: TAnswers): Promise<string>
    {
        return (await this.Resolver.GetPackage(answers))?.Name ?? this.CreateSuggestedModuleName(answers);
    }

    /**
     * @inheritdoc
     *
     * @param input
     * The input provided by the user.
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * Either a value indicating whether the specified {@linkcode input} is valid or a string containing an error message.
     */
    public async ValidateModuleName(input: string, answers: TAnswers): Promise<string | boolean>
    {
        let result = validate(input);
        let errors = (result.errors ?? []).concat(result.warnings ?? []);

        if (result.validForNewPackages)
        {
            if (this.Settings.BannedModuleNames.includes(input))
            {
                return `The module name \`${input}\` has already been used in this project.`;
            }
            else
            {
                return true;
            }
        }
        else
        {
            return errors[0] ?? "Please provide a name according to the npm naming conventions.";
        }
    }

    /**
     * @inheritdoc
     *
     * @param settings
     * The settings of the generator.
     *
     * @returns
     * The metadata of the package.
     */
    public GetMetadata(settings: TSettings): TAnswers
    {
        return this.Settings.GetMetadata(settings);
    }

    /**
     * Determines a suggested module name for the package.
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * A suggested module name for the package.
     */
    protected async CreateSuggestedModuleName(answers: TAnswers): Promise<string>
    {
        return kebabCase(this.Resolver.GetDisplayName(answers));
    }
}
