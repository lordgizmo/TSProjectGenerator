import { GeneratorOptions, GeneratorSettingKey, IFileMapping } from "@manuth/extended-yo-generator";
import { TSConfigJSON } from "types-tsconfig";
import path from "upath";
import { ICreationContextSettings } from "./ICreationContextSettings.js";
import { PackageCreationContext } from "./PackageCreationContext.js";
import { TSProjectPackageFileMapping } from "../../Project/FileMappings/NPMPackaging/TSProjectPackageFileMapping.js";
import { TSConfigFileMapping } from "../../Serialization/TSConfigFileMapping.js";
import { NPMIgnoreFileMapping } from "../FileMappings/NPMIgnoreFileMapping.js";
import { ITSProjectPackage } from "../Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../Settings/ITSProjectSettings.js";
import { TSProjectComponent } from "../Settings/TSProjectComponent.js";
import { TSProjectPackageKey } from "../Settings/TSProjectPackageKey.js";
import { TSProjectSettingKey } from "../Settings/TSProjectSettingKey.js";
import { TSProjectGenerator } from "../TSProjectGenerator.js";

const { dirname, join, joinSafe, parse, relative } = path;

/**
 * Represents a typescript project reference.
 */
type Reference = Required<TSConfigJSON>["references"] extends Array<infer U> ? U : never;

/**
 * Represents a typescript project plugin.
 */
type Plugin = Required<Required<TSConfigJSON>["compilerOptions"]>["plugins"] extends Array<infer U> ? U : never;

/**
 * Provides a context and settings for creating typescript projects.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 *
 * @template TAnswers
 * The type of the answers of the context.
 */
export class ProjectCreationContext<TSettings extends ITSProjectSettings, TOptions extends GeneratorOptions, TAnswers extends ITSProjectPackage> extends PackageCreationContext<TSettings, TOptions, TAnswers>
{
    /**
     * Initializes a new instance of the {@linkcode PackageCreationContext} class.
     *
     * @param generator
     * The generator this context belongs to.
     *
     * @param settings
     * The settings for the creation context.
     */
    public constructor(generator: TSProjectGenerator<TSettings, TOptions, TAnswers>, settings?: ICreationContextSettings<TSettings, TAnswers>)
    {
        super(generator, settings);
    }

    /**
     * @inheritdoc
     */
    public override get DestinationMessage(): string
    {
        return "Where do you want to save your project to?";
    }

    /**
     * @inheritdoc
     */
    public override get DisplayNameMessage(): string
    {
        return "What's the name of your project?";
    }

    /**
     * @inheritdoc
     */
    public override get DescriptionMessage(): string
    {
        return "Please enter a description for your project.";
    }

    /**
     * Gets the base name of the readme files.
     */
    protected get ReadmeBaseName(): string
    {
        return "README.md";
    }

    /**
     * Gets the path to the template of the readme files.
     */
    protected get ReadmeTemplatePath(): string
    {
        return this.Generator.commonTemplatePath(`${this.ReadmeBaseName}.ejs`);
    }

    /**
     * Gets a file mapping for generating the readme file.
     */
    protected get ReadmeFileMapping(): IFileMapping<TSettings, TOptions>
    {
        return {
            Source: () => this.ReadmeTemplatePath,
            Destination: join(this.Destination, this.ReadmeBaseName),
            Context: () =>
            {
                return this.GetReadmeContext();
            }
        };
    }

    /**
     * Gets the file mapping for creating the `package.json` file.
     */
    protected get NPMPackageFileMapping(): IFileMapping<TSettings, TOptions>
    {
        return new TSProjectPackageFileMapping(this.Generator, this as any);
    }

    /**
     * @inheritdoc
     */
    protected override get CommonFileMappings(): Array<IFileMapping<TSettings, TOptions>>
    {
        let destination = (): string => this.Destination;
        let isMonoRepo = (): boolean => this.IsMonoRepo;
        let isWorkspaceRoot = (): boolean => this.IsWorkspaceRoot;
        let isWorkspace = (): boolean => this.IsWorkspace;
        let workspaces = (): ITSProjectPackage[] => this.Generator.Settings[TSProjectSettingKey.Workspaces];

        return [
            ...super.CommonFileMappings,
            this.ReadmeFileMapping,
            this.NPMPackageFileMapping,
            new class extends TSConfigFileMapping<TSettings, TOptions>
            {
                /**
                 * @inheritdoc
                 */
                public override get Source(): string
                {
                    return this.Generator.modulePath(super.Source);
                }

                /**
                 * @inheritdoc
                 */
                public override get MiddleExtension(): string | undefined
                {
                    return "build";
                }

                /**
                 * @inheritdoc
                 */
                public override get Destination(): string
                {
                    return join(destination(), super.Destination);
                }

                /**
                 * @inheritdoc
                 *
                 * @param tsConfig
                 * Processes the specified {@linkcode tsConfig}.
                 */
                public override async Transform(tsConfig: TSConfigJSON): Promise<TSConfigJSON>
                {
                    if (isMonoRepo() && isWorkspaceRoot())
                    {
                        tsConfig.references = workspaces().map(
                            (workspace) =>
                            {
                                return {
                                    path: joinSafe("./", workspace[TSProjectPackageKey.Destination], this.BaseName)
                                };
                            });
                    }
                    else
                    {
                        tsConfig.references = tsConfig.references?.filter(
                            (reference) =>
                            {
                                return !reference.path.startsWith("../../");
                            });
                    }

                    return tsConfig;
                }
            }(this.Generator),
            new class extends TSConfigFileMapping<TSettings, TOptions>
            {
                /**
                 * @inheritdoc
                 */
                public override get Source(): string
                {
                    return this.Generator.modulePath(super.Source);
                }

                /**
                 * @inheritdoc
                 */
                public override get Destination(): string
                {
                    return join(destination(), super.Destination);
                }

                /**
                 * @inheritdoc
                 *
                 * @param tsConfig
                 * The typescript-configuration to process.
                 *
                 * @returns
                 * The processed data.
                 */
                public override async Transform(tsConfig: TSConfigJSON): Promise<TSConfigJSON>
                {
                    let excludedFiles: string[] = [];
                    let eslintBaseName = TSConfigFileMapping.GetFileName("eslint");
                    let editorBaseName = TSConfigFileMapping.GetFileName("editor");

                    let workspaceRootFiles = [
                        eslintBaseName,
                        editorBaseName
                    ];

                    let createFilter = (excludedFiles: string[]): ((reference: Reference) => boolean) =>
                    {
                        return (reference: Reference): boolean =>
                        {
                            return !excludedFiles.includes(parse(reference.path).base);
                        };
                    };

                    if (isMonoRepo() && isWorkspaceRoot())
                    {
                        let workspaceRootFilter = createFilter(workspaceRootFiles);
                        tsConfig.references = tsConfig.references?.filter((reference) => !workspaceRootFilter(reference));
                    }

                    if (!this.Generator.Settings[GeneratorSettingKey.Components].includes(TSProjectComponent.Linting))
                    {
                        excludedFiles.push(eslintBaseName);
                    }

                    if (isMonoRepo() && isWorkspace())
                    {
                        excludedFiles.push(editorBaseName);
                    }

                    if (excludedFiles.length > 0)
                    {
                        let exclusionFilter = createFilter(excludedFiles);
                        tsConfig.references = tsConfig.references?.filter((reference) => exclusionFilter(reference)) ?? [];
                    }

                    return tsConfig;
                }
            }(this.Generator),

            new class extends TSConfigFileMapping<TSettings, TOptions>
            {
                /**
                 * @inheritdoc
                 */
                public override get Source(): string
                {
                    return this.Generator.modulePath(super.Source);
                }

                /**
                 * @inheritdoc
                 */
                public override get MiddleExtension(): string | undefined
                {
                    return "base";
                }

                /**
                 * @inheritdoc
                 */
                public override get Destination(): string
                {
                    return join(destination(), super.Destination);
                }

                /**
                 * @inheritdoc
                 *
                 * @param tsConfig
                 * The typescript-configuration to process.
                 *
                 * @returns
                 * The processed data.
                 */
                public override async Transform(tsConfig: TSConfigJSON): Promise<TSConfigJSON>
                {
                    if (isWorkspaceRoot())
                    {
                        let plugins: Plugin[] = [];
                        tsConfig = await super.Transform(tsConfig);
                        tsConfig.compilerOptions ??= {};
                        delete tsConfig.compilerOptions.declarationMap;
                        delete tsConfig.compilerOptions.paths;

                        for (let plugin of tsConfig.compilerOptions.plugins ?? [])
                        {
                            if ((plugin as any).transform !== "@typescript-nameof/nameof")
                            {
                                plugins.push(plugin);
                            }
                        }

                        if (plugins.length > 0)
                        {
                            tsConfig.compilerOptions.plugins = plugins;
                        }
                        else
                        {
                            delete tsConfig.compilerOptions.plugins;
                        }

                        return tsConfig;
                    }
                    else
                    {
                        return {
                            extends: join(
                                relative(
                                    this.Generator.destinationPath(dirname(this.Destination)),
                                    this.Generator.destinationPath()),
                                this.BaseName)
                        };
                    }
                }
            }(this.Generator)
        ];
    }

    /**
     * @inheritdoc
     */
    protected override get WorkspaceRootFileMappings(): Array<IFileMapping<TSettings, TOptions>>
    {
        let gitIgnoreFileName = ".gitignore";
        let changeLogFileName = "CHANGELOG.md";

        return [
            ...super.WorkspaceRootFileMappings,
            {
                Source: this.Generator.commonTemplatePath(`${gitIgnoreFileName}.ejs`),
                Destination: gitIgnoreFileName
            },
            ...(
                this.IsMonoRepo ?
                    [
                        {
                            Destination: ".npmignore",
                            Processor: (item, generator) =>
                            {
                                generator.fs.write(item.Destination, "");
                            }
                        }
                    ] as Array<IFileMapping<TSettings, TOptions>> :
                    []),
            {
                Source: this.Generator.commonTemplatePath(`${changeLogFileName}.ejs`),
                Destination: changeLogFileName,
                Context: () => (
                    {
                        Name: this.DisplayName
                    })
            },
            new class extends TSConfigFileMapping<TSettings, TOptions>
            {
                /**
                 * @inheritdoc
                 */
                public override get MiddleExtension(): string
                {
                    return "editor";
                }

                /**
                 * @inheritdoc
                 */
                public override get Source(): string
                {
                    return this.Generator.modulePath(super.Source);
                }
            }(this.Generator),
            {
                Source: this.Generator.modulePath("./scripts/realBumpVersion.ts"),
                Destination: "./scripts/bumpVersion.mts"
            }
        ];
    }

    /**
     * @inheritdoc
     */
    protected override get WorkspaceFileMappings(): Array<IFileMapping<TSettings, TOptions>>
    {
        let mochaConfigFileName = ".mocharc.jsonc";
        let destination = (): string => this.Destination;
        let sourceRoot = (): string => this.Generator.SourceRoot;

        return [
            ...super.WorkspaceFileMappings,
            new NPMIgnoreFileMapping(this.Generator, this as any),
            {
                Source: () => this.Generator.modulePath(mochaConfigFileName),
                Destination: () => join(destination(), mochaConfigFileName)
            },
            new class extends TSConfigFileMapping<TSettings, TOptions>
            {
                /**
                 * @inheritdoc
                 */
                public override get Source(): string
                {
                    return this.Generator.modulePath(super.Source);
                }

                /**
                 * @inheritdoc
                 */
                public override get MiddleExtension(): string | undefined
                {
                    return "app";
                }

                /**
                 * @inheritdoc
                 */
                public override get Destination(): string
                {
                    return join(destination(), super.Destination);
                }
            }(this.Generator),
            new class extends TSConfigFileMapping<TSettings, TOptions>
            {
                /**
                 * @inheritdoc
                 */
                public override get BaseName(): string
                {
                    return join(sourceRoot(), "tests", super.BaseName);
                }

                /**
                 * @inheritdoc
                 */
                public override get Source(): string
                {
                    return this.Generator.modulePath(super.Source);
                }

                /**
                 * @inheritdoc
                 */
                public override get Destination(): string
                {
                    return join(destination(), super.Destination);
                }

                /**
                 * @inheritdoc
                 *
                 * @param tsConfig
                 * The typescript-configuration to process.
                 *
                 * @returns
                 * The processed data.
                 */
                public override async Transform(tsConfig: TSConfigJSON): Promise<TSConfigJSON>
                {
                    tsConfig = await super.Transform(tsConfig);
                    delete tsConfig.exclude;

                    tsConfig.references = tsConfig.references?.filter(
                        (reference) =>
                        {
                            return !reference.path.startsWith("../../../");
                        }) ?? [];

                    return tsConfig;
                }
            }(this.Generator)
        ];
    }

    /**
     * Gets the context of the readme file.
     *
     * @returns
     * The context of the readme file.
     */
    protected async GetReadmeContext(): Promise<any>
    {
        return {
            Name: this.DisplayName,
            Description: await this.Description
        };
    }
}
