import { ICreationContextSettings } from "./ICreationContextSettings.js";
import { ITSProjectPackage } from "../Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../Settings/ITSProjectSettings.js";
import { TSProjectPackageKey } from "../Settings/TSProjectPackageKey.js";

/**
 * Provides settings for a workspace root.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TAnswers
 * The type of the answers of the context.
 */
export class RootSettings<TSettings extends ITSProjectSettings, TAnswers extends ITSProjectPackage> implements ICreationContextSettings<TSettings, TAnswers>
{
    /**
     * Initializes a new instance of the {@linkcode RootSettings} class.
     */
    public constructor()
    { }

    /**
     * @inheritdoc
     */
    public get AllowOutside(): boolean
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public get IsWorkspaceRoot(): boolean
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public get DestinationKey(): keyof TAnswers
    {
        return TSProjectPackageKey.Destination;
    }

    /**
     * @inheritdoc
     */
    public get DisplayNameKey(): keyof TAnswers
    {
        return TSProjectPackageKey.DisplayName;
    }

    /**
     * @inheritdoc
     */
    public get ModuleNameKey(): keyof TAnswers
    {
        return TSProjectPackageKey.Name;
    }

    /**
     * @inheritdoc
     */
    public get DescriptionKey(): keyof TAnswers
    {
        return TSProjectPackageKey.Description;
    }

    /**
     * @inheritdoc
     */
    public get BannedDirectories(): string[]
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public get BannedDisplayNames(): string[]
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public get BannedModuleNames(): string[]
    {
        return [];
    }

    /**
     * @inheritdoc
     *
     * @param answers
     * The answers provided by the user up to this point.
     *
     * @returns
     * The root of the package to create.
     */
    public async GetPackageRoot(answers: TAnswers): Promise<string>
    {
        return "./";
    }

    /**
     * @inheritdoc
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * The destination path of the generator.
     */
    public GetDestination(answers: TAnswers): string
    {
        return answers[this.DestinationKey] as string ?? ".";
    }

    /**
     * @inheritdoc
     *
     * @param settings
     * The settings of the generator.
     *
     * @returns
     * The metadata of the package.
     */
    public GetMetadata(settings: TSettings): TAnswers
    {
        return settings as any;
    }
}
