import path from "upath";
import { ICreationContextSettings } from "./ICreationContextSettings.js";
import { RootSettings } from "./RootSettings.js";
import { ITSProjectPackage } from "../Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../Settings/ITSProjectSettings.js";
import { TSProjectPackageKey } from "../Settings/TSProjectPackageKey.js";
import { TSProjectSettingKey } from "../Settings/TSProjectSettingKey.js";

const { joinSafe, normalizeSafe } = path;

/**
 * Provides settings for a running workspace creation.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TAnswers
 * The type of the answers of the context.
 */
export class RunningWorkspaceSettings<TSettings extends ITSProjectSettings, TAnswers extends ITSProjectPackage> implements ICreationContextSettings<TSettings, TAnswers>
{
    /**
     * The settings of the running workspace creation.
     */
    private settings: ICreationContextSettings<TSettings, TAnswers>;

    /**
     * The settings of the generator.
     */
    private generatorSettings: TSettings;

    /**
     * Initializes a new instance of the {@linkcode RunningWorkspaceSettings} class.
     *
     * @param settings
     * The settings of the running workspace creation.
     *
     * @param generatorSettings
     * The settings of the generator.
     */
    public constructor(settings: ICreationContextSettings<TSettings, TAnswers>, generatorSettings: TSettings)
    {
        this.settings = settings;
        this.generatorSettings = generatorSettings;
    }

    /**
     * @inheritdoc
     */
    public get DestinationKey(): keyof TAnswers
    {
        return this.Settings.DestinationKey;
    }

    /**
     * @inheritdoc
     */
    public get DisplayNameKey(): keyof TAnswers
    {
        return this.Settings.DisplayNameKey;
    }

    /**
     * @inheritdoc
     */
    public get ModuleNameKey(): keyof TAnswers
    {
        return this.Settings.ModuleNameKey;
    }

    /**
     * @inheritdoc
     */
    public get DescriptionKey(): keyof TAnswers
    {
        return this.Settings.DescriptionKey;
    }

    /**
     * @inheritdoc
     */
    public get AllowOutside(): boolean
    {
        return this.Settings.AllowOutside;
    }

    /**
     * @inheritdoc
     */
    public get IsWorkspaceRoot(): boolean
    {
        return this.Settings.IsWorkspaceRoot;
    }

    /**
     * @inheritdoc
     */
    public get BannedDirectories(): string[]
    {
        return (this.GeneratorSettings[TSProjectSettingKey.Workspaces] ?? []).map(
            (workspace) =>
            {
                return workspace[TSProjectPackageKey.Destination];
            });
    }

    /**
     * @inheritdoc
     */
    public get BannedDisplayNames(): string[]
    {
        return [
            ...this.Settings.BannedDisplayNames,
            ...(this.GeneratorSettings[TSProjectSettingKey.Workspaces] ?? []).map(
                (workspace) =>
                {
                    return workspace[TSProjectPackageKey.DisplayName];
                })
        ];
    }

    /**
     * @inheritdoc
     */
    public get BannedModuleNames(): string[]
    {
        return (this.GeneratorSettings[TSProjectSettingKey.Workspaces] ?? []).map(
            (workspace) =>
            {
                return workspace[TSProjectPackageKey.Name];
            });
    }

    /**
     * Gets the context of the running workspace creation.
     */
    protected get Settings(): ICreationContextSettings<TSettings, TAnswers>
    {
        return this.settings;
    }

    /**
     * Gets the settings of the generator.
     */
    protected get GeneratorSettings(): TSettings
    {
        return this.generatorSettings;
    }

    /**
     * @inheritdoc
     *
     * @param answers
     * The answers provided by the user up to this point.
     *
     * @returns
     * The root of the package to create.
     */
    public async GetPackageRoot(answers: TAnswers): Promise<string>
    {
        return this.Settings.GetPackageRoot(answers);
    }

    /**
     * @inheritdoc
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * The destination path of the generator.
     */
    public GetDestination(answers: TAnswers): string
    {
        return normalizeSafe(
            joinSafe(
                "./",
                new RootSettings().GetDestination(this.GeneratorSettings as any),
                this.Settings.GetDestination(answers)));
    }

    /**
     * @inheritdoc
     *
     * @param settings
     * The settings of the generator.
     *
     * @returns
     * The metadata of the package.
     */
    public GetMetadata(settings: TSettings): TAnswers
    {
        return this.Settings.GetMetadata(settings);
    }
}
