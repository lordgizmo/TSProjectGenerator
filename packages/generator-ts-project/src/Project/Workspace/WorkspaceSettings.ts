import { IGenerator } from "@manuth/extended-yo-generator";
import path from "upath";
import { RootSettings } from "./RootSettings.js";
import { VSCodeJSONProcessor } from "../../VSCode/VSCodeJSONProcessor.js";
import { ITSProjectPackage } from "../Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../Settings/ITSProjectSettings.js";
import { TSProjectSettingKey } from "../Settings/TSProjectSettingKey.js";

const { joinSafe } = path;

/**
 * Provides settings for a workspace.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TAnswers
 * The type of the answers of the context.
 */
export class WorkspaceSettings<TSettings extends ITSProjectSettings, TAnswers extends ITSProjectPackage> extends RootSettings<TSettings, TAnswers>
{
    /**
     * The generator of the workspace creation.
     */
    private generator: IGenerator<TSettings, any>;

    /**
     * The index of the workspace.
     */
    private index: number;

    /**
     * Initializes a new instance of the {@linkcode WorkspaceSettings} class.
     *
     * @param generator
     * The generator of the workspace creation.
     *
     * @param index
     * The index of the workspace.
     */
    public constructor(generator: IGenerator<any, any>, index: number)
    {
        super();
        this.generator = generator;
        this.index = index;
    }

    /**
     * @inheritdoc
     */
    public override get AllowOutside(): boolean
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public override get IsWorkspaceRoot(): boolean
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public override get BannedDisplayNames(): string[]
    {
        return [
            ...super.BannedDisplayNames,
            VSCodeJSONProcessor.WorkspaceRootName
        ];
    }

    /**
     * Gets the generator of the workspace creation.
     */
    protected get Generator(): IGenerator<TSettings, any>
    {
        return this.generator;
    }

    /**
     * Gets the index of the workspace.
     */
    protected get Index(): number
    {
        return this.index;
    }

    /**
     * @inheritdoc
     *
     * @param answers
     * The answers provided by the user up to this point.
     *
     * @returns
     * The root of the package to create.
     */
    public override async GetPackageRoot(answers: TAnswers): Promise<string>
    {
        return "./packages/";
    }

    /**
     * @inheritdoc
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * The destination path of the generator.
     */
    public override GetDestination(answers: TAnswers): string
    {
        return joinSafe(
            "./",
            super.GetDestination(super.GetMetadata(this.Generator.Settings)),
            super.GetDestination(answers));
    }

    /**
     * @inheritdoc
     *
     * @param settings
     * The settings of the generator.
     *
     * @returns
     * The metadata of the package.
     */
    public override GetMetadata(settings: TSettings): TAnswers
    {
        return settings[TSProjectSettingKey.Workspaces][this.Index] as TAnswers;
    }
}
