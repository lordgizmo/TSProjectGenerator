/**
 * Provides the functionality to dump text.
 */
export interface IDumper<T>
{
    /**
     * Dumps the specified {@linkcode data}.
     *
     * @param data
     * The data to dump.
     *
     * @returns
     * A {@linkcode String} representing the specified {@linkcode data}.
     */
    Dump(data: T): string;
}
