/**
 * Provides the functionality to parse text.
 */
export interface IParser<T>
{
    /**
     * Parses the specified {@linkcode text}.
     *
     * @param text
     * The data to parse.
     *
     * @returns
     * The parsed representation of the specified {@linkcode text}.
     */
    Parse(text: string): T;
}
