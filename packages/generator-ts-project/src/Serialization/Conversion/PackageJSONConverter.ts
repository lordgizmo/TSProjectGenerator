import { IPackageJSON, Package } from "@manuth/package-json-editor";
import { JSONCConverter } from "./JSONCConverter.js";
import { TextConverter } from "./TextConverter.js";

/**
 * Provides the functionality to parse and dump `package.json`-files.
 */
export class PackageJSONConverter extends TextConverter<Package>
{
    /**
     * The path the resulting code is supposed to be saved to.
     */
    private destinationPath: string | undefined;

    /**
     * A component for parsing and dumping the package-metadata.
     */
    private innerConverter: TextConverter<IPackageJSON>;

    /**
     * Initializes a new instance of the {@linkcode PackageJSONConverter} class.
     *
     * @param destinationPath
     * The path the resulting code is supposed to be saved to.
     */
    public constructor(destinationPath?: string)
    {
        super();
        this.destinationPath = destinationPath;
        this.innerConverter = new JSONCConverter();
    }

    /**
     * Gets or sets the path the resulting code is supposed to be saved to.
     */
    public get DestinationPath(): string | undefined
    {
        return this.destinationPath;
    }

    /**
     * @inheritdoc
     */
    public set DestinationPath(value: string | undefined)
    {
        this.destinationPath = value;
    }

    /**
     * Gets a component for parsing and dumping the package-metadata.
     */
    protected get InnerConverter(): TextConverter<IPackageJSON>
    {
        return this.innerConverter;
    }

    /**
     * @inheritdoc
     *
     * @param text
     * The text to parse.
     *
     * @returns
     * The parsed representation of the specified {@linkcode text}.
     */
    public override Parse(text: string): Package
    {
        let metadata = this.InnerConverter.Parse(text);

        if (this.DestinationPath)
        {
            return new Package(this.DestinationPath, metadata);
        }
        else
        {
            return new Package(metadata);
        }
    }

    /**
     * @inheritdoc
     *
     * @param npmPackage
     * The package to dump.
     *
     * @returns
     * A {@linkcode String} representing the specified {@linkcode npmPackage}.
     */
    public Dump(npmPackage: Package): string
    {
        return this.InnerConverter.Dump(npmPackage.ToJSON());
    }
}
