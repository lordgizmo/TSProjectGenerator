import { IDumper } from "./IDumper.js";
import { Parser } from "./Parser.js";

/**
 * Provides the functionality to parse and dump text.
 *
 * @template T
 * The type of the data to convert.
 */
export abstract class TextConverter<T> extends Parser<T> implements IDumper<T>
{
    /**
     * Initializes a new instance of the {@linkcode TextConverter} class.
     */
    public constructor()
    {
        super();
    }

    /**
     * @inheritdoc
     *
     * @param data
     * The data to dump.
     *
     * @returns
     * A {@linkcode String} representing the specified {@linkcode data}.
     */
    public abstract Dump(data: T): string;
}
