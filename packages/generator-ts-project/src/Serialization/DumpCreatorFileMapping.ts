import { ok } from "node:assert";
import { GeneratorOptions, IGenerator, IGeneratorSettings } from "@manuth/extended-yo-generator";
import { DumpFileMapping } from "./DumpFileMapping.js";

/**
 * Provides the functionality to create dumps from existing data.
 */
export abstract class DumpCreatorFileMapping<TSettings extends IGeneratorSettings, TOptions extends GeneratorOptions, TData> extends DumpFileMapping<TSettings, TOptions, TData>
{
    /**
     * The name of the file to write the dump to.
     */
    private destination: string | undefined;

    /**
     * The data to dump.
     */
    private data: TData | undefined;

    /**
     * Initializes a new instance of the {@linkcode DumpCreatorFileMapping} class.
     *
     * @param generator
     * The generator of the file-mapping.
     *
     * @param fileName
     * The name of the file to write the dump to.
     *
     * @param data
     * The data to dump.
     */
    public constructor(generator: IGenerator<TSettings, TOptions>, fileName: string | undefined, data: TData | undefined)
    {
        super(generator);
        this.destination = fileName;
        this.data = data;
    }

    /**
     * @inheritdoc
     *
     * @returns
     * The object to dump.
     */
    public override async GetSourceObject(): Promise<TData>
    {
        ok(this.data);
        return this.data;
    }

    /**
     * @inheritdoc
     */
    public get Destination(): string
    {
        ok(this.destination);
        return this.destination;
    }
}
