import { GeneratorOptions, IGenerator, IGeneratorSettings } from "@manuth/extended-yo-generator";
import { TempFileSystem } from "@manuth/temp-files";
import { CompilerNodeToWrappedType, createWrappedNode, Expression, ExpressionStatement, printNode, Project, SourceFile, ts } from "ts-morph";
import { IDumper } from "../Serialization/Conversion/IDumper.js";
import { TypeScriptConverter } from "../Serialization/Conversion/TypeScriptConverter.js";
import { DumpCreatorFileMapping } from "../Serialization/DumpCreatorFileMapping.js";

/**
 * Provides the functionality to create typescript-files.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 */
export abstract class TypeScriptCreatorMapping<TSettings extends IGeneratorSettings, TOptions extends GeneratorOptions> extends DumpCreatorFileMapping<TSettings, TOptions, SourceFile>
{
    /**
     * The cached projects of this mapping.
     */
    private projects: Project[] = [];

    /**
     * Initializes a new instance of the {@linkcode TypeScriptCreatorMapping} class.
     *
     * @param generator
     * The generator of this file-mapping.
     *
     * @param sourceFile
     * The sourceFile to write to the file.
     */
    public constructor(generator: IGenerator<TSettings, TOptions>, sourceFile?: SourceFile)
    {
        super(generator, undefined, sourceFile);
    }

    /**
     * @inheritdoc
     */
    public abstract override get Destination(): string;

    /**
     * @inheritdoc
     */
    public get Converter(): TypeScriptConverter
    {
        return new TypeScriptConverter(this.Resolved.Destination);
    }

    /**
     * @inheritdoc
     */
    public get Dumper(): IDumper<SourceFile>
    {
        return new TypeScriptConverter(this.Resolved.Destination);
    }

    /**
     * Gets the cached projects of this mapping.
     */
    protected get Projects(): Project[]
    {
        return this.projects;
    }

    /**
     * @inheritdoc
     *
     * @returns
     * The source-file to dump.
     */
    public override async GetSourceObject(): Promise<SourceFile>
    {
        let result: SourceFile | undefined;

        try
        {
            result = await super.GetSourceObject();
        }
        catch
        {
            result = undefined;
        }

        return result ??
            new Project().createSourceFile(this.Resolved.Destination, undefined, { overwrite: true });
    }

    /**
     * @inheritdoc
     */
    public override async Processor(): Promise<void>
    {
        let data = await this.GetOutputObject();
        let result = this.WriteOutput(this.Dump(data));
        data.forget();
        return result;
    }

    /**
     * Processes the specified {@linkcode sourceFile}.
     *
     * @param sourceFile
     * The source-file to process.
     *
     * @returns
     * The processed data.
     */
    protected override async Transform(sourceFile: SourceFile): Promise<SourceFile>
    {
        return super.Transform(sourceFile);
    }

    /**
     * Gets the object to write to the output file.
     *
     * @returns
     * The object to write to the output file.
     */
    protected override async GetOutputObject(): Promise<SourceFile>
    {
        let result = super.GetOutputObject();
        this.Dispose();
        return result;
    }

    /**
     * Wraps the specified {@linkcode node} in a file.
     *
     * @template TNode
     * The type of the node to wrap.
     *
     * @param node
     * The node to wrap into a file.
     *
     * @returns
     * The wrapped node.
     */
    protected WrapNode<TNode extends ts.Node>(node: TNode): CompilerNodeToWrappedType<TNode>
    {
        if (!node.getSourceFile())
        {
            let project = new Project();
            let file = project.createSourceFile(TempFileSystem.TempName(), undefined, { overwrite: true });
            this.projects.push(project);
            file.addStatements(printNode(node));
            let result = file.getFirstDescendantByKind(node.kind) as CompilerNodeToWrappedType<TNode>;
            result.formatText(this.Converter.FormatSettings);
            return result;
        }
        else
        {
            return createWrappedNode(node);
        }
    }

    /**
     * Wraps the specified {@linkcode expression} into an {@linkcode ExpressionStatement}.
     *
     * @template TExpression
     * The type of the expression to wrap.
     *
     * @param expression
     * The expression to wrap into an {@linkcode ExpressionStatement}.
     *
     * @returns
     * The wrapped {@linkcode expression}.
     */
    protected WrapExpression<TExpression extends Expression>(expression: TExpression): ExpressionStatement
    {
        let result = this.WrapNode(ts.factory.createExpressionStatement(ts.factory.createStringLiteral("")));
        result.setExpression(expression.getFullText());
        return result;
    }

    /**
     * Releases all resources used by this file-mapping.
     */
    protected Dispose(): void
    {
        for (let project of this.Projects)
        {
            for (let file of project.getSourceFiles())
            {
                file.forget();
            }
        }
    }
}
