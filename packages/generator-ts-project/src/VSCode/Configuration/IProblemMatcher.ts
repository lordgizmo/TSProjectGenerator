/**
 * Represents a problem matcher.
 */
export interface IProblemMatcher
{
    /**
     * The base of the problem matcher.
     */
    base?: string;

    /**
     * The descriptor of the file location.
     */
    fileLocation?: string | [string, string];
}
