/**
 * Represents options of a task.
 */
export interface ITaskOptions
{
    /**
     * The working directory of the task.
     */
    cwd?: string;
}
