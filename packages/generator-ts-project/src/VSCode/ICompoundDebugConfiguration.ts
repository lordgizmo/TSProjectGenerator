/**
 * Represents a compound debug configuration.
 */
export interface ICompoundDebugConfiguration
{
    /**
     * The name of the compound configuration.
     */
    name: string;

    /**
     * The name of the configurations that are combined by this configuration.
     */
    configurations: string[];
}
