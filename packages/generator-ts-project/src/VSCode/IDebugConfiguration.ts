import { DebugConfiguration } from "vscode";

/**
 * Represents a debug configuration.
 */
export interface IDebugConfiguration extends DebugConfiguration
{
    /**
     * The program to run.
     */
    program?: string;

    /**
     * The directory to run the program in.
     */
    cwd?: string;

    /**
     * The arguments to pass to the program.
     */
    args?: string[];
}
