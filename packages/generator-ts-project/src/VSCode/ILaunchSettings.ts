import { ICompoundDebugConfiguration } from "./ICompoundDebugConfiguration.js";
import { IDebugConfiguration } from "./IDebugConfiguration.js";

/**
 * Represents the launch-settings.
 */
export interface ILaunchSettings
{
    /**
     * The version of the meta-data format.
     */
    version?: string;

    /**
     * Gets or sets the launch-configurations.
     */
    configurations?: IDebugConfiguration[];

    /**
     * Gets or sets the compound debug configurations.
     */
    compounds?: ICompoundDebugConfiguration[];
}
