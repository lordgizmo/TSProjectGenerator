import { TaskDefinition } from "vscode";
import { ITaskOptions } from "./Configuration/ITaskOptions.js";
import { ProblemMatcher } from "./ProblemMatcher.js";

/**
 * Represents a vscode task.
 */
export interface ITaskDefinition extends TaskDefinition
{
    /**
     * The name of the task.
     */
    label?: string;

    /**
     * The command to run.
     */
    command?: string;

    /**
     * The npm script to run.
     */
    script?: string;

    /**
     * The arguments to pass to the command.
     */
    args?: string[];

    /**
     * The options of the task.
     */
    options?: ITaskOptions;

    /**
     * The problem matcher of the task.
     */
    problemMatcher?: ProblemMatcher | ProblemMatcher[];
}
