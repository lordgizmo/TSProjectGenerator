import { GeneratorOptions, IGeneratorSettings } from "@manuth/extended-yo-generator";
import { DebugConfiguration } from "vscode";
import { CodeWorkspaceComponent } from "./Components/CodeWorkspaceComponent.js";
import { IDebugConfiguration } from "./IDebugConfiguration.js";
import { ILaunchSettings } from "./ILaunchSettings.js";
import { VSCodeJSONProcessor } from "./VSCodeJSONProcessor.js";

/**
 * Provides the functionality to process vscode debug configurations.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 */
export class LaunchSettingsProcessor<TSettings extends IGeneratorSettings, TOptions extends GeneratorOptions> extends VSCodeJSONProcessor<TSettings, TOptions, ILaunchSettings>
{
    /**
     * Initializes a new instance of the {@linkcode LaunchSettingsProcessor} class.
     *
     * @param component
     * The component of the processor.
     */
    public constructor(component: CodeWorkspaceComponent<TSettings, TOptions>)
    {
        super(component);
    }

    /**
     * @inheritdoc
     *
     * @param launchSettings
     * The data to process.
     *
     * @returns
     * The processed data.
     */
    public override async Process(launchSettings: ILaunchSettings): Promise<ILaunchSettings>
    {
        let result = await super.Process(launchSettings);

        if (result?.configurations)
        {
            for (let i = result.configurations.length - 1; i >= 0; i--)
            {
                if (await this.FilterDebugConfig(result.configurations[i]))
                {
                    result.configurations[i] = await this.ProcessDebugConfig(result.configurations[i]);
                }
                else
                {
                    result.configurations.splice(i, 1);
                }
            }
        }

        return result;
    }

    /**
     * Determines whether a debug-configuration should be included.
     *
     * @param debugConfig
     * The debug-configuration to filter.
     *
     * @returns
     * A value indicating whether the debug-configuration should be included.
     */
    protected async FilterDebugConfig(debugConfig: DebugConfiguration): Promise<boolean>
    {
        return true;
    }

    /**
     * Processes a debug-configuration.
     *
     * @param debugConfig
     * The debug-configuration to process.
     *
     * @returns
     * The processed debug-configuration.
     */
    protected async ProcessDebugConfig(debugConfig: DebugConfiguration): Promise<DebugConfiguration>
    {
        return debugConfig;
    }

    /**
     * Replaces all workspace directives in the specified {@linkcode config}.
     *
     * @param config
     * The configuration to replace the workspace directives in.
     *
     * @param newName
     * The new name of the workspace.
     */
    protected ReplaceWorkspaceDirectives(config: IDebugConfiguration, newName?: string): void
    {
        let destinationSetting = "outFiles";
        let substitute = (text: string): string => this.ReplaceWorkspaceFolder(text, newName);

        if (config.program)
        {
            config.program = substitute(config.program);
        }

        if (config.args)
        {
            config.args = config.args.map((arg) => substitute(arg));
        }

        if (Array.isArray(config[destinationSetting]))
        {
            let outFiles: string[] = config[destinationSetting];

            config[destinationSetting] = [
                ...new Set(outFiles.map((pattern) => substitute(pattern)))
            ];
        }

        if (config.cwd)
        {
            config.cwd = substitute(config.cwd);

            if (config.cwd === this.GetWorkspaceFolderDirective())
            {
                delete config.cwd;
            }
        }

        if (newName && !config.cwd)
        {
            config.cwd = this.GetWorkspaceFolderDirective(newName);
        }
    }
}
