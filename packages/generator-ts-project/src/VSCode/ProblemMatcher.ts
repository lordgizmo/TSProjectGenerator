import { IProblemMatcher } from "./Configuration/IProblemMatcher.js";

/**
 * Represents a problem matcher.
 */
export type ProblemMatcher = string | IProblemMatcher;
