import { GeneratorOptions, IGeneratorSettings } from "@manuth/extended-yo-generator";
import { CodeWorkspaceComponent } from "./Components/CodeWorkspaceComponent.js";
import { ITaskDefinition } from "./ITaskDefinition.js";
import { ITaskSettings } from "./ITaskSettings.js";
import { ProblemMatcher } from "./ProblemMatcher.js";
import { VSCodeJSONProcessor } from "./VSCodeJSONProcessor.js";

/**
 * Provides the functionality to process vscode-tasks.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 */
export class TasksProcessor<TSettings extends IGeneratorSettings, TOptions extends GeneratorOptions> extends VSCodeJSONProcessor<TSettings, TOptions, ITaskSettings>
{
    /**
     * Initializes a new instance of the {@linkcode TasksProcessor} class.
     *
     * @param component
     * The component of the processor.
     */
    public constructor(component: CodeWorkspaceComponent<TSettings, TOptions>)
    {
        super(component);
    }

    /**
     * @inheritdoc
     *
     * @param data
     * The data to process.
     *
     * @returns
     * The processed data.
     */
    public override async Process(data: ITaskSettings): Promise<ITaskSettings>
    {
        let result = await super.Process(data);

        if (result?.tasks)
        {
            for (let i = result.tasks.length - 1; i >= 0; i--)
            {
                if (await this.FilterTask(result.tasks[i]))
                {
                    result.tasks[i] = await this.ProcessTask(result.tasks[i]);
                }
                else
                {
                    result.tasks.splice(i, 1);
                }
            }
        }

        return result;
    }

    /**
     * Determines whether a task should be included.
     *
     * @param task
     * The task to filter.
     *
     * @returns
     * A value indicating whether the task should be included.
     */
    protected async FilterTask(task: ITaskDefinition): Promise<boolean>
    {
        return true;
    }

    /**
     * Processes a task-configuration.
     *
     * @param task
     * The task to process.
     *
     * @returns
     * The processed task.
     */
    protected async ProcessTask(task: ITaskDefinition): Promise<ITaskDefinition>
    {
        return task;
    }

    /**
     * Replaces all workspace directives in the specified {@linkcode task}.
     *
     * @param task
     * The task to replace the workspace directives in.
     *
     * @param newName
     * The name of the new workspace.
     */
    protected ReplaceWorkspaceDirectives(task: ITaskDefinition, newName?: string): void
    {
        let substitute = (item: string): string => this.ReplaceWorkspaceFolder(item, newName);

        if (task.command)
        {
            task.command = substitute(task.command);
        }

        if (task.script)
        {
            task.script = substitute(task.script);
        }

        if (task.args)
        {
            task.args = task.args.map((arg) => substitute(arg));
        }

        if (task.options?.cwd)
        {
            task.options.cwd = substitute(task.options.cwd);
        }

        if (task.problemMatcher)
        {
            let problemMatchers: ProblemMatcher[];

            if (Array.isArray(task.problemMatcher))
            {
                problemMatchers = task.problemMatcher;
            }
            else
            {
                problemMatchers = [task.problemMatcher];
            }

            problemMatchers = problemMatchers.map(
                (problemMatcher =>
                {
                    if (typeof problemMatcher === "object")
                    {
                        if (Array.isArray(problemMatcher.fileLocation))
                        {
                            problemMatcher.fileLocation[1] = substitute(problemMatcher.fileLocation[1]);

                            if (problemMatcher.fileLocation[0] === "relative" &&
                                problemMatcher.fileLocation[1] === this.GetWorkspaceFolderDirective())
                            {
                                delete problemMatcher.fileLocation;
                            }

                            if (Object.keys(problemMatcher).length === 1 && problemMatcher.base)
                            {
                                return problemMatcher.base;
                            }
                        }
                    }

                    return problemMatcher;
                }));

            if (problemMatchers.length === 1)
            {
                task.problemMatcher = problemMatchers[0];
            }
            else
            {
                task.problemMatcher = problemMatchers;
            }
        }
    }
}
