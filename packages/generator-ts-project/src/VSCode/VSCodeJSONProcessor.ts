import { GeneratorOptions, IGenerator, IGeneratorSettings } from "@manuth/extended-yo-generator";
import { CodeWorkspaceComponent } from "./Components/CodeWorkspaceComponent.js";
import { JSONProcessor } from "../Serialization/JSONProcessor.js";

/**
 * Provides the functionality to process json objects related to vscode.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 */
export class VSCodeJSONProcessor<TSettings extends IGeneratorSettings, TOptions extends GeneratorOptions, TData> extends JSONProcessor<TSettings, TOptions, TData>
{
    /**
     * The component of this processor.
     */
    private component: CodeWorkspaceComponent<TSettings, TOptions>;

    /**
     * Initializes a new instance of the {@linkcode VSCodeJSONProcessor} class.
     *
     * @param component
     * The component of the processor.
     */
    public constructor(component: CodeWorkspaceComponent<TSettings, TOptions>)
    {
        super(component.Generator);
        this.component = component;
    }

    /**
     * Gets the generator of this component.
     */
    public override get Generator(): IGenerator<TSettings, TOptions>
    {
        return this.Component.Generator;
    }

    /**
     * Gets the component of this processor.
     */
    public get Component(): CodeWorkspaceComponent<TSettings, TOptions>
    {
        return this.component;
    }

    /**
     * Gets the name of the workspace root.
     */
    public static get WorkspaceRootName(): string
    {
        return "Solution Items";
    }

    /**
     * Gets the name of the workspace root.
     */
    protected get WorkspaceRootName(): string
    {
        return VSCodeJSONProcessor.WorkspaceRootName;
    }

    /**
     * Creates a workspace-folder directive.
     *
     * @param name
     * The name of the workspace-folder.
     *
     * @returns
     * A normal workspace-folder directive or a named workspace-folder directive if a {@linkcode name} is passed.
     */
    public GetWorkspaceFolderDirective(name?: string): string
    {
        return `\${workspaceFolder${name ? `:${name}` : ""}}`;
    }

    /**
     * Replaces named workspace-folders (such as `${workspaceFolder:Example}`) in the specified {@linkcode value}.
     *
     * @param value
     * The value containing the workspace-folder references to strip.
     *
     * @param newName
     * Either the name of the new workspace folder reference or `undefined` to remove named workspace references.
     *
     * @returns
     * A value containing `${workspaceFolder}` directives in place of named workspace-folders.
     */
    public ReplaceWorkspaceFolder(value: string, newName?: string): string
    {
        let replacement;

        if (newName)
        {
            replacement = `$1:${newName}$3`;
        }
        else
        {
            replacement = "$1$3";
        }

        return value.replace(/(?<=\$)(\{workspaceFolder)(:[^}]+)?(\})/g, replacement);
    }
}
