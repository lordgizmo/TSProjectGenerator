import { IGeneratorSettings } from "@manuth/extended-yo-generator";
import { ProjectSelectorSettingKey } from "./ProjectSelectorSettingKey.js";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import type { ProjectTypeSelector } from "../ProjectTypeSelector.js";

/**
 * Provides settings for the {@linkcode ProjectTypeSelector}.
 *
 * @template T
 * The type of the project-names.
 */
export interface IProjectSelectorSettings<T extends string | number> extends IGeneratorSettings
{
    /**
     * Gets the project-type to create.
     */
    [ProjectSelectorSettingKey.ProjectType]: T;
}
