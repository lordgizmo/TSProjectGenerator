import { join } from "node:path";
import { ComponentCategoryBase, GeneratorOptions, GeneratorSettingKey, IComponent, IFileMapping } from "@manuth/extended-yo-generator";
import { DistinctQuestion } from "inquirer";
import { GeneratorName } from "../../../Core/GeneratorName.js";
import { SubGeneratorPrompt } from "../../../Inquiry/Prompts/SubGeneratorPrompt.js";
import { TSProjectComponent } from "../../../Project/Settings/TSProjectComponent.js";
import { TSProjectSettingKey } from "../../../Project/Settings/TSProjectSettingKey.js";
import { TSProjectGenerator } from "../../../Project/TSProjectGenerator.js";
import { ICreationContext } from "../../../Project/Workspace/ICreationContext.js";
import { GeneratorClassFileMapping } from "../FileMappings/TypeScript/GeneratorClassFileMapping.js";
import { GeneratorIndexFileMapping } from "../FileMappings/TypeScript/GeneratorIndexFileMapping.js";
import { GeneratorTestFileMapping } from "../FileMappings/TypeScript/GeneratorTestFileMapping.js";
import { LicenseTypeFileMapping } from "../FileMappings/TypeScript/LicenseTypeFileMapping.js";
import { NamingContext } from "../FileMappings/TypeScript/NamingContext.js";
import { SettingKeyFileMapping } from "../FileMappings/TypeScript/SettingKeyFileMapping.js";
import { SettingsInterfaceFileMapping } from "../FileMappings/TypeScript/SettingsInterfaceFileMapping.js";
import { ITSGeneratorPackage } from "../Settings/ITSGeneratorPackage.js";
import { ITSGeneratorSettings } from "../Settings/ITSGeneratorSettings.js";
import { SubGeneratorSettingKey } from "../Settings/SubGeneratorSettingKey.js";
import { TSGeneratorComponent } from "../Settings/TSGeneratorComponent.js";
import { TSGeneratorPackageKey } from "../Settings/TSGeneratorPackageKey.js";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { TSGeneratorGenerator } from "../TSGeneratorGenerator.js";

/**
 * Provides general components for {@linkcode TSGeneratorGenerator}s.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 *
 * @template TPackage
 * The type of the packages of the generator.
 */
export class TSGeneratorCategory<TSettings extends ITSGeneratorSettings, TOptions extends GeneratorOptions, TPackage extends ITSGeneratorPackage> extends ComponentCategoryBase<TSettings, TOptions>
{
    /**
     * The generator of the category.
     */
    private projectGenerator: TSProjectGenerator<TSettings, TOptions, TPackage>;

    /**
     * Initializes a new instance of the {@linkcode TSGeneratorCategory} class.
     *
     * @param generator
     * The generator of the category.
     */
    public constructor(generator: TSProjectGenerator<TSettings, TOptions, TPackage>)
    {
        super(generator);
        this.projectGenerator = generator;
    }

    /**
     * @inheritdoc
     */
    public override get Generator(): TSProjectGenerator<TSettings, TOptions, TPackage>
    {
        return this.projectGenerator;
    }

    /**
     * @inheritdoc
     */
    public get DisplayName(): string
    {
        return "Generators";
    }

    /**
     * @inheritdoc
     */
    public get Components(): Array<IComponent<TSettings, TOptions>>
    {
        return [
            this.GeneratorComponent,
            this.SubGeneratorComponent
        ];
    }

    /**
     * Gets a component for creating an example generator.
     */
    public get GeneratorComponent(): IComponent<TSettings, TOptions>
    {
        return {
            ID: TSGeneratorComponent.GeneratorExample,
            DisplayName: "Example Generator (recommended)",
            DefaultEnabled: true,
            FileMappings: (component, generator) =>
            {
                let contexts: Array<ICreationContext<TSettings, TPackage>>;

                if (this.Generator.PackageCreationContext.IsMonoRepo)
                {
                    contexts = this.Generator.WorkspaceCreationContexts;
                }
                else
                {
                    contexts = [this.Generator.PackageCreationContext];
                }

                return contexts.flatMap(
                    (context) =>
                    {
                        return this.GetGeneratorFileMappings(context, GeneratorName.Main, context.DisplayName);
                    });
            }
        };
    }

    /**
     * Gets a component for creating sub-generators.
     */
    public get SubGeneratorComponent(): IComponent<TSettings, TOptions>
    {
        return {
            ID: TSGeneratorComponent.SubGeneratorExample,
            DisplayName: "Example Sub-Generator",
            FileMappings: () =>
            {
                let contexts: Array<ICreationContext<TSettings, TPackage>>;

                if (this.Generator.PackageCreationContext.IsMonoRepo)
                {
                    contexts = this.Generator.WorkspaceCreationContexts;
                }
                else
                {
                    contexts = [this.Generator.PackageCreationContext];
                }

                return contexts.flatMap(
                    (context) =>
                    {
                        return (context.Metadata[TSGeneratorPackageKey.SubGenerators] ?? []).flatMap(
                            (subGeneratorOptions) =>
                            {
                                return this.GetGeneratorFileMappings(
                                    context,
                                    subGeneratorOptions[SubGeneratorSettingKey.Name],
                                    subGeneratorOptions[SubGeneratorSettingKey.DisplayName]);
                            });
                    });
            },
            Questions: [
                {
                    ...this.SubGeneratorQuestion,
                    when: (answers) =>
                    {
                        return !answers[GeneratorSettingKey.Components]?.includes(TSProjectComponent.MonoRepo);
                    }
                } as DistinctQuestion<TSettings> as any
            ]
        };
    }

    /**
     * Gets the question for asking for sub generator details.
     */
    public get SubGeneratorQuestion(): DistinctQuestion<TSettings>
    {
        return {
            type: SubGeneratorPrompt.TypeName,
            name: TSGeneratorPackageKey.SubGenerators,
            message: "Please specify the details of the sub-generators to create",
            defaultRepeat: false
        };
    }

    /**
     * Creates file-mappings for a generator.
     *
     * @param context
     * The context of the generator creation.
     *
     * @param id
     * The id of the generator.
     *
     * @param displayName
     * The human readable name of the generator.
     *
     * @returns
     * File-mappings for a generator.
     */
    protected GetGeneratorFileMappings(context: ICreationContext<TSettings, TPackage>, id: string, displayName: string): Array<IFileMapping<TSettings, TOptions>>
    {
        let readmeFileName = "README.md.ejs";

        let namingContext = new NamingContext(
            id,
            displayName,
            join(context.Destination, this.Generator.SourceRoot),
            this.Generator.Settings[TSProjectSettingKey.ESModule]);

        return [
            new LicenseTypeFileMapping<TSettings, TOptions>(this.Generator, namingContext),
            new SettingKeyFileMapping<TSettings, TOptions>(this.Generator, namingContext),
            new SettingsInterfaceFileMapping<TSettings, TOptions>(this.Generator, namingContext),
            new GeneratorClassFileMapping<TSettings, TOptions>(this.Generator, namingContext),
            new GeneratorIndexFileMapping<TSettings, TOptions>(this.Generator, namingContext),
            new GeneratorTestFileMapping<TSettings, TOptions>(this.Generator, namingContext),
            {
                Source: join("generator", "templates"),
                Destination: () => join(context.Destination, "templates", id)
            },
            {
                Source: () => this.Generator.commonTemplatePath(readmeFileName),
                Destination: () => join(context.Destination, "templates", id, readmeFileName)
            }
        ];
    }
}
