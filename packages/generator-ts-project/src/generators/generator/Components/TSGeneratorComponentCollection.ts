import { GeneratorOptions, IComponentCategory } from "@manuth/extended-yo-generator";
import { TSGeneratorCategory } from "./TSGeneratorCategory.js";
import { TSGeneratorGeneralCategory } from "./TSGeneratorGeneralCategory.js";
import { TSProjectComponentCollection } from "../../../Project/Components/TSProjectComponentCollection.js";
import { TSProjectGenerator } from "../../../Project/TSProjectGenerator.js";
import { ITSGeneratorPackage } from "../Settings/ITSGeneratorPackage.js";
import { ITSGeneratorSettings } from "../Settings/ITSGeneratorSettings.js";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import type { TSGeneratorGenerator } from "../TSGeneratorGenerator.js";

/**
 * Provides components for {@linkcode TSGeneratorGenerator}s.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 *
 * @template TPackage
 * The type of the packages of the generator.
 */
export class TSGeneratorComponentCollection<TSettings extends ITSGeneratorSettings, TOptions extends GeneratorOptions, TPackage extends ITSGeneratorPackage> extends TSProjectComponentCollection<TSettings, TOptions, TPackage>
{
    /**
     * Initializes a new instance of the {@linkcode TSGeneratorComponentCollection} class.
     *
     * @param generator
     * The generator of the collection.
     */
    public constructor(generator: TSProjectGenerator<TSettings, TOptions, TPackage>)
    {
        super(generator);
    }

    /**
     * @inheritdoc
     */
    public override get Categories(): Array<IComponentCategory<TSettings, TOptions>>
    {
        return [
            new TSGeneratorGeneralCategory(this.Generator),
            new TSGeneratorCategory(this.Generator)
        ];
    }
}
