import { GeneratorOptions, IComponent } from "@manuth/extended-yo-generator";
import { TSGeneratorCodeWorkspaceFolder } from "./TSGeneratorCodeWorkspaceFolder.js";
import { TSProjectGeneralCategory } from "../../../Project/Components/TSProjectGeneralCategory.js";
import { TSProjectGenerator } from "../../../Project/TSProjectGenerator.js";
import { ITSGeneratorPackage } from "../Settings/ITSGeneratorPackage.js";
import { ITSGeneratorSettings } from "../Settings/ITSGeneratorSettings.js";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import type { TSGeneratorGenerator } from "../TSGeneratorGenerator.js";

/**
 * Provides general components for {@linkcode TSGeneratorGenerator}s.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 *
 * @template TPackage
 * The type of the packages of the generator.
 */
export class TSGeneratorGeneralCategory<TSettings extends ITSGeneratorSettings, TOptions extends GeneratorOptions, TPackage extends ITSGeneratorPackage> extends TSProjectGeneralCategory<TSettings, TOptions, TPackage>
{
    /**
     * Initializes a new instance of the {@linkcode TSGeneratorGeneralCategory} class.
     *
     * @param generator
     * The generator of the category.
     */
    public constructor(generator: TSProjectGenerator<TSettings, TOptions, TPackage>)
    {
        super(generator);
    }

    /**
     * @inheritdoc
     */
    protected override get WorkspaceComponent(): IComponent<TSettings, TOptions>
    {
        return new TSGeneratorCodeWorkspaceFolder(this.Generator);
    }
}
