import { IDependencyCollectionOptions } from "@manuth/package-json-editor";
import { ESModuleDependencyCollection } from "../../../NPM/Dependencies/ESModuleDependencyCollection.js";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import type { TSGeneratorGenerator } from "../TSGeneratorGenerator.js";

/**
 * Provides all common dependencies for {@linkcode TSGeneratorGenerator}s.
 */
export class TSGeneratorDependencies extends ESModuleDependencyCollection
{
    /**
     * The name of the generator package.
     */
    private static readonly generatorBasePackageName = "@manuth/extended-yo-generator";

    /**
     * Initializes a new instance of the {@linkcode TSGeneratorDependencies} class.
     *
     * @param esModule
     * A value indicating whether the ESModule dependencies are allowed.
     */
    public constructor(esModule: boolean)
    {
        super(
            {
                dependencies: [
                    TSGeneratorDependencies.generatorBasePackageName
                ],
                devDependencies: [
                    "yo"
                ]
            },
            esModule);
    }

    /**
     * @inheritdoc
     */
    protected override get CommonJSOverrides(): IDependencyCollectionOptions
    {
        return {
            dependencies: {
                [TSGeneratorDependencies.generatorBasePackageName]: "^11.0.8"
            }
        };
    }
}
