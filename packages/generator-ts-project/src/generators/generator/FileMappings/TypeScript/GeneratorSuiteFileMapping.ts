import { Generator, GeneratorOptions } from "@manuth/extended-yo-generator";
import { ArrowFunction, ImportDeclarationStructure, OptionalKind, printNode, SourceFile, StatementStructures, ts, WriterFunction } from "ts-morph";
import path from "upath";
import { GeneratorSuiteFileMappingBase } from "./GeneratorSuiteFileMappingBase.js";
import { GeneratorTestFileMapping } from "./GeneratorTestFileMapping.js";
import { NamingContext } from "./NamingContext.js";
import { ISuiteContext } from "../../../../Project/FileMappings/TypeScript/ISuiteContext.js";
import { ITSProjectSettings } from "../../../../Project/Settings/ITSProjectSettings.js";

const { isAbsolute, relative } = path;

/**
 * Provides the functionality to create a file which contains test-suites for generators.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 */
export class GeneratorSuiteFileMapping<TSettings extends ITSProjectSettings, TOptions extends GeneratorOptions> extends GeneratorSuiteFileMappingBase<TSettings, TOptions>
{
    /**
     * Initializes a new instance of the {@linkcode GeneratorSuiteFileMapping} class.
     *
     * @param generator
     * The generator of this file-mapping.
     *
     * @param namingContext
     * A component which provides constants for the file-mapping.
     */
    public constructor(generator: Generator<TSettings, TOptions>, namingContext: NamingContext)
    {
        super(generator, namingContext);
    }

    /**
     * @inheritdoc
     */
    public override get Generator(): Generator<TSettings, TOptions>
    {
        return super.Generator as Generator<TSettings, TOptions>;
    }

    /**
     * @inheritdoc
     */
    public override get Destination(): string
    {
        return this.NamingContext.GeneratorSuiteFileName;
    }

    /**
     * Gets the file mappings of the generator test files.
     */
    protected get GeneratorTestFiles(): Array<GeneratorTestFileMapping<TSettings, TOptions>>
    {
        return this.Generator.FileMappingCollection.Items.map(
            (fileMapping) =>
            {
                return fileMapping.ResolverContext;
            }).filter(
                (fileMapping): fileMapping is GeneratorTestFileMapping<TSettings, TOptions> =>
                {
                    return (fileMapping instanceof GeneratorTestFileMapping);
                }).filter(
                    (fileMapping) =>
                    {
                        let path = relative(this.NamingContext.SourceRoot, fileMapping.Destination);
                        return !isAbsolute(path) && !path.startsWith("..");
                    });
    }

    /**
     * @inheritdoc
     *
     * @returns
     * The context.
     */
    public override async Context(): Promise<Required<ISuiteContext>>
    {
        return {
            SuiteName: "Generators",
            SuiteFunction: {
                Name: this.NamingContext.GeneratorSuiteFunctionName,
                Description: "Registers tests for the generators."
            }
        };
    }

    /**
     * @inheritdoc
     *
     * @returns
     * The function for registering the suite.
     */
    protected override async GetSuiteFunction(): Promise<ArrowFunction>
    {
        let result = await super.GetSuiteFunction();
        let statements: Array<string | WriterFunction | StatementStructures> = [];

        for (let fileMapping of this.GeneratorTestFiles)
        {
            let suiteFunctionInfo = await fileMapping.GetSuiteFunctionInfo();

            statements.push(
                printNode(
                    ts.factory.createCallExpression(
                        ts.factory.createIdentifier(suiteFunctionInfo.Name),
                        [],
                        [])));
        }

        result.addStatements(statements);
        return result;
    }

    /**
     * @inheritdoc
     *
     * @param sourceFile
     * The source-file to process.
     *
     * @returns
     * The processed data.
     */
    protected override async Transform(sourceFile: SourceFile): Promise<SourceFile>
    {
        let result = await super.Transform(sourceFile);
        let importDeclarations: Array<OptionalKind<ImportDeclarationStructure>> = [];

        for (let fileMapping of this.GeneratorTestFiles)
        {
            let suiteFunctionInfo = await fileMapping.GetSuiteFunctionInfo();

            importDeclarations.push(
                {
                    ...await this.GetImportDeclaration(fileMapping.Destination),
                    namedImports: [
                        suiteFunctionInfo.Name
                    ]
                });
        }

        result.addImportDeclarations(importDeclarations);
        return result;
    }
}
