import { ISubGenerator } from "./ISubGenerator.js";
import { TSGeneratorPackageKey } from "./TSGeneratorPackageKey.js";
import { ITSProjectPackage } from "../../../Project/Settings/ITSProjectPackage.js";

/**
 * Provides settings for creating a generator package.
 */
export interface ITSGeneratorPackage extends ITSProjectPackage
{
    /**
     * Gets or sets the sub-generator-settings.
     */
    [TSGeneratorPackageKey.SubGenerators]: ISubGenerator[];
}
