import { ITSGeneratorPackage } from "./ITSGeneratorPackage.js";
import { ITSProjectSettings } from "../../../Project/Settings/ITSProjectSettings.js";
import { TSProjectSettingKey } from "../../../Project/Settings/TSProjectSettingKey.js";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import type { TSGeneratorGenerator } from "../TSGeneratorGenerator.js";

/**
 * Provides settings for the {@linkcode TSGeneratorGenerator}.
 */
export interface ITSGeneratorSettings extends ITSProjectSettings, ITSGeneratorPackage
{
    /**
     * @inheritdoc
     */
    [TSProjectSettingKey.Workspaces]: ITSGeneratorPackage[];
}
