/**
 * Represents a sub-generator setting.
 */
export enum SubGeneratorSettingKey
{
    /**
     * Indicates the {@linkcode Name}-setting.
     */
    Name = "name",

    /**
     * Indicates the {@linkcode DisplayName}-setting.
     */
    DisplayName = "displayName"
}
