/**
 * Specifies the key of a generator package setting.
 */
export enum TSGeneratorPackageKey
{
    /**
     * Indicates the {@linkcode SubGenerators}-setting.
     */
    SubGenerators = "subGenerators"
}
