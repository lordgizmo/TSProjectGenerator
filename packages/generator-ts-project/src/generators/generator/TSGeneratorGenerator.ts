import { GeneratorOptions, IComponentCollection } from "@manuth/extended-yo-generator";
import chalk from "chalk";
// eslint-disable-next-line @typescript-eslint/tslint/config
import dedent from "dedent";
import yosay from "yosay";
import { TSGeneratorComponentCollection } from "./Components/TSGeneratorComponentCollection.js";
import { ITSGeneratorPackage } from "./Settings/ITSGeneratorPackage.js";
import { ITSGeneratorSettings } from "./Settings/ITSGeneratorSettings.js";
import { GeneratorCreationContext } from "./Workspace/GeneratorCreationContext.js";
import { GeneratorName } from "../../Core/GeneratorName.js";
import { GeneratorWorkspacePrompt } from "../../Inquiry/Prompts/GeneratorWorkspacePrompt.js";
import { SubGeneratorPrompt } from "../../Inquiry/Prompts/SubGeneratorPrompt.js";
import { WorkspacePrompt } from "../../Inquiry/Prompts/WorkspacePrompt.js";
import { TSProjectGenerator } from "../../Project/TSProjectGenerator.js";
import { ICreationContextSettings } from "../../Project/Workspace/ICreationContextSettings.js";
import { PackageCreationContext } from "../../Project/Workspace/PackageCreationContext.js";

const { whiteBright } = chalk;

/**
 * Provides the functionality to generate a generator written in TypeScript.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 *
 * @template TPackage
 * The type of the packages of the generator.
 */
export class TSGeneratorGenerator<TSettings extends ITSGeneratorSettings = ITSGeneratorSettings, TOptions extends GeneratorOptions = GeneratorOptions, TPackage extends ITSGeneratorPackage = ITSGeneratorPackage> extends TSProjectGenerator<TSettings, TOptions, TPackage>
{
    /**
     * Initializes a new instance of the {@linkcode TSGeneratorGenerator} class.
     *
     * @param args
     * A set of arguments for the generator.
     *
     * @param options
     * A set of options for the generator.
     */
    public constructor(args: string | string[], options: TOptions)
    {
        super(args, options);
        this.env.adapter.promptModule.registerPrompt(SubGeneratorPrompt.TypeName, SubGeneratorPrompt);
        this.env.adapter.promptModule.registerPrompt(WorkspacePrompt.TypeName, GeneratorWorkspacePrompt);
    }

    /**
     * Gets the name of the `GettingStarted`-file.
     */
    public get GettingStartedFileName(): string
    {
        return "GettingStarted.md";
    }

    /**
     * @inheritdoc
     */
    public override get TemplateRoot(): string
    {
        return GeneratorName.Generator;
    }

    /**
     * @inheritdoc
     */
    public override get Components(): IComponentCollection<TSettings, TOptions>
    {
        return new TSGeneratorComponentCollection(this);
    }

    /**
     * @inheritdoc
     *
     * @param settings
     * The settings for the creation context.
     *
     * @returns
     * The newly created creation context.
     */
    public override GetCreationContext(settings: ICreationContextSettings<TSettings, TPackage>): PackageCreationContext<TSettings, TOptions, TPackage>
    {
        return new GeneratorCreationContext(this, settings);
    }

    /**
     * @inheritdoc
     */
    public override async prompting(): Promise<void>
    {
        this.log(yosay(`Welcome to the ${whiteBright.bold("TypeScript Generator")} generator!`));
        return super.prompting();
    }

    /**
     * @inheritdoc
     */
    public override async writing(): Promise<void>
    {
        return super.writing();
    }

    /**
     * @inheritdoc
     */
    public override async install(): Promise<void>
    {
        return super.install();
    }

    /**
     * @inheritdoc
     */
    public override async cleanup(): Promise<void>
    {
        return super.cleanup();
    }

    /**
     * @inheritdoc
     */
    public override async end(): Promise<void>
    {
        await super.end();

        this.log("");

        this.log(
            dedent(
                `
                    Open "${this.GettingStartedFileName}" in order to learn more about how to create your very own generator.
                    Thanks for using TSGeneratorGenerator!`));
    }
}
