import { ok } from "node:assert";
import { GeneratorOptions, GeneratorSettingKey } from "@manuth/extended-yo-generator";
import upath from "upath";
import { GeneratorName } from "../../../Core/GeneratorName.js";
import { TSProjectComponent } from "../../../Project/Settings/TSProjectComponent.js";
import { TSProjectPackageKey } from "../../../Project/Settings/TSProjectPackageKey.js";
import { TSProjectSettingKey } from "../../../Project/Settings/TSProjectSettingKey.js";
import { TSProjectLaunchSettingsProcessor } from "../../../Project/VSCode/TSProjectLaunchSettingsProcessor.js";
import { CodeWorkspaceComponent } from "../../../VSCode/Components/CodeWorkspaceComponent.js";
import { IDebugConfiguration } from "../../../VSCode/IDebugConfiguration.js";
import { ILaunchSettings } from "../../../VSCode/ILaunchSettings.js";
import { ISubGenerator } from "../Settings/ISubGenerator.js";
import { ITSGeneratorPackage } from "../Settings/ITSGeneratorPackage.js";
import { ITSGeneratorSettings } from "../Settings/ITSGeneratorSettings.js";
import { SubGeneratorSettingKey } from "../Settings/SubGeneratorSettingKey.js";
import { TSGeneratorComponent } from "../Settings/TSGeneratorComponent.js";
import { TSGeneratorPackageKey } from "../Settings/TSGeneratorPackageKey.js";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import type { TSGeneratorGenerator } from "../TSGeneratorGenerator.js";

const { join, normalize } = upath;

/**
 * Provides the functionality to process vscode debug configurations for {@linkcode TSGeneratorGenerator}s.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 */
export class TSGeneratorLaunchSettingsProcessor<TSettings extends ITSGeneratorSettings, TOptions extends GeneratorOptions> extends TSProjectLaunchSettingsProcessor<TSettings, TOptions>
{
    /**
     * Initializes a new instance of the {@linkcode TSGeneratorLaunchSettingsProcessor} class.
     *
     * @param component
     * The component of the processor.
     */
    public constructor(component: CodeWorkspaceComponent<TSettings, TOptions>)
    {
        super(component);
    }

    /**
     * @inheritdoc
     *
     * @param data
     * The data to process.
     *
     * @returns
     * The processed data.
     */
    public override async Process(data: ILaunchSettings): Promise<ILaunchSettings>
    {
        let result = await super.Process(data);
        let configurations: IDebugConfiguration[] = [];
        let generatorNameCount: Record<string, number> = {};
        result.configurations ??= [];

        let workspaces: Map<string | undefined, ITSGeneratorPackage> = new Map();

        if (this.Generator.Settings[GeneratorSettingKey.Components]?.includes(TSProjectComponent.MonoRepo))
        {
            for (let workspace of this.Generator.Settings[TSProjectSettingKey.Workspaces])
            {
                workspaces.set(workspace[TSProjectPackageKey.DisplayName], workspace);
            }
        }
        else
        {
            workspaces.set(undefined, this.Generator.Settings);
        }

        for (let workspace of workspaces.values())
        {
            for (let generator of workspace[TSGeneratorPackageKey.SubGenerators] ?? [])
            {
                let displayName = generator[SubGeneratorSettingKey.DisplayName];
                generatorNameCount[displayName] ??= 0;
                generatorNameCount[displayName]++;
            }
        }

        for (let [displayName, workspace] of workspaces.entries())
        {
            let generators: ISubGenerator[] = [];

            if (this.Generator.Settings[GeneratorSettingKey.Components]?.includes(TSGeneratorComponent.GeneratorExample))
            {
                generators.push(
                    {
                        [SubGeneratorSettingKey.DisplayName]: workspace[TSProjectPackageKey.DisplayName],
                        [SubGeneratorSettingKey.Name]: GeneratorName.Main
                    });
            }

            if (this.Generator.Settings[GeneratorSettingKey.Components]?.includes(TSGeneratorComponent.SubGeneratorExample))
            {
                generators.push(...(workspace[TSGeneratorPackageKey.SubGenerators] ?? []));
            }

            for (let generator of generators)
            {
                let template = await this.GetYeomanTemplate();
                let subGeneratorName = generator[SubGeneratorSettingKey.DisplayName];
                let name = generator[SubGeneratorSettingKey.Name];
                let thresholdCheck: () => boolean;

                if (name === GeneratorName.Main)
                {
                    template.name = "Launch Yeoman";
                    thresholdCheck = () => workspaces.size > 1;
                }
                else
                {
                    template.name = `Launch ${subGeneratorName} generator`;
                    thresholdCheck = () => generatorNameCount[subGeneratorName] > 1;
                }

                if (thresholdCheck())
                {
                    template.name += ` in ${displayName}`;
                }

                this.ReplaceWorkspaceDirectives(template, displayName);

                template.args = [
                    join(this.GetWorkspaceFolderDirective(displayName), "lib", "generators", name)
                ];

                if (
                    template.cwd &&
                    displayName)
                {
                    template.cwd = this.ReplaceWorkspaceFolder(template.cwd, this.WorkspaceRootName);
                }

                configurations.push(template);
            }
        }

        result.configurations.unshift(...configurations);
        return result;
    }

    /**
     * Gets a template-configuration for yeoman-tasks.
     *
     * @returns
     * A template-configuration for yeoman-tasks.
     */
    protected async GetYeomanTemplate(): Promise<IDebugConfiguration>
    {
        return (
            async () =>
            {
                let template = (await this.Component.Source.GetLaunchMetadata())?.configurations?.find(
                    (debugConfig) =>
                    {
                        return normalize(debugConfig.program ?? "").toLowerCase().endsWith(
                            join("node_modules", "yo", "lib", "cli.js"));
                    });

                ok(template);
                return this.ProcessDebugConfig(template);
            })();
    }
}
