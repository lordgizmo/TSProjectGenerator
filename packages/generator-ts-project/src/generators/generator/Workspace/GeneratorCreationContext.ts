import { GeneratorOptions, GeneratorSettingKey, IFileMapping } from "@manuth/extended-yo-generator";
import fs from "fs-extra";
import parsePackageName from "parse-pkg-name";
import path from "upath";
import { GeneratorName } from "../../../Core/GeneratorName.js";
import { TSProjectComponent } from "../../../Project/Settings/TSProjectComponent.js";
import { TSProjectSettingKey } from "../../../Project/Settings/TSProjectSettingKey.js";
import { ICreationContextSettings } from "../../../Project/Workspace/ICreationContextSettings.js";
import { ProjectCreationContext } from "../../../Project/Workspace/ProjectCreationContext.js";
import { TSGeneratorPackageFileMapping } from "../FileMappings/NPMPackaging/TSGeneratorPackageFileMapping.js";
import { GeneratorMainSuiteFileMapping } from "../FileMappings/TypeScript/GeneratorMainSuiteFileMapping.js";
import { GeneratorSuiteFileMapping } from "../FileMappings/TypeScript/GeneratorSuiteFileMapping.js";
import { NamingContext } from "../FileMappings/TypeScript/NamingContext.js";
import { ITSGeneratorPackage } from "../Settings/ITSGeneratorPackage.js";
import { ITSGeneratorSettings } from "../Settings/ITSGeneratorSettings.js";
import { SubGeneratorSettingKey } from "../Settings/SubGeneratorSettingKey.js";
import { TSGeneratorComponent } from "../Settings/TSGeneratorComponent.js";
import { TSGeneratorPackageKey } from "../Settings/TSGeneratorPackageKey.js";
import { TSGeneratorGenerator } from "../TSGeneratorGenerator.js";

const { ensureDir } = fs;
const { join } = path;

/**
 * Provides a context and settings for creating a typescript generator.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 *
 * @template TAnswers
 * The type of the answers of the context.
 */
export class GeneratorCreationContext<TSettings extends ITSGeneratorSettings, TOptions extends GeneratorOptions, TAnswers extends ITSGeneratorPackage> extends ProjectCreationContext<TSettings, TOptions, TAnswers>
{
    /**
     * The generator of the component.
     */
    private generatorGenerator: TSGeneratorGenerator<TSettings, TOptions, TAnswers>;

    /**
     * Initializes a new instance of the {@linkcode GeneratorCreationContext} class.
     *
     * @param generator
     * The generator this context belongs to.
     *
     * @param settings
     * The settings for the creation context.
     */
    public constructor(generator: TSGeneratorGenerator<TSettings, TOptions, TAnswers>, settings?: ICreationContextSettings<TSettings, TAnswers>)
    {
        super(generator, settings);
        this.generatorGenerator = generator;
    }

    /**
     * @inheritdoc
     */
    public override get Generator(): TSGeneratorGenerator<TSettings, TOptions, TAnswers>
    {
        return this.generatorGenerator;
    }

    /**
     * @inheritdoc
     */
    public override get DescriptionMessage(): string
    {
        return "Please enter a description for your generator.";
    }

    /**
     * @inheritdoc
     */
    protected override get ReadmeTemplatePath(): string
    {
        if (this.IsWorkspace)
        {
            return `${this.ReadmeBaseName}.ejs`;
        }
        else
        {
            return super.ReadmeTemplatePath;
        }
    }

    /**
     * @inheritdoc
     */
    protected override get NPMPackageFileMapping(): IFileMapping<TSettings, TOptions>
    {
        return new TSGeneratorPackageFileMapping(this.Generator, this as any);
    }

    /**
     * @inheritdoc
     */
    protected override get WorkspaceFileMappings(): Array<IFileMapping<TSettings, TOptions>>
    {
        let namingContext = new NamingContext(
            GeneratorName.Main,
            this.DisplayName,
            join(this.Destination, this.Generator.SourceRoot),
            this.Generator.Settings[TSProjectSettingKey.ESModule]);

        return [
            ...super.WorkspaceFileMappings,
            {
                Source: `${this.Generator.GettingStartedFileName}.ejs`,
                Destination: () => join(this.Destination, this.Generator.GettingStartedFileName),
                Context: async () =>
                {
                    return {
                        ID: (await this.ModuleName).replace(/^generator-/, ""),
                        Name: await this.ModuleName,
                        ESModule: this.Generator.Settings[TSProjectSettingKey.ESModule],
                        HasCodeWorkspace: this.Generator.Settings[GeneratorSettingKey.Components].includes(TSProjectComponent.VSCode),
                        HasLinting: this.Generator.Settings[GeneratorSettingKey.Components].includes(TSProjectComponent.Linting),
                        HasGenerator: this.Generator.Settings[GeneratorSettingKey.Components].includes(TSGeneratorComponent.GeneratorExample),
                        SubGenerators: (this.Metadata[TSGeneratorPackageKey.SubGenerators] ?? []).map(
                            (subGeneratorOptions) =>
                            {
                                let name = subGeneratorOptions[SubGeneratorSettingKey.Name];

                                return {
                                    Name: name,
                                    Path: join(this.Generator.SourceRoot, "generators", name)
                                };
                            })
                    };
                }
            },
            new GeneratorMainSuiteFileMapping<TSettings, TOptions>(this.Generator, namingContext),
            new GeneratorSuiteFileMapping<TSettings, TOptions>(this.Generator, namingContext),
            {
                Destination: join(this.Destination, this.Generator.SourceRoot, "generators"),
                Processor: (target) =>
                {
                    return ensureDir(target.Destination);
                }
            },
            {
                Destination: join(this.Destination, "templates"),
                Processor: (target) =>
                {
                    return ensureDir(target.Destination);
                }
            }
        ];
    }

    /**
     * Determines a suggested module name for the package.
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * A suggested module name for the package.
     */
    public override async GetSuggestedModuleName(answers: TAnswers): Promise<string>
    {
        let parsedName = parsePackageName((await super.GetSuggestedModuleName(answers)));

        parsedName.name = "generator-" + parsedName.name.replaceAll(
            /(^|-)generator(-|$)/gi, "");

        return parsePackageName.from(parsedName);
    }

    /**
     * @inheritdoc
     *
     * @param input
     * The input provided by the user.
     *
     * @param answers
     * The answers provided by the user.
     */
    public override async ValidateModuleName(input: string, answers: TAnswers): Promise<string | boolean>
    {
        let result = await super.ValidateModuleName(input, answers);

        if ((typeof result === "boolean") && result)
        {
            let packageName = parsePackageName(input).name;
            return /^generator-.+/.test(packageName) ? true : `The module name \`${input}\` is invalid. The name \`${packageName}\` must start with \`generator-\`.`;
        }
        else
        {
            return result;
        }
    }

    /**
     * Gets the context of the readme file.
     *
     * @returns
     * The context of the readme file.
     */
    protected override async GetReadmeContext(): Promise<any>
    {
        let result = await super.GetReadmeContext();

        if (this.IsWorkspace)
        {
            result = {
                ...result,
                Name: await this.ModuleName,
                DisplayName: result.Name,
                Description: await this.Description
            };
        }

        return result;
    }
}
