import { GeneratorOptions } from "@manuth/extended-yo-generator";
import { Package, ResolveMatrix } from "@manuth/package-json-editor";
import { TSProjectPackageFileMapping } from "../../../Project/FileMappings/NPMPackaging/TSProjectPackageFileMapping.js";
import { ITSProjectPackage } from "../../../Project/Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../../../Project/Settings/ITSProjectSettings.js";
import { TSProjectSettingKey } from "../../../Project/Settings/TSProjectSettingKey.js";
import { TSProjectGenerator } from "../../../Project/TSProjectGenerator.js";
import { ICreationContext } from "../../../Project/Workspace/ICreationContext.js";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import type { TSModuleGenerator } from "../TSModuleGenerator.js";

/**
 * Represents a file-mapping for the `package.json` file of {@linkcode TSModuleGenerator}s.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 *
 * @template TPackage
 * The type of the packages of the generator.
 */
export class TSModulePackageFileMapping<TSettings extends ITSProjectSettings, TOptions extends GeneratorOptions, TPackage extends ITSProjectPackage> extends TSProjectPackageFileMapping<TSettings, TOptions, TPackage>
{
    /**
     * Initializes a new instance of the {@linkcode TSModulePackageFileMapping} class.
     *
     * @param generator
     * The generator of the file-mapping.
     *
     * @param context
     * The context of the package creation.
     */
    public constructor(generator: TSProjectGenerator<TSettings, TOptions, TPackage>, context: ICreationContext<TSettings, TPackage>)
    {
        super(generator, context);
    }

    /**
     * @inheritdoc
     *
     * @returns
     * The loaded package.
     */
    protected override async LoadPackage(): Promise<Package>
    {
        let result = await super.LoadPackage();

        if (this.CreationContext.IsWorkspace)
        {
            result.Main = "./lib/index.js";
            result.Types = "./lib/index.d.ts";

            let exportMap = {
                types: result.Types,
                default: result.Main
            };

            result.Exports = {
                ".": {
                    ...(this.Generator.Settings[TSProjectSettingKey.ESModule] ?
                        {
                            import: exportMap
                        } :
                        exportMap)
                },
                ...result.Exports as ResolveMatrix
            };
        }

        return result;
    }
}
