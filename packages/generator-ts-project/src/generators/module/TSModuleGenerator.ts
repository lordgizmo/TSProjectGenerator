import { GeneratorOptions } from "@manuth/extended-yo-generator";
import chalk from "chalk";
// eslint-disable-next-line @typescript-eslint/tslint/config
import dedent from "dedent";
import yosay from "yosay";
import { ModuleCreationContext } from "./Workspace/ModuleCreationContext.js";
import { GeneratorName } from "../../Core/GeneratorName.js";
import { ITSProjectPackage } from "../../Project/Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../../Project/Settings/ITSProjectSettings.js";
import { TSProjectGenerator } from "../../Project/TSProjectGenerator.js";
import { ICreationContextSettings } from "../../Project/Workspace/ICreationContextSettings.js";
import { PackageCreationContext } from "../../Project/Workspace/PackageCreationContext.js";

const { whiteBright } = chalk;

/**
 * Provides the functionality to generate a module written in TypeScript.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 *
 * @template TPackage
 * The type of the packages of the generator.
 */
export class TSModuleGenerator<TSettings extends ITSProjectSettings = ITSProjectSettings, TOptions extends GeneratorOptions = GeneratorOptions, TPackage extends ITSProjectPackage = ITSProjectPackage> extends TSProjectGenerator<TSettings, TOptions, TPackage>
{
    /**
     * Initializes a new instance of the {@linkcode TSModuleGenerator} class.
     *
     * @param args
     * A set of arguments for the generator.
     *
     * @param options
     * A set of options for the generator.
     */
    public constructor(args: string | string[], options: TOptions)
    {
        super(args, options);
    }

    /**
     * @inheritdoc
     */
    public override get TemplateRoot(): string
    {
        return GeneratorName.Module;
    }

    /**
     * @inheritdoc
     *
     * @param settings
     * The settings for the creation context.
     *
     * @returns
     * The newly created creation context.
     */
    public override GetCreationContext(settings: ICreationContextSettings<TSettings, TPackage>): PackageCreationContext<TSettings, TOptions, TPackage>
    {
        return new ModuleCreationContext(this, settings);
    }

    /**
     * @inheritdoc
     */
    public override async prompting(): Promise<void>
    {
        this.log(yosay(`Welcome to the ${whiteBright.bold("TypeScript Module")} generator!`));
        return super.prompting();
    }

    /**
     * @inheritdoc
     */
    public override async writing(): Promise<void>
    {
        return super.writing();
    }

    /**
     * @inheritdoc
     */
    public override async install(): Promise<void>
    {
        return super.install();
    }

    /**
     * @inheritdoc
     */
    public override async cleanup(): Promise<void>
    {
        return super.cleanup();
    }

    /**
     * @inheritdoc
     */
    public override async end(): Promise<void>
    {
        await super.end();

        this.log("");

        this.log(
            dedent(
                `
                    Thanks for using TSModuleGenerator!`));
    }
}
