import { join } from "node:path";
import { GeneratorOptions, IFileMapping } from "@manuth/extended-yo-generator";
import { ISuiteContext } from "../../../Project/FileMappings/TypeScript/ISuiteContext.js";
import { ModuleIndexFileMapping } from "../../../Project/FileMappings/TypeScript/ModuleIndexFileMapping.js";
import { TestFileMapping } from "../../../Project/FileMappings/TypeScript/TestFileMapping.js";
import { ITSProjectPackage } from "../../../Project/Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../../../Project/Settings/ITSProjectSettings.js";
import { TSProjectGenerator } from "../../../Project/TSProjectGenerator.js";
import { ICreationContextSettings } from "../../../Project/Workspace/ICreationContextSettings.js";
import { ProjectCreationContext } from "../../../Project/Workspace/ProjectCreationContext.js";
import { TSModulePackageFileMapping } from "../FileMappings/TSModulePackageFileMapping.js";

/**
 * Provides a context and settings for creating a typescript module.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 *
 * @template TAnswers
 * The type of the answers of the context.
 */
export class ModuleCreationContext<TSettings extends ITSProjectSettings, TOptions extends GeneratorOptions, TAnswers extends ITSProjectPackage> extends ProjectCreationContext<TSettings, TOptions, TAnswers>
{
    /**
     * Initializes a new instance of the {@linkcode ModuleCreationContext} class.
     *
     * @param generator
     * The generator this context belongs to.
     *
     * @param settings
     * The settings for the creation context.
     */
    public constructor(generator: TSProjectGenerator<TSettings, TOptions, TAnswers>, settings?: ICreationContextSettings<TSettings, TAnswers>)
    {
        super(generator, settings);
    }

    /**
     * @inheritdoc
     */
    protected override get WorkspaceFileMappings(): Array<IFileMapping<TSettings, TOptions>>
    {
        let displayName = async (): Promise<string> => this.DisplayName;
        let destination = (): string => this.Destination;
        let sourcePath = (): string => join(destination(), this.Generator.SourceRoot);

        return [
            ...super.WorkspaceFileMappings,
            new class extends ModuleIndexFileMapping<TSettings, TOptions>
            {
                /**
                 * @inheritdoc
                 */
                public get Destination(): string
                {
                    return join(sourcePath(), "index.ts");
                }
            }(this.Generator),
            new class extends TestFileMapping<TSettings, TOptions>
            {
                /**
                 * @inheritdoc
                 */
                public override get Destination(): string
                {
                    return join(sourcePath(), "tests", "main.test.ts");
                }

                /**
                 * @inheritdoc
                 *
                 * @returns
                 * The context of the file-mapping.
                 */
                public override async Context(): Promise<ISuiteContext>
                {
                    let name = await displayName();

                    return {
                        ...await super.Context(),
                        SuiteName: name
                    };
                }
            }(this.Generator)
        ];
    }

    /**
     * @inheritdoc
     */
    protected override get NPMPackageFileMapping(): IFileMapping<TSettings, TOptions>
    {
        return new TSModulePackageFileMapping(this.Generator, this as any);
    }
}
