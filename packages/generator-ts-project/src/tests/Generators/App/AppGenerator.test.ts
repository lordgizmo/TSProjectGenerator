import { doesNotReject, ok } from "node:assert";
import { RunContext, RunContextSettings } from "@manuth/extended-yo-generator-test";
import { TempDirectory } from "@manuth/temp-files";
import { PromptModule } from "inquirer";
import { createSandbox, SinonExpectation, SinonSandbox } from "sinon";
import _default from "yeoman-test";
import { AppGenerator } from "../../../generators/app/AppGenerator.js";
import { ProjectType } from "../../../generators/app/ProjectType.js";
import { ProjectSelectorSettingKey } from "../../../generators/app/Settings/ProjectSelectorSettingKey.js";
import { TSGeneratorGenerator } from "../../../generators/generator/TSGeneratorGenerator.js";
import { TSModuleGenerator } from "../../../generators/module/TSModuleGenerator.js";
import { TSProjectGenerator } from "../../../Project/TSProjectGenerator.js";
import { TestContext } from "../../TestContext.js";

// eslint-disable-next-line deprecation/deprecation
const { mockPrompt } = _default;

/**
 * Registers tests for the generators.
 *
 * @param context
 * The test-context.
 */
export function AppGeneratorTests(context: TestContext<AppGenerator>): void
{
    suite(
        nameof(AppGenerator),
        () =>
        {
            let sandbox: SinonSandbox;
            let workingDirectory: string;
            let tempDir: TempDirectory;
            let contextCreator: (settings?: RunContextSettings) => RunContext<AppGenerator>;
            let teardownActions: Array<() => void>;

            suiteSetup(
                async () =>
                {
                    teardownActions = [];

                    contextCreator = (settings?: RunContextSettings) =>
                    {
                        let result = context.ExecuteGenerator(undefined, settings ?? {});
                        teardownActions.push(() => result.removeAllListeners());

                        result.onGenerator(
                            () =>
                            {
                                let mockedPrompt = sandbox.mock(result.generator.env.adapter.promptModule);

                                /**
                                 * Adds the fake prompt-registration to the prompt-module.
                                 */
                                function AddFake(): void
                                {
                                    let mockedRegister = mockedPrompt.expects(nameof<PromptModule>((m) => m.registerPrompt));
                                    mockedRegister.atLeast(0);
                                    mockedRegister.callsFake(GetFakeRegisterPrompt(mockedRegister));
                                }

                                /**
                                 * Fakes the registration of a prompt.
                                 *
                                 * @param expectation
                                 * The {@linkcode SinonExpectation} to add the faked prompt-registration to.
                                 *
                                 * @returns
                                 * A method for faking the prompt-registration.
                                 */
                                function GetFakeRegisterPrompt(expectation: SinonExpectation): (...args: any[]) => void
                                {
                                    return (...args: any[]): void =>
                                    {
                                        let register = expectation.wrappedMethod;
                                        register = register.bind(result.generator.env.adapter.promptModule);
                                        register(...args);
                                        mockedPrompt.restore();
                                        // eslint-disable-next-line deprecation/deprecation
                                        mockPrompt(result.generator, result["answers"]);
                                        AddFake();
                                    };
                                }

                                AddFake();
                            });

                        return result;
                    };
                });

            setup(
                function()
                {
                    this.timeout(0.5 * 60 * 1000);
                    sandbox = createSandbox();
                    workingDirectory = process.cwd();
                    tempDir = new TempDirectory();

                    let prototype = TSProjectGenerator.prototype;
                    sandbox.replace(prototype, nameof.typed<TSProjectGenerator>().cleanup, async () => { });
                    sandbox.replace(prototype, nameof.typed<TSProjectGenerator>().end, async () => { });
                    sandbox.replace(prototype, nameof.typed<TSProjectGenerator>().initializing, async () => { });
                    sandbox.replace(prototype, nameof.typed<TSProjectGenerator>().install, async () => { });
                    sandbox.replace(prototype, nameof.typed<TSProjectGenerator>().prompting, async () => { });
                    sandbox.replace(prototype, nameof.typed<TSProjectGenerator>().writing, async () => { });
                });

            teardown(
                function()
                {
                    this.timeout(45 * 1000);
                    sandbox.restore();
                    teardownActions.forEach((action) => action());
                    process.chdir(workingDirectory);
                    tempDir.Dispose();
                });

            suite(
                nameof<AppGenerator>((generator) => generator.projectTypeSelection),
                () =>
                {
                    test(
                        "Checking whether the generator can be executed…",
                        async function()
                        {
                            this.timeout(6 * 60 * 1000);
                            this.slow(3 * 60 * 1000);

                            await doesNotReject(
                                async () =>
                                {
                                    return contextCreator(
                                        {

                                        });
                                });

                            test(
                                `Checking whether the \`${nameof(TSModuleGenerator)}\` can be executed…`,
                                async function()
                                {
                                    this.timeout(15 * 1000);
                                    this.slow(7.5 * 1000);
                                    let moduleGeneratorRan: boolean | undefined;

                                    sandbox.replace(
                                        TSModuleGenerator.prototype,
                                        nameof.typed<TSModuleGenerator>().end,
                                        async () =>
                                        {
                                            moduleGeneratorRan = true;
                                        });

                                    await doesNotReject(
                                        async () =>
                                        {
                                            return contextCreator(
                                                {
                                                    cwd: tempDir.FullName
                                                }).withAnswers(
                                                {
                                                    [ProjectSelectorSettingKey.ProjectType]: ProjectType.Module
                                                });
                                        });

                                    ok(moduleGeneratorRan);
                                });

                            test(
                                `Checking whether the \`${nameof(TSGeneratorGenerator)}\` can be executed…`,
                                async function()
                                {
                                    this.timeout(15 * 1000);
                                    this.slow(7.5 * 1000);
                                    let generatorGeneratorRan: boolean | undefined;

                                    sandbox.replace(
                                        TSGeneratorGenerator.prototype,
                                        nameof.typed<TSGeneratorGenerator>().end,
                                        async () =>
                                        {
                                            generatorGeneratorRan = true;
                                        });

                                    await doesNotReject(
                                        async () =>
                                        {
                                            return contextCreator(
                                                {
                                                    cwd: tempDir.FullName
                                                }).withAnswers(
                                                    {
                                                        [ProjectSelectorSettingKey.ProjectType]: ProjectType.Generator
                                                    });
                                        });

                                    ok(generatorGeneratorRan);
                                });
                        });
                });
        });
}
