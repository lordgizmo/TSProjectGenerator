import { doesNotReject, ok } from "node:assert";
import { GeneratorOptions, GeneratorSettingKey, IComponent, IFileMapping } from "@manuth/extended-yo-generator";
import { RunContext, TestContext as GeneratorContext } from "@manuth/extended-yo-generator-test";
import { TempDirectory } from "@manuth/temp-files";
import fs from "fs-extra";
import { GeneratorName } from "../../../../Core/GeneratorName.js";
import { TSGeneratorCategory } from "../../../../generators/generator/Components/TSGeneratorCategory.js";
import { NamingContext } from "../../../../generators/generator/FileMappings/TypeScript/NamingContext.js";
import { ITSGeneratorPackage } from "../../../../generators/generator/Settings/ITSGeneratorPackage.js";
import { ITSGeneratorSettings } from "../../../../generators/generator/Settings/ITSGeneratorSettings.js";
import { SubGeneratorSettingKey } from "../../../../generators/generator/Settings/SubGeneratorSettingKey.js";
import { TSGeneratorComponent } from "../../../../generators/generator/Settings/TSGeneratorComponent.js";
import { TSGeneratorPackageKey } from "../../../../generators/generator/Settings/TSGeneratorPackageKey.js";
import { TSGeneratorGenerator } from "../../../../generators/generator/TSGeneratorGenerator.js";
import { TSProjectPackageKey } from "../../../../Project/Settings/TSProjectPackageKey.js";
import { ICreationContext } from "../../../../Project/Workspace/ICreationContext.js";
import { TestContext } from "../../../TestContext.js";
import { spawnNPM } from "../../../Utilities.js";
import { GeneratorPath } from "../TSGeneratorGenerator.test.js";

const { pathExists } = fs;

/**
 * Registers tests for the {@linkcode TSGeneratorCategory} class.
 *
 * @param context
 * The test-context.
 */
export function TSGeneratorCategoryTests(context: TestContext<TSGeneratorGenerator>): void
{
    suite(
        nameof(TSGeneratorCategory),
        () =>
        {
            let generator: TSGeneratorGenerator;
            let runContext: RunContext<TSGeneratorGenerator>;
            let tempDir: TempDirectory;
            let settings: ITSGeneratorSettings;
            let collection: TSGeneratorCategory<ITSGeneratorSettings, GeneratorOptions, ITSGeneratorPackage>;
            context.RegisterWorkingDirRestorer();

            /**
             * Provides an implementation of the {@linkcode TSGeneratorCategory} class for testing.
             */
            class TestTSGeneratorCategory extends TSGeneratorCategory<any, any, any>
            {
                /**
                 * Gets a component for creating an example generator.
                 */
                public override get GeneratorComponent(): IComponent<any, any>
                {
                    return super.GeneratorComponent;
                }

                /**
                 * @inheritdoc
                 */
                public override get SubGeneratorComponent(): IComponent<any, any>
                {
                    return super.SubGeneratorComponent;
                }

                /**
                 * @inheritdoc
                 *
                 * @param context
                 * The context of the generator creation.
                 *
                 * @param id
                 * The id of the generator.
                 *
                 * @param displayName
                 * The human readable name of the generator.
                 *
                 * @returns
                 * File-mappings for a generator.
                 */
                public override GetGeneratorFileMappings(context: ICreationContext<any, any>, id: string, displayName: string): Array<IFileMapping<any, any>>
                {
                    return super.GetGeneratorFileMappings(context, id, displayName);
                }
            }

            suiteSetup(
                async function()
                {
                    this.timeout(5 * 60 * 1000);

                    settings = {
                        ...(await context.Generator).Settings,
                        [TSProjectPackageKey.DisplayName]: "Z",
                        [GeneratorSettingKey.Components]: [
                            TSGeneratorComponent.GeneratorExample,
                            TSGeneratorComponent.SubGeneratorExample
                        ],
                        [TSGeneratorPackageKey.SubGenerators]: [
                            {
                                [SubGeneratorSettingKey.DisplayName]: "A",
                                [SubGeneratorSettingKey.Name]: "a"
                            },
                            {
                                [SubGeneratorSettingKey.DisplayName]: "B",
                                [SubGeneratorSettingKey.Name]: "b"
                            }
                        ]
                    };

                    runContext = context.ExecuteGenerator();
                    runContext.withAnswers(settings);
                    await runContext;
                    generator = runContext.generator;
                    collection = new TSGeneratorCategory(generator);

                    spawnNPM(
                        [
                            "install",
                            "--silent"
                        ],
                        {
                            cwd: generator.destinationPath(),
                            stdio: "ignore"
                        });

                    spawnNPM(
                        [
                            "run",
                            "build"
                        ],
                        {
                            cwd: generator.destinationPath(),
                            stdio: "ignore"
                        });
                });

            suiteTeardown(
                function()
                {
                    this.timeout(1 * 60 * 1000);
                    runContext.cleanTestDirectory();
                });

            setup(
                () =>
                {
                    tempDir = new TempDirectory();
                });

            teardown(
                () =>
                {
                    tempDir.Dispose();
                });

            suite(
                nameof<TSGeneratorCategory<any, any, any>>((category) => category.Components),
                () =>
                {
                    test(
                        `Checking whether all components for the \`${nameof(TSGeneratorGenerator)}\`s are present…`,
                        async () =>
                        {
                            for (let componentID of [TSGeneratorComponent.GeneratorExample, TSGeneratorComponent.SubGeneratorExample])
                            {
                                ok(collection.Components.some((component) => component.ID === componentID));
                            }
                        });
                });

            suite(
                nameof<TestTSGeneratorCategory>((category) => category.GeneratorComponent),
                () =>
                {
                    test(
                        "Checking whether the generator is created correctly…",
                        async function()
                        {
                            this.timeout(45 * 1000);
                            this.slow(22.5 * 1000);
                            let testContext = new TestContext(new GeneratorContext(GeneratorPath(generator.PackageCreationContext, GeneratorName.Main)));

                            await doesNotReject(
                                async () => testContext.ExecuteGenerator(
                                    undefined,
                                    {
                                        cwd: tempDir.FullName
                                    }));
                        });
                });

            suite(
                nameof<TestTSGeneratorCategory>((category) => category.SubGeneratorComponent),
                () =>
                {
                    test(
                        "Checking whether sub-generators are created correctly…",
                        async function()
                        {
                            this.timeout(20 * 1000);
                            this.slow(10 * 1000);

                            for (let subGeneratorOptions of settings[TSGeneratorPackageKey.SubGenerators])
                            {
                                let name = subGeneratorOptions[SubGeneratorSettingKey.Name];
                                let testContext = new TestContext(new GeneratorContext(GeneratorPath(generator.PackageCreationContext, name)));

                                await doesNotReject(
                                    async () => testContext.ExecuteGenerator(
                                        undefined,
                                        {
                                            cwd: tempDir.FullName
                                        }));
                            }
                        });
                });

            suite(
                nameof<TestTSGeneratorCategory>((category) => category.GetGeneratorFileMappings),
                () =>
                {
                    test(
                        "Checking whether test-files for all generators are present…",
                        async () =>
                        {
                            for (
                                let generatorName of
                                [
                                    GeneratorName.Main,
                                    ...generator.Settings[TSGeneratorPackageKey.SubGenerators].map(
                                        (subGenerator) =>
                                        {
                                            return subGenerator[SubGeneratorSettingKey.Name];
                                        })
                                ])
                            {
                                let namingContext = new NamingContext(generatorName, context.RandomString, generator.SourceRoot, true);
                                ok(await pathExists(generator.destinationPath("src", "tests", "Generators", `${namingContext.GeneratorClassName}.test.ts`)));
                            }
                        });
                });
        });
}
