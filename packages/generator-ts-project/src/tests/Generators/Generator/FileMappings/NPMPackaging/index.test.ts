import { basename } from "node:path";
import { TSGeneratorPackageFileMappingTests } from "./TSGeneratorPackageFileMapping.test.js";
import { TSGeneratorGenerator } from "../../../../../generators/generator/TSGeneratorGenerator.js";
import { TestContext } from "../../../../TestContext.js";

/**
 * Registers tests for npm file-mappings for {@linkcode TSGeneratorGenerator}s.
 *
 * @param context
 * The test-context.
 */
export function NPMPackagingFileMappingTests(context: TestContext<TSGeneratorGenerator>): void
{
    suite(
        basename(new URL(".", import.meta.url).pathname),
        () =>
        {
            TSGeneratorPackageFileMappingTests(context);
        });
}
