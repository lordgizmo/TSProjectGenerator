import { doesNotThrow, notStrictEqual, ok, strictEqual } from "node:assert";
import { GeneratorOptions } from "@manuth/extended-yo-generator";
import { FileMappingTester } from "@manuth/extended-yo-generator-test";
import { TypeScriptFileMappingTester } from "@manuth/generator-ts-project-test";
import { ESLint } from "eslint";
import { SourceFile, SyntaxKind } from "ts-morph";
import { TSGeneratorPackageFileMapping } from "../../../../../generators/generator/FileMappings/NPMPackaging/TSGeneratorPackageFileMapping.js";
import { GeneratorClassFileMapping } from "../../../../../generators/generator/FileMappings/TypeScript/GeneratorClassFileMapping.js";
import { LicenseTypeFileMapping } from "../../../../../generators/generator/FileMappings/TypeScript/LicenseTypeFileMapping.js";
import { NamingContext } from "../../../../../generators/generator/FileMappings/TypeScript/NamingContext.js";
import { SettingKeyFileMapping } from "../../../../../generators/generator/FileMappings/TypeScript/SettingKeyFileMapping.js";
import { SettingsInterfaceFileMapping } from "../../../../../generators/generator/FileMappings/TypeScript/SettingsInterfaceFileMapping.js";
import { TSGeneratorGenerator } from "../../../../../generators/generator/TSGeneratorGenerator.js";
import { ESLintRCFileMapping } from "../../../../../Project/FileMappings/ESLintRCFileMapping.js";
import { ITSProjectSettings } from "../../../../../Project/Settings/ITSProjectSettings.js";
import { TestContext } from "../../../../TestContext.js";
import { spawnNPM } from "../../../../Utilities.js";

/**
 * Registers tests for the {@linkcode SettingsInterfaceFileMapping} class.
 *
 * @param context
 * The test-context.
 */
export function SettingsInterfaceFileMappingTests(context: TestContext<TSGeneratorGenerator>): void
{
    suite(
        nameof(SettingsInterfaceFileMapping),
        () =>
        {
            /**
             * Provides an implementation of the {@linkcode SettingsInterfaceFileMapping} class for testing.
             */
            class TestSettingsInterfaceFileMapping extends SettingsInterfaceFileMapping<ITSProjectSettings, GeneratorOptions>
            {
                /**
                 * @inheritdoc
                 *
                 * @param sourceFile
                 * The source-file to process.
                 *
                 * @returns
                 * The processed data.
                 */
                public override async Transform(sourceFile: SourceFile): Promise<SourceFile>
                {
                    this.Dispose();
                    return super.Transform(sourceFile);
                }
            }

            let generator: TSGeneratorGenerator;
            let namingContext: NamingContext;
            let fileMapping: TestSettingsInterfaceFileMapping;
            let tester: TypeScriptFileMappingTester<TSGeneratorGenerator, ITSProjectSettings, GeneratorOptions, TestSettingsInterfaceFileMapping>;
            let eslintConfigFileName: string;

            suiteSetup(
                async function()
                {
                    this.timeout(10 * 60 * 1000);
                    generator = await context.Generator;
                    let eslintConfigTester = new FileMappingTester(generator, new ESLintRCFileMapping(generator, generator.PackageCreationContext));
                    namingContext = new NamingContext("test", "Test", generator.SourceRoot, true);
                    await eslintConfigTester.Run();
                    await new FileMappingTester(generator, new TSGeneratorPackageFileMapping(generator, generator.PackageCreationContext)).Run();
                    await new FileMappingTester(generator, new GeneratorClassFileMapping(generator, namingContext)).Run();
                    await new FileMappingTester(generator, new SettingKeyFileMapping(generator, namingContext)).Run();
                    await new FileMappingTester(generator, new SettingsInterfaceFileMapping(generator, namingContext)).Run();
                    await new FileMappingTester(generator, new LicenseTypeFileMapping(generator, namingContext)).Run();
                    fileMapping = new TestSettingsInterfaceFileMapping(generator, namingContext);
                    tester = new TypeScriptFileMappingTester(generator, fileMapping);
                    eslintConfigFileName = eslintConfigTester.FileMapping.Destination;

                    spawnNPM(
                        [
                            "install",
                            "--silent"
                        ],
                        {
                            cwd: generator.destinationPath(),
                            stdio: "ignore"
                        });
                });

            suite(
                nameof<TestSettingsInterfaceFileMapping>((fileMapping) => fileMapping.Destination),
                () =>
                {
                    test(
                        `Checking whether the \`${nameof<TestSettingsInterfaceFileMapping>((fm) => fm.Destination)}\` points to the proper location…`,
                        () =>
                        {
                            strictEqual(fileMapping.Destination, namingContext.SettingsInterfaceFileName);
                        });
                });

            suite(
                nameof<TestSettingsInterfaceFileMapping>((fileMapping) => fileMapping.Transform),
                () =>
                {
                    let sourceFile: SourceFile;

                    suiteSetup(
                        async function()
                        {
                            this.timeout(2 * 60 * 1000);
                            sourceFile = await fileMapping.Transform(await fileMapping.GetSourceObject());
                        });

                    suiteTeardown(
                        () =>
                        {
                            sourceFile.forget();
                        });

                    test(
                        "Checking whether an interface with the expected name is present…",
                        function()
                        {
                            this.timeout(30 * 1000);
                            this.slow(15 * 1000);

                            doesNotThrow(
                                async () =>
                                {
                                    let declaration = sourceFile.getExportedDeclarations().get(namingContext.SettingsInterfaceName)?.[0];
                                    ok(declaration);
                                    declaration.asKindOrThrow(SyntaxKind.InterfaceDeclaration);
                                });
                        });

                    test(
                        "Checking whether all expected members are present in the interface…",
                        () =>
                        {
                            let interfaceDeclaration = sourceFile.getInterface(namingContext.SettingsInterfaceName);
                            let properties = interfaceDeclaration?.getProperties();

                            for (
                                let member of [
                                    namingContext.DestinationMember,
                                    namingContext.NameMember,
                                    namingContext.DescriptionMember,
                                    namingContext.LicenseTypeMember
                                ])
                            {
                                ok(
                                    properties?.some(
                                        (property) =>
                                        {
                                            let propertyName = property.getNodeProperty("name").asKind(
                                                SyntaxKind.ComputedPropertyName)?.getExpression().asKind(
                                                    SyntaxKind.PropertyAccessExpression);

                                            return propertyName?.getExpression().getText() === namingContext.SettingKeyEnumName &&
                                                propertyName.getName() === member;
                                        }));
                            }
                        });

                    test(
                        "Checking whether naming collisions are prevented…",
                        async function()
                        {
                            this.timeout(30 * 1000);
                            this.slow(15 * 1000);

                            let namingContext = new NamingContext("generator", "Generator", generator.SourceRoot, true);
                            tester = new TypeScriptFileMappingTester(generator, new TestSettingsInterfaceFileMapping(generator, namingContext));
                            await tester.Run();
                            let file = await tester.ParseOutput();

                            let settingsInterface = file.getInterface(namingContext.SettingsInterfaceName);
                            notStrictEqual(settingsInterface?.getExtends()[0].getText(), namingContext.SettingsInterfaceName);
                        });

                    test(
                        "Checking whether the resulting code does not contain any linting issues…",
                        async function()
                        {
                            this.timeout(2 * 60 * 1000);
                            this.slow(1 * 60 * 1000);

                            let linter = new ESLint(
                                {
                                    useEslintrc: false,
                                    overrideConfigFile: eslintConfigFileName
                                });

                            await tester.DumpOutput(sourceFile);
                            let result = await linter.lintFiles(tester.FileMapping.Destination);

                            strictEqual(
                                result.flatMap(
                                    (eslintResult) => eslintResult.messages).length,
                                0,
                                "Expected no linting issues. Got following issues instead:\n" +
                                result.flatMap((lintResult) => lintResult.messages).map(
                                    (message) => `${message.ruleId}: ${message.message}`).join("\n"));
                        });
                });
        });
}
