import { doesNotReject, ok, strictEqual } from "node:assert";
import { join } from "node:path";
import { GeneratorSettingKey } from "@manuth/extended-yo-generator";
import { RunContext, TestContext as GeneratorContext } from "@manuth/extended-yo-generator-test";
import { PackageType } from "@manuth/package-json-editor";
import { TempDirectory } from "@manuth/temp-files";
import { GeneratorName } from "../../../Core/GeneratorName.js";
import { NamingContext } from "../../../generators/generator/FileMappings/TypeScript/NamingContext.js";
import { ISubGenerator } from "../../../generators/generator/Settings/ISubGenerator.js";
import { ITSGeneratorPackage } from "../../../generators/generator/Settings/ITSGeneratorPackage.js";
import { ITSGeneratorSettings } from "../../../generators/generator/Settings/ITSGeneratorSettings.js";
import { SubGeneratorSettingKey } from "../../../generators/generator/Settings/SubGeneratorSettingKey.js";
import { TSGeneratorComponent } from "../../../generators/generator/Settings/TSGeneratorComponent.js";
import { TSGeneratorPackageKey } from "../../../generators/generator/Settings/TSGeneratorPackageKey.js";
import { TSGeneratorGenerator } from "../../../generators/generator/TSGeneratorGenerator.js";
import { TSProjectComponent } from "../../../Project/Settings/TSProjectComponent.js";
import { TSProjectPackageKey } from "../../../Project/Settings/TSProjectPackageKey.js";
import { TSProjectSettingKey } from "../../../Project/Settings/TSProjectSettingKey.js";
import { ICreationContext } from "../../../Project/Workspace/ICreationContext.js";
import { SuiteInspector } from "../../Mocha/SuiteInspector.js";
import { TestContext } from "../../TestContext.js";
import { spawnNPM } from "../../Utilities.js";

/**
 * Registers tests for the {@linkcode TSGeneratorGenerator} class.
 *
 * @param context
 * The test-context.
 */
export function TSGeneratorGeneratorTests(context: TestContext<TSGeneratorGenerator>): void
{
    suite(
        nameof(TSGeneratorGenerator),
        () =>
        {
            let tempDir: TempDirectory;
            context.RegisterWorkingDirRestorer();

            suiteSetup(
                async function()
                {
                    this.timeout(30 * 1000);
                });

            setup(
                async () =>
                {
                    tempDir = new TempDirectory();
                });

            teardown(
                function()
                {
                    this.timeout(10 * 1000);
                    tempDir.Dispose();
                });

            for (let monoRepo of [false, true])
            {
                /**
                 * Registers the tests.
                 */
                function register(): void
                {
                    for (let esModule of [true, false])
                    {
                        let packageType = esModule ? nameof(PackageType.ESModule) : nameof(PackageType.CommonJS);

                        suite(
                            packageType,
                            () =>
                            {
                                let runContext: RunContext<TSGeneratorGenerator>;
                                let generator: TSGeneratorGenerator;
                                let settings: ITSGeneratorSettings;
                                let creationContexts: Array<ICreationContext<ITSGeneratorSettings, ITSGeneratorPackage>>;

                                suiteSetup(
                                    async function()
                                    {
                                        this.timeout(10 * 60 * 1000);
                                        runContext = context.ExecuteGenerator();

                                        let clientGenerators: ISubGenerator[] = [
                                            {
                                                [SubGeneratorSettingKey.DisplayName]: "Extension",
                                                [SubGeneratorSettingKey.Name]: "extension"
                                            },
                                            {
                                                [SubGeneratorSettingKey.DisplayName]: "Theme",
                                                [SubGeneratorSettingKey.Name]: "theme"
                                            }
                                        ];

                                        settings = {
                                            ...(await context.Generator).Settings,
                                            [TSProjectPackageKey.Destination]: ".",
                                            [TSProjectPackageKey.DisplayName]: "MyGenerator",
                                            [TSProjectPackageKey.Name]: "generator-my",
                                            [TSProjectPackageKey.Description]: "",
                                            [GeneratorSettingKey.Components]: [
                                                TSGeneratorComponent.GeneratorExample,
                                                TSGeneratorComponent.SubGeneratorExample,
                                                ...(monoRepo ? [TSProjectComponent.MonoRepo] : [])
                                            ],
                                            ...(
                                                monoRepo ?
                                                    {
                                                        [TSProjectSettingKey.Workspaces]: [
                                                            {
                                                                [TSProjectPackageKey.Destination]: "./packages/client",
                                                                [TSProjectPackageKey.DisplayName]: "Client",
                                                                [TSProjectPackageKey.Name]: "generator-client",
                                                                [TSProjectPackageKey.Description]: "",
                                                                [TSGeneratorPackageKey.SubGenerators]: clientGenerators
                                                            },
                                                            {
                                                                [TSProjectPackageKey.Destination]: "./packages/server",
                                                                [TSProjectPackageKey.DisplayName]: "Server",
                                                                [TSProjectPackageKey.Name]: "generator-server",
                                                                [TSProjectPackageKey.Description]: "",
                                                                [TSGeneratorPackageKey.SubGenerators]: [
                                                                    {
                                                                        [SubGeneratorSettingKey.DisplayName]: "Base",
                                                                        [SubGeneratorSettingKey.Name]: "base"
                                                                    },
                                                                    {
                                                                        [SubGeneratorSettingKey.DisplayName]: "Plugin",
                                                                        [SubGeneratorSettingKey.Name]: "plugin"
                                                                    }
                                                                ]
                                                            }
                                                        ]
                                                    } :
                                                    {
                                                        [TSGeneratorPackageKey.SubGenerators]: clientGenerators
                                                    })
                                        } as ITSGeneratorSettings;

                                        if (monoRepo)
                                        {
                                            runContext.withAnswers(
                                                {
                                                    ...settings,
                                                    [TSProjectSettingKey.ESModule]: esModule
                                                });
                                        }
                                        else
                                        {
                                            runContext.withAnswers(
                                                {
                                                    ...settings,
                                                    [TSProjectSettingKey.ESModule]: esModule
                                                });
                                        }

                                        await runContext;
                                        generator = runContext.generator;

                                        spawnNPM(
                                            [
                                                "install",
                                                "--silent"
                                            ],
                                            {
                                                cwd: generator.destinationPath(),
                                                stdio: "ignore"
                                            });

                                        spawnNPM(
                                            [
                                                "run",
                                                "build"
                                            ],
                                            {
                                                cwd: generator.destinationPath(),
                                                stdio: "ignore"
                                            });
                                    });

                                setup(
                                    () =>
                                    {
                                        if (monoRepo)
                                        {
                                            creationContexts = generator.WorkspaceCreationContexts;
                                        }
                                        else
                                        {
                                            creationContexts = [generator.PackageCreationContext];
                                        }
                                    });

                                suite(
                                    "General",
                                    () =>
                                    {
                                        test(
                                            "Checking whether the generated project can be installed and built…",
                                            function()
                                            {
                                                this.timeout(10 * 60 * 1000);
                                                this.slow(5 * 60 * 1000);

                                                let installationResult = spawnNPM(
                                                    [
                                                        "install",
                                                        "--silent"
                                                    ],
                                                    {
                                                        cwd: generator.destinationPath(),
                                                        stdio: "ignore"
                                                    });

                                                let buildResult = spawnNPM(
                                                    [
                                                        "run",
                                                        "build"
                                                    ],
                                                    {
                                                        cwd: generator.destinationPath(),
                                                        stdio: "ignore"
                                                    });

                                                strictEqual(installationResult.status, 0);
                                                strictEqual(buildResult.status, 0);
                                            });

                                        test(
                                            `Checking whether ${monoRepo ? "all" : "the"} main generator${monoRepo ? "s" : ""} can be executed…`,
                                            async function()
                                            {
                                                this.timeout(1 * 60 * 1000);
                                                this.slow(30 * 1000);

                                                for (let creationContext of creationContexts)
                                                {
                                                    let testContext = new TestContext(new GeneratorContext(GeneratorPath(creationContext, GeneratorName.Main)));

                                                    return doesNotReject(
                                                        async () => testContext.ExecuteGenerator(
                                                            undefined,
                                                            {
                                                                cwd: tempDir.FullName
                                                            }));
                                                }
                                            });

                                        test(
                                            `Checking whether ${monoRepo ? "all" : "the"} sub-generators can be executed…`,
                                            async function()
                                            {
                                                this.timeout(20 * 1000);
                                                this.slow(10 * 1000);

                                                for (let creationContext of creationContexts)
                                                {
                                                    for (let subGenerator of creationContext.Metadata[TSGeneratorPackageKey.SubGenerators])
                                                    {
                                                        let testContext = new TestContext(new GeneratorContext(GeneratorPath(creationContext, subGenerator[SubGeneratorSettingKey.Name])));

                                                        await doesNotReject(
                                                            async () => testContext.ExecuteGenerator(
                                                                undefined,
                                                                {
                                                                    cwd: tempDir.FullName
                                                                }));
                                                    }
                                                }
                                            });

                                        test(
                                            "Checking whether the tests can be executed…",
                                            function()
                                            {
                                                this.timeout(8 * 1000);
                                                this.slow(4 * 1000);

                                                let result = spawnNPM(
                                                    [
                                                        "run",
                                                        "test"
                                                    ],
                                                    {
                                                        cwd: generator.destinationPath(),
                                                        stdio: "ignore"
                                                    });

                                                strictEqual(result.status, 0);
                                            });
                                    });

                                suite(
                                    nameof<TSGeneratorGenerator>((generator) => generator.FileMappings),
                                    () =>
                                    {
                                        test(
                                            `Checking whether all tests of ${monoRepo ? "all" : "the"} generated project${monoRepo ? "s" : ""} are being included…`,
                                            async function()
                                            {
                                                this.timeout(4 * 1000);
                                                this.slow(2 * 1000);

                                                for (let creationContext of creationContexts)
                                                {
                                                    let suiteNames = (await SuiteInspector.Fetch(generator.destinationPath(creationContext.Destination, "lib", "tests", "main.test.js"))).Suites;

                                                    for (
                                                        let generatorName of
                                                        [
                                                            GeneratorName.Main,
                                                            ...creationContext.Metadata[TSGeneratorPackageKey.SubGenerators].map(
                                                                (subGenerator) =>
                                                                {
                                                                    return subGenerator[SubGeneratorSettingKey.Name];
                                                                })
                                                        ])
                                                    {
                                                        let namingContext = new NamingContext(generatorName, context.RandomString, join(creationContext.Destination, generator.SourceRoot), true);
                                                        ok(suiteNames.includes(namingContext.GeneratorClassName));
                                                    }
                                                }
                                            });
                                    });
                            });
                    }
                }

                if (monoRepo)
                {
                    suite("Mono-Repo", register);
                }
                else
                {
                    register();
                }
            }
        });
}

/**
 * Joins the specified {@linkcode path} relative to the package root.
 *
 * @param creationContext
 * The creation context for resolving the path.
 *
 * @param path
 * The path to join.
 *
 * @returns
 * The joined path.
 */
export function GeneratorPath(creationContext: ICreationContext<ITSGeneratorSettings, ITSGeneratorPackage>, ...path: string[]): string
{
    return join(creationContext.PackagePath, "lib", "generators", ...path);
}
