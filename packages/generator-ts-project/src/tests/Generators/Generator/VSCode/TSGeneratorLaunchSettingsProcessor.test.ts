import { ok, strictEqual } from "node:assert";
import { GeneratorOptions, GeneratorSettingKey } from "@manuth/extended-yo-generator";
import { TSGeneratorCodeWorkspaceFolder } from "../../../../generators/generator/Components/TSGeneratorCodeWorkspaceFolder.js";
import { ITSGeneratorPackage } from "../../../../generators/generator/Settings/ITSGeneratorPackage.js";
import { ITSGeneratorSettings } from "../../../../generators/generator/Settings/ITSGeneratorSettings.js";
import { SubGeneratorSettingKey } from "../../../../generators/generator/Settings/SubGeneratorSettingKey.js";
import { TSGeneratorComponent } from "../../../../generators/generator/Settings/TSGeneratorComponent.js";
import { TSGeneratorPackageKey } from "../../../../generators/generator/Settings/TSGeneratorPackageKey.js";
import { TSGeneratorGenerator } from "../../../../generators/generator/TSGeneratorGenerator.js";
import { TSGeneratorLaunchSettingsProcessor } from "../../../../generators/generator/VSCode/TSGeneratorLaunchSettingsProcessor.js";
import { TSProjectComponent } from "../../../../Project/Settings/TSProjectComponent.js";
import { TSProjectPackageKey } from "../../../../Project/Settings/TSProjectPackageKey.js";
import { TSProjectSettingKey } from "../../../../Project/Settings/TSProjectSettingKey.js";
import { CodeWorkspaceComponent } from "../../../../VSCode/Components/CodeWorkspaceComponent.js";
import { TestContext } from "../../../TestContext.js";

/**
 * Registers tests for the {@linkcode TSGeneratorLaunchSettingsProcessor} class.
 *
 * @param context
 * The test-context.
 */
export function TSGeneratorLaunchSettingsProcessorTests(context: TestContext<TSGeneratorGenerator>): void
{
    suite(
        nameof(TSGeneratorLaunchSettingsProcessor),
        () =>
        {
            let generator: TSGeneratorGenerator;
            let component: CodeWorkspaceComponent<ITSGeneratorSettings, GeneratorOptions>;
            let processor: TSGeneratorLaunchSettingsProcessor<ITSGeneratorSettings, GeneratorOptions>;

            suiteSetup(
                async function()
                {
                    this.timeout(5 * 60 * 1000);
                    generator = await context.Generator;

                    component = new TSGeneratorCodeWorkspaceFolder(await context.Generator);
                    processor = new TSGeneratorLaunchSettingsProcessor(component);
                });

            setup(
                () =>
                {
                    Object.assign(
                        generator.Settings,
                        {
                            [GeneratorSettingKey.Components]: [
                                TSProjectComponent.VSCode,
                                TSGeneratorComponent.GeneratorExample,
                                TSGeneratorComponent.SubGeneratorExample
                            ],
                            [TSGeneratorPackageKey.SubGenerators]: [
                                {
                                    [SubGeneratorSettingKey.DisplayName]: "A",
                                    [SubGeneratorSettingKey.Name]: "a"
                                },
                                {
                                    [SubGeneratorSettingKey.DisplayName]: "B",
                                    [SubGeneratorSettingKey.Name]: "b"
                                }
                            ]
                        });
                });

            suite(
                nameof<TSGeneratorLaunchSettingsProcessor<any, any>>((processor) => processor.Process),
                () =>
                {
                    test(
                        "Checking whether a launch-configuration for each generator is present…",
                        async function()
                        {
                            this.timeout(1 * 1000);
                            this.slow(0.5 * 1000);
                            let launchSettings = await processor.Process(await component.Source.GetLaunchMetadata() as any);
                            let debugConfigs = launchSettings.configurations ?? [];

                            ok(
                                debugConfigs.some(
                                    (debugConfig) =>
                                    {
                                        return debugConfig.name === "Launch Yeoman";
                                    }));

                            ok(
                                generator.Settings[TSGeneratorPackageKey.SubGenerators]?.every(
                                    (subGeneratorOptions) =>
                                    {
                                        return debugConfigs.some(
                                            (debugConfig) =>
                                            {
                                                return debugConfig.name.includes(`${subGeneratorOptions[SubGeneratorSettingKey.DisplayName]} generator`);
                                            });
                                    }));
                        });

                    test(
                        "Checking whether generator configurations are created for mono repos properly…",
                        async () =>
                        {
                            generator.Settings[GeneratorSettingKey.Components] = [
                                TSProjectComponent.MonoRepo,
                                TSGeneratorComponent.SubGeneratorExample
                            ];

                            generator.Settings[TSProjectSettingKey.Workspaces] = [
                                {
                                    [TSProjectPackageKey.Destination]: "./packages/hello",
                                    [TSProjectPackageKey.DisplayName]: "Hello",
                                    [TSProjectPackageKey.Name]: "hello",
                                    [TSProjectPackageKey.Description]: "",
                                    [TSGeneratorPackageKey.SubGenerators]: [
                                        {
                                            [SubGeneratorSettingKey.DisplayName]: "Theme",
                                            [SubGeneratorSettingKey.Name]: "theme"
                                        },
                                        {
                                            [SubGeneratorSettingKey.DisplayName]: "Extension",
                                            [SubGeneratorSettingKey.Name]: "extension"
                                        }
                                    ]
                                },
                                {
                                    [TSProjectPackageKey.Destination]: "./packages/world",
                                    [TSProjectPackageKey.DisplayName]: "World",
                                    [TSProjectPackageKey.Name]: "world",
                                    [TSProjectPackageKey.Description]: "",
                                    [TSGeneratorPackageKey.SubGenerators]: [
                                        {
                                            [SubGeneratorSettingKey.DisplayName]: "Theme",
                                            [SubGeneratorSettingKey.Name]: "theme"
                                        },
                                        {
                                            [SubGeneratorSettingKey.DisplayName]: "Extension",
                                            [SubGeneratorSettingKey.Name]: "extension"
                                        }
                                    ]
                                }
                            ] as ITSGeneratorPackage[];

                            let launchSettings = await processor.Process(await component.Source.GetLaunchMetadata() as any);
                            let debugConfigs = launchSettings.configurations ?? [];

                            ok(
                                (generator.Settings[TSProjectSettingKey.Workspaces] ?? []).every(
                                    (workspace) =>
                                    {
                                        return workspace[TSGeneratorPackageKey.SubGenerators].every(
                                            (subGenerator) =>
                                            {
                                                let name = subGenerator[SubGeneratorSettingKey.DisplayName];

                                                return debugConfigs.some(
                                                    (debugConfig) =>
                                                    {
                                                        return debugConfig.name.includes(`${name} generator`);
                                                    });
                                            });
                                    }));

                            strictEqual(debugConfigs.length, new Set(debugConfigs.map((config) => config.name)).size);
                        });
                });
        });
}
