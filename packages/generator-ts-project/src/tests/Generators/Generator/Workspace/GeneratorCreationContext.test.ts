import { notStrictEqual, ok, strictEqual } from "node:assert";
import { isAbsolute, relative } from "node:path";
import { FileMapping, GeneratorOptions, GeneratorSettingKey, IFileMapping } from "@manuth/extended-yo-generator";
import { FileMappingTester } from "@manuth/extended-yo-generator-test";
import { Package } from "@manuth/package-json-editor";
import { TempDirectory } from "@manuth/temp-files";
import fs from "fs-extra";
import { createSandbox, SinonSandbox, SinonStub } from "sinon";
import { ITSGeneratorPackage } from "../../../../generators/generator/Settings/ITSGeneratorPackage.js";
import { ITSGeneratorSettings } from "../../../../generators/generator/Settings/ITSGeneratorSettings.js";
import { TSGeneratorGenerator } from "../../../../generators/generator/TSGeneratorGenerator.js";
import { GeneratorCreationContext } from "../../../../generators/generator/Workspace/GeneratorCreationContext.js";
import { TSProjectComponent } from "../../../../Project/Settings/TSProjectComponent.js";
import { TSProjectPackageKey } from "../../../../Project/Settings/TSProjectPackageKey.js";
import { TSProjectSettingKey } from "../../../../Project/Settings/TSProjectSettingKey.js";
import { ICreationContextSettings } from "../../../../Project/Workspace/ICreationContextSettings.js";
import { ProjectCreationContext } from "../../../../Project/Workspace/ProjectCreationContext.js";
import { RootSettings } from "../../../../Project/Workspace/RootSettings.js";
import { WorkspaceSettings } from "../../../../Project/Workspace/WorkspaceSettings.js";
import { TestContext } from "../../../TestContext.js";

const { writeJSON } = fs;

/**
 * Registers tests for the {@linkcode GeneratorCreationContext} class.
 *
 * @param context
 * The test context.
 */
export function GeneratorCreationContextTests(context: TestContext<TSGeneratorGenerator>): void
{
    /**
     * Provides a test implementation of the {@linkcode GeneratorCreationContext} class.
     */
    class TestGeneratorCreationContext extends GeneratorCreationContext<ITSGeneratorSettings, GeneratorOptions, ITSGeneratorPackage>
    {
        /**
         * @inheritdoc
         */
        public override get ReadmeTemplatePath(): string
        {
            return super.ReadmeTemplatePath;
        }

        /**
         * @inheritdoc
         */
        public override get ReadmeFileMapping(): IFileMapping<ITSGeneratorSettings, GeneratorOptions>
        {
            return super.ReadmeFileMapping;
        }

        /**
         * @inheritdoc
         */
        public override get WorkspaceFileMappings(): Array<IFileMapping<ITSGeneratorSettings, GeneratorOptions>>
        {
            return super.WorkspaceFileMappings;
        }
    }

    suite(
        nameof(GeneratorCreationContext),
        () =>
        {
            let sandbox: SinonSandbox;
            let isMonoRepo = false;
            let generator: TSGeneratorGenerator;
            let settings: ITSGeneratorSettings;
            let creationContext: TestGeneratorCreationContext;

            suiteSetup(
                async function()
                {
                    this.timeout(5 * 60 * 1000);
                    generator = await context.Generator;
                    creationContext = new TestGeneratorCreationContext(generator);
                });

            setup(
                () =>
                {
                    sandbox = createSandbox();

                    let stub: SinonStub;

                    let replacement = (): string[] =>
                    {
                        stub.restore();
                        let result = generator.Settings[GeneratorSettingKey.Components] ?? [];
                        stub.get(replacement);

                        if (isMonoRepo)
                        {
                            result.push(TSProjectComponent.MonoRepo);
                        }

                        return result;
                    };

                    stub = sandbox.stub(generator.Settings, GeneratorSettingKey.Components).get(replacement);

                    generator.Settings[TSProjectSettingKey.Workspaces] = [
                        {
                            [TSProjectPackageKey.Destination]: "./packages/test"
                        } as ITSGeneratorPackage
                    ];

                    settings = {
                        ...generator.Settings
                    };
                });

            teardown(
                () =>
                {
                    sandbox.restore();
                });

            suite(
                nameof<TestGeneratorCreationContext>((context) => context.ReadmeTemplatePath),
                () =>
                {
                    test(
                        "Checking whether generator related readme files are generated in workspaces…",
                        async () =>
                        {
                            let settings: ICreationContextSettings<ITSGeneratorSettings, ITSGeneratorPackage> = {} as any;

                            let setups = [
                                () =>
                                {
                                    settings = new RootSettings();
                                },
                                () =>
                                {
                                    isMonoRepo = true;
                                    settings = new WorkspaceSettings(generator, 0);
                                }
                            ];

                            generator.Settings[GeneratorSettingKey.Components] ??= [];

                            for (let setup of setups)
                            {
                                setup();
                                let creationContext = new TestGeneratorCreationContext(generator, settings);
                                let tester = new FileMappingTester<TSGeneratorGenerator, ITSGeneratorSettings, GeneratorOptions, IFileMapping<ITSGeneratorSettings, GeneratorOptions>>(generator, creationContext.ReadmeFileMapping);
                                let content = await tester.ReadSource();
                                ok(/\bGenerator\b/.test(content));
                            }
                        });

                    test(
                        "Checking whether a generic `README.md` template is returned for roots of mono repos…",
                        () =>
                        {
                            isMonoRepo = true;

                            ok(
                                new ProjectCreationContext(generator).FileMappings.some(
                                    (fileMapping) =>
                                    {
                                        return new FileMapping(generator, fileMapping).Source === creationContext.ReadmeTemplatePath;
                                    }));
                        });
                });

            suite(
                nameof<TestGeneratorCreationContext>((context) => context.WorkspaceFileMappings),
                () =>
                {
                    test(
                        "Checking whether workspace files are nested in their corresponding workspace…",
                        () =>
                        {
                            isMonoRepo = true;

                            for (let settings of [
                                new RootSettings<ITSGeneratorSettings, ITSGeneratorPackage>(),
                                new WorkspaceSettings<ITSGeneratorSettings, ITSGeneratorPackage>(generator, 0)
                            ])
                            {
                                let creationContext = new TestGeneratorCreationContext(generator, settings);

                                for (let fileMappingOptions of creationContext.FileMappings)
                                {
                                    let fileMapping = new FileMapping(generator, fileMappingOptions);

                                    let relativePath = relative(
                                        generator.destinationPath(creationContext.Destination),
                                        generator.destinationPath(fileMapping.Destination));

                                    ok(!isAbsolute(relativePath) && !relativePath.startsWith("../"));
                                }
                            }
                        });
                });

            suite(
                nameof<TestGeneratorCreationContext>((context) => context.GetSuggestedModuleName),
                () =>
                {
                    let tempDir: TempDirectory;
                    let prefix = "generator-";
                    let displayName: string;
                    let id: string;
                    let prefixedID: string;

                    suiteSetup(
                        async function()
                        {
                            tempDir = new TempDirectory();
                            displayName = "ThisIsATest";
                            id = "this-is-a-test";
                            prefixedID = `${prefix}${id}`;
                        });

                    suiteTeardown(
                        () =>
                        {
                            tempDir.Dispose();
                        });

                    setup(
                        () =>
                        {
                            settings = {
                                ...settings,
                                [TSProjectPackageKey.Destination]: tempDir.FullName
                            };
                        });

                    test(
                        `Checking whether \`${prefix}\`-prefix is added properly…`,
                        async () =>
                        {
                            for (let name of
                                [
                                    `Generator${displayName}`,
                                    `${displayName}Generator`,
                                    `Generator${displayName}Generator`
                                ])
                            {
                                settings[TSProjectPackageKey.DisplayName] = name;
                                strictEqual(await creationContext.GetSuggestedModuleName(settings), prefixedID);
                            }
                        });

                    test(
                        `Checking whether the \`${prefix}\`-prefix is added to scoped module names properly…`,
                        async () =>
                        {
                            let expected = "@test/generator-this-is-a-test";
                            let npmPackage = new Package(tempDir.MakePath(Package.FileName), {});

                            for (let name of
                                [
                                    `@test/generator-${id}`,
                                    `@test/${id}`,
                                    `@test/${id}-generator`,
                                    `@test/generator-${id}-generator`
                                ])
                            {
                                npmPackage.Name = name;
                                await writeJSON(npmPackage.FileName, npmPackage.ToJSON());
                                strictEqual(await creationContext.GetSuggestedModuleName(settings), expected);
                            }
                        });
                });

            suite(
                nameof<TestGeneratorCreationContext>((context) => context.ValidateModuleName),
                () =>
                {
                    /**
                     * Asserts the result of the module name validation.
                     *
                     * @param assertions
                     * The assertions to make.
                     */
                    async function AssertValidation(assertions: Array<[name: string, valid: boolean]>): Promise<void>
                    {
                        for (let assertion of assertions)
                        {
                            if (assertion[1])
                            {
                                strictEqual(await creationContext.ValidateModuleName(assertion[0], settings), true);
                            }
                            else
                            {
                                notStrictEqual(await creationContext.ValidateModuleName(assertion[0], settings), true);
                            }
                        }
                    }

                    test(
                        "Checking whether non-scoped module names are validated properly…",
                        async () =>
                        {
                            await AssertValidation(
                                [
                                    ["InvalidName", false],
                                    ["generator-test", true],
                                    ["test", false],
                                    ["test-generator", false],
                                    ["generator-test-generator", true]
                                ]);
                        });

                    test(
                        "Checking whether scoped module names are validated properly…",
                        async () =>
                        {
                            await AssertValidation(
                                [
                                    ["@invalid/InvalidName", false],
                                    ["@Invalid/invalid-name", false],
                                    ["@test/generator-test", true],
                                    ["@test/test-generator", false],
                                    ["@test/generator-test-generator", true]
                                ]);
                        });
                });
        });
}
