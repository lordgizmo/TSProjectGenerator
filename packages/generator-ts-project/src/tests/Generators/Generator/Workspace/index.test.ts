import { basename } from "node:path";
import { GeneratorCreationContextTests } from "./GeneratorCreationContext.test.js";
import { TSGeneratorGenerator } from "../../../../generators/generator/TSGeneratorGenerator.js";
import { TestContext } from "../../../TestContext.js";

/**
 * Registers tests for workspace components.
 *
 * @param context
 * The test context.
 */
export function WorkspaceTests(context: TestContext<TSGeneratorGenerator>): void
{
    suite(
        basename(new URL(".", import.meta.url).pathname),
        () =>
        {
            GeneratorCreationContextTests(context);
        });
}
