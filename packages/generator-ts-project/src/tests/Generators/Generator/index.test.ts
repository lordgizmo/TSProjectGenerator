import { basename } from "node:path";
import { ComponentTests } from "./Components/index.test.js";
import { DependencyTests } from "./Dependencies/index.test.js";
import { FileMappingTests } from "./FileMappings/index.test.js";
import { TSGeneratorGeneratorTests } from "./TSGeneratorGenerator.test.js";
import { VSCodeTests } from "./VSCode/index.test.js";
import { WorkspaceTests } from "./Workspace/index.test.js";
import { TSGeneratorGenerator } from "../../../generators/generator/TSGeneratorGenerator.js";
import { TestContext } from "../../TestContext.js";

/**
 * Registers tests for the {@linkcode TSGeneratorGenerator}.
 *
 * @param context
 * The test-context.
 */
export function GeneratorTests(context: TestContext<TSGeneratorGenerator>): void
{
    suite(
        basename(new URL(".", import.meta.url).pathname),
        () =>
        {
            DependencyTests();
            FileMappingTests(context);
            WorkspaceTests(context);
            VSCodeTests(context);
            ComponentTests(context);
            TSGeneratorGeneratorTests(context);
        });
}
