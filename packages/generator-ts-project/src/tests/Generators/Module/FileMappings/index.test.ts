import { basename } from "node:path";
import { TSModulePackageFileMappingTests } from "./TSModulePackageFileMapping.test.js";
import { TSModuleGenerator } from "../../../../generators/module/TSModuleGenerator.js";
import { TestContext } from "../../../TestContext.js";

/**
 * Registers tests for file-mappings for the {@linkcode TSModuleGenerator}.
 *
 * @param context
 * The test-context.
 */
export function FileMappingTests(context: TestContext<TSModuleGenerator>): void
{
    suite(
        basename(new URL(".", import.meta.url).pathname),
        () =>
        {
            TSModulePackageFileMappingTests(context);
        });
}
