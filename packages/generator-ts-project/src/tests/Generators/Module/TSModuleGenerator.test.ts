import { doesNotReject, doesNotThrow, strictEqual } from "node:assert";
import { createRequire } from "node:module";
import { dirname, join } from "node:path";
import { fileURLToPath } from "node:url";
import { GeneratorSettingKey } from "@manuth/extended-yo-generator";
import { PackageType } from "@manuth/package-json-editor";
import fs from "fs-extra";
import { packageDirectory } from "pkg-dir";
import RandExp from "randexp";
import { TSModuleGenerator } from "../../../generators/module/TSModuleGenerator.js";
import { ITSProjectPackage } from "../../../Project/Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../../../Project/Settings/ITSProjectSettings.js";
import { TSProjectComponent } from "../../../Project/Settings/TSProjectComponent.js";
import { TSProjectPackageKey } from "../../../Project/Settings/TSProjectPackageKey.js";
import { TSProjectSettingKey } from "../../../Project/Settings/TSProjectSettingKey.js";
import { ICreationContext } from "../../../Project/Workspace/ICreationContext.js";
import { TestContext } from "../../TestContext.js";
import { spawnNPM } from "../../Utilities.js";

const { remove } = fs;
const { randexp } = RandExp;

/**
 * Registers tests for the {@linkcode TSModuleGenerator}.
 *
 * @param context
 * The test-context.
 */
export function TSModuleGeneratorTests(context: TestContext<TSModuleGenerator>): void
{
    suite(
        nameof(TSModuleGenerator),
        () =>
        {
            let workspaceRoot: string;
            context.RegisterWorkingDirRestorer();

            suiteSetup(
                async () =>
                {
                    workspaceRoot = await packageDirectory(
                        {
                            cwd: dirname(
                                await packageDirectory(
                                    {
                                        cwd: fileURLToPath(new URL(".", import.meta.url))
                                    }) as string)
                        }) as string;
                });

            for (let monoRepo of [false, true])
            {
                /**
                 * Registers the tests.
                 */
                function register(): void
                {
                    for (let esModule of [true, false])
                    {
                        let moduleType = esModule ? nameof(PackageType.ESModule) : nameof(PackageType.CommonJS);

                        suite(moduleType,
                            () =>
                            {
                                let generator: TSModuleGenerator;
                                let creationContexts: Array<ICreationContext<ITSProjectSettings, ITSProjectPackage>>;
                                let moduleNames: string[];

                                suiteSetup(
                                    async function()
                                    {
                                        this.timeout(10 * 60 * 1000);
                                        let runContext = context.ExecuteGenerator();
                                        moduleNames = [];

                                        runContext.withAnswers(
                                            {
                                                [GeneratorSettingKey.Components]: monoRepo ? [TSProjectComponent.MonoRepo] : [],
                                                [TSProjectSettingKey.ESModule]: esModule,
                                                [TSProjectSettingKey.Workspaces]: [
                                                    {
                                                        [TSProjectPackageKey.Destination]: "./packages/test-1",
                                                        [TSProjectPackageKey.DisplayName]: "Test 1",
                                                        [TSProjectPackageKey.Name]: "test-1",
                                                        [TSProjectPackageKey.Description]: ""
                                                    },
                                                    {
                                                        [TSProjectPackageKey.Destination]: "./packages/test-2",
                                                        [TSProjectPackageKey.DisplayName]: "Test 2",
                                                        [TSProjectPackageKey.Name]: "test-2",
                                                        [TSProjectPackageKey.Description]: ""
                                                    }
                                                ]
                                            } as ITSProjectSettings);

                                        await runContext;
                                        generator = runContext.generator;

                                        if (monoRepo)
                                        {
                                            creationContexts = generator.WorkspaceCreationContexts;
                                        }
                                        else
                                        {
                                            creationContexts = [generator.PackageCreationContext];
                                        }

                                        spawnNPM(
                                            [
                                                "install",
                                                "--silent"
                                            ],
                                            {
                                                cwd: generator.destinationPath(),
                                                stdio: "ignore"
                                            });

                                        spawnNPM(
                                            [
                                                "run",
                                                "build"
                                            ],
                                            {
                                                cwd: generator.destinationPath(),
                                                stdio: "ignore"
                                            });

                                        let localModuleSpecifiers: string[] = [];

                                        for (let creationContext of creationContexts)
                                        {
                                            let moduleName = randexp(/@ts-module-generator-test\/[a-z]{10}/);
                                            moduleNames.push(moduleName);
                                            localModuleSpecifiers.push(`${moduleName}@file:${generator.destinationPath(creationContext.Destination)}`);
                                        }

                                        spawnNPM(
                                            [
                                                "install",
                                                "--no-save",
                                                ...localModuleSpecifiers
                                            ],
                                            {
                                                cwd: workspaceRoot
                                            });
                                    });

                                suiteTeardown(
                                    async function()
                                    {
                                        this.timeout(1 * 60 * 1000);

                                        for (let moduleName of moduleNames)
                                        {
                                            spawnNPM(
                                                [
                                                    "uninstall",
                                                    "--no-save",
                                                    moduleName
                                                ],
                                                {
                                                    cwd: workspaceRoot
                                                });
                                        }

                                        await remove(join(workspaceRoot, "node_modules", "@ts-module-generator-test"));
                                    });

                                test(
                                    "Checking whether the generated project can be installed and built…",
                                    function()
                                    {
                                        this.timeout(6 * 60 * 1000);
                                        this.slow(3 * 60 * 1000);

                                        let installationResult = spawnNPM(
                                            [
                                                "install",
                                                "--silent"
                                            ],
                                            {
                                                cwd: generator.destinationPath(),
                                                stdio: "ignore"
                                            });

                                        let buildResult = spawnNPM(
                                            [
                                                "run",
                                                "build"
                                            ],
                                            {
                                                cwd: generator.destinationPath(),
                                                stdio: "ignore"
                                            });

                                        strictEqual(installationResult.status, 0);
                                        strictEqual(buildResult.status, 0);
                                    });

                                if (esModule)
                                {
                                    test(
                                        `Checking whether the generated module${monoRepo ? "s" : ""} can be imported…`,
                                        async () =>
                                        {
                                            for (let moduleName of moduleNames)
                                            {
                                                await doesNotReject(() => import(moduleName));
                                            }
                                        });
                                }
                                else
                                {
                                    test(
                                        `Checking whether the generated \`${moduleType}\` module${monoRepo ? "s" : ""} can be imported…`,
                                        () =>
                                        {
                                            for (let moduleName of moduleNames)
                                            {
                                                doesNotThrow(() => createRequire(import.meta.url)(moduleName));
                                            }
                                        });
                                }

                                test(
                                    "Checking whether the tests can be executed…",
                                    function()
                                    {
                                        this.timeout(30 * 1000);
                                        this.slow(15 * 1000);

                                        let result = spawnNPM(
                                            [
                                                "run",
                                                "test"
                                            ],
                                            {
                                                cwd: generator.destinationPath(),
                                                stdio: "ignore"
                                            });

                                        strictEqual(
                                            result.status,
                                            0);
                                    });
                            });
                    }
                }

                if (monoRepo)
                {
                    suite("Mono-Repo", register);
                }
                else
                {
                    register();
                }
            }
        });
}
