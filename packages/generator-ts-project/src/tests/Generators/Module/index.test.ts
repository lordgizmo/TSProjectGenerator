import { basename } from "node:path";
import { FileMappingTests } from "./FileMappings/index.test.js";
import { TSModuleGeneratorTests } from "./TSModuleGenerator.test.js";
import { TSModuleGenerator } from "../../../generators/module/TSModuleGenerator.js";
import { TestContext } from "../../TestContext.js";

/**
 * Registers tests for the {@linkcode TSModuleGenerator}.
 *
 * @param context
 * The test-context.
 */
export function ModuleTests(context: TestContext<TSModuleGenerator>): void
{
    suite(
        basename(new URL(".", import.meta.url).pathname),
        () =>
        {
            FileMappingTests(context);
            TSModuleGeneratorTests(context);
        });
}
