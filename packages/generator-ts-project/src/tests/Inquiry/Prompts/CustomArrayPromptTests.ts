import { ok, strictEqual } from "node:assert";
import inquirer, { Answers, DistinctQuestion, Question } from "inquirer";
import { IArrayQuestionOptions } from "../../../Inquiry/Prompts/IArrayQuestionOptions.js";
import { PathPrompt } from "../../../Inquiry/Prompts/PathPrompt.js";
import { SubGeneratorPrompt } from "../../../Inquiry/Prompts/SubGeneratorPrompt.js";
import { TestContext } from "../../TestContext.js";

/**
 * Registers tests for a custom array prompt.
 *
 * @param prompt
 * The prompt to test.
 *
 * @param typeName
 * The name of the prompt type.
 *
 * @param question
 * The options for the question to ask.
 *
 * @param resourceName
 * The name of the resources used in question titles.
 *
 * @param keyNames
 * The name of the keys expected in the result.
 *
 * @param answers
 * The pre-defined answers.
 */
export function CustomArrayPromptTests(prompt: inquirer.prompts.PromptConstructor, typeName: string, question: () => Question, resourceName: string, keyNames: string[], answers?: Answers): void
{
    /**
     * Represents the prompt to test.
     */
    class TestPrompt extends prompt
    {
        /**
         * @inheritdoc
         *
         * @returns
         * The result of the prompt.
         */
        public override run(): Promise<any>
        {
            return super.run();
        }
    }

    let context = TestContext.Default;
    let repeat: (answers: any) => boolean;
    let length: number;
    let questions: DistinctQuestion[];
    const testKey = "test";

    setup(
        () =>
        {
            length = 0;
            context.RegisterTestPrompt(inquirer.prompt);
            context.RegisterTestPrompt(inquirer.prompt, "input");
            context.RegisterTestPrompt(inquirer.prompt, "confirm");
            context.RegisterTestPrompt(inquirer.prompt, PathPrompt.TypeName);
            context.RegisterTestPrompt(inquirer.prompt, SubGeneratorPrompt.TypeName);
            inquirer.registerPrompt(typeName, TestPrompt);
            repeat = (answers) => (answers[testKey] as any[]).length < length;

            questions = [
                {
                    ...question(),
                    type: typeName as any,
                    defaultRepeat: (answers) => repeat(answers),
                    name: testKey,
                    promptTypes: {
                        ...inquirer.prompt.prompts
                    }
                } as IArrayQuestionOptions
            ] as DistinctQuestion[];
        });

    teardown(
        () =>
        {
            inquirer.restoreDefaultPrompts();
        });

    suite(
        nameof<TestPrompt>((prompt) => prompt.run),
        () =>
        {
            test(
                `Checking whether ${resourceName} can be prompted…`,
                async function()
                {
                    this.timeout(4 * 1000);
                    this.slow(2 * 1000);
                    let result = await inquirer.prompt(questions, answers);
                    let value = result[testKey] as any[];

                    for (let item of value)
                    {
                        for (let key of keyNames)
                        {
                            ok(key in item);
                        }
                    }
                });

            test(
                `Checking whether any number of ${resourceName} can be prompted…`,
                async function()
                {
                    this.timeout(4 * 1000);
                    this.slow(2 * 1000);
                    length = 3;
                    let result = await inquirer.prompt(questions, answers);
                    let value = result[testKey] as any[];
                    strictEqual(value.length, length);
                });
        });
}
