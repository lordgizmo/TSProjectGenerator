import { GeneratorSettingKey } from "@manuth/extended-yo-generator";
import { CustomArrayPromptTests } from "./CustomArrayPromptTests.js";
import { TSGeneratorComponent } from "../../../generators/generator/Settings/TSGeneratorComponent.js";
import { TSGeneratorPackageKey } from "../../../generators/generator/Settings/TSGeneratorPackageKey.js";
import { TSGeneratorGenerator } from "../../../generators/generator/TSGeneratorGenerator.js";
import { GeneratorWorkspacePrompt } from "../../../Inquiry/Prompts/GeneratorWorkspacePrompt.js";
import { WorkspacePrompt } from "../../../Inquiry/Prompts/WorkspacePrompt.js";
import { TSProjectPackageKey } from "../../../Project/Settings/TSProjectPackageKey.js";
import { TestContext } from "../../TestContext.js";

/**
 * Registers tests for the {@linkcode GeneratorWorkspacePrompt} class.
 *
 * @param context
 * The test context.
 */
export function GeneratorWorkspacePromptTests(context: TestContext<TSGeneratorGenerator>): void
{
    suite(
        nameof(GeneratorWorkspacePrompt),
        () =>
        {
            let generator: TSGeneratorGenerator;

            suiteSetup(
                async function()
                {
                    this.timeout(5 * 60 * 1000);
                    generator = await context.Generator;
                });

            CustomArrayPromptTests(
                GeneratorWorkspacePrompt,
                WorkspacePrompt.TypeName,
                () =>
                {
                    return {
                        generator
                    } as any;
                },
                "generator workspaces",
                [
                    TSProjectPackageKey.Destination,
                    TSProjectPackageKey.DisplayName,
                    TSProjectPackageKey.Name,
                    TSProjectPackageKey.Description,
                    TSGeneratorPackageKey.SubGenerators
                ],
                {
                    [GeneratorSettingKey.Components]: [TSGeneratorComponent.SubGeneratorExample]
                });
        });
}
