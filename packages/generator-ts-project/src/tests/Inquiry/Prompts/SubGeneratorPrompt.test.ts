import { CustomArrayPromptTests } from "./CustomArrayPromptTests.js";
import { ISubGenerator } from "../../../generators/generator/Settings/ISubGenerator.js";
import { SubGeneratorPrompt } from "../../../Inquiry/Prompts/SubGeneratorPrompt.js";

/**
 * Provides tests for the {@linkcode SubGeneratorPrompt} class.
 */
export function SubGeneratorPromptTests(): void
{
    suite(
        nameof(SubGeneratorPrompt),
        () =>
        {
            CustomArrayPromptTests(
                SubGeneratorPrompt,
                SubGeneratorPrompt.TypeName,
                () => ({}),
                "sub-generators",
                [
                    nameof<ISubGenerator>((g) => g.name),
                    nameof<ISubGenerator>((g) => g.displayName)
                ]);
        });
}
