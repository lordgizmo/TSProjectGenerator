import { Question } from "inquirer";
import { CustomArrayPromptTests } from "./CustomArrayPromptTests.js";
import { WorkspacePrompt } from "../../../Inquiry/Prompts/WorkspacePrompt.js";
import { TSProjectPackageKey } from "../../../Project/Settings/TSProjectPackageKey.js";
import { TSProjectGenerator } from "../../../Project/TSProjectGenerator.js";
import { TestContext } from "../../TestContext.js";

/**
 * Provides tests for the {@linkcode WorkspacePrompt} class.
 *
 * @param context
 * The test context.
 */
export function WorkspacePromptTests(context: TestContext<TSProjectGenerator>): void
{
    suite(
        nameof(WorkspacePrompt),
        () =>
        {
            let generator: TSProjectGenerator;

            suiteSetup(
                async function()
                {
                    this.timeout(5 * 60 * 1000);
                    generator = await context.Generator;
                });

            CustomArrayPromptTests(
                WorkspacePrompt,
                WorkspacePrompt.TypeName,
                () =>
                {
                    return {
                        generator
                    } as Question;
                },
                "workspaces",
                [
                    TSProjectPackageKey.Destination,
                    TSProjectPackageKey.DisplayName,
                    TSProjectPackageKey.Name,
                    TSProjectPackageKey.Description
                ]);
        });
}
