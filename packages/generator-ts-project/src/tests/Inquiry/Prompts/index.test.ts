import { basename } from "node:path";
import { ArrayPromptTests } from "./ArrayPrompt.test.js";
import { GeneratorWorkspacePromptTests } from "./GeneratorWorkspacePrompt.test.js";
import { NestedPromptTests } from "./NestedPrompt.test.js";
import { PathPromptTests } from "./PathPrompt.test.js";
import { PromptBaseTests } from "./PromptBase.test.js";
import { QuestionSetPromptTests } from "./QuestionSetPrompt.test.js";
import { SubGeneratorPromptTests } from "./SubGeneratorPrompt.test.js";
import { SuspendablePromptTests } from "./SuspendablePrompt.test.js";
import { WorkspacePromptTests } from "./WorkspacePrompt.test.js";
import { TSGeneratorGenerator } from "../../../generators/generator/TSGeneratorGenerator.js";
import { TSProjectGenerator } from "../../../Project/TSProjectGenerator.js";
import { TestContext } from "../../TestContext.js";

/**
 * Registers tests for prompts.
 *
 * @param context
 * The test context.
 *
 * @param generatorContext
 * The generator test context.
 */
export function PromptTests(context: TestContext<TSProjectGenerator>, generatorContext: TestContext<TSGeneratorGenerator>): void
{
    suite(
        basename(new URL(".", import.meta.url).pathname),
        () =>
        {
            PromptBaseTests();
            SuspendablePromptTests();
            NestedPromptTests();
            QuestionSetPromptTests();
            ArrayPromptTests();
            SubGeneratorPromptTests();
            WorkspacePromptTests(context);
            GeneratorWorkspacePromptTests(generatorContext);
            PathPromptTests();
        });
}
