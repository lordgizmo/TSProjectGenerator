import { basename } from "node:path";
import { PromptTests } from "./Prompts/index.test.js";
import { TSGeneratorGenerator } from "../../generators/generator/TSGeneratorGenerator.js";
import { TSProjectGenerator } from "../../Project/TSProjectGenerator.js";
import { TestContext } from "../TestContext.js";

/**
 * Registers tests for inquiry-components.
 *
 * @param context
 * The test context.
 *
 * @param generatorContext
 * The generator test context.
 */
export function InquiryTests(context: TestContext<TSProjectGenerator>, generatorContext: TestContext<TSGeneratorGenerator>): void
{
    suite(
        basename(new URL(".", import.meta.url).pathname),
        () =>
        {
            PromptTests(context, generatorContext);
        });
}
