import Mocha from "mocha";
import { SuiteList } from "./SuiteList.js";

/**
 * Provides the functionality to inspect a {@linkcode SuiteList}.
 */
export class SuiteInspector
{
    /**
     * The suite list to inspect.
     */
    private suiteList: SuiteList;

    /**
     * Gets the suite list to inspect.
     */
    public get SuiteList(): SuiteList
    {
        return this.suiteList;
    }

    /**
     * Initializes a new instance of the {@linkcode SuiteInspector} class.
     *
     * @param suiteList
     * The suite list to inspect.
     */
    protected constructor(suiteList: SuiteList)
    {
        this.suiteList = suiteList;
    }

    /**
     * Gets the names of all suites in the list.
     */
    public get Suites(): string[]
    {
        return this.GetSuites(this.SuiteList);
    }

    /**
     * Gets the number of suites in the list.
     */
    public get SuiteCount(): number
    {
        return this.Suites.length;
    }

    /**
     * Gets the names of all tests in the list.
     */
    public get Tests(): string[]
    {
        return this.GetTests(this.SuiteList);
    }

    /**
     * Gets the number of tests in the list.
     */
    public get TestCount(): number
    {
        return this.Tests.length;
    }

    /**
     * Gets the names of all nodes in the list.
     */
    public get Nodes(): string[]
    {
        return this.GetNodes(this.SuiteList);
    }

    /**
     * Gets the number of nodes in the list.
     */
    public get NodeCount(): number
    {
        return this.Nodes.length;
    }

    /**
     * Fetches the suites and tests from the file with the specified {@linkcode fileName}.
     *
     * @param fileName
     * The name of the file to fetch the mocha suite from.
     *
     * @returns
     * A list of all suites and tests exposed by the file with the specified {@linkcode fileName}.
     */
    public static async Fetch(fileName: string): Promise<SuiteInspector>
    {
        let suiteList: SuiteList = {};

        let mocha = new Mocha(
            {
                ui: "tdd",
                dryRun: true,
                reporter: class extends Mocha.reporters.Base
                {
                    /**
                     * @inheritdoc
                     *
                     * @param runner
                     * The mocha runner.
                     *
                     * @param options
                     * The options which were passed to mocha.
                     */
                    public constructor(runner: Mocha.Runner, options: Mocha.MochaOptions)
                    {
                        super(runner, options);

                        runner.on(
                            "test",
                            () =>
                            {
                                let currentSuite: SuiteList = suiteList;
                                let currentTestList: string[] = [];
                                let path = runner.suite.titlePath();

                                for (let i = 0; i < path.length; i++)
                                {
                                    let pathPart = path[i];

                                    if (!(pathPart in currentSuite))
                                    {
                                        if (i < path.length - 1)
                                        {
                                            currentSuite[pathPart] = {};
                                        }
                                        else
                                        {
                                            currentSuite[pathPart] = [];
                                        }
                                    }

                                    if (i === path.length - 1)
                                    {
                                        currentTestList = currentSuite[pathPart] as string[];
                                    }
                                }

                                currentTestList.push(runner.test?.title ?? "");
                            });
                    }
                }
            });

        mocha.addFile(fileName);
        await mocha.loadFilesAsync();
        await new Promise((resolve) => mocha.run((resolve)));
        return new SuiteInspector(suiteList);
    }

    /**
     * Gets the names of all suites inside the specified {@linkcode suiteList}.
     *
     * @param suiteList
     * The suite list to get the suite names from.
     *
     * @returns
     * The names of all suites inside the specified {@linkcode suiteList}.
     */
    protected GetSuites(suiteList: SuiteList): string[]
    {
        return this.GetNodes(suiteList, true, false);
    }

    /**
     * Gets the names of all tests inside the specified {@linkcode suiteList}.
     *
     * @param suiteList
     * The suite list to get the test names from.
     *
     * @returns
     * The names of all tests inside the specified {@linkcode suiteList}.
     */
    protected GetTests(suiteList: SuiteList): string[]
    {
        return this.GetNodes(suiteList, false, true);
    }

    /**
     * Gets the names of all nodes inside the specified {@linkcode suiteList}.
     *
     * @param suiteList
     * The suite list to get the node names from.
     *
     * @param includeSuites
     * A value indicating whether the names of all suites should be included.
     *
     * @param includeTests
     * A value indicating whether the name of all tests should be included.
     *
     * @returns
     * The names of all nodes inside the specified {@linkcode suiteList}.
     */
    protected GetNodes(suiteList: SuiteList, includeSuites = true, includeTests = true): string[]
    {
        return Object.keys(suiteList).flatMap(
            (suiteName) =>
            {
                let suitePart = suiteList[suiteName];
                let result = includeSuites ? [suiteName] : [];

                if (Array.isArray(suitePart))
                {
                    return [
                        ...result,
                        ...(includeTests ? suitePart : [])
                    ];
                }
                else
                {
                    return [
                        ...result,
                        ...this.GetNodes(suitePart, includeSuites, includeTests)
                    ];
                }
            });
    }
}
