/**
 * Represents a list of mocha suites.
 */
export type SuiteList = { [suiteName: string]: string[] | SuiteList };
