import { strictEqual } from "node:assert";
import { DependencyCollectionTester } from "@manuth/generator-ts-project-test";
import { DependencyCollection, Dictionary } from "@manuth/package-json-editor";
import { PickKeys } from "ts-essentials";
import { BuildDependencies } from "../../../NPM/Dependencies/BuildDependencies.js";
import { LintEssentials } from "../../../NPM/Dependencies/LintEssentials.js";
import { MergedDependencyCollection } from "../../../NPM/Dependencies/MergedDependencyCollection.js";
import { TestingDependencies } from "../../../NPM/Dependencies/TestingDependencies.js";

/**
 * Registers tests for the {@linkcode MergedDependencyCollection} class.
 */
export function MergedDependencyCollectionTests(): void
{
    suite(
        nameof(MergedDependencyCollection),
        () =>
        {
            /**
             * Provides an implementation of the {@linkcode MergedDependencyCollection} class for testing.
             */
            class TestMergedDependencyCollection extends MergedDependencyCollection
            {
                /**
                 * @inheritdoc
                 */
                protected override get MergedCollections(): DependencyCollection[]
                {
                    return mergedCollections;
                }

                /**
                 * @inheritdoc
                 *
                 * @param key
                 * The key to load the dependencies from.
                 *
                 * @returns
                 * The merged dependencies.
                 */
                public override MergeDependencies(key: PickKeys<DependencyCollection, Dictionary<string, string>>): Dictionary<string, string>
                {
                    return super.MergeDependencies(key);
                }
            }

            let dependencies: TestMergedDependencyCollection;
            let tester: DependencyCollectionTester;
            let mergedCollections: DependencyCollection[];

            setup(
                () =>
                {
                    mergedCollections = [
                        new BuildDependencies(),
                        new LintEssentials(),
                        new TestingDependencies()
                    ];

                    dependencies = new TestMergedDependencyCollection();
                    tester = new DependencyCollectionTester(dependencies);
                });

            suite(
                "General",
                () =>
                {
                    test(
                        "Checking whether dependencies are merged properly…",
                        async () =>
                        {
                            for (let collection of mergedCollections)
                            {
                                await tester.AssertDependencies(collection, true, true);
                            }
                        });
                });

            suite(
                nameof<TestMergedDependencyCollection>((collection) => collection.MergeDependencies),
                () =>
                {
                    test(
                        "Checking whether individual dependency lists can be merged…",
                        () =>
                        {
                            let listNames = [
                                nameof.typed<DependencyCollection>().Dependencies,
                                nameof.typed<DependencyCollection>().DevelopmentDependencies,
                                nameof.typed<DependencyCollection>().PeerDependencies,
                                nameof.typed<DependencyCollection>().OptionalDependencies
                            ];

                            for (let listName of listNames)
                            {
                                let mergedDependencies = dependencies.MergeDependencies(listName);

                                for (let collection of mergedCollections)
                                {
                                    for (let dependency of collection[listName].Keys)
                                    {
                                        strictEqual(mergedDependencies.Get(dependency), collection[listName].Get(dependency));
                                    }
                                }
                            }
                        });
                });
        });
}
