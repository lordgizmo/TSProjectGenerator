import { DependencyCollectionTester } from "@manuth/generator-ts-project-test";
import { TestingDependencies } from "../../../NPM/Dependencies/TestingDependencies.js";

/**
 * Registers tests for the {@}
 */
export function TestingDependencyTests(): void
{
    suite(
        nameof(TestingDependencies),
        () =>
        {
            let dependencies: TestingDependencies;
            let tester: DependencyCollectionTester;

            suiteSetup(
                async function()
                {
                    this.timeout(30 * 1000);
                    dependencies = new TestingDependencies();
                    tester = new DependencyCollectionTester(dependencies);
                });

            suite(
                nameof(TestingDependencies.constructor),
                () =>
                {
                    test(
                        "Checking whether all required dependencies are present…",
                        async function()
                        {
                            this.timeout(4 * 1000);
                            this.slow(2 * 1000);

                            await tester.AssertDependencyNames(
                                {
                                    devDependencies: [
                                        "@types/mocha",
                                        "mocha",
                                        "source-map-support"
                                    ]
                                });
                        });
                });
        });
}
