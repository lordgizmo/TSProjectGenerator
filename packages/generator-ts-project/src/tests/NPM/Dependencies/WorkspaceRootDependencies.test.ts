import { DependencyCollectionTester } from "@manuth/generator-ts-project-test";
import { BuildDependencies } from "../../../NPM/Dependencies/BuildDependencies.js";
import { TestingDependencies } from "../../../NPM/Dependencies/TestingDependencies.js";
import { WorkspaceRootDependencies } from "../../../NPM/Dependencies/WorkspaceRootDependencies.js";

/**
 * Registers tests for the {@linkcode WorkspaceRootDependencies} class.
 */
export function WorkspaceRootDependencyTests(): void
{
    suite(
        nameof(WorkspaceRootDependencies),
        () =>
        {
            let dependencies: WorkspaceRootDependencies;
            let tester: DependencyCollectionTester;

            suiteSetup(
                async function()
                {
                    this.timeout(30 * 1000);
                    dependencies = new WorkspaceRootDependencies();
                    tester = new DependencyCollectionTester(dependencies);
                });

            test(
                "Checking whether all expected dependencies are present…",
                async () =>
                {
                    await tester.AssertDependencies(new BuildDependencies());
                    await tester.AssertDependencies(new TestingDependencies());

                    await tester.AssertDependencyNames(
                        {
                            devDependencies: [
                                "@types/node"
                            ]
                        });
                });
        });
}
