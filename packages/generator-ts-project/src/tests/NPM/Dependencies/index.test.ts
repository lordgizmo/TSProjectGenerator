import { basename } from "node:path";
import { BuildDependencyTests } from "./BuildDependencies.test.js";
import { ESModuleDependencyCollectionTests } from "./ESModuleDependencyCollection.test.js";
import { LintEssentialTests } from "./LintEssentials.test.js";
import { MergedDependencyCollectionTests } from "./MergedDependencyCollection.test.js";
import { MyPackageDependencyCollectionTests } from "./MyPackageDependencyCollection.test.js";
import { TestingDependencyTests } from "./TestingDependencies.test.js";
import { WorkspaceRootDependencyTests } from "./WorkspaceRootDependencies.test.js";

/**
 * Registers tests for npm-packaging dependencies.
 */
export function DependencyTests(): void
{
    suite(
        basename(new URL(".", import.meta.url).pathname),
        () =>
        {
            MyPackageDependencyCollectionTests();
            ESModuleDependencyCollectionTests();
            MergedDependencyCollectionTests();
            BuildDependencyTests();
            TestingDependencyTests();
            LintEssentialTests();
            WorkspaceRootDependencyTests();
        });
}
