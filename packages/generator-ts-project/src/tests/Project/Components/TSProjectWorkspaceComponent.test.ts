import { ok } from "node:assert";
import { GeneratorOptions, GeneratorSettingKey } from "@manuth/extended-yo-generator";
import { TSProjectWorkspaceComponent } from "../../../Project/Components/TSProjectWorkspaceComponent.js";
import { ITSProjectSettings } from "../../../Project/Settings/ITSProjectSettings.js";
import { TSProjectComponent } from "../../../Project/Settings/TSProjectComponent.js";
import { TSProjectGenerator } from "../../../Project/TSProjectGenerator.js";
import { CodeFileMappingCreator } from "../../../VSCode/FileMappings/CodeFileMappingCreator.js";
import { WorkspaceFileCreator } from "../../../VSCode/FileMappings/WorkspaceFileCreator.js";
import { WorkspaceFolderCreator } from "../../../VSCode/FileMappings/WorkspaceFolderCreator.js";
import { TestContext } from "../../TestContext.js";

/**
 * Registers tests for the {@linkcode TSProjectWorkspaceComponent} class.
 *
 * @param context
 * The test context.
 */
export function TSProjectWorkspaceComponentTests(context: TestContext<TSProjectGenerator>): void
{
    suite(
        nameof(TSProjectWorkspaceComponent),
        () =>
        {
            /**
             * Provides an implementation of the {@linkcode TSProjectWorkspaceComponent} class.
             */
            class TestTSProjectWorkspaceComponent extends TSProjectWorkspaceComponent<ITSProjectSettings, GeneratorOptions>
            {
                /**
                 * @inheritdoc
                 */
                public override get FileMappingCreator(): CodeFileMappingCreator<ITSProjectSettings, GeneratorOptions>
                {
                    return super.FileMappingCreator;
                }
            }

            let generator: TSProjectGenerator;
            let component: TestTSProjectWorkspaceComponent;

            suiteSetup(
                async function()
                {
                    this.timeout(5 * 60 * 1000);
                    generator = await context.Generator;
                });

            setup(
                () =>
                {
                    component = new TestTSProjectWorkspaceComponent(generator);
                });

            suite(
                nameof<TestTSProjectWorkspaceComponent>((component) => component.FileMappingCreator),
                () =>
                {
                    test(
                        "Checking whether a `.code-workspace` file is created for mono repos…",
                        () =>
                        {
                            for (let monoRepo of [false, true])
                            {
                                generator.Settings[GeneratorSettingKey.Components] = monoRepo ? [TSProjectComponent.MonoRepo] : [];

                                if (monoRepo)
                                {
                                    ok(component.FileMappingCreator instanceof WorkspaceFileCreator);
                                }
                                else
                                {
                                    ok(component.FileMappingCreator instanceof WorkspaceFolderCreator);
                                }
                            }
                        });
                });
        });
}
