import { basename } from "node:path";
import { LintingComponentTests } from "./LintingComponent.test.js";
import { TSProjectComponentCollectionTests } from "./TSProjectComponentCollection.test.js";
import { TSProjectGeneralCategoryTests } from "./TSProjectGeneralCategory.test.js";
import { TSProjectWorkspaceComponentTests } from "./TSProjectWorkspaceComponent.test.js";
import { TSProjectGenerator } from "../../../Project/TSProjectGenerator.js";
import { TestContext } from "../../TestContext.js";

/**
 * Registers tests for components for {@linkcode TSProjectGenerator}s.
 *
 * @param context
 * The test-context.
 */
export function ComponentTests(context: TestContext<TSProjectGenerator>): void
{
    suite(
        basename(new URL(".", import.meta.url).pathname),
        () =>
        {
            LintingComponentTests(context);
            TSProjectWorkspaceComponentTests(context);
            TSProjectGeneralCategoryTests(context);
            TSProjectComponentCollectionTests(context);
        });
}
