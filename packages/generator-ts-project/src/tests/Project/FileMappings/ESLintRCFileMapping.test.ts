import { doesNotReject, ok, strictEqual } from "node:assert";
import { basename } from "node:path";
import { PluginName, PresetName } from "@manuth/eslint-plugin-typescript";
import { GeneratorOptions, GeneratorSettingKey } from "@manuth/extended-yo-generator";
import { FileMappingTester, JavaScriptFileMappingTester } from "@manuth/extended-yo-generator-test";
import { TempDirectory } from "@manuth/temp-files";
import { Linter } from "eslint";
import { createSandbox, SinonSandbox } from "sinon";
import { LintRuleset } from "../../../Linting/LintRuleset.js";
import { ESLintRCFileMapping } from "../../../Project/FileMappings/ESLintRCFileMapping.js";
import { TSProjectPackageFileMapping } from "../../../Project/FileMappings/NPMPackaging/TSProjectPackageFileMapping.js";
import { ITSProjectPackage } from "../../../Project/Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../../../Project/Settings/ITSProjectSettings.js";
import { TSProjectComponent } from "../../../Project/Settings/TSProjectComponent.js";
import { TSProjectSettingKey } from "../../../Project/Settings/TSProjectSettingKey.js";
import { TSProjectGenerator } from "../../../Project/TSProjectGenerator.js";
import { ICreationContext } from "../../../Project/Workspace/ICreationContext.js";
import { ProjectCreationContext } from "../../../Project/Workspace/ProjectCreationContext.js";
import { RootSettings } from "../../../Project/Workspace/RootSettings.js";
import { TSConfigFileMapping } from "../../../Serialization/TSConfigFileMapping.js";
import { TestContext } from "../../TestContext.js";
import { spawnNPM } from "../../Utilities.js";

/**
 * Registers tests for the  {@linkcode ESLintRCFileMapping} class.
 *
 * @param context
 * The test-context.
 */
export function ESLintRCFileMappingTests(context: TestContext<TSProjectGenerator>): void
{
    suite(
        nameof(ESLintRCFileMapping),
        () =>
        {
            /**
             * Provides an implementation of the {@linkcode TestESLintRCFileMapping} class for testing.
             */
            class TestESLintRCFileMapping extends ESLintRCFileMapping<ITSProjectSettings, GeneratorOptions, ITSProjectPackage>
            { }

            let sandbox: SinonSandbox;
            let tempDir: TempDirectory;
            let generator: TSProjectGenerator;
            let settings: Partial<ITSProjectSettings>;
            let creationContext: ICreationContext<ITSProjectSettings, ITSProjectPackage>;
            let fileMapping: TestESLintRCFileMapping;
            let tester: JavaScriptFileMappingTester<TSProjectGenerator, ITSProjectSettings, GeneratorOptions, TestESLintRCFileMapping>;

            suiteSetup(
                async function()
                {
                    this.timeout(7.5 * 60 * 1000);
                    tempDir = new TempDirectory();

                    settings = {
                        [GeneratorSettingKey.Components]: [
                            TSProjectComponent.Linting
                        ]
                    };

                    generator = context.CreateGenerator(
                        TSProjectGenerator,
                        [],
                        {
                            resolved: (await context.Generator).modulePath()
                        });

                    generator.destinationRoot(tempDir.FullName);
                    Object.assign(generator.Settings, settings);
                    await new FileMappingTester(generator, new TSProjectPackageFileMapping(generator, generator.PackageCreationContext)).Run();

                    let installationResult = spawnNPM(
                        [
                            "install",
                            "--silent",
                            "--ignore-scripts"
                        ],
                        {
                            cwd: generator.destinationPath(),
                            stdio: "ignore"
                        });

                    strictEqual(installationResult.status, 0);
                });

            suiteTeardown(
                function()
                {
                    this.timeout(10 * 1000);
                    tempDir.Dispose();
                });

            setup(
                () =>
                {
                    sandbox = createSandbox();
                    Object.assign(generator.Settings, settings);
                    creationContext = new ProjectCreationContext(generator, new RootSettings());
                    fileMapping = new ESLintRCFileMapping(generator, creationContext);
                    tester = new JavaScriptFileMappingTester(generator, fileMapping);
                });

            teardown(
                () =>
                {
                    sandbox.restore();
                });

            suite(
                nameof(ESLintRCFileMapping.FileName),
                () =>
                {
                    test(
                        "Checking whether the proper file-name is returned…",
                        () =>
                        {
                            strictEqual(ESLintRCFileMapping.FileName, ".eslintrc.cjs");
                        });
                });

            suite(
                nameof<TestESLintRCFileMapping>((fileMapping) => fileMapping.DefaultBaseName),
                () =>
                {
                    test(
                        `Checking whether the default base-name equals \`${nameof(ESLintRCFileMapping)}.${nameof(ESLintRCFileMapping.FileName)}\`…`,
                        () =>
                        {
                            strictEqual(fileMapping.DefaultBaseName, ESLintRCFileMapping.FileName);
                        });
                });

            suite(
                nameof<TestESLintRCFileMapping>((fileMapping) => fileMapping.BaseName),
                () =>
                {
                    test(
                        `Checking whether the \`${nameof<TestESLintRCFileMapping>((fm) => fm.BaseName)}\` equals the \`${nameof<TestESLintRCFileMapping>((fm) => fm.DefaultBaseName)}\`…`,
                        () =>
                        {
                            strictEqual(fileMapping.BaseName, fileMapping.DefaultBaseName);
                        });
                });

            suite(
                nameof<TestESLintRCFileMapping>((fileMapping) => fileMapping.Processor),
                () =>
                {
                    let config: Linter.Config;

                    let absentProperties = [
                        nameof<Linter.Config>((config) => config.ignorePatterns),
                        nameof<Linter.Config>((config) => config.overrides)
                    ] as Array<keyof Linter.Config>;

                    setup(
                        async function()
                        {
                            this.timeout(15 * 1000);
                            this.slow(7.5 * 1000);
                            await tester.Run();
                            config = await tester.Require();
                        });

                    for (let property of absentProperties)
                    {
                        test(
                            `Checking whether the \`${property}\` property is not present in workspace roots…`,
                            async function()
                            {
                                ok(!(property in config));
                            });
                    }

                    test(
                        "Checking whether config files in workspace roots are flagged as root…",
                        async function()
                        {
                            this.timeout(15 * 1000);
                            this.slow(7.5 * 1000);
                            strictEqual(config.root, true);
                        });

                    test(
                        `Checking whether the \`${nameof<Linter.Config>((config) => config.extends)}\`-property of the eslint-config is applied correctly in workspace roots…`,
                        async function()
                        {
                            this.timeout(10 * 1000);
                            this.slow(5 * 1000);

                            for (let ruleSet of [LintRuleset.Weak, LintRuleset.Recommended])
                            {
                                let configName: string;
                                let baseConfigs: string[];
                                tester.Generator.Settings[TSProjectSettingKey.LintRuleset] = ruleSet;
                                await tester.Run();
                                await doesNotReject(async () => config = await tester.Require());
                                ok(Array.isArray(config.extends));
                                baseConfigs = config.extends;

                                switch (ruleSet)
                                {
                                    case LintRuleset.Weak:
                                        configName = PresetName.WeakWithTypeChecking;
                                        break;
                                    case LintRuleset.Recommended:
                                    default:
                                        configName = PresetName.RecommendedWithTypeChecking;
                                        break;
                                }

                                ok(
                                    baseConfigs.some(
                                        (baseConfig) =>
                                        {
                                            return baseConfig === `plugin:${PluginName}/${configName}`;
                                        }));
                            }
                        });

                    test(
                        "Checking whether eslint files are transformed properly for non-root workspaces…",
                        async function()
                        {
                            this.timeout(15 * 1000);
                            this.slow(7.5 * 1000);
                            generator.Settings[GeneratorSettingKey.Components].push(TSProjectComponent.MonoRepo);
                            sandbox.replaceGetter(creationContext, nameof.typed(() => creationContext).IsWorkspaceRoot, () => false);
                            await tester.Run();
                            config = await tester.Require();
                            strictEqual(config.root, undefined);
                            strictEqual(config.extends, undefined);
                        });

                    test(
                        "Checking whether unnecessary TypeScript project files are excluded…",
                        async function()
                        {
                            this.timeout(15 * 1000);
                            this.slow(7.5 * 1000);

                            let isWorkspace: boolean;

                            sandbox.replaceGetter(
                                creationContext,
                                nameof.typed(() => creationContext).IsMonoRepo,
                                () => false);

                            sandbox.replaceGetter(
                                creationContext,
                                nameof.typed(() => creationContext).IsWorkspace,
                                () => isWorkspace);

                            for (let isRoot of [true, false])
                            {
                                let projects: string[];
                                let bannedNames: string[];
                                isWorkspace = !isRoot;
                                await tester.Run();
                                config = await tester.Require();
                                projects = config.parserOptions?.project as string[];

                                if (isWorkspace)
                                {
                                    bannedNames = [TSConfigFileMapping.GetFileName("editor")];
                                }
                                else
                                {
                                    bannedNames = [
                                        TSConfigFileMapping.GetFileName(),
                                        TSConfigFileMapping.GetFileName("app")
                                    ];
                                }

                                ok((projects).every(
                                    (projectFile) =>
                                    {
                                        return !projectFile.includes("type-tests");
                                    }));

                                bannedNames.every(
                                    (bannedName) =>
                                    {
                                        return projects.every(
                                            (project) =>
                                            {
                                                return basename(project) !== bannedName;
                                            });
                                    });
                            }
                        });
                });
        });
}
