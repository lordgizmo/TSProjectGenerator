import { deepStrictEqual, doesNotReject, notStrictEqual, ok, rejects, strictEqual } from "node:assert";
import { GeneratorOptions, GeneratorSettingKey } from "@manuth/extended-yo-generator";
import { PackageFileMappingTester } from "@manuth/generator-ts-project-test";
import { IPackageMetadata, Package, PackageType, ResolveMatrix } from "@manuth/package-json-editor";
import { TempDirectory } from "@manuth/temp-files";
import { minimatch } from "minimatch";
import minimist from "minimist";
import { createSandbox, SinonSandbox } from "sinon";
import parseArgsStringToArgv from "string-argv";
import path from "upath";
import { Constants } from "../../../../Core/Constants.js";
import { LintEssentials } from "../../../../NPM/Dependencies/LintEssentials.js";
import { WorkspaceRootDependencies } from "../../../../NPM/Dependencies/WorkspaceRootDependencies.js";
import { IScriptMapping } from "../../../../NPM/Scripts/IScriptMapping.js";
import { TSProjectPackageFileMapping } from "../../../../Project/FileMappings/NPMPackaging/TSProjectPackageFileMapping.js";
import { ITSProjectPackage } from "../../../../Project/Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../../../../Project/Settings/ITSProjectSettings.js";
import { TSProjectComponent } from "../../../../Project/Settings/TSProjectComponent.js";
import { TSProjectPackageKey } from "../../../../Project/Settings/TSProjectPackageKey.js";
import { TSProjectSettingKey } from "../../../../Project/Settings/TSProjectSettingKey.js";
import { TSProjectGenerator } from "../../../../Project/TSProjectGenerator.js";
import { ICreationContext } from "../../../../Project/Workspace/ICreationContext.js";
import { ProjectCreationContext } from "../../../../Project/Workspace/ProjectCreationContext.js";
import { RootSettings } from "../../../../Project/Workspace/RootSettings.js";
import { WorkspaceSettings } from "../../../../Project/Workspace/WorkspaceSettings.js";
import { TestContext } from "../../../TestContext.js";
import { WrappedSettings } from "../../Workspace/WrappedSettings.js";

const { joinSafe, relative } = path;

/**
 * Registers tests for the {@linkcode TSProjectPackageFileMapping} class.
 *
 * @param context
 * The text-context.
 */
export function TSProjectPackageFileMappingTests(context: TestContext<TSProjectGenerator>): void
{
    suite(
        nameof(TSProjectPackageFileMapping),
        () =>
        {
            let sandbox: SinonSandbox;
            let tempDir: TempDirectory;
            let generator: TSProjectGenerator;
            let monoRepoSettings: ITSProjectSettings;
            let contextSettings: WrappedSettings<ITSProjectSettings, ITSProjectPackage>;
            let creationContext: ICreationContext<ITSProjectSettings, ITSProjectPackage>;
            let fileMapping: TestTSProjectPackageFileMapping;
            let tester: PackageFileMappingTester<TSProjectGenerator, ITSProjectSettings, GeneratorOptions, TestTSProjectPackageFileMapping>;

            /**
             * Provides an implementation of the {@linkcode TSProjectPackageFileMapping} class for testing.
             */
            class TestTSProjectPackageFileMapping extends TSProjectPackageFileMapping<ITSProjectSettings, GeneratorOptions, ITSProjectPackage>
            {
                /**
                 * @inheritdoc
                 */
                public override get ScriptMappings(): Array<IScriptMapping<ITSProjectSettings, GeneratorOptions> | string>
                {
                    return super.ScriptMappings;
                }

                /**
                 * @inheritdoc
                 *
                 * @returns
                 * The loaded package.
                 */
                public override async LoadPackage(): Promise<Package>
                {
                    return super.LoadPackage();
                }
            }

            /**
             * Asserts that a script has been copied.
             *
             * @param source
             * The source of the script.
             *
             * @param destination
             * The destination of the script-.
             */
            async function AssertScriptCopy(source: string, destination?: string): Promise<void>
            {
                destination = destination ?? source;
                return tester.AssertScript(destination, Constants.Package.Scripts.Get(source));
            }

            /**
             * Gets the package of the filemapping.
             *
             * @returns
             * The package of the filemapping.
             */
            async function GetPackage(): Promise<Package>
            {
                return fileMapping.GetPackage();
            }

            suiteSetup(
                async function()
                {
                    this.timeout(5 * 60 * 1000);
                    generator = await context.Generator;
                });

            setup(
                async () =>
                {
                    sandbox = createSandbox();
                    tempDir = new TempDirectory();
                    generator.destinationRoot(tempDir.FullName);

                    monoRepoSettings = {
                        [GeneratorSettingKey.Components]: [TSProjectComponent.MonoRepo],
                        [TSProjectSettingKey.Workspaces]: [
                            {
                                [TSProjectPackageKey.Destination]: "./packages/hello",
                                [TSProjectPackageKey.DisplayName]: "hello",
                                [TSProjectPackageKey.Name]: "hello",
                                [TSProjectPackageKey.Description]: ""
                            },
                            {
                                [TSProjectPackageKey.Destination]: "./packages/world",
                                [TSProjectPackageKey.DisplayName]: "world",
                                [TSProjectPackageKey.Name]: "world",
                                [TSProjectPackageKey.Description]: ""
                            }
                        ]
                    } as ITSProjectSettings;

                    contextSettings = new WrappedSettings(new RootSettings());
                    creationContext = new ProjectCreationContext(generator, contextSettings);
                    fileMapping = new TestTSProjectPackageFileMapping(generator, creationContext);
                    tester = new PackageFileMappingTester(await context.Generator, fileMapping);
                    await tester.Clean();
                });

            teardown(
                () =>
                {
                    sandbox.restore();
                    tempDir.Dispose();
                });

            suite(
                nameof<TestTSProjectPackageFileMapping>((fileMapping) => fileMapping.Destination),
                () =>
                {
                    test(
                        "Checking whether the destination points to the package file of its respective creation context…",
                        () =>
                        {
                            let packagePath = "./packages/test";

                            sandbox.replaceGetter(
                                creationContext,
                                nameof.typed(() => creationContext).Destination,
                                () => packagePath);

                            strictEqual(
                                generator.destinationPath(fileMapping.Destination),
                                generator.destinationPath(packagePath, Package.FileName));
                        });
                });

            suite(
                nameof<TestTSProjectPackageFileMapping>((fileMapping) => fileMapping.LoadPackage),
                () =>
                {
                    let workspaceSetting = "workspaces";

                    setup(
                        async () =>
                        {
                            await tester.DumpOutput(await fileMapping.LoadPackage());
                        });

                    test(
                        `Checking whether the file can be created without the need of the \`${nameof(GeneratorSettingKey.Components)}\`-setting to be specified…`,
                        async () =>
                        {
                            delete (tester.Generator.Settings as Partial<ITSProjectSettings>)[GeneratorSettingKey.Components];
                            await doesNotReject(() => tester.Run());
                        });

                    test(
                        "Checking whether whether the metadata is adjusted properly for workspace roots of mono repos…",
                        async function()
                        {
                            this.timeout(10 * 1000);
                            this.slow(5 * 1000);
                            generator.Settings[GeneratorSettingKey.Components] = [TSProjectComponent.MonoRepo];
                            let npmPackage = await GetPackage();
                            strictEqual(npmPackage.Private, true);
                            deepStrictEqual(npmPackage.Files, []);
                            ok(npmPackage.AdditionalProperties.Has("workspaces"));
                        });

                    test(
                        "Checking whether the name and the description are loaded from the prompts…",
                        async function()
                        {
                            this.timeout(10 * 1000);
                            this.slow(5 * 1000);
                            let randomName = context.RandomString;
                            let randomDescription = context.RandomString;
                            tester.Generator.Settings[TSProjectPackageKey.Name] = randomName;
                            tester.Generator.Settings[TSProjectPackageKey.Description] = randomDescription;
                            strictEqual((await GetPackage()).Name, randomName);
                            strictEqual((await GetPackage()).Description, randomDescription);
                        });

                    test(
                        "Checking whether common dependencies are present in workspace roots…",
                        async function()
                        {
                            this.timeout(10 * 1000);
                            this.slow(5 * 1000);
                            await tester.AssertDependencies(new WorkspaceRootDependencies());
                            Object.assign(generator.Settings, monoRepoSettings);
                            await tester.AssertDependencies(new WorkspaceRootDependencies());

                            for (let i = 0; i < generator.Settings[TSProjectSettingKey.Workspaces].length; i++)
                            {
                                contextSettings.Settings = new WorkspaceSettings(generator, i);
                                await tester.Clean();
                                await tester.Run();
                                await rejects(() => tester.AssertDependencies(new WorkspaceRootDependencies()));
                            }
                        });

                    test(
                        "Checking whether lint-dependencies are present in workspace roots if linting is enabled…",
                        async function()
                        {
                            this.timeout(10 * 1000);
                            this.slow(5 * 1000);
                            Object.assign(generator.Settings, monoRepoSettings);

                            for (let lintingEnabled of [true, false])
                            {
                                for (let workspaceRoot of [true, false])
                                {
                                    if (workspaceRoot)
                                    {
                                        contextSettings.Settings = new RootSettings();
                                    }
                                    else
                                    {
                                        contextSettings.Settings = new WorkspaceSettings(generator, 0);
                                    }

                                    tester.Generator.Settings[GeneratorSettingKey.Components] = lintingEnabled ? [TSProjectComponent.Linting] : [];
                                    await tester.Clean();
                                    await tester.Run();
                                    await tester.AssertDependencies(new LintEssentials(), workspaceRoot && lintingEnabled);
                                    sandbox.restore();
                                }
                            }
                        });

                    test(
                        `Checking whether the \`${Package.FileName}\` file is exposed in the \`${nameof<IPackageMetadata>((pkg) => pkg.exports)}\` field in workspaces…`,
                        async () =>
                        {
                            await tester.Clean();
                            Object.assign(generator.Settings, monoRepoSettings);

                            for (let isWorkspace of [true, false])
                            {
                                let packageFileName = joinSafe("./", Package.FileName);

                                if (isWorkspace)
                                {
                                    contextSettings.Settings = new WorkspaceSettings(generator, 0);
                                }
                                else
                                {
                                    contextSettings.Settings = new RootSettings();
                                }

                                if (isWorkspace)
                                {
                                    strictEqual(((await GetPackage()).Exports as ResolveMatrix)[packageFileName] as string, packageFileName);
                                }
                                else
                                {
                                    strictEqual((await GetPackage()).Exports, undefined);
                                }

                                sandbox.restore();
                            }
                        });

                    test(
                        `Checking whether the \`${nameof<IPackageMetadata>((pkg) => pkg.type)}\`-field is set in workspaces according to the project type…`,
                        async () =>
                        {
                            await tester.Clean();
                            Object.assign(generator.Settings, monoRepoSettings);

                            for (let isWorkspace of [true, false])
                            {
                                if (isWorkspace)
                                {
                                    contextSettings.Settings = new WorkspaceSettings(generator, 0);
                                }
                                else
                                {
                                    contextSettings.Settings = new RootSettings();
                                }

                                if (isWorkspace)
                                {
                                    for (let esModule of [true, false])
                                    {
                                        let expectedType = esModule ? PackageType.ESModule : PackageType.CommonJS;
                                        tester.Generator.Settings[TSProjectSettingKey.ESModule] = esModule;
                                        strictEqual((await GetPackage()).Type, expectedType);
                                    }
                                }
                                else
                                {
                                    strictEqual((await GetPackage()).Type, undefined);
                                }

                                sandbox.restore();
                            }
                        });

                    test(
                        `Checking whether the \`${nameof<IPackageMetadata>((pkg) => pkg.publishConfig)}\` field is set properly in workspaces…`,
                        async () =>
                        {
                            await tester.Clean();
                            Object.assign(generator.Settings, monoRepoSettings);

                            for (let isWorkspace of [true, false])
                            {
                                if (isWorkspace)
                                {
                                    contextSettings.Settings = new WorkspaceSettings(generator, 0);
                                }
                                else
                                {
                                    contextSettings.Settings = new RootSettings();
                                }

                                sandbox.replaceGetter(
                                    creationContext,
                                    nameof.typed(() => creationContext).IsWorkspace,
                                    () => isWorkspace);

                                if (isWorkspace)
                                {
                                    strictEqual((await GetPackage()).PublishConfig["access"], "public");
                                }
                                else
                                {
                                    deepStrictEqual((await GetPackage()).PublishConfig, new Package().PublishConfig);
                                }

                                sandbox.restore();
                            }
                        });

                    test(
                        `Checking whether all workspaces are included by the \`${workspaceSetting}\` setting…`,
                        async () =>
                        {
                            Object.assign(generator.Settings, monoRepoSettings);
                            let patterns = ((await GetPackage()).AdditionalProperties.Get(workspaceSetting) as any).packages as string[];

                            for (let workspace of generator.Settings[TSProjectSettingKey.Workspaces])
                            {
                                let destination = workspace[TSProjectPackageKey.Destination];

                                ok(
                                    patterns.some(
                                        (pattern) =>
                                        {
                                            return minimatch(destination, pattern);
                                        }));
                            }
                        });
                });

            suite(
                nameof<TestTSProjectPackageFileMapping>((fileMapping) => fileMapping.ScriptMappings),
                () =>
                {
                    let testScriptName = "test";
                    const cleanScriptName = "clean";
                    const prepackScriptName = "prepack";

                    test(
                        "Checking whether all expected scripts are present in non mono repo workspaces…",
                        async function()
                        {
                            this.timeout(4 * 1000);
                            this.slow(2 * 1000);
                            generator.Settings[GeneratorSettingKey.Components].push(TSProjectComponent.Linting);
                            await tester.Run();

                            await AssertScriptCopy("build");
                            await AssertScriptCopy("rebuild");
                            await AssertScriptCopy("watch");
                            await AssertScriptCopy("clean-base", cleanScriptName);
                            await AssertScriptCopy("lint-local", "lint");
                            await AssertScriptCopy("lint-ide");
                            await tester.AssertScript("test", (script) => !script.includes("tsd"));
                            ok(!(await GetPackage()).Scripts.Has("prepare"));
                            await AssertScriptCopy("bump-version");
                            await tester.AssertScript(prepackScriptName, "npm run build");
                        });

                    test(
                        "Checking whether linting scripts are only included if enabled…",
                        async () =>
                        {
                            let lintScriptName = "lint";
                            let ideScriptName = "lint-ide";

                            for (let linting of [true, false])
                            {
                                generator.Settings[GeneratorSettingKey.Components] = linting ? [TSProjectComponent.Linting] : [];
                                await tester.Clean();
                                await tester.Run();

                                if (linting)
                                {
                                    await AssertScriptCopy("lint-local", lintScriptName);
                                    await AssertScriptCopy(ideScriptName);
                                }
                                else
                                {
                                    for (let script of [lintScriptName, ideScriptName])
                                    {
                                        ok(!(await GetPackage()).Scripts.Has(script));
                                    }
                                }
                            }
                        });

                    test(
                        `Checking whether the \`${testScriptName}\` script is adjusted for mono repos…`,
                        async () =>
                        {
                            let scripts = (await GetPackage()).Scripts;
                            let testScript = scripts.Get(testScriptName);
                            Object.assign(generator.Settings, monoRepoSettings);
                            notStrictEqual((await GetPackage()).Scripts.Get(testScriptName), testScript);
                        });

                    test(
                        "Checking whether the scripts are adjusted for mono repo workspace roots properly…",
                        async function()
                        {
                            this.timeout(4 * 1000);
                            this.slow(2 * 1000);
                            Object.assign(generator.Settings, monoRepoSettings);
                            generator.Settings[GeneratorSettingKey.Components].push(TSProjectComponent.Linting);
                            await tester.Run();

                            await tester.AssertScript(
                                cleanScriptName,
                                (script) =>
                                {
                                    return generator.Settings[TSProjectSettingKey.Workspaces].every(
                                        (workspace) =>
                                        {
                                            let libDir = joinSafe(workspace[TSProjectPackageKey.Destination], "lib");

                                            return script.split("&&").map(
                                                (command) => command.trim()).filter(
                                                    (command) =>
                                                    {
                                                        return command.startsWith("rimraf");
                                                    }).some(
                                                        (command) =>
                                                        {
                                                            let parsedArgs = minimist(
                                                                parseArgsStringToArgv(command),
                                                                {
                                                                    boolean: [
                                                                        "g"
                                                                    ]
                                                                });

                                                            return parsedArgs["g"] &&
                                                                parsedArgs._.some(
                                                                    (pattern) =>
                                                                    {
                                                                        return minimatch(libDir, pattern);
                                                                    });
                                                        });
                                        });
                                });

                            await tester.AssertScript(
                                "lint",
                                (script) =>
                                {
                                    let patterns = minimist(parseArgsStringToArgv(script))._;

                                    return !script.includes(" ./src ") &&
                                        generator.Settings[TSProjectSettingKey.Workspaces].every(
                                            (workspace) =>
                                            {
                                                let destination = workspace[TSProjectPackageKey.Destination];

                                                return ["src", ".eslintrc.cjs"].every(
                                                    (item) =>
                                                    {
                                                        let path = joinSafe(destination, item);

                                                        return patterns.some(
                                                            (pattern) =>
                                                            {
                                                                return minimatch(path, pattern);
                                                            });
                                                    });
                                            });
                                });
                        });

                    test(
                        "Checking whether the scripts are adjusted for mono repo workspaces properly…",
                        async function()
                        {
                            this.timeout(4 * 1000);
                            this.slow(2 * 1000);
                            Object.assign(generator.Settings, monoRepoSettings);

                            let destination = generator.Settings[TSProjectPackageKey.Destination];

                            for (let i = 0; i < generator.Settings[TSProjectSettingKey.Workspaces].length; i++)
                            {
                                contextSettings.Settings = new WorkspaceSettings(generator, i);
                                await tester.Run();

                                await tester.AssertScript(
                                    prepackScriptName,
                                    (script) =>
                                    {
                                        let prefix = minimist(parseArgsStringToArgv(script))["prefix"];
                                        return prefix === relative(creationContext.Destination, destination);
                                    });
                            }
                        });

                    test(
                        "Checking whether scripts for workspaces are created properly…",
                        async function()
                        {
                            this.timeout(4 * 1000);
                            this.slow(2 * 1000);
                            await tester.Clean();
                            Object.assign(generator.Settings, monoRepoSettings);
                            generator.Settings[GeneratorSettingKey.Components].push(TSProjectComponent.Linting);
                            contextSettings.Settings = new WorkspaceSettings(generator, 0);

                            sandbox.replaceGetter(
                                creationContext,
                                nameof.typed(() => creationContext).IsWorkspaceRoot,
                                () => false);

                            let scripts = (await GetPackage()).Scripts;
                            ok(!scripts.Has("build"));
                            ok(!scripts.Has("rebuild"));
                            ok(!scripts.Has("watch"));
                            ok(!scripts.Has("clean"));
                            ok(!scripts.Has("lint"));
                            ok(!scripts.Has("lint-ide"));
                        });
                });
        });
}
