import { basename } from "node:path";
import { TSProjectPackageFileMappingTests } from "./TSProjectPackageFileMapping.test.js";
import { TSProjectGenerator } from "../../../../Project/TSProjectGenerator.js";
import { TestContext } from "../../../TestContext.js";

/**
 * Registers npm-packaging tests for {@linkcode TSProjectGenerator}s.
 *
 * @param context
 * The test-context.
 */
export function NPMPackagingTests(context: TestContext<TSProjectGenerator>): void
{
    suite(
        basename(new URL(".", import.meta.url).pathname),
        () =>
        {
            TSProjectPackageFileMappingTests(context);
        });
}
