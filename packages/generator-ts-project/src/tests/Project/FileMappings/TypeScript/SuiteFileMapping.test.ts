import { ok, strictEqual } from "node:assert";
import { GeneratorOptions } from "@manuth/extended-yo-generator";
import { FileMappingTester, RunContext } from "@manuth/extended-yo-generator-test";
import { PackageType } from "@manuth/package-json-editor";
import { TempFile } from "@manuth/temp-files";
import RandExp from "randexp";
import { ArrowFunction, CallExpression, FunctionDeclaration, Node, SourceFile, SyntaxKind, ts } from "ts-morph";
import { ISuiteContext } from "../../../../Project/FileMappings/TypeScript/ISuiteContext.js";
import { ISuiteFunctionInfo } from "../../../../Project/FileMappings/TypeScript/ISuiteFunctionInfo.js";
import { SuiteFileMapping } from "../../../../Project/FileMappings/TypeScript/SuiteFileMapping.js";
import { ITSProjectSettings } from "../../../../Project/Settings/ITSProjectSettings.js";
import { TSProjectSettingKey } from "../../../../Project/Settings/TSProjectSettingKey.js";
import { TSProjectGenerator } from "../../../../Project/TSProjectGenerator.js";
import { SuiteInspector } from "../../../Mocha/SuiteInspector.js";
import { TestContext } from "../../../TestContext.js";
import { spawnNPM } from "../../../Utilities.js";

const { randexp } = RandExp;

/**
 * Registers tests for the {@linkcode SuiteFileMapping} class.
 *
 * @param context
 * The test-context.
 */
export function SuiteFileMappingTests(context: TestContext<TSProjectGenerator>): void
{
    suite(
        nameof(SuiteFileMapping),
        () =>
        {
            /**
             * Provides an implementation of the {@linkcode SuiteFileMapping} class for testing.
             */
            class TestSuiteFileMapping extends SuiteFileMapping<ITSProjectSettings, GeneratorOptions>
            {
                /**
                 * @inheritdoc
                 */
                public get Destination(): string
                {
                    return destination ?? outputFile.FullName;
                }

                /**
                 * @inheritdoc
                 *
                 * @returns
                 * The context of the file-mapping.
                 */
                public override async Context(): Promise<ISuiteContext>
                {
                    return {
                        SuiteName: suiteName,
                        get SuiteFunction()
                        {
                            return suiteFunctionInfo;
                        }
                    };
                }

                /**
                 * Gets the function for registering the suite.
                 *
                 * @returns
                 * The function for registering the suite.
                 */
                public override async GetSuiteFunction(): Promise<ArrowFunction>
                {
                    let result = await super.GetSuiteFunction();

                    result.addStatements(
                        this.WrapExpression(
                            this.WrapNode(
                                ts.factory.createCallExpression(
                                    ts.factory.createIdentifier(nameof(test)),
                                    [],
                                    [
                                        ts.factory.createStringLiteral(testName),
                                        ts.factory.createArrowFunction(
                                            [],
                                            [],
                                            [],
                                            undefined,
                                            undefined,
                                            ts.factory.createBlock([]))
                                    ]))).getFullText());

                    return result;
                }

                /**
                 * @inheritdoc
                 *
                 * @param sourceFile
                 * The source-file to process.
                 *
                 * @returns
                 * The processed data.
                 */
                public override async Transform(sourceFile: SourceFile): Promise<SourceFile>
                {
                    this.Dispose();
                    return super.Transform(sourceFile);
                }
            }

            let generator: TSProjectGenerator;
            let suiteName: string;
            let testName: string;
            let suiteFunctionInfo: ISuiteFunctionInfo | undefined;
            let destination: string | undefined;
            let outputFile: TempFile;
            let fileMapping: TestSuiteFileMapping;

            suiteSetup(
                async function()
                {
                    this.timeout(30 * 1000);
                    generator = await context.Generator;
                });

            setup(
                () =>
                {
                    suiteName = "example-suite";
                    testName = "example-test";
                    suiteFunctionInfo = undefined;
                    destination = undefined;

                    outputFile = new TempFile(
                        {
                            Suffix: ".ts"
                        });

                    fileMapping = new TestSuiteFileMapping(generator);
                });

            teardown(
                () =>
                {
                    outputFile.Dispose();
                });

            suite(
                "General",
                () =>
                {
                    suite(
                        nameof<TestSuiteFileMapping>((fileMapping) => fileMapping.Transform),
                        () =>
                        {
                            let files: SourceFile[];

                            setup(
                                () =>
                                {
                                    files = [];
                                });

                            teardown(
                                () =>
                                {
                                    for (let file of files)
                                    {
                                        file.forget();
                                    }
                                });

                            /**
                             * Gets the transformed file.
                             *
                             * @returns
                             * The transformed file.
                             */
                            async function GetTransformedFile(): Promise<SourceFile>
                            {
                                let file = await fileMapping.Transform(await fileMapping.GetSourceObject());
                                files.push(file);
                                return file;
                            }

                            /**
                             * Gets the {@linkcode suite} calls inside the specified {@linkcode node}.
                             *
                             * @param node
                             * The node to get the {@linkcode suite} calls from.
                             *
                             * @returns
                             * The {@linkcode suite} calls inside the specified {@linkcode node}.
                             */
                            function GetDescendantSuiteCalls(node: Node): CallExpression[]
                            {
                                return node.getDescendantsOfKind(
                                    SyntaxKind.CallExpression).filter(
                                        (callExpression) =>
                                        {
                                            return callExpression.getExpression().getText() === nameof(suite);
                                        });
                            }

                            /**
                             * Gets the suite function in the transformed file.
                             *
                             * @returns
                             * The suite function in the transformed file.
                             */
                            async function GetSuiteFunction(): Promise<FunctionDeclaration>
                            {
                                return (await GetTransformedFile()).getFirstChildByKindOrThrow(SyntaxKind.FunctionDeclaration);
                            }

                            /**
                             * Gets all calls to the {@linkcode suite}-method.
                             *
                             * @returns
                             * All calls to the {@linkcode suite}-method.
                             */
                            async function GetSuiteCalls(): Promise<CallExpression[]>
                            {
                                return GetDescendantSuiteCalls(await GetTransformedFile());
                            }

                            /**
                             * Gets the first call to the {@linkcode suite}-method.
                             *
                             * @returns
                             * The first call to the {@linkcode suite}-method.
                             */
                            async function GetSuiteCall(): Promise<CallExpression>
                            {
                                return (await GetSuiteCalls())[0];
                            }

                            test(
                                `Checking whether a call to mocha's \`${nameof(suite)}\` is present…`,
                                async function()
                                {
                                    this.timeout(4 * 1000);
                                    this.slow(2 * 1000);
                                    strictEqual((await GetSuiteCalls()).length, 1);
                                });

                            test(
                                `Checking whether the name of the suite is set according to \`${nameof<TestSuiteFileMapping>((fm) => fm.GetSuiteName)}\`…`,
                                async function()
                                {
                                    this.timeout(4 * 1000);
                                    this.slow(2 * 1000);

                                    strictEqual(
                                        (await GetSuiteCall()).getArguments()[0].asKindOrThrow(SyntaxKind.StringLiteral).getLiteralValue(),
                                        await fileMapping.GetSuiteName());
                                });

                            test(
                                "Checking whether the suite can be generated inside a named function…",
                                async function()
                                {
                                    this.timeout(4 * 1000);
                                    this.slow(2 * 1000);

                                    suiteFunctionInfo = {
                                        Name: randexp(/[a-zA-Z][a-zA-Z0-9]{9}/),
                                        Description: context.RandomString
                                    };

                                    let suiteFunction = await GetSuiteFunction();
                                    strictEqual(suiteFunction.getName(), suiteFunctionInfo.Name);
                                    ok(suiteFunction.isExported());
                                    strictEqual(suiteFunction.getJsDocs()[0].getDescription().trim(), suiteFunctionInfo.Description.trim());
                                    strictEqual(GetDescendantSuiteCalls(suiteFunction).length, 1);
                                });

                            test(
                                "Checking whether the suite function is not exported if it is flagged as an entry point…",
                                async function()
                                {
                                    this.timeout(30 * 1000);
                                    this.slow(15 * 1000);

                                    suiteFunctionInfo = {
                                        IsEntrypoint: true,
                                        Name: "Test",
                                        Description: "Registers the tests."
                                    };

                                    ok(!(await GetSuiteFunction()).isExported());
                                });

                            test(
                                "Checking whether all suite calls in entry points are nested in a function…",
                                async () =>
                                {
                                    suiteFunctionInfo = {
                                        Description: "Registers the tests.",
                                        Name: "Tests",
                                        IsEntrypoint: true
                                    };

                                    ok(
                                        (await GetSuiteCalls()).every(
                                            (suiteCall) =>
                                            {
                                                try
                                                {
                                                    suiteCall.getFirstAncestorByKindOrThrow(ts.SyntaxKind.FunctionDeclaration);
                                                    return true;
                                                }
                                                catch
                                                {
                                                    return false;
                                                }
                                            }));
                                });

                            test(
                                `Checking whether the contents of the suite can be adjusted using \`${nameof<TestSuiteFileMapping>((fm) => fm.GetSuiteFunction)}\`…`,
                                async function()
                                {
                                    this.timeout(4 * 1000);
                                    this.slow(2 * 1000);

                                    strictEqual(
                                        (await GetSuiteCall()).getArguments()[1].asKindOrThrow(SyntaxKind.ArrowFunction).getDescendantsOfKind(
                                            SyntaxKind.StringLiteral).filter(
                                                (stringLiteral) =>
                                                {
                                                    return stringLiteral.getLiteralValue() === testName;
                                                }).length,
                                        1);
                                });

                            test(
                                "Checking whether the suite function is called if the file is flagged as an entry point…",
                                async () =>
                                {
                                    for (let isEntrypoint of [true, false])
                                    {
                                        suiteFunctionInfo = {
                                            Name: "Test",
                                            Description: "This is a test.",
                                            IsEntrypoint: isEntrypoint
                                        };

                                        let suiteFunction = await GetSuiteFunction();
                                        let references = suiteFunction.findReferencesAsNodes();

                                        if (isEntrypoint)
                                        {
                                            strictEqual(references.length, 1);
                                            let functionCall = references[0].getFirstAncestorByKindOrThrow(SyntaxKind.CallExpression);
                                            strictEqual(functionCall.getExpression().getText(), suiteFunctionInfo.Name);
                                        }
                                        else
                                        {
                                            strictEqual(references.length, 0);
                                        }
                                    }
                                });
                        });
                });

            for (let esModule of [true, false])
            {
                let packageType = esModule ? nameof(PackageType.ESModule) : nameof(PackageType.CommonJS);

                suite(
                    packageType,
                    () =>
                    {
                        suite(
                            nameof<TestSuiteFileMapping>((fileMapping) => fileMapping.Transform),
                            () =>
                            {
                                let runContext: RunContext<TSProjectGenerator>;
                                let generator: TSProjectGenerator;
                                let fileMappingTester: FileMappingTester<TSProjectGenerator, ITSProjectSettings, GeneratorOptions, TestSuiteFileMapping>;
                                let outputFileName: string;

                                suiteSetup(
                                    async function()
                                    {
                                        this.timeout(5 * 60 * 1000);

                                        runContext = context.ExecuteGenerator().withAnswers(
                                            {
                                                [TSProjectSettingKey.ESModule]: esModule
                                            });

                                        await runContext;
                                        generator = runContext.generator;

                                        spawnNPM(
                                            ["install"],
                                            {
                                                cwd: generator.destinationPath()
                                            });
                                    });

                                suiteTeardown(
                                    async function()
                                    {
                                        this.timeout(20 * 1000);
                                        runContext.cleanup();
                                    });

                                setup(
                                    async function()
                                    {
                                        this.timeout(30 * 1000);

                                        destination = "./src/tests/main.test.ts";
                                        outputFileName = "./lib/tests/main.test.js";
                                        suiteName = "Tests";
                                        fileMappingTester = new FileMappingTester(generator, new TestSuiteFileMapping(generator));

                                        suiteFunctionInfo = {
                                            IsEntrypoint: true,
                                            Name: "Tests",
                                            Description: "This is a test"
                                        };

                                        await fileMappingTester.Run();

                                        spawnNPM(
                                            ["run", "build"],
                                            {
                                                cwd: generator.destinationPath()
                                            });
                                    });

                                teardown(
                                    async () =>
                                    {
                                        await fileMappingTester.Clean();
                                    });

                                test(
                                    "Checking whether at least one suite is provided by the file…",
                                    async function()
                                    {
                                        this.timeout(4 * 1000);
                                        this.slow(2 * 1000);

                                        let suiteInfo = await SuiteInspector.Fetch(generator.destinationPath(outputFileName));
                                        ok(suiteInfo.SuiteCount > 0);
                                        ok(suiteInfo.Suites.includes(suiteName));
                                        ok(suiteInfo.Tests.includes(testName));
                                    });
                            });
                    });
            }
        });
}
