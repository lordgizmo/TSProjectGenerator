import { ok, strictEqual } from "node:assert";
import { GeneratorOptions } from "@manuth/extended-yo-generator";
import { FileMappingTester, RunContext, TestContext } from "@manuth/extended-yo-generator-test";
import { PackageType } from "@manuth/package-json-editor";
import { TempFile } from "@manuth/temp-files";
import { CallExpression, SourceFile, SyntaxKind } from "ts-morph";
import { ISuiteContext } from "../../../../Project/FileMappings/TypeScript/ISuiteContext.js";
import { ISuiteFunctionInfo } from "../../../../Project/FileMappings/TypeScript/ISuiteFunctionInfo.js";
import { TestFileMapping } from "../../../../Project/FileMappings/TypeScript/TestFileMapping.js";
import { ITSProjectSettings } from "../../../../Project/Settings/ITSProjectSettings.js";
import { TSProjectSettingKey } from "../../../../Project/Settings/TSProjectSettingKey.js";
import { TSProjectGenerator } from "../../../../Project/TSProjectGenerator.js";
import { SuiteInspector } from "../../../Mocha/SuiteInspector.js";
import { spawnNPM } from "../../../Utilities.js";

/**
 * Registers tests for the {@linkcode TestFileMapping} class.
 *
 * @param context
 * The test-context.
 */
export function TestFileMappingTests(context: TestContext<TSProjectGenerator>): void
{
    suite(
        nameof(TestFileMapping),
        () =>
        {
            /**
             * Provides an implementation of the {@linkcode TestFileMapping} class.
             */
            class TestTestFileMapping extends TestFileMapping<ITSProjectSettings, GeneratorOptions>
            {
                /**
                 * @inheritdoc
                 */
                public get Destination(): string
                {
                    return destination ?? outputFile.FullName;
                }

                /**
                 * @inheritdoc
                 *
                 * @returns
                 * The context of the file-mapping.
                 */
                public override async Context(): Promise<ISuiteContext>
                {
                    return {
                        SuiteName: suiteName,
                        SuiteFunction: suiteFunctionInfo
                    };
                }

                /**
                 * @inheritdoc
                 *
                 * @param sourceFile
                 * The source-file to process.
                 *
                 * @returns
                 * The processed data.
                 */
                public override async Transform(sourceFile: SourceFile): Promise<SourceFile>
                {
                    this.Dispose();
                    return super.Transform(sourceFile);
                }
            }

            let generator: TSProjectGenerator;
            let suiteName: string;
            let suiteFunctionInfo: ISuiteFunctionInfo;
            let destination: string | undefined;
            let outputFile: TempFile;
            let fileMapping: TestTestFileMapping;
            let unitTestName = "Example…";

            suiteSetup(
                async function()
                {
                    this.timeout(30 * 1000);
                    generator = await context.Generator;
                });

            setup(
                () =>
                {
                    suiteName = context.RandomString;

                    outputFile = new TempFile(
                        {
                            Suffix: ".ts"
                        });

                    fileMapping = new TestTestFileMapping(generator);
                });

            teardown(
                () =>
                {
                    outputFile.Dispose();
                });

            suite(
                "General",
                () =>
                {
                    suite(
                        nameof<TestTestFileMapping>((fileMapping) => fileMapping.Transform),
                        () =>
                        {
                            let files: SourceFile[];

                            setup(
                                () =>
                                {
                                    files = [];
                                });

                            teardown(
                                () =>
                                {
                                    for (let file of files)
                                    {
                                        file.forget();
                                    }
                                });

                            /**
                             * Gets all calls to mocha's {@linkcode test}-method.
                             *
                             * @returns
                             * All calls to mocha's {@linkcode test}-method.
                             */
                            async function GetTestCalls(): Promise<CallExpression[]>
                            {
                                let file = await fileMapping.Transform(await fileMapping.GetSourceObject());
                                files.push(file);

                                let suiteCalls = file.getDescendantsOfKind(SyntaxKind.CallExpression).filter(
                                    (callExpression) =>
                                    {
                                        return callExpression.getExpression().getText() === nameof(suite) &&
                                            (callExpression.getArguments()[0].asKind(SyntaxKind.StringLiteral)?.getLiteralValue() ?? null) === suiteName;
                                    });

                                strictEqual(suiteCalls.length, 1);
                                let suiteCall = suiteCalls[0];

                                return suiteCall.getDescendantsOfKind(SyntaxKind.CallExpression).filter(
                                    (callExpression) =>
                                    {
                                        return callExpression.getExpression().getText() === nameof(test);
                                    });
                            }

                            /**
                             * Gets the first call to mocha's {@linkcode test}-method.
                             *
                             * @returns
                             * The first call to mocha's {@linkcode test}-method.
                             */
                            async function GetTestCall(): Promise<CallExpression>
                            {
                                return (await GetTestCalls())[0];
                            }

                            test(
                                `Checking whether a default unit-test named \`${unitTestName}\` is present…`,
                                async function()
                                {
                                    this.timeout(4 * 1000);
                                    this.slow(2 * 1000);

                                    strictEqual((await GetTestCalls()).length, 1);

                                    strictEqual(
                                        (await GetTestCall()).getArguments()[0].asKindOrThrow(SyntaxKind.StringLiteral).getLiteralValue(),
                                        unitTestName);
                                });

                            test(
                                "Checking whether a default assertion exists inside the unit-test…",
                                async function()
                                {
                                    this.timeout(4 * 1000);
                                    this.slow(2 * 1000);

                                    strictEqual(
                                        (await GetTestCall()).getArguments()[1].asKindOrThrow(SyntaxKind.ArrowFunction).getDescendantsOfKind(SyntaxKind.CallExpression).filter(
                                            (callExpression) =>
                                            {
                                                return callExpression.getExpression().getText() === nameof(strictEqual) &&
                                                    callExpression.getArguments().length === 2 &&
                                                    callExpression.getArguments()[0].getText() === callExpression.getArguments()[1].getText();
                                            }).length,
                                        1);
                                });
                        });
                });

            for (let esModule of [true, false])
            {
                let packageType = esModule ? nameof(PackageType.ESModule) : nameof(PackageType.CommonJS);

                suite(
                    packageType,
                    () =>
                    {
                        suite(
                            nameof<TestTestFileMapping>((fileMapping) => fileMapping.Transform),
                            () =>
                            {
                                let runContext: RunContext<TSProjectGenerator>;
                                let generator: TSProjectGenerator;
                                let fileMappingTester: FileMappingTester<TSProjectGenerator, ITSProjectSettings, GeneratorOptions, TestTestFileMapping>;
                                let outputFileName: string;

                                suiteSetup(
                                    async function()
                                    {
                                        this.timeout(5 * 60 * 1000);

                                        runContext = context.ExecuteGenerator().withAnswers(
                                            {
                                                [TSProjectSettingKey.ESModule]: esModule
                                            });

                                        await runContext;
                                        generator = runContext.generator;

                                        spawnNPM(
                                            ["install"],
                                            {
                                                cwd: generator.destinationPath()
                                            });
                                    });

                                suiteTeardown(
                                    async function()
                                    {
                                        this.timeout(20 * 1000);
                                        runContext.cleanup();
                                    });

                                setup(
                                    async function()
                                    {
                                        this.timeout(30 * 1000);

                                        destination = "./src/tests/main.test.ts";
                                        outputFileName = "./lib/tests/main.test.js";
                                        fileMappingTester = new FileMappingTester(generator, new TestTestFileMapping(generator));

                                        suiteFunctionInfo = {
                                            IsEntrypoint: true,
                                            Name: "Tests",
                                            Description: "This is a test"
                                        };

                                        await fileMappingTester.Run();

                                        spawnNPM(
                                            ["run", "build"],
                                            {
                                                cwd: generator.destinationPath()
                                            });
                                    });

                                teardown(
                                    async () =>
                                    {
                                        await fileMappingTester.Clean();
                                    });

                                test(
                                    "Checking whether the expected test is exposed…",
                                    async function()
                                    {
                                        this.timeout(4 * 1000);
                                        this.slow(2 * 1000);

                                        let tests = (await SuiteInspector.Fetch(outputFileName)).Tests;
                                        ok(tests.length > 0);
                                        ok(tests.includes(unitTestName));
                                    });
                            });
                    });
            }
        });
}
