import { basename } from "node:path";
import { ESLintRCFileMappingTests } from "./ESLintRCFileMapping.test.js";
import { NPMIgnoreFileMappingTests } from "./NPMIgnoreFileMapping.test.js";
import { NPMPackagingTests } from "./NPMPackaging/index.test.js";
import { TypeScriptTests } from "./TypeScript/index.test.js";
import { TSProjectGenerator } from "../../../Project/TSProjectGenerator.js";
import { TestContext } from "../../TestContext.js";

/**
 * Registers tests for file-mappings for the {@linkcode TSProjectGenerator}.
 *
 * @param context
 * The test-context.
 */
export function FileMappingTests(context: TestContext<TSProjectGenerator>): void
{
    suite(
        basename(new URL(".", import.meta.url).pathname),
        () =>
        {
            ESLintRCFileMappingTests(context);
            NPMIgnoreFileMappingTests(context);
            NPMPackagingTests(context);
            TypeScriptTests(context);
        });
}
