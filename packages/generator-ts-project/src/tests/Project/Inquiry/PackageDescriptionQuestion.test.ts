import { ok, strictEqual } from "node:assert";
import { createSandbox, SinonSandbox } from "sinon";
import { PackageDescriptionQuestion } from "../../../Project/Inquiry/PackageDescriptionQuestion.js";
import { ITSProjectPackage } from "../../../Project/Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../../../Project/Settings/ITSProjectSettings.js";
import { TSProjectGenerator } from "../../../Project/TSProjectGenerator.js";
import { CreationPromptResolver } from "../../../Project/Workspace/CreationPromptResolver.js";
import { TestContext } from "../../TestContext.js";

/**
 * Registers tests for the {@linkcode PackageDescriptionQuestion} class.
 *
 * @param context
 * The test-context.
 */
export function PackageDescriptionQuestionTests(context: TestContext<TSProjectGenerator>): void
{
    suite(
        nameof(PackageDescriptionQuestion),
        () =>
        {
            let sandbox: SinonSandbox;
            let randomString: string;
            let generator: TSProjectGenerator;
            let resolver: CreationPromptResolver<ITSProjectSettings, ITSProjectPackage>;
            let question: PackageDescriptionQuestion<ITSProjectPackage>;

            suiteSetup(
                async function()
                {
                    this.timeout(5 * 60 * 1000);
                    generator = await context.Generator;
                });

            setup(
                () =>
                {
                    sandbox = createSandbox();
                    randomString = context.RandomString;
                    resolver = new CreationPromptResolver(generator, generator.PackageCreationContext);
                    question = new PackageDescriptionQuestion(resolver);
                });

            teardown(
                () =>
                {
                    sandbox.restore();
                });

            suite(
                nameof<PackageDescriptionQuestion<any>>((question) => question.name),
                () =>
                {
                    test(
                        "Checking whether the name is taken from the resolver…",
                        () =>
                        {
                            sandbox.replaceGetter(
                                resolver,
                                nameof.typed(() => resolver).DescriptionKey,
                                () => randomString as keyof ITSProjectPackage);

                            strictEqual(new PackageDescriptionQuestion(resolver).name, randomString);
                        });
                });

            suite(
                nameof<PackageDescriptionQuestion<any>>((question) => question.Message),
                () =>
                {
                    test(
                        "Checking whether the message is taken from the resolver…",
                        async () =>
                        {
                            sandbox.replaceGetter(
                                resolver,
                                nameof.typed(() => resolver).DescriptionMessage,
                                () => randomString);

                            strictEqual(await question.Message(generator.Settings), randomString);
                        });
                });

            suite(
                nameof<PackageDescriptionQuestion<any>>((question) => question.Default),
                () =>
                {
                    test(
                        "Checking whether the suggested description is taken from the resolver…",
                        async () =>
                        {
                            let stub = sandbox.stub(
                                resolver,
                                nameof.typed(() => resolver).GetSuggestedDescription);

                            stub.resolves(randomString);
                            strictEqual(await question.Default(generator.Settings), randomString);
                            ok(stub.calledOnce);
                        });
                });
        });
}
