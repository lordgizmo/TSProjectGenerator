import { ok, strictEqual } from "node:assert";
import { TempDirectory } from "@manuth/temp-files";
import { createSandbox, SinonSandbox } from "sinon";
import { IPathPromptRootDescriptor } from "../../../Inquiry/Prompts/IPathPromptRootDescriptor.js";
import { PackageDestinationQuestion } from "../../../Project/Inquiry/PackageDestinationQuestion.js";
import { ITSProjectPackage } from "../../../Project/Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../../../Project/Settings/ITSProjectSettings.js";
import { TSProjectGenerator } from "../../../Project/TSProjectGenerator.js";
import { CreationPromptResolver } from "../../../Project/Workspace/CreationPromptResolver.js";
import { TestContext } from "../../TestContext.js";

/**
 * Registers tests for the {@linkcode PackageDestinationQuestion} class.
 *
 * @param context
 * The test-context.
 */
export function PackageDestinationQuestionTests(context: TestContext<TSProjectGenerator>): void
{
    suite(
        nameof(PackageDestinationQuestion),
        () =>
        {
            /**
             * Provides an implementation of the {@linkcode PackageDestinationQuestion} class for testing.
             */
            class TestPackageDestinationQuestion extends PackageDestinationQuestion<ITSProjectPackage>
            {
                /**
                 * @inheritdoc
                 *
                 * @param input
                 * The answer provided by the user.
                 *
                 * @param answers
                 * The answers provided by the user.
                 *
                 * @returns
                 * Either a value indicating whether the answer is valid or a {@linkcode String} which describes the error.
                 */
                public override Validate(input: any, answers: ITSProjectPackage): Promise<string | boolean>
                {
                    return super.Validate(input, answers);
                }
            }

            let sandbox: SinonSandbox;
            let randomString: string;
            let tempDir: TempDirectory;
            let generator: TSProjectGenerator;
            let resolver: CreationPromptResolver<ITSProjectSettings, ITSProjectPackage>;
            let question: TestPackageDestinationQuestion;

            suiteSetup(
                async function()
                {
                    this.timeout(5 * 60 * 1000);
                    tempDir = new TempDirectory();
                    generator = await context.Generator;
                });

            suiteTeardown(
                () =>
                {
                    tempDir.Dispose();
                });

            setup(
                () =>
                {
                    sandbox = createSandbox();
                    randomString = context.RandomString;
                    resolver = new CreationPromptResolver(generator, generator.PackageCreationContext);
                    question = new TestPackageDestinationQuestion(resolver);
                });

            teardown(
                () =>
                {
                    sandbox.restore();
                });

            suite(
                nameof<TestPackageDestinationQuestion>((question) => question.name),
                () =>
                {
                    test(
                        "Checking whether the name is set according to the resolver…",
                        () =>
                        {
                            sandbox.replaceGetter(
                                resolver,
                                nameof.typed(() => resolver).DestinationKey,
                                () => randomString as keyof ITSProjectPackage);

                            strictEqual(new PackageDestinationQuestion(resolver).name, randomString);
                        });
                });

            suite(
                nameof<TestPackageDestinationQuestion>((context) => context.PrefixedRoot),
                () =>
                {
                    test(
                        "Checking whether the settings are loaded from the resolver…",
                        async () =>
                        {
                            for (let allowOutside of [true, false])
                            {
                                let prefixedRoot: IPathPromptRootDescriptor | undefined;

                                let stub = sandbox.stub(
                                    resolver,
                                    nameof.typed(() => resolver).GetPackageRoot);

                                stub.resolves(randomString);

                                sandbox.replaceGetter(
                                    resolver,
                                    nameof.typed(() => resolver).AllowOutside,
                                    () => allowOutside);

                                prefixedRoot = await question.PrefixedRoot(generator.Settings);
                                strictEqual(prefixedRoot?.path, randomString);
                                strictEqual(prefixedRoot?.allowOutside, allowOutside);
                                sandbox.restore();
                            }
                        });
                });

            suite(
                nameof<TestPackageDestinationQuestion>((question) => question.Message),
                () =>
                {
                    test(
                        "Checking whether the message is taken from the resolver…",
                        async () =>
                        {
                            sandbox.replaceGetter(
                                resolver,
                                nameof.typed(() => resolver).DestinationMessage,
                                () => randomString);

                            strictEqual(await question.Message(generator.Settings), randomString);
                        });
                });

            suite(
                nameof<TestPackageDestinationQuestion>((question) => question.Default),
                () =>
                {
                    test(
                        "Checking whether the default value is determined using the resolver…",
                        async () =>
                        {
                            let stub = sandbox.stub(
                                resolver,
                                nameof.typed(() => resolver).GetSuggestedPackagePath);

                            stub.resolves(randomString);
                            strictEqual(await question.default(generator.Settings), randomString);
                            ok(stub.calledOnce);
                        });
                });

            suite(
                nameof<TestPackageDestinationQuestion>((question) => question.Validate),
                () =>
                {
                    test(
                        "Checking whether the validation is performed using the resolver…",
                        async () =>
                        {
                            let stub = sandbox.stub(
                                resolver,
                                nameof.typed(() => resolver).ValidateDestination);

                            stub.resolves(randomString);
                            strictEqual(await question.validate("", generator.Settings), randomString);
                            ok(stub.calledOnce);
                        });
                });
        });
}
