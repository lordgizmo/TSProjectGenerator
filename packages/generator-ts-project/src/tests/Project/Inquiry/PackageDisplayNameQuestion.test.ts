import { notStrictEqual, strictEqual } from "node:assert";
import { createSandbox, SinonSandbox } from "sinon";
import { PackageDisplayNameQuestion } from "../../../Project/Inquiry/PackageDisplayNameQuestion.js";
import { ITSProjectPackage } from "../../../Project/Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../../../Project/Settings/ITSProjectSettings.js";
import { TSProjectGenerator } from "../../../Project/TSProjectGenerator.js";
import { CreationPromptResolver } from "../../../Project/Workspace/CreationPromptResolver.js";
import { TestContext } from "../../TestContext.js";

/**
 * Registers tests for the {@linkcode PackageDisplayNameQuestion} class.
 *
 * @param context
 * The test-context.
 */
export function PackageDisplayNameQuestionTests(context: TestContext<TSProjectGenerator>): void
{
    suite(
        nameof(PackageDisplayNameQuestion),
        () =>
        {
            let sandbox: SinonSandbox;
            let randomString: string;
            let generator: TSProjectGenerator;
            let resolver: CreationPromptResolver<ITSProjectSettings, ITSProjectPackage>;
            let question: PackageDisplayNameQuestion<ITSProjectPackage>;

            suiteSetup(
                async function()
                {
                    this.timeout(5 * 60 * 1000);
                    generator = await context.Generator;
                });

            setup(
                () =>
                {
                    sandbox = createSandbox();
                    randomString = context.RandomString;
                    resolver = new CreationPromptResolver(generator, generator.PackageCreationContext);
                    question = new PackageDisplayNameQuestion(resolver);
                });

            teardown(
                () =>
                {
                    sandbox.restore();
                });

            suite(
                nameof<PackageDisplayNameQuestion<any>>((question) => question.name),
                () =>
                {
                    test(
                        "Checking whether the question key is taken from the resolver…",
                        () =>
                        {
                            sandbox.replaceGetter(
                                resolver,
                                nameof.typed(() => resolver).DisplayNameKey,
                                () => randomString as keyof ITSProjectPackage);

                            strictEqual(new PackageDisplayNameQuestion(resolver).name, randomString);
                        });
                });

            suite(
                nameof<PackageDisplayNameQuestion<any>>((question) => question.Message),
                () =>
                {
                    test(
                        "Checking whether the message is taken from the resolver…",
                        async () =>
                        {
                            sandbox.replaceGetter(
                                resolver,
                                nameof.typed(() => resolver).DisplayNameMessage,
                                () => randomString);

                            strictEqual(await question.Message(generator.Settings), randomString);
                        });
                });

            suite(
                nameof<PackageDisplayNameQuestion<any>>((question) => question.Default),
                () =>
                {
                    test(
                        "Checking whether the name is taken from the resolver…",
                        async () =>
                        {
                            let stub = sandbox.stub(
                                resolver,
                                nameof.typed(() => resolver).GetSuggestedDisplayName);

                            stub.resolves(randomString);
                            strictEqual(await question.default(generator.Settings), randomString);
                        });
                });

            suite(
                nameof<PackageDisplayNameQuestion<any>>((question) => question.Validate),
                () =>
                {
                    test(
                        "Checking whether empty names are reported as invalid…",
                        async () =>
                        {
                            notStrictEqual(await question.Validate("", generator.Settings), true);
                            notStrictEqual(await question.validate("   ", generator.Settings), true);
                            strictEqual(await question.validate(" not empty ", generator.Settings), true);
                        });
                });
        });
}
