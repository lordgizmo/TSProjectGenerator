import { strictEqual } from "node:assert";
import { createSandbox, SinonSandbox } from "sinon";
import { PackageModuleNameQuestion } from "../../../Project/Inquiry/PackageModuleNameQuestion.js";
import { ITSProjectPackage } from "../../../Project/Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../../../Project/Settings/ITSProjectSettings.js";
import { TSProjectGenerator } from "../../../Project/TSProjectGenerator.js";
import { CreationPromptResolver } from "../../../Project/Workspace/CreationPromptResolver.js";
import { TestContext } from "../../TestContext.js";

/**
 * Registers tests for the {@linkcode PackageModuleNameQuestion} class.
 *
 * @param context
 * The test-context.
 */
export function PackageModuleNameQuestionTests(context: TestContext<TSProjectGenerator>): void
{
    suite(
        nameof(PackageModuleNameQuestion),
        () =>
        {
            let sandbox: SinonSandbox;
            let randomString: string;
            let generator: TSProjectGenerator;
            let resolver: CreationPromptResolver<ITSProjectSettings, ITSProjectPackage>;
            let question: PackageModuleNameQuestion<ITSProjectPackage>;

            suiteSetup(
                async function()
                {
                    this.timeout(5 * 60 * 1000);
                    generator = await context.Generator;
                });

            setup(
                () =>
                {
                    sandbox = createSandbox();
                    randomString = context.RandomString;
                    resolver = new CreationPromptResolver(generator, generator.PackageCreationContext);
                    question = new PackageModuleNameQuestion(resolver);
                });

            teardown(
                () =>
                {
                    sandbox.restore();
                });

            suite(
                nameof<PackageModuleNameQuestion<any>>((question) => question.name),
                () =>
                {
                    test(
                        "Checking whether the question key is taken from the resolver…",
                        () =>
                        {
                            sandbox.replaceGetter(
                                resolver,
                                nameof.typed(() => resolver).ModuleNameKey,
                                () => randomString as keyof ITSProjectPackage);

                            strictEqual(new PackageModuleNameQuestion(resolver).name, randomString);
                        });
                });

            suite(
                nameof<PackageModuleNameQuestion<any>>((question) => question.Message),
                () =>
                {
                    test(
                        "Checking whether the message is taken from the resolver…",
                        async () =>
                        {
                            sandbox.replaceGetter(
                                resolver,
                                nameof.typed(() => resolver).ModuleNameMessage,
                                () => randomString);

                            strictEqual(await question.Message(generator.Settings), randomString);
                        });
                });

            suite(
                nameof<PackageModuleNameQuestion<any>>((question) => question.Default),
                () =>
                {
                    test(
                        "Checking whether the default value is determined using the resolver…",
                        async () =>
                        {
                            let stub = sandbox.stub(
                                resolver,
                                nameof.typed(() => resolver).GetSuggestedModuleName);

                            stub.resolves(randomString);
                            strictEqual(await question.Default(generator.Settings), randomString);
                        });
                });

            suite(
                nameof<PackageModuleNameQuestion<any>>((question) => question.Validate),
                () =>
                {
                    test(
                        "Checking whether the answer is validated using the resolver…",
                        async () =>
                        {
                            let stub = sandbox.stub(
                                resolver,
                                nameof.typed(() => resolver).ValidateModuleName);

                            stub.resolves(randomString);
                            strictEqual(await question.Validate("", generator.Settings), randomString);
                        });
                });
        });
}
