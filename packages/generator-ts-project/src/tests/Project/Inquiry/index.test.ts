import { basename } from "node:path";
import { PackageDescriptionQuestionTests } from "./PackageDescriptionQuestion.test.js";
import { PackageDestinationQuestionTests } from "./PackageDestinationQuestion.test.js";
import { PackageDisplayNameQuestionTests } from "./PackageDisplayNameQuestion.test.js";
import { PackageModuleNameQuestionTests } from "./PackageModuleNameQuestion.test.js";
import { TSProjectGenerator } from "../../../Project/TSProjectGenerator.js";
import { TestContext } from "../../TestContext.js";

/**
 * Registers tests for inquiry-components for {@linkcode TSProjectGenerator}s.
 *
 * @param context
 * The test-context.
 */
export function InquiryTests(context: TestContext<TSProjectGenerator>): void
{
    suite(
        basename(new URL(".", import.meta.url).pathname),
        () =>
        {
            PackageDestinationQuestionTests(context);
            PackageDisplayNameQuestionTests(context);
            PackageModuleNameQuestionTests(context);
            PackageDescriptionQuestionTests(context);
        });
}
