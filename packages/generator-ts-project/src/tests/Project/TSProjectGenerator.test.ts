import { ok, strictEqual } from "node:assert";
import { GeneratorSettingKey } from "@manuth/extended-yo-generator";
import { RunContext } from "@manuth/extended-yo-generator-test";
import { Package, PackageType } from "@manuth/package-json-editor";
// eslint-disable-next-line @typescript-eslint/tslint/config
import dedent from "dedent";
import fs from "fs-extra";
import { globby } from "globby";
import minimist from "minimist";
import { SimpleGit, simpleGit } from "simple-git";
import parseArgsStringToArgv from "string-argv";
import { TSConfigJSON } from "types-tsconfig";
import path from "upath";
import { ESLintRCFileMapping } from "../../Project/FileMappings/ESLintRCFileMapping.js";
import { ITSProjectPackage } from "../../Project/Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../../Project/Settings/ITSProjectSettings.js";
import { TSProjectComponent } from "../../Project/Settings/TSProjectComponent.js";
import { TSProjectPackageKey } from "../../Project/Settings/TSProjectPackageKey.js";
import { TSProjectSettingKey } from "../../Project/Settings/TSProjectSettingKey.js";
import { TSProjectGenerator } from "../../Project/TSProjectGenerator.js";
import { ICreationContext } from "../../Project/Workspace/ICreationContext.js";
import { TSConfigFileMapping } from "../../Serialization/TSConfigFileMapping.js";
import { TypeScriptCreatorMapping } from "../../Serialization/TypeScriptCreatorMapping.js";
import { TestContext } from "../TestContext.js";
import { spawnNPM } from "../Utilities.js";

const { pathExists, readFile, readJSON, writeFile } = fs;
const { normalize, parse, resolve } = path;

/**
 * Registers tests for the {@linkcode TSProjectGenerator} class.
 *
 * @param context
 * The test-context.
 */
export function TSProjectGeneratorTests(context: TestContext<TSProjectGenerator>): void
{
    suite(
        nameof(TSProjectGenerator),
        () =>
        {
            let tsConfigFileName = TSConfigFileMapping.GetFileName("base");
            let transformName = "@typescript-nameof/nameof";
            let testCode: string;
            let testFileName: string;
            let generator: TSProjectGenerator;
            context.RegisterCleanupRestorer();

            suiteSetup(
                async function()
                {
                    this.timeout(5 * 60 * 1000);

                    testCode = dedent(
                        `
                                            console.log('hello world');`) + "\n";

                    testFileName = "src/test.ts";
                    generator = await context.Generator;
                });

            suite(
                "General",
                () =>
                {
                    suite(
                        "Common",
                        () =>
                        {
                            test(
                                `Checking whether the \`${transformName}\`-plugin is stripped from \`${tsConfigFileName}\`…`,
                                async () =>
                                {
                                    let tsConfig = await readJSON(generator.destinationPath(tsConfigFileName)) as TSConfigJSON;

                                    strictEqual(
                                        (tsConfig.compilerOptions?.plugins ?? []).filter(
                                            (plugin) =>
                                            {
                                                return (plugin as any).transform === transformName;
                                            }).length,
                                        0);
                                });
                        });

                    for (let monoRepo of [false, true])
                    {
                        let register: () => void = () =>
                        {
                            for (let esModule of [true, false])
                            {
                                let moduleKind = esModule ? nameof(PackageType.ESModule) : nameof(PackageType.CommonJS);

                                suite(
                                    moduleKind,
                                    () =>
                                    {
                                        let gitClient: SimpleGit;
                                        let testContext: RunContext<TSProjectGenerator>;
                                        let generator: TSProjectGenerator;
                                        let workspaceContexts: Array<ICreationContext<ITSProjectSettings, ITSProjectPackage>>;

                                        suiteSetup(
                                            async function()
                                            {
                                                this.timeout(5 * 60 * 1000);
                                                process.env["EMAIL"] = "john.doe@contoso.com";
                                                testContext = context.ExecuteGenerator();

                                                let components = [TSProjectComponent.Linting];

                                                if (monoRepo)
                                                {
                                                    components.push(TSProjectComponent.MonoRepo);
                                                }

                                                testContext.withAnswers(
                                                    {
                                                        [GeneratorSettingKey.Components]: components,
                                                        [TSProjectSettingKey.ESModule]: esModule,
                                                        [TSProjectSettingKey.Workspaces]: [
                                                            {
                                                                [TSProjectPackageKey.Destination]: "./packages/hello",
                                                                [TSProjectPackageKey.DisplayName]: "Hello",
                                                                [TSProjectPackageKey.Name]: "hello"
                                                            },
                                                            {
                                                                [TSProjectPackageKey.Destination]: "./packages/world",
                                                                [TSProjectPackageKey.DisplayName]: "World",
                                                                [TSProjectPackageKey.Name]: "world"
                                                            }
                                                        ]
                                                    });

                                                await testContext;
                                                generator = testContext.generator;
                                                gitClient = simpleGit(generator.destinationPath());

                                                spawnNPM(
                                                    ["install"],
                                                    {
                                                        cwd: generator.destinationPath()
                                                    });
                                            });

                                        suiteTeardown(
                                            function()
                                            {
                                                this.timeout(30 * 1000);
                                                testContext.cleanup();
                                            });

                                        setup(
                                            () =>
                                            {
                                                if (monoRepo)
                                                {
                                                    workspaceContexts = generator.WorkspaceCreationContexts;
                                                }
                                                else
                                                {
                                                    workspaceContexts = [generator.PackageCreationContext];
                                                }
                                            });

                                        test(
                                            "Checking whether the version script works properly…",
                                            async function()
                                            {
                                                this.timeout(2 * 60 * 1000);
                                                this.slow(1 * 60 * 1000);
                                                let version = "4.2.0";
                                                let releaseBranch = `release/v${version}`;
                                                await gitClient.init();
                                                await gitClient.checkoutLocalBranch(releaseBranch);
                                                await gitClient.add(".");
                                                await gitClient.commit("Initial commit.");

                                                spawnNPM(
                                                    [
                                                        "run",
                                                        "bump-version"
                                                    ],
                                                    {
                                                        cwd: generator.destinationPath()
                                                    });

                                                ok((await gitClient.status()).isClean());

                                                let contexts: Array<ICreationContext<ITSProjectSettings, ITSProjectPackage>>;

                                                if (monoRepo)
                                                {
                                                    contexts = generator.WorkspaceCreationContexts;
                                                }
                                                else
                                                {
                                                    contexts = [generator.PackageCreationContext];
                                                }

                                                for (let context of contexts)
                                                {
                                                    strictEqual((await context.Package)?.Version, version);
                                                }
                                            });

                                        test(
                                            "Checking whether the build script works properly…",
                                            async function()
                                            {
                                                this.timeout(1 * 60 * 1000);
                                                this.slow(30 * 1000);

                                                spawnNPM(
                                                    [
                                                        "run",
                                                        "build"
                                                    ],
                                                    {
                                                        cwd: generator.destinationPath()
                                                    });

                                                for (let context of workspaceContexts)
                                                {
                                                    ok(await pathExists(generator.destinationPath(context.Destination, "lib")));
                                                }
                                            });

                                        test(
                                            "Checking whether the build output can be cleaned…",
                                            async function()
                                            {
                                                this.timeout(1 * 60 * 1000);
                                                this.slow(30 * 1000);

                                                for (let script of ["build", "clean"])
                                                {
                                                    spawnNPM(
                                                        [
                                                            "run",
                                                            script
                                                        ],
                                                        {
                                                            cwd: generator.destinationPath()
                                                        });
                                                }

                                                for (let context of workspaceContexts)
                                                {
                                                    ok(!await pathExists(generator.destinationPath(context.Destination, "lib")));
                                                }
                                            });

                                        test(
                                            "Checking whether all typescript files are being linted…",
                                            async function()
                                            {
                                                let npmPackage = new Package(generator.destinationPath(Package.FileName));
                                                let patterns = minimist(parseArgsStringToArgv(npmPackage.Scripts.Get("lint")))._;

                                                let fileMappings = generator.FileMappingCollection.Items.filter(
                                                    (fileMapping) =>
                                                    {
                                                        return fileMapping.ResolverContext instanceof TypeScriptCreatorMapping ||
                                                            fileMapping.ResolverContext instanceof ESLintRCFileMapping ||
                                                            [
                                                                ".mts"
                                                            ].includes(parse(fileMapping.Destination).ext);
                                                    });

                                                let matches = await globby(
                                                    await globby(
                                                        patterns,
                                                        {
                                                            cwd: generator.destinationPath(),
                                                            onlyFiles: false
                                                        }),
                                                    {
                                                        cwd: generator.destinationPath()
                                                    });

                                                ok(
                                                    fileMappings.every(
                                                        (fileMapping) =>
                                                        {
                                                            let fileName = generator.destinationPath(fileMapping.Destination);

                                                            return matches.some(
                                                                (match) =>
                                                                {
                                                                    return generator.destinationPath(match) === fileName;
                                                                });
                                                        }));
                                            });
                                    });
                            }
                        };

                        if (monoRepo)
                        {
                            suite("Mono-Repo", register);
                        }
                        else
                        {
                            register();
                        }
                    }
                });

            suite(
                nameof<TSProjectGenerator>((generator) => generator.writing),
                () =>
                {
                    let expectedPath = ".";
                    let originalDestination: string;
                    let destination: string;
                    let testContext: RunContext<TSProjectGenerator>;
                    let generator: TSProjectGenerator;

                    suiteSetup(
                        async function()
                        {
                            this.timeout(5 * 60 * 1000);
                            testContext = context.ExecuteGenerator();
                            await testContext;
                            generator = testContext.generator;
                        });

                    suiteTeardown(
                        function()
                        {
                            this.timeout(30 * 1000);
                            testContext.cleanup();
                        });

                    setup(
                        () =>
                        {
                            originalDestination = generator.destinationPath();
                            destination = "./test";
                            generator.Settings[TSProjectPackageKey.Destination] = destination;
                        });

                    teardown(
                        () =>
                        {
                            generator.destinationRoot(originalDestination);
                        });

                    test(
                        "Checking whether the destination path is changed properly when writing…",
                        async function()
                        {
                            this.timeout(5 * 60 * 1000);
                            this.timeout(2.5 * 60 * 1000);

                            let expected = resolve(generator.destinationPath(destination));
                            await generator.writing();
                            strictEqual(normalize(resolve(generator.destinationPath())), normalize(expected));
                        });

                    test(
                        "Checking whether the destination setting still points to the same directory after writing…",
                        async function()
                        {
                            this.timeout(5 * 60 * 1000);
                            this.timeout(2.5 * 60 * 1000);

                            let expected = generator.destinationPath(destination);
                            await generator.writing();

                            strictEqual(
                                normalize(resolve(generator.destinationPath(generator.Settings[TSProjectPackageKey.Destination]))),
                                normalize(resolve(expected)));
                        });

                    test(
                        `Checking whether the destination is changed to \`${JSON.stringify(expectedPath)}\` after after writing…`,
                        async function()
                        {
                            this.timeout(5 * 60 * 1000);
                            this.timeout(2.5 * 60 * 1000);
                            await generator.writing();
                            strictEqual(generator.Settings[TSProjectPackageKey.Destination], expectedPath);
                        });
                });

            suite(
                nameof<TSProjectGenerator>((generator) => generator.cleanup),
                () =>
                {
                    /**
                     * Provides an implementation of the {@linkcode TSProjectGenerator} class for testing.
                     */
                    class TestGenerator extends TSProjectGenerator
                    {
                        /**
                         * @inheritdoc
                         */
                        public override async Cleanup(): Promise<void>
                        {
                            cleanupRan = true;
                        }
                    }

                    let cleanupRan: boolean;

                    setup(
                        () =>
                        {
                            cleanupRan = false;
                        });

                    test(
                        `Checking whether the \`${nameof<TestGenerator>((g) => g.Cleanup)}\`-method is executed by default…`,
                        async function()
                        {
                            this.timeout(10 * 1000);
                            this.slow(5 * 1000);
                            await context.CreateGenerator(TestGenerator).cleanup();
                            ok(cleanupRan);
                        });

                    test(
                        `Checking whether the \`${nameof<TestGenerator>((g) => g.Cleanup)}\`-method can be skipped…`,
                        async () =>
                        {
                            let generator = context.CreateGenerator(TestGenerator);
                            generator.options.skipCleanup = true;
                            await generator.cleanup();
                            ok(!cleanupRan);
                        });

                    test(
                        "Checking whether the source-code is cleaned up correctly…",
                        async function()
                        {
                            this.timeout(15 * 60 * 1000);
                            this.slow(7.5 * 60 * 1000);
                            await writeFile(generator.destinationPath(testFileName), testCode);
                            await generator.cleanup();
                            strictEqual((await readFile(generator.destinationPath(testFileName))).toString(), testCode.replace(/'/g, '"'));
                        });
                });
        });
}
