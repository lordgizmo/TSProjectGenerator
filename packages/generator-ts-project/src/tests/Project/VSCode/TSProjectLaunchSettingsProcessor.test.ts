import { ok, strictEqual } from "node:assert";
import { GeneratorOptions, GeneratorSettingKey } from "@manuth/extended-yo-generator";
import upath from "upath";
import { DebugConfiguration } from "vscode";
import { TSProjectWorkspaceComponent } from "../../../Project/Components/TSProjectWorkspaceComponent.js";
import { ITSProjectPackage } from "../../../Project/Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../../../Project/Settings/ITSProjectSettings.js";
import { TSProjectComponent } from "../../../Project/Settings/TSProjectComponent.js";
import { TSProjectPackageKey } from "../../../Project/Settings/TSProjectPackageKey.js";
import { TSProjectSettingKey } from "../../../Project/Settings/TSProjectSettingKey.js";
import { TSProjectGenerator } from "../../../Project/TSProjectGenerator.js";
import { TSProjectLaunchSettingsProcessor } from "../../../Project/VSCode/TSProjectLaunchSettingsProcessor.js";
import { IDebugConfiguration } from "../../../VSCode/IDebugConfiguration.js";
import { ILaunchSettings } from "../../../VSCode/ILaunchSettings.js";
import { TestContext } from "../../TestContext.js";

const { join, normalize } = upath;

/**
 * Registers tests for the {@linkcode TSProjectLaunchSettingsProcessor} class.
 *
 * @param context
 * The test-context.
 */
export function TSProjectLaunchSettingsProcessorTests(context: TestContext<TSProjectGenerator>): void
{
    suite(
        nameof(TSProjectLaunchSettingsProcessor),
        () =>
        {
            /**
             * Provides an implementation of the {@linkcode TSProjectLaunchSettingsProcessor} class for testing.
             */
            class TestTSProjectLaunchSettingsProcessor extends TSProjectLaunchSettingsProcessor<ITSProjectSettings, GeneratorOptions>
            {
                /**
                 * @inheritdoc
                 */
                public override get WorkspaceRootName(): string
                {
                    return super.WorkspaceRootName;
                }

                /**
                 * @inheritdoc
                 *
                 * @param config
                 * The configuration to replace the workspace directives in.
                 *
                 * @param newName
                 * The new name of the workspace.
                 */
                public override ReplaceWorkspaceDirectives(config: IDebugConfiguration, newName?: string | undefined): void
                {
                    super.ReplaceWorkspaceDirectives(config, newName);
                }

                /**
                 * @inheritdoc
                 *
                 * @param debugConfig
                 * The debug-configuration to filter.
                 *
                 * @returns
                 * A value indicating whether the debug-configuration should be included.
                 */
                public override async FilterDebugConfig(debugConfig: DebugConfiguration): Promise<boolean>
                {
                    return super.FilterDebugConfig(debugConfig);
                }

                /**
                 * @inheritdoc
                 *
                 * @param debugConfig
                 * The debug-configuration to process.
                 *
                 * @returns
                 * The processed debug-configuration.
                 */
                public override ProcessDebugConfig(debugConfig: IDebugConfiguration): Promise<IDebugConfiguration>
                {
                    return super.ProcessDebugConfig(debugConfig);
                }
            }

            let generator: TSProjectGenerator;
            let workspaceName = "TSProjectGenerator";
            let component: TSProjectWorkspaceComponent<ITSProjectSettings, GeneratorOptions>;
            let processor: TestTSProjectLaunchSettingsProcessor;

            /**
             * Processes the underlying {@linkcode ILaunchSettings}.
             *
             * @returns
             * The processed {@linkcode ILaunchSettings}.
             */
            async function processLaunchMetadata(): Promise<ILaunchSettings>
            {
                let launchMetadata = await component.Source.GetLaunchMetadata();
                ok(launchMetadata);
                return processor.Process(launchMetadata);
            }

            suiteSetup(
                async function()
                {
                    this.timeout(5 * 60 * 1000);
                    generator = await context.Generator;
                });

            setup(
                () =>
                {
                    component = new TSProjectWorkspaceComponent(generator);
                    processor = new TestTSProjectLaunchSettingsProcessor(component);
                });

            suite(
                nameof<TestTSProjectLaunchSettingsProcessor>((processor) => processor.ReplaceWorkspaceDirectives),
                () =>
                {
                    test(
                        "Checking whether program paths are adjusted properly for mono repos…",
                        () =>
                        {
                            generator.Settings[GeneratorSettingKey.Components] = [TSProjectComponent.MonoRepo];

                            let debugConfig = {
                                program: "${workspaceFolder}/node_modules/.bin/yo"
                            } as IDebugConfiguration;

                            processor.ReplaceWorkspaceDirectives(debugConfig);
                            ok(debugConfig.program?.startsWith(processor.GetWorkspaceFolderDirective(processor.WorkspaceRootName)));
                        });
                });

            suite(
                nameof<TestTSProjectLaunchSettingsProcessor>((processor) => processor.FilterDebugConfig),
                () =>
                {
                    test(
                        `Checking whether only debug-configurations for the \`${workspaceName}\` are included…`,
                        async function()
                        {
                            this.timeout(4 * 1000);
                            this.slow(2 * 1000);
                            let workspaceDirective = processor.GetWorkspaceFolderDirective(workspaceName);

                            strictEqual(
                                (await processor.Process(
                                    {
                                        version: "",
                                        configurations: [
                                            {
                                                name: "",
                                                request: "",
                                                type: ""
                                            }
                                        ]
                                    })).configurations?.length,
                                0);

                            strictEqual(
                                (await processor.Process(
                                    {
                                        version: "",
                                        configurations: [
                                            {
                                                name: "",
                                                request: "",
                                                type: "",
                                                cwd: workspaceDirective
                                            }
                                        ]
                                    })).configurations?.length,
                                1);

                            strictEqual(
                                (await processor.Process(
                                    {
                                        version: "",
                                        configurations: [
                                            {
                                                name: "",
                                                request: "",
                                                type: "",
                                                args: [
                                                    workspaceDirective
                                                ]
                                            }
                                        ]
                                    })).configurations?.length,
                                1);
                        });

                    test(
                        "Checking whether `yeoman` debug-configurations are not present…",
                        async function()
                        {
                            this.timeout(4 * 1000);
                            this.slow(2 * 1000);

                            let launchSettings = await processLaunchMetadata();

                            ok(
                                launchSettings.configurations?.every(
                                    (debugConfig) => !normalize(debugConfig.program ?? "").toLowerCase().endsWith(
                                        join("yo", "lib", "cli.js"))));
                        });
                });

            suite(
                nameof<TestTSProjectLaunchSettingsProcessor>((processor) => processor.ProcessDebugConfig),
                () =>
                {
                    test(
                        "Checking whether unnecessary settings are being removed…",
                        async function()
                        {
                            this.timeout(4 * 1000);
                            this.slow(2 * 1000);
                            let launchSettings = await processor.Process(await component.Source.GetLaunchMetadata() as any);

                            ok(
                                launchSettings.configurations?.every(
                                    (debugConfig) =>
                                    {
                                        return (
                                            debugConfig.presentation === undefined &&
                                            debugConfig.autoAttachChildProcesses === undefined &&
                                            debugConfig.SkipFiles === undefined);
                                    }));
                        });

                    test(
                        "Checking whether workspace directives are replaced with a placeholder for mono repos…",
                        async () =>
                        {
                            generator.Settings[GeneratorSettingKey.Components] = [TSProjectComponent.MonoRepo];
                            let workspaceName = "Test";

                            let debugConfig: IDebugConfiguration = {
                                type: "node",
                                name: "Test",
                                request: "launch",
                                cwd: "${workspaceFolder}"
                            };

                            debugConfig = await processor.ProcessDebugConfig(debugConfig);
                            ok(debugConfig.cwd);

                            strictEqual(
                                processor.ReplaceWorkspaceFolder(debugConfig.cwd, workspaceName),
                                processor.GetWorkspaceFolderDirective(workspaceName));
                        });
                });

            suite(
                nameof<TSProjectLaunchSettingsProcessor<any, any>>((processor) => processor.Process),
                () =>
                {
                    setup(
                        () =>
                        {
                            generator.Settings[GeneratorSettingKey.Components] = [TSProjectComponent.MonoRepo];

                            generator.Settings[TSProjectSettingKey.Workspaces] = ["Thor", "Odin"].map(
                                (workspaceName): ITSProjectPackage =>
                                {
                                    return {
                                        [TSProjectPackageKey.Destination]: "",
                                        [TSProjectPackageKey.DisplayName]: workspaceName,
                                        [TSProjectPackageKey.Name]: "",
                                        [TSProjectPackageKey.Description]: ""
                                    };
                                });
                        });

                    /**
                     * Gets all mocha configurations emitted by the processor.
                     *
                     * @returns
                     * The mocha configurations.
                     */
                    async function getMochaConfigs(): Promise<IDebugConfiguration[]>
                    {
                        return (await processLaunchMetadata()).configurations?.filter(
                            (config) =>
                            {
                                return config.program?.includes("mocha");
                            }) ?? [];
                    }

                    test(
                        "Checking whether for mono repos a mocha config is added for each workspace…",
                        async () =>
                        {
                            let mochaConfigs = await getMochaConfigs();

                            ok(
                                generator.Settings[TSProjectSettingKey.Workspaces].every(
                                    (workspace) =>
                                    {
                                        let workspaceName = workspace[TSProjectPackageKey.DisplayName];

                                        return mochaConfigs.some(
                                            (config) =>
                                            {
                                                return config.cwd === processor.GetWorkspaceFolderDirective(workspaceName);
                                            });
                                    }));
                        });

                    test(
                        "Checking whether a compound configuration is added for mono repos which executes all mocha tasks…",
                        async () =>
                        {
                            let launchSettings = await processLaunchMetadata();
                            strictEqual(launchSettings.compounds?.length, 1);
                            let globalTask = launchSettings.compounds[0];

                            ok(
                                (await getMochaConfigs()).every(
                                    (mochaConfig) =>
                                    {
                                        return globalTask.configurations.includes(mochaConfig.name);
                                    }));
                        });
                });
        });
}
