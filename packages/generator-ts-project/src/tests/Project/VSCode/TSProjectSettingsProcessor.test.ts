import { ok, strictEqual } from "node:assert";
import { GeneratorOptions, GeneratorSettingKey } from "@manuth/extended-yo-generator";
import { TSProjectWorkspaceComponent } from "../../../Project/Components/TSProjectWorkspaceComponent.js";
import { ITSProjectSettings } from "../../../Project/Settings/ITSProjectSettings.js";
import { TSProjectComponent } from "../../../Project/Settings/TSProjectComponent.js";
import { TSProjectGenerator } from "../../../Project/TSProjectGenerator.js";
import { TSProjectSettingsProcessor } from "../../../Project/VSCode/TSProjectSettingsProcessor.js";
import { TestContext } from "../../TestContext.js";

/**
 * Registers tests for the {@linkcode TSProjectSettingsProcessor} class.
 *
 * @param context
 * The test-context.
 */
export function TSProjectSettingsProcessorTests(context: TestContext<TSProjectGenerator>): void
{
    suite(
        nameof(TSProjectSettingsProcessor),
        () =>
        {
            const workingDirectorySetting = "terminal.integrated.cwd";

            let excludedSettings = [
                "files.associations",
                "mochaExplorer.debuggerConfig",
                "search.exclude",
                workingDirectorySetting,
                "typescript.tsdk"
            ];

            let generator: TSProjectGenerator;
            let settings: Record<string, any>;
            let component: TSProjectWorkspaceComponent<ITSProjectSettings, GeneratorOptions>;
            let processor: TSProjectSettingsProcessor<ITSProjectSettings, GeneratorOptions>;

            suiteSetup(
                async function()
                {
                    this.timeout(5 * 60 * 1000);
                    generator = await context.Generator;
                });

            setup(
                async () =>
                {
                    component = new TSProjectWorkspaceComponent(generator);
                    processor = new TSProjectSettingsProcessor(component);
                    settings = await processor.Process(await component.Source.GetSettingsMetadata() ?? {});
                });

            suite(
                nameof<TSProjectSettingsProcessor<any, any>>((processor) => processor.Process),
                () =>
                {
                    for (let excludedSetting of excludedSettings)
                    {
                        test(
                            `Checking whether the \`${excludedSetting}\` setting is excluded…`,
                            async () =>
                            {
                                ok(!(excludedSetting in settings));
                            });
                    }

                    test(
                        `Checking whether the \`${workingDirectorySetting}\` is set for mono repos…`,
                        async () =>
                        {
                            generator.Settings[GeneratorSettingKey.Components] = [TSProjectComponent.MonoRepo];
                            let settings = await processor.Process(await component.Source.GetSettingsMetadata() ?? {});
                            strictEqual(settings[workingDirectorySetting], "${workspaceFolder:Solution Items}");
                        });
                });
        });
}
