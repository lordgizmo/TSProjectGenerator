import { deepStrictEqual, ok, strictEqual } from "node:assert";
import { GeneratorOptions, GeneratorSettingKey } from "@manuth/extended-yo-generator";
import { TSProjectWorkspaceComponent } from "../../../Project/Components/TSProjectWorkspaceComponent.js";
import { ITSProjectSettings } from "../../../Project/Settings/ITSProjectSettings.js";
import { TSProjectComponent } from "../../../Project/Settings/TSProjectComponent.js";
import { TSProjectGenerator } from "../../../Project/TSProjectGenerator.js";
import { TSProjectTasksProcessor } from "../../../Project/VSCode/TSProjectTasksProcessor.js";
import { ITaskDefinition } from "../../../VSCode/ITaskDefinition.js";
import { TestContext } from "../../TestContext.js";

/**
 * Registers tests for the {@linkcode TSProjectTasksProcessor} class.
 *
 * @param context
 * The test-context.
 */
export function TSProjectTasksProcessorTests(context: TestContext<TSProjectGenerator>): void
{
    suite(
        nameof(TSProjectTasksProcessor),
        () =>
        {
            /**
             * Provides an implementation of the {@linkcode TSProjectTasksProcessor} class for testing.
             */
            class TestTSProjectTasksProcessor extends TSProjectTasksProcessor<ITSProjectSettings, GeneratorOptions>
            {
                /**
                 * @inheritdoc
                 *
                 * @param task
                 * The task to process.
                 *
                 * @returns
                 * The processed task.
                 */
                public override ProcessTask(task: ITaskDefinition): Promise<ITaskDefinition>
                {
                    return super.ProcessTask(task);
                }
            }

            let lintTaskName = "Lint";
            let generator: TSProjectGenerator;
            let component: TSProjectWorkspaceComponent<ITSProjectSettings, GeneratorOptions>;
            let processor: TestTSProjectTasksProcessor;

            suiteSetup(
                async function()
                {
                    this.timeout(5 * 60 * 1000);
                    generator = await context.Generator;
                    component = new TSProjectWorkspaceComponent(generator);
                    processor = new TestTSProjectTasksProcessor(component);
                });

            /**
             * Processes the specified {@linkcode task}.
             *
             * @param task
             * The task to process.
             *
             * @returns
             * The processed task.
             */
            async function ProcessTask(task: ITaskDefinition): Promise<ITaskDefinition | undefined>
            {
                let result = (await processor.Process(
                    {
                        version: "",
                        tasks: [
                            task
                        ]
                    }))?.tasks?.[0];

                return result;
            }

            suite(
                nameof<TestTSProjectTasksProcessor>((processor) => processor.ProcessTask),
                () =>
                {
                    test(
                        "Checking whether `cwd` option is removed if it points to the workspace folder…",
                        async () =>
                        {
                            let config = {
                                options: {
                                    cwd: "${workspaceFolder}"
                                }
                            } as ITaskDefinition;

                            config = await processor.ProcessTask(config);
                            strictEqual(config.options?.cwd, undefined);
                        });

                    test(
                        "Checking whether the `cwd` option persists if it points to any other path…",
                        async () =>
                        {
                            let path = "${workspaceFolder}/src";

                            let config = {
                                options: {
                                    cwd: path
                                }
                            } as ITaskDefinition;

                            config = await processor.ProcessTask(config);
                            strictEqual(config.options?.cwd, path);
                        });

                    test(
                        "Checking whether `options` are deleted if left empty…",
                        async () =>
                        {
                            let configurations = [
                                {
                                    options: {}
                                },
                                {
                                    options: {
                                        cwd: "${workspaceFolder:Server}"
                                    }
                                }
                            ] as ITaskDefinition[];

                            for (let configuration of configurations)
                            {
                                configuration = await processor.ProcessTask(configuration);
                                ok(!("options" in configuration));
                            }
                        });

                    test(
                        "Checking whether problem-matchers only containing a `base` are processed correctly if possible…",
                        async () =>
                        {
                            let base = "$eslint-compact";

                            let fileLocationConfig = {
                                problemMatcher: {
                                    fileLocation: [
                                        "relative",
                                        "${workspaceFolder:Named}"
                                    ],
                                    base
                                }
                            } as ITaskDefinition;

                            let configurations = [
                                {
                                    problemMatcher: {
                                        base
                                    }
                                },
                                fileLocationConfig
                            ] as ITaskDefinition[];

                            for (let config of configurations)
                            {
                                config = await processor.ProcessTask({ ...config });
                                strictEqual(config.problemMatcher, base);
                            }

                            generator.Settings[GeneratorSettingKey.Components] = [TSProjectComponent.MonoRepo];

                            let config = await processor.ProcessTask({ ...fileLocationConfig });
                            ok(typeof config.problemMatcher === "object");
                        });
                });

            suite(
                nameof<TSProjectTasksProcessor<any, any>>((processor) => processor.Process),
                () =>
                {
                    test(
                        `Checking whether the problem-matcher of the \`${lintTaskName}\` task is correct…`,
                        async () =>
                        {
                            let tasks = await processor.Process(await component.Source.GetTasksMetadata() as any);

                            let lintTask = tasks.tasks?.find(
                                (task) =>
                                    typeof task.label === "string" &&
                                    task.label.toLowerCase() === lintTaskName.toLowerCase());

                            ok(
                                typeof lintTask?.problemMatcher === "string" &&
                                lintTask.problemMatcher.startsWith("$eslint"));
                        });

                    test(
                        "Checking whether shell-scripts are converted to npm-scripts…",
                        async () =>
                        {
                            deepStrictEqual(
                                await ProcessTask(
                                    {
                                        type: "shell",
                                        command: "npm",
                                        args: [
                                            "run",
                                            "lint"
                                        ]
                                    }),
                                {
                                    type: "npm",
                                    script: "lint"
                                });
                        });

                    test(
                        "Checking whether shell-scripts with unspecified npm-script names aren't converted…",
                        async () =>
                        {
                            let task = {
                                type: "shell",
                                command: "npm",
                                args: [
                                    "run"
                                ]
                            };

                            deepStrictEqual(await ProcessTask(task), task);
                        });

                    test(
                        "Checking whether arguments for the npm-scripts are preserved…",
                        async () =>
                        {
                            let args = [
                                "--target",
                                "Release"
                            ];

                            deepStrictEqual(
                                await ProcessTask(
                                    {
                                        type: "shell",
                                        command: "npm",
                                        args: [
                                            "run",
                                            "build",
                                            ...args
                                        ]
                                    }),
                                {
                                    type: "npm",
                                    script: "build",
                                    args
                                });
                        });

                    test(
                        "Checking whether inexistent properties aren't added to the task…",
                        async () =>
                        {
                            let task = {
                                type: "shell",
                                command: "npm",
                                args: [
                                    "run",
                                    "build"
                                ]
                            };

                            let processedTask = await ProcessTask(task);
                            ok(processedTask);
                            ok(!("label" in processedTask));
                        });
                });
        });
}
