import { ok } from "node:assert";
import { GeneratorOptions, GeneratorSettingKey } from "@manuth/extended-yo-generator";
import { TSProjectWorkspaceComponent } from "../../../Project/Components/TSProjectWorkspaceComponent.js";
import { ITSProjectSettings } from "../../../Project/Settings/ITSProjectSettings.js";
import { TSProjectComponent } from "../../../Project/Settings/TSProjectComponent.js";
import { TSProjectPackageKey } from "../../../Project/Settings/TSProjectPackageKey.js";
import { TSProjectSettingKey } from "../../../Project/Settings/TSProjectSettingKey.js";
import { TSProjectGenerator } from "../../../Project/TSProjectGenerator.js";
import { TSProjectWorkspaceProcessor } from "../../../Project/VSCode/TSProjectWorkspaceProcessor.js";
import { TestContext } from "../../TestContext.js";

/**
 * Registers tests for the {@linkcode TSProjectWorkspaceProcessor} class.
 *
 * @param context
 * The test context.
 */
export function TSProjectWorkspaceProcessorTests(context: TestContext<TSProjectGenerator>): void
{
    suite(
        nameof(TSProjectWorkspaceProcessor),
        () =>
        {
            let generator: TSProjectGenerator;
            let component: TSProjectWorkspaceComponent<ITSProjectSettings, GeneratorOptions>;
            let processor: TSProjectWorkspaceProcessor<ITSProjectSettings, GeneratorOptions>;

            suiteSetup(
                async function()
                {
                    this.timeout(5 * 60 * 1000);
                    generator = await context.Generator;
                });

            setup(
                () =>
                {
                    component = new TSProjectWorkspaceComponent(generator);
                    processor = new TSProjectWorkspaceProcessor(component);
                });

            suite(
                nameof<TSProjectWorkspaceProcessor<any, any>>((processor) => processor.Process),
                () =>
                {
                    test(
                        "Checking whether a workspace folder for each workspace is added…",
                        async () =>
                        {
                            generator.Settings[GeneratorSettingKey.Components] = [TSProjectComponent.MonoRepo];

                            generator.Settings[TSProjectSettingKey.Workspaces] = [
                                {
                                    [TSProjectPackageKey.Destination]: "./a",
                                    [TSProjectPackageKey.DisplayName]: "Test A",
                                    [TSProjectPackageKey.Name]: "a",
                                    [TSProjectPackageKey.Description]: ""
                                },
                                {
                                    [TSProjectPackageKey.Destination]: "./b",
                                    [TSProjectPackageKey.DisplayName]: "Test B",
                                    [TSProjectPackageKey.Name]: "b",
                                    [TSProjectPackageKey.Description]: ""
                                }
                            ];

                            let workspaceData = await processor.Process(await component.GetWorkspaceMetadata());

                            for (let workspace of [generator.Settings, ...generator.Settings[TSProjectSettingKey.Workspaces]])
                            {
                                let workspacePath = generator.destinationPath(workspace[TSProjectPackageKey.Destination]);

                                ok(
                                    workspaceData.folders.some(
                                        (folder) =>
                                        {
                                            if ("path" in folder)
                                            {
                                                return generator.destinationPath(folder.path) === workspacePath;
                                            }
                                            else
                                            {
                                                return true;
                                            }
                                        }));
                            }
                        });
                });
        });
}
