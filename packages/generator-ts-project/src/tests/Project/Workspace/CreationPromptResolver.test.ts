import { deepStrictEqual, notStrictEqual, ok, strictEqual } from "node:assert";
import { basename, join } from "node:path";
import { Package } from "@manuth/package-json-editor";
import { TempDirectory, TempFileSystem } from "@manuth/temp-files";
// eslint-disable-next-line @typescript-eslint/tslint/config
import dedent from "dedent";
import fs from "fs-extra";
import { createSandbox, SinonSandbox } from "sinon";
import { ITSProjectPackage } from "../../../Project/Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../../../Project/Settings/ITSProjectSettings.js";
import { TSProjectPackageKey } from "../../../Project/Settings/TSProjectPackageKey.js";
import { TSProjectGenerator } from "../../../Project/TSProjectGenerator.js";
import { CreationPromptResolver } from "../../../Project/Workspace/CreationPromptResolver.js";
import { ICreationContextSettings } from "../../../Project/Workspace/ICreationContextSettings.js";
import { RootSettings } from "../../../Project/Workspace/RootSettings.js";
import { TestContext } from "../../TestContext.js";

const { remove, writeFile, writeJSON } = fs;

/**
 * Registers tests for the {@linkcode CreationPromptResolver} class.
 *
 * @param context
 * The test context.
 */
export function CreationPromptResolverTests(context: TestContext<TSProjectGenerator>): void
{
    suite(
        nameof(CreationPromptResolver),
        () =>
        {
            /**
             * Provides an implementation of the {@linkcode CreationPromptResolver} class for testing.
             */
            class TestPromptResolver extends CreationPromptResolver<ITSProjectSettings, ITSProjectPackage>
            { }

            let sandbox: SinonSandbox;
            let tempDir: TempDirectory;
            let generator: TSProjectGenerator;
            let destination: string;
            let contextSettings: ICreationContextSettings<ITSProjectSettings, ITSProjectPackage>;
            let resolver: TestPromptResolver;
            let npmPackage: Package;
            let answers: ITSProjectPackage;

            /**
             * Saves the {@linkcode npmPackage}.
             */
            async function SavePackage(): Promise<void>
            {
                await writeJSON(npmPackage.FileName, npmPackage.ToJSON());
            }

            suiteSetup(
                async function()
                {
                    this.timeout(30 * 1000);
                    generator = await context.Generator;
                });

            setup(
                () =>
                {
                    sandbox = createSandbox();
                    tempDir = new TempDirectory();
                    destination = tempDir.FullName;
                    contextSettings = new RootSettings();
                    resolver = new TestPromptResolver(generator, generator.GetCreationContext(contextSettings));
                    npmPackage = new Package(join(tempDir.FullName, Package.FileName), {});

                    answers = {
                        get [TSProjectPackageKey.Destination]()
                        {
                            return destination;
                        }
                    } as ITSProjectPackage;
                });

            teardown(
                () =>
                {
                    tempDir.Dispose();
                    sandbox.restore();
                });

            suite(
                nameof<TestPromptResolver>((resolver) => resolver.GetSuggestedDisplayName),
                () =>
                {
                    test(
                        "Checking whether the human readable name is suggested based on the chosen package path…",
                        async () =>
                        {
                            strictEqual(await resolver.GetSuggestedDisplayName(answers), basename(destination));
                        });
                });

            suite(
                nameof<TestPromptResolver>((resolver) => resolver.GetSuggestedDescription),
                () =>
                {
                    let description: string;

                    setup(
                        () =>
                        {
                            description = context.RandomString;
                        });

                    test(
                        `Checking whether the suggested description is taken from the \`${Package.FileName}\` file if possible…`,
                        async () =>
                        {
                            strictEqual(await resolver.GetSuggestedDescription(answers), undefined);
                            npmPackage.Description = description;
                            await SavePackage();
                            strictEqual(await resolver.GetSuggestedDescription(answers), description);
                        });

                    test(
                        "Checking whether the suggested description is otherwise taken from the `README.md` file if existent…",
                        async () =>
                        {
                            await writeFile(
                                tempDir.MakePath("README.md"),
                                dedent(
                                    `
                                        # Test
                                        ${description}`));

                            strictEqual(await resolver.GetSuggestedDescription(answers), description);
                        });
                });

            suite(
                nameof<TestPromptResolver>((resolver) => resolver.GetDestination),
                () =>
                {
                    test(
                        "Checking whether the destination chosen by the user is returned…",
                        async () =>
                        {
                            strictEqual(resolver.GetDestination(answers), destination);
                        });
                });

            suite(
                nameof<TestPromptResolver>((resolver) => resolver.GetPackagePath),
                () =>
                {
                    test(
                        "Checking whether relative paths are resolved relative to the generator's current destination path…",
                        async () =>
                        {
                            destination = "./test";
                            strictEqual(resolver.GetPackagePath(answers), generator.destinationPath(destination));
                        });

                    test(
                        "Checking whether absolute paths are returned properly…",
                        async () =>
                        {
                            destination = TempFileSystem.TempName();
                            strictEqual(resolver.GetPackagePath(answers), destination);
                        });
                });

            suite(
                nameof<TestPromptResolver>((resolver) => resolver.GetPackageFileName),
                () =>
                {
                    test(
                        `Checking whether a path to the \`${Package.FileName}\` file inside the package directory is returned…`,
                        async () =>
                        {
                            strictEqual(
                                await resolver.GetPackageFileName(answers),
                                join(resolver.GetPackagePath(answers), Package.FileName));
                        });
                });

            suite(
                nameof<TestPromptResolver>((resolver) => resolver.GetPackage),
                () =>
                {
                    test(
                        `Checking whether a package is returned even if the \`${Package.FileName}\` does not exist…`,
                        async () =>
                        {
                            try
                            {
                                await remove(await resolver.GetPackageFileName(answers));
                            }
                            catch
                            { }

                            deepStrictEqual(await resolver.GetPackage(answers), new Package(await resolver.GetPackageFileName(answers), {}));
                        });

                    test(
                        `Checking whether a parsed representation of the package is returned if the \`${Package.FileName}\` exists…`,
                        async () =>
                        {
                            let fileName = await resolver.GetPackageFileName(answers);
                            let npmPackage = new Package(fileName, {});
                            npmPackage.Name = context.RandomString;
                            await writeJSON(fileName, npmPackage.ToJSON());
                            strictEqual((await resolver.GetPackage(answers))?.Name, npmPackage.Name);
                        });
                });

            suite(
                nameof<TestPromptResolver>((resolver) => resolver.GetDisplayName),
                () =>
                {
                    test(
                        "Checking whether the human readable name chosen by the user is returned…",
                        async () =>
                        {
                            let displayName = context.RandomString;
                            answers[TSProjectPackageKey.DisplayName] = displayName;
                            strictEqual(resolver.GetDisplayName(answers), displayName);
                        });
                });

            suite(
                nameof<TestPromptResolver>((resolver) => resolver.ValidateDestination),
                () =>
                {
                    test(
                        "Checking whether banned directories are invalid…",
                        async () =>
                        {
                            let banned: string;

                            sandbox.replaceGetter(
                                contextSettings,
                                nameof.typed(() => contextSettings).BannedDirectories,
                                () => [banned]);

                            let assertions: Array<[string, boolean]> = [
                                ["valid", true],
                                ["llama", false],
                                ["./llama", false],
                                ["./packages/../llama", false],
                                ["././llama", false]
                            ];

                            for (let item of ["llama", "./llama"])
                            {
                                banned = item;

                                for (let assertion of assertions)
                                {
                                    if (assertion[1])
                                    {
                                        ok(await resolver.ValidateDestination(assertion[0], answers));
                                    }
                                    else
                                    {
                                        notStrictEqual(await resolver.ValidateDestination(assertion[0], answers), true);
                                    }
                                }
                            }
                        });
                });

            suite(
                nameof<TestPromptResolver>((resolver) => resolver.ValidateDisplayName),
                () =>
                {
                    test(
                        "Checking whether banned display names are invalid…",
                        async () =>
                        {
                            let banned = "Interceptor";

                            sandbox.replaceGetter(
                                contextSettings,
                                nameof.typed(() => contextSettings).BannedDisplayNames,
                                () => [banned]);

                            ok(await resolver.ValidateDisplayName("Truce", answers));
                            notStrictEqual(await resolver.ValidateDisplayName(banned, answers), true);
                        });
                });

            suite(
                nameof<TestPromptResolver>((resolver) => resolver.ValidateModuleName),
                () =>
                {
                    test(
                        "Checking whether banned module names are invalid…",
                        async () =>
                        {
                            let banned = "my-client";

                            sandbox.replaceGetter(
                                contextSettings,
                                nameof.typed(() => contextSettings).BannedModuleNames,
                                () => [banned]);

                            ok(await resolver.ValidateModuleName("my-test", answers));
                            notStrictEqual(await resolver.ValidateModuleName(banned, answers), true);
                        });
                });
        });
}
