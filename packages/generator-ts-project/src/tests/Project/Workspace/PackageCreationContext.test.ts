import { deepStrictEqual, ok, strictEqual } from "node:assert";
import { GeneratorOptions, GeneratorSettingKey, IFileMapping, Question } from "@manuth/extended-yo-generator";
import { Package } from "@manuth/package-json-editor";
import { TempDirectory } from "@manuth/temp-files";
import fs from "fs-extra";
import isEqual from "lodash.isequal";
import { createSandbox, SinonSandbox } from "sinon";
import { ITSProjectPackage } from "../../../Project/Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../../../Project/Settings/ITSProjectSettings.js";
import { TSProjectComponent } from "../../../Project/Settings/TSProjectComponent.js";
import { TSProjectPackageKey } from "../../../Project/Settings/TSProjectPackageKey.js";
import { TSProjectSettingKey } from "../../../Project/Settings/TSProjectSettingKey.js";
import { TSProjectGenerator } from "../../../Project/TSProjectGenerator.js";
import { ICreationContextSettings } from "../../../Project/Workspace/ICreationContextSettings.js";
import { PackageCreationContext } from "../../../Project/Workspace/PackageCreationContext.js";
import { RootSettings } from "../../../Project/Workspace/RootSettings.js";
import { TestContext } from "../../TestContext.js";

const { writeJson } = fs;

/**
 * Registers tests for the {@linkcode PackageCreationContext} class.
 *
 * @param context
 * The test context.
 */
export function PackageCreationContextTests(context: TestContext<TSProjectGenerator>): void
{
    suite(
        nameof(PackageCreationContext),
        () =>
        {
            /**
             * Provides an implementation of the {@linkcode PackageCreationContext} class for testing.
             */
            class TestCreationContext extends PackageCreationContext<ITSProjectSettings, GeneratorOptions, ITSProjectPackage>
            {
                /**
                 * @inheritdoc
                 */
                public override get ModuleKindKey(): TSProjectSettingKey
                {
                    return moduleKindKey as TSProjectSettingKey;
                }

                /**
                 * @inheritdoc
                 */
                public override get DestinationMessage(): string
                {
                    return "";
                }

                /**
                 * @inheritdoc
                 */
                public override get DisplayNameMessage(): string
                {
                    return "";
                }

                /**
                 * @inheritdoc
                 */
                public override get ModuleKindMessage(): string
                {
                    return moduleKindMessage;
                }

                /**
                 * @inheritdoc
                 */
                public override get DescriptionMessage(): string
                {
                    return "";
                }

                /**
                 * @inheritdoc
                 */
                public override get CommonFileMappings(): Array<IFileMapping<ITSProjectSettings, GeneratorOptions>>
                {
                    return super.CommonFileMappings;
                }

                /**
                 * @inheritdoc
                 */
                public override get WorkspaceRootFileMappings(): Array<IFileMapping<ITSProjectSettings, GeneratorOptions>>
                {
                    return super.WorkspaceRootFileMappings;
                }

                /**
                 * @inheritdoc
                 */
                public override get WorkspaceFileMappings(): Array<IFileMapping<ITSProjectSettings, GeneratorOptions>>
                {
                    return super.WorkspaceFileMappings;
                }

                /**
                 * @inheritdoc
                 */
                public override get ModuleKindQuestion(): Question<ITSProjectSettings>
                {
                    return super.ModuleKindQuestion;
                }

                /**
                 * @inheritdoc
                 *
                 * @param answers
                 * The answers provided by the user.
                 *
                 * @returns
                 * A suggested module name for the package.
                 */
                public override CreateSuggestedModuleName(answers: ITSProjectPackage): Promise<string>
                {
                    return super.CreateSuggestedModuleName(answers);
                }
            }

            let sandbox: SinonSandbox;
            let randomString: string;
            let generator: TSProjectGenerator;
            let contextSettings: ICreationContextSettings<ITSProjectSettings, ITSProjectPackage>;
            let creationContext: TestCreationContext;
            let moduleKindKey: string;
            let moduleKindMessage: string;
            let settings: ITSProjectSettings;

            suiteSetup(
                async function()
                {
                    this.timeout(30 * 1000);
                    generator = await context.Generator;
                });

            setup(
                () =>
                {
                    sandbox = createSandbox();
                    randomString = context.RandomString;
                    contextSettings = new RootSettings();
                    creationContext = new TestCreationContext(generator, contextSettings);
                    moduleKindKey = context.RandomString;
                    moduleKindMessage = `${context.RandomString}2`;
                    settings = {} as ITSProjectSettings;
                });

            teardown(
                () =>
                {
                    sandbox.restore();
                });

            suite(
                nameof<TestCreationContext>((context) => context.AllowOutside),
                () =>
                {
                    test(
                        "Checking whether the value is taken from the context settings…",
                        () =>
                        {
                            for (let allowOutside of [true, false])
                            {
                                sandbox.replaceGetter(
                                    contextSettings,
                                    nameof.typed(() => contextSettings).AllowOutside,
                                    () => allowOutside);

                                strictEqual(creationContext.AllowOutside, allowOutside);
                                sandbox.restore();
                            }
                        });
                });

            let redirectedProperties: Array<[propertyName: string, methodName: string]> = [
                [
                    nameof<TestCreationContext>((context) => context.Destination),
                    nameof<TestCreationContext>((context) => context.Resolver.GetDestination)
                ],
                [
                    nameof<TestCreationContext>((context) => context.PackagePath),
                    nameof<TestCreationContext>((context) => context.Resolver.GetPackagePath)
                ],
                [
                    nameof<TestCreationContext>((context) => context.PackageFileName),
                    nameof<TestCreationContext>((context) => context.Resolver.GetPackageFileName)
                ],
                [
                    nameof<TestCreationContext>((context) => context.Package),
                    nameof<TestCreationContext>((context) => context.Resolver.GetPackage)
                ],
                [
                    nameof<TestCreationContext>((context) => context.ModuleName),
                    nameof<TestCreationContext>((context) => context.Resolver.GetModuleName)
                ],
                [
                    nameof<TestCreationContext>((context) => context.DisplayName),
                    nameof<TestCreationContext>((context) => context.Resolver.GetDisplayName)
                ],
                [
                    nameof<TestCreationContext>((context) => context.Description),
                    nameof<TestCreationContext>((context) => context.Resolver.GetDescription)
                ]
            ];

            for (let redirectedProperty of redirectedProperties)
            {
                suite(
                    redirectedProperty[0],
                    () =>
                    {
                        test(
                            "Checking whether the property is forwarded to the resolver…",
                            async () =>
                            {
                                let stub = sandbox.stub(creationContext.Resolver, redirectedProperty[1] as any);
                                stub.returns(randomString);
                                strictEqual(await (creationContext as any)[redirectedProperty[0]], randomString);
                                ok(stub.calledOnce);
                            });

                        test(
                            "Checking whether the correct value for the metadata of the creation context is returned…",
                            async () =>
                            {
                                deepStrictEqual(
                                    await (creationContext as any)[redirectedProperty[0]],
                                    await (creationContext.Resolver as any)[redirectedProperty[1]](creationContext.GetMetadata(generator.Settings)));
                            });
                    });
            }

            suite(
                nameof<TestCreationContext>((context) => context.IsMonoRepo),
                () =>
                {
                    test(
                        "Checking whether the project is a mono repo is detected properly…",
                        () =>
                        {
                            strictEqual(creationContext.IsMonoRepo, false);

                            generator.Settings[GeneratorSettingKey.Components] ??= [];
                            generator.Settings[GeneratorSettingKey.Components].push(TSProjectComponent.MonoRepo);
                            strictEqual(creationContext.IsMonoRepo, true);
                        });
                });

            suite(
                nameof<TestCreationContext>((context) => context.IsWorkspaceRoot),
                () =>
                {
                    test(
                        "Checking whether the value is taken from the context settings…",
                        () =>
                        {
                            for (let isWorkspaceRoot of [true, false])
                            {
                                sandbox.replaceGetter(
                                    contextSettings,
                                    nameof.typed(() => contextSettings).IsWorkspaceRoot,
                                    () => isWorkspaceRoot);

                                strictEqual(creationContext.IsWorkspaceRoot, isWorkspaceRoot);
                                sandbox.restore();
                            }
                        });
                });

            suite(
                nameof<TestCreationContext>((context) => context.IsWorkspace),
                () =>
                {
                    test(
                        "Checking whether workspaces are detected properly…",
                        () =>
                        {
                            let expectations: Array<[[monoRepo: boolean, root: boolean], boolean]> = [
                                [[false, true], true],
                                [[false, false], true],
                                [[true, true], false],
                                [[true, false], true]
                            ];

                            for (let expectation of expectations)
                            {
                                sandbox.replaceGetter(
                                    creationContext,
                                    nameof.typed(() => creationContext).IsMonoRepo,
                                    () => expectation[0][0]);

                                sandbox.replaceGetter(
                                    creationContext,
                                    nameof.typed(() => creationContext).IsWorkspaceRoot,
                                    () => expectation[0][1]);

                                strictEqual(creationContext.IsWorkspace, expectation[1]);
                                sandbox.restore();
                            }
                        });
                });

            suite(
                nameof<TestCreationContext>((context) => context.ModuleKindQuestion),
                () =>
                {
                    test(
                        "Checking whether the options of the question are set properly…",
                        () =>
                        {
                            strictEqual(creationContext.ModuleKindQuestion.name, moduleKindKey);
                            strictEqual(creationContext.ModuleKindQuestion.message, moduleKindMessage);
                        });
                });

            suite(
                nameof<TestCreationContext>((context) => context.Questions),
                () =>
                {
                    test(
                        "Checking whether the module kind question is only asked for workspace roots…",
                        () =>
                        {
                            for (let isWorkspaceRoot of [true, false])
                            {
                                sandbox.replaceGetter(
                                    creationContext,
                                    nameof.typed(() => creationContext).IsWorkspaceRoot,
                                    () => isWorkspaceRoot);

                                strictEqual(
                                    creationContext.Questions.some(
                                        (question) =>
                                        {
                                            return question.name === creationContext.ModuleKindKey;
                                        }), isWorkspaceRoot);

                                sandbox.restore();
                            }
                        });
                });

            suite(
                nameof<TestCreationContext>((context) => context.FileMappings),
                () =>
                {
                    test(
                        "Checking whether the correct file mappings are returned for each configuration…",
                        () =>
                        {
                            for (let isWorkspaceRoot of [true, false])
                            {
                                for (let isMonoRepo of [true, false])
                                {
                                    sandbox.replaceGetter(
                                        creationContext,
                                        nameof.typed(() => creationContext).IsWorkspaceRoot,
                                        () => isWorkspaceRoot);

                                    sandbox.replaceGetter(
                                        creationContext,
                                        nameof.typed(() => creationContext).IsMonoRepo,
                                        () => isMonoRepo);

                                    let expectedSets = [creationContext.CommonFileMappings];

                                    if (creationContext.IsWorkspaceRoot)
                                    {
                                        expectedSets.push(creationContext.WorkspaceRootFileMappings);
                                    }

                                    if (creationContext.IsWorkspace)
                                    {
                                        expectedSets.push(creationContext.WorkspaceFileMappings);
                                    }

                                    for (let set of expectedSets)
                                    {
                                        ok(
                                            set.every(
                                                (fileMapping) =>
                                                {
                                                    return creationContext.FileMappings.some(
                                                        (otherFileMapping) =>
                                                        {
                                                            return isEqual(fileMapping, otherFileMapping);
                                                        });
                                                }));
                                    }

                                    sandbox.restore();
                                }
                            }
                        });
                });

            suite(
                nameof<TestCreationContext>((context) => context.GetSuggestedModuleName),
                () =>
                {
                    let tempDir: TempDirectory;

                    setup(
                        () =>
                        {
                            tempDir = new TempDirectory();
                            generator.Settings[TSProjectPackageKey.Destination] = tempDir.FullName;
                        });

                    teardown(
                        () =>
                        {
                            tempDir.Dispose();
                        });

                    /**
                     * Saves the specified {@linkcode npmPackage} to the project's `package.json` file.
                     *
                     * @param npmPackage
                     * The package to save.
                     */
                    async function SavePackage(npmPackage: Package): Promise<void>
                    {
                        await writeJson(await creationContext.PackageFileName, npmPackage.ToJSON());
                    }

                    test(
                        `Checking whether the name specified in the \`${Package.FileName}\` file is returned if it exists…`,
                        async () =>
                        {
                            let npmPackage = new Package();
                            npmPackage.Name = randomString;
                            await SavePackage(npmPackage);
                            strictEqual(await creationContext.GetSuggestedModuleName(generator.Settings), npmPackage.Name);
                        });
                });

            suite(
                nameof<TestCreationContext>((context) => context.ValidateModuleName),
                () =>
                {
                    test(
                        "Checking whether module names are validated according to npm's naming convention…",
                        async () =>
                        {
                            let assertions: Array<[string, boolean]> = [
                                ["Test", false],
                                ["test", true],
                                ["@hello", false],
                                ["@hello/world", true],
                                ["hello?", false]
                            ];

                            for (let assertion of assertions)
                            {
                                ok((await creationContext.ValidateModuleName(assertion[0], settings) === true && assertion[1]) || !assertion[1]);
                            }
                        });
                });

            suite(
                nameof<TestCreationContext>((context) => context.CreateSuggestedModuleName),
                () =>
                {
                    test(
                        "Checking whether the module name is inferred from the display name…",
                        async () =>
                        {
                            settings[TSProjectPackageKey.DisplayName] = "HelloWorld";
                            strictEqual(await creationContext.CreateSuggestedModuleName(settings), "hello-world");
                        });
                });
        });
}
