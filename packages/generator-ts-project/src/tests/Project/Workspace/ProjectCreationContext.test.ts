import { ok } from "node:assert";
import { FileMapping, GeneratorOptions, GeneratorSettingKey, IFileMapping } from "@manuth/extended-yo-generator";
import { RunContext } from "@manuth/extended-yo-generator-test";
import { JSONCFileMappingTester } from "@manuth/generator-ts-project-test";
import { TSConfigJSON } from "types-tsconfig";
import path from "upath";
import { ITSProjectPackage } from "../../../Project/Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../../../Project/Settings/ITSProjectSettings.js";
import { TSProjectComponent } from "../../../Project/Settings/TSProjectComponent.js";
import { TSProjectPackageKey } from "../../../Project/Settings/TSProjectPackageKey.js";
import { TSProjectSettingKey } from "../../../Project/Settings/TSProjectSettingKey.js";
import { TSProjectGenerator } from "../../../Project/TSProjectGenerator.js";
import { ProjectCreationContext } from "../../../Project/Workspace/ProjectCreationContext.js";
import { TSConfigFileMapping } from "../../../Serialization/TSConfigFileMapping.js";
import { TestContext } from "../../TestContext.js";

const { dirname, join, normalize, relative } = path;

/**
 * Registers tests for the {@linkcode ProjectCreationContext} class.
 *
 * @param context
 * The test context.
 */
export function ProjectCreationContextTests(context: TestContext<TSProjectGenerator>): void
{
    suite(
        nameof(ProjectCreationContext),
        () =>
        {
            /**
             * Provides the functionality to test jsonc file mappings.
             */
            type JSONCTester = JSONCFileMappingTester<TSProjectGenerator, ITSProjectSettings, GeneratorOptions, IFileMapping<ITSProjectSettings, GeneratorOptions>, TSConfigJSON>;

            /**
             * Represents a predicate.
             */
            type Predicate<T> = (item: T) => Promise<boolean>;

            /**
             * Provides an implementation of the {@linkcode ProjectCreationContext} class for testing.
             */
            class TestCreationContext extends ProjectCreationContext<ITSProjectSettings, GeneratorOptions, ITSProjectPackage>
            {
                /**
                 * @inheritdoc
                 */
                public override get CommonFileMappings(): Array<IFileMapping<ITSProjectSettings, GeneratorOptions>>
                {
                    return super.CommonFileMappings;
                }

                /**
                 * @inheritdoc
                 */
                public override get WorkspaceRootFileMappings(): Array<IFileMapping<ITSProjectSettings, GeneratorOptions>>
                {
                    return super.WorkspaceRootFileMappings;
                }

                /**
                 * @inheritdoc
                 */
                public override get WorkspaceFileMappings(): Array<IFileMapping<ITSProjectSettings, GeneratorOptions>>
                {
                    return super.WorkspaceFileMappings;
                }
            }

            let runContext: RunContext<TSProjectGenerator>;
            let generator: TSProjectGenerator;
            let creationContext: TestCreationContext;

            suiteSetup(
                async function()
                {
                    this.timeout(30 * 1000);
                    runContext = context.ExecuteGenerator();
                    await runContext;
                    generator = runContext.generator;
                });

            setup(
                () =>
                {
                    creationContext = new TestCreationContext(generator);
                });

            /**
             * Asserts that the specified {@linkcode predicate} applies to the tsconfig files in the specified {@linkcode fileMappings}.
             *
             * @param fileMappings
             * The file mappings to check.
             *
             * @param predicate
             * The predicate to check the tsconfig files for.
             *
             * @param all
             * A value indicating whether all tsconfig files have to apply to the predicate.
             */
            async function assertConfigFiles(fileMappings: Array<IFileMapping<ITSProjectSettings, GeneratorOptions>>, predicate: Predicate<JSONCTester>, all = true): Promise<void>
            {
                let configFileCandidates = fileMappings.filter(
                    (fileMapping) =>
                    {
                        return fileMapping instanceof TSConfigFileMapping ||
                            new FileMapping(generator, fileMapping).Destination.includes("tsconfig");
                    });

                let testers: JSONCTester[] = [];

                for (let configFileCandidate of configFileCandidates)
                {
                    testers.push(new JSONCFileMappingTester(generator, configFileCandidate));
                }

                if (all)
                {
                    ok(testers.every(predicate));
                }
                else
                {
                    ok(testers.some(predicate));
                }
            }

            /**
             * Asserts that no tsconfig reference points to an extraneous package.
             *
             * @param fileMappings
             * The file mappings to check.
             *
             * @param allowCrossWorkspace
             * A value indicating whether references to other workspaces and the workspace root are allowed.
             */
            async function assertNoExtraneous(fileMappings: Array<IFileMapping<ITSProjectSettings, GeneratorOptions>>, allowCrossWorkspace = false): Promise<void>
            {
                return assertConfigFiles(
                    fileMappings,
                    async (tester) =>
                    {
                        let configDirName = generator.destinationPath(dirname(tester.FileMapping.Destination));
                        let relativeRoot = relative(configDirName, generator.destinationPath(allowCrossWorkspace ? "." : creationContext.Destination));
                        let bannedDirName = normalize(join(relativeRoot, ".."));
                        await tester.Run();
                        let config = await tester.ParseOutput();

                        return (config.references ?? []).every(
                            (reference) =>
                            {
                                return !normalize(reference.path).startsWith(bannedDirName);
                            });
                    },
                    true);
            }

            /**
             * Asserts that all tsconfig references point to existent files.
             *
             * @param fileMappings
             * The file mappings to check.
             */
            async function assertReferencesExist(fileMappings: Array<IFileMapping<ITSProjectSettings, GeneratorOptions>>): Promise<void>
            {
                return assertConfigFiles(
                    fileMappings,
                    async (tester) =>
                    {
                        let configDirName = generator.destinationPath(dirname(tester.FileMapping.Destination));
                        await tester.Run();
                        let config = await tester.ParseOutput();

                        return (config.references ?? []).every(
                            (reference) =>
                            {
                                let fullName = join(configDirName, reference.path);

                                return generator.FileMappingCollection.Items.some(
                                    (fileMapping) =>
                                    {
                                        return [
                                            fullName,
                                            join(fullName, TSConfigFileMapping.FileName)
                                        ].includes(generator.destinationPath(fileMapping.Destination));
                                    });
                            });
                    },
                    true);
            }

            suite(
                nameof<TestCreationContext>((context) => context.CommonFileMappings),
                () =>
                {
                    test(
                        "Checking whether only existing config files are referenced by the tsconfig files…",
                        async () =>
                        {
                            for (let monoRepo of [false, true])
                            {
                                for (let eslint of [true, false])
                                {
                                    generator.Settings[GeneratorSettingKey.Components] = [
                                        ...(monoRepo ? [TSProjectComponent.MonoRepo] : []),
                                        ...(eslint ? [TSProjectComponent.Linting] : [])
                                    ];

                                    generator.Settings[TSProjectSettingKey.Workspaces] = ["hello", "world"].map(
                                        (workspaceName): ITSProjectPackage =>
                                        {
                                            return {
                                                [TSProjectPackageKey.Destination]: `./packages/test/${workspaceName}`,
                                                [TSProjectPackageKey.DisplayName]: workspaceName,
                                                [TSProjectPackageKey.Name]: workspaceName,
                                                [TSProjectPackageKey.Description]: ""
                                            };
                                        });

                                    await assertNoExtraneous(creationContext.CommonFileMappings, true);
                                    await assertReferencesExist(creationContext.CommonFileMappings);

                                    for (let workspaceContext of generator.WorkspaceCreationContexts)
                                    {
                                        await assertNoExtraneous(
                                            new TestCreationContext(generator, workspaceContext.Settings).CommonFileMappings,
                                            true);

                                        await assertReferencesExist(new TestCreationContext(generator, workspaceContext.Settings).CommonFileMappings);
                                    }
                                }
                            }
                        });
                });

            suite(
                nameof<TestCreationContext>((context) => context.WorkspaceRootFileMappings),
                () =>
                {
                    test(
                        "Checking whether no extraneous projects are referenced in tsconfig files…",
                        async () =>
                        {
                            generator.Settings[GeneratorSettingKey.Components] = [TSProjectComponent.MonoRepo];

                            for (let eslint of [true, false])
                            {
                                if (eslint)
                                {
                                    generator.Settings[GeneratorSettingKey.Components].push(TSProjectComponent.Linting);
                                }

                                await assertNoExtraneous(creationContext.WorkspaceRootFileMappings);
                                await assertReferencesExist(creationContext.WorkspaceRootFileMappings);
                            }
                        });
                });

            suite(
                nameof<TestCreationContext>((context) => context.WorkspaceFileMappings),
                () =>
                {
                    test(
                        "Checking whether no extraneous projects are referenced in tsconfig files…",
                        async () =>
                        {
                            await assertNoExtraneous(creationContext.WorkspaceFileMappings);
                            await assertReferencesExist(creationContext.WorkspaceFileMappings);
                        });
                });
        });
}
