import { strictEqual } from "node:assert";
import { ITSProjectPackage } from "../../../Project/Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../../../Project/Settings/ITSProjectSettings.js";
import { TSProjectGenerator } from "../../../Project/TSProjectGenerator.js";
import { RootSettings } from "../../../Project/Workspace/RootSettings.js";
import { TestContext } from "../../TestContext.js";

/**
 * Registers tests for the {@linkcode RootSettings} class.
 *
 * @param context
 * The test context.
 */
export function RootSettingTests(context: TestContext<TSProjectGenerator>): void
{
    suite(
        nameof(RootSettings),
        () =>
        {
            let generator: TSProjectGenerator;
            let settings: RootSettings<ITSProjectSettings, ITSProjectPackage>;

            suiteSetup(
                async function()
                {
                    this.timeout(30 * 1000);
                    generator = await context.Generator;
                });

            setup(
                () =>
                {
                    settings = new RootSettings();
                });

            suite(
                nameof<RootSettings<any, any>>((settings) => settings.GetDestination),
                () =>
                {
                    test(
                        "Checking whether a default path is returned if none is specified…",
                        () =>
                        {
                            strictEqual(
                                settings.GetDestination({} as any),
                                ".");
                        });
                });

            suite(
                nameof<RootSettings<any, any>>((settings) => settings.GetMetadata),
                () =>
                {
                    test(
                        "Checking whether the metadata is returned properly…",
                        () =>
                        {
                            strictEqual(settings.GetMetadata(generator.Settings), generator.Settings);
                        });
                });
        });
}
