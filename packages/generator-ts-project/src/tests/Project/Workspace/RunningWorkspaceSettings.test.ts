import { ok, strictEqual } from "node:assert";
import { createSandbox, SinonSandbox } from "sinon";
import path from "upath";
import { ITSProjectPackage } from "../../../Project/Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../../../Project/Settings/ITSProjectSettings.js";
import { TSProjectPackageKey } from "../../../Project/Settings/TSProjectPackageKey.js";
import { TSProjectSettingKey } from "../../../Project/Settings/TSProjectSettingKey.js";
import { TSProjectGenerator } from "../../../Project/TSProjectGenerator.js";
import { ICreationContextSettings } from "../../../Project/Workspace/ICreationContextSettings.js";
import { RunningWorkspaceSettings } from "../../../Project/Workspace/RunningWorkspaceSettings.js";
import { WorkspaceSettings } from "../../../Project/Workspace/WorkspaceSettings.js";
import { TestContext } from "../../TestContext.js";

const { normalize } = path;

/**
 * Registers tests for the {@linkcode RunningWorkspaceSettings} class.
 *
 * @param context
 * The test context.
 */
export function RunningWorkspaceSettingTests(context: TestContext<TSProjectGenerator>): void
{
    suite(
        nameof(RunningWorkspaceSettings),
        () =>
        {
            /**
             * Represents the workspace settings to test.
             */
            type TestSettings = RunningWorkspaceSettings<ITSProjectSettings, ITSProjectPackage>;

            let sandbox: SinonSandbox;
            let randomString: string;
            let generatorSettings: ITSProjectSettings;
            let generator: TSProjectGenerator;
            let nestedSettings: ICreationContextSettings<ITSProjectSettings, ITSProjectPackage>;
            let settings: TestSettings;

            suiteSetup(
                async function()
                {
                    this.timeout(30 * 1000);
                    generator = await context.Generator;
                });

            setup(
                () =>
                {
                    sandbox = createSandbox();
                    randomString = context.RandomString;

                    generatorSettings = {
                        [TSProjectSettingKey.Workspaces]: [
                            {
                                [TSProjectPackageKey.Destination]: "./packages/rocket",
                                [TSProjectPackageKey.DisplayName]: "Rocket",
                                [TSProjectPackageKey.Name]: "rocket"
                            },
                            {
                                [TSProjectPackageKey.Destination]: "./packages/groot",
                                [TSProjectPackageKey.DisplayName]: "Groot",
                                [TSProjectPackageKey.Name]: "groot"
                            }
                        ]
                    } as ITSProjectSettings;

                    nestedSettings = new WorkspaceSettings(generator, 0);
                    settings = new RunningWorkspaceSettings(nestedSettings, generatorSettings);
                });

            teardown(
                () =>
                {
                    sandbox.restore();
                });

            let redirectedProperties = [
                nameof.typed<TestSettings>().DestinationKey,
                nameof.typed<TestSettings>().ModuleNameKey,
                nameof.typed<TestSettings>().DescriptionKey,
                nameof.typed<TestSettings>().AllowOutside,
                nameof.typed<TestSettings>().IsWorkspaceRoot
            ];

            let banProperties: Array<[string, TSProjectPackageKey]> = [
                [nameof<TestSettings>((settings) => settings.BannedDirectories), TSProjectPackageKey.Destination],
                [nameof<TestSettings>((settings) => settings.BannedDisplayNames), TSProjectPackageKey.DisplayName],
                [nameof<TestSettings>((settings) => settings.BannedModuleNames), TSProjectPackageKey.Name]
            ];

            let redirectedMethods = [
                nameof.typed<TestSettings>().GetPackageRoot,
                nameof.typed<TestSettings>().GetDestination,
                nameof.typed<TestSettings>().GetMetadata
            ];

            for (let property of redirectedProperties)
            {
                suite(
                    property,
                    () =>
                    {
                        test(
                            "Checking whether the property is redirected to the nested settings…",
                            () =>
                            {
                                sandbox.replaceGetter(nestedSettings, property, () => randomString as any);
                                strictEqual(settings[property], randomString);
                            });
                    });
            }

            for (let banProperty of banProperties)
            {
                let [propertyName, workspaceSetting] = banProperty;

                suite(
                    propertyName,
                    () =>
                    {
                        test(
                            "Checking whether taken values chosen for other workspaces are listed as banned…",
                            () =>
                            {
                                for (let workspace of generatorSettings[TSProjectSettingKey.Workspaces])
                                {
                                    ok(((settings as any)[propertyName] as string[]).includes(workspace[workspaceSetting]));
                                }
                            });
                    });
            }

            for (let method of redirectedMethods)
            {
                suite(
                    method,
                    () =>
                    {
                        test(
                            "Checking whether the method is redirected to the nested settings…",
                            async () =>
                            {
                                let stub = sandbox.stub(nestedSettings, method);
                                stub.returns(randomString);

                                if (method === nameof<TestSettings>((settings) => settings.GetDestination))
                                {
                                    strictEqual(normalize(await (settings as any)[method]()), normalize(randomString));
                                }
                                else
                                {
                                    strictEqual(await (settings as any)[method](), randomString);
                                }

                                ok(stub.called);
                            });
                    });
            }
        });
}
