import { ok, strictEqual } from "node:assert";
import { ITSProjectPackage } from "../../../Project/Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../../../Project/Settings/ITSProjectSettings.js";
import { TSProjectPackageKey } from "../../../Project/Settings/TSProjectPackageKey.js";
import { TSProjectSettingKey } from "../../../Project/Settings/TSProjectSettingKey.js";
import { TSProjectGenerator } from "../../../Project/TSProjectGenerator.js";
import { WorkspaceSettings } from "../../../Project/Workspace/WorkspaceSettings.js";
import { TestContext } from "../../TestContext.js";

/**
 * Registers tests for the {@linkcode WorkspaceSettings} class.
 *
 * @param context
 * The test context.
 */
export function WorkspaceSettingTests(context: TestContext<TSProjectGenerator>): void
{
    suite(
        nameof(WorkspaceSettings),
        () =>
        {
            let generator: TSProjectGenerator;
            let workspaceConfig: ITSProjectPackage;
            let settings: WorkspaceSettings<ITSProjectSettings, ITSProjectPackage>;

            suiteSetup(
                async function()
                {
                    this.timeout(30 * 1000);
                    generator = await context.Generator;
                });

            setup(
                () =>
                {
                    workspaceConfig = {
                        [TSProjectPackageKey.Destination]: "./packages/test",
                        [TSProjectPackageKey.DisplayName]: "test",
                        [TSProjectPackageKey.Name]: "test",
                        [TSProjectPackageKey.Description]: ""
                    };

                    generator.Settings[TSProjectPackageKey.Destination] = "./my-project";
                    generator.Settings[TSProjectSettingKey.Workspaces] = [workspaceConfig];
                    settings = new WorkspaceSettings(generator, 0);
                });

            suite(
                nameof<WorkspaceSettings<any, any>>((settings) => settings.BannedDisplayNames),
                () =>
                {
                    test(
                        "Checking whether display names which are in use are banned…",
                        () =>
                        {
                            ok(settings.BannedDisplayNames.includes("Solution Items"));
                        });
                });

            suite(
                nameof<WorkspaceSettings<any, any>>((settings) => settings.GetDestination),
                () =>
                {
                    test(
                        "Checking whether the path points to the correct directory…",
                        () =>
                        {
                            strictEqual(
                                generator.destinationPath(settings.GetDestination(workspaceConfig)),
                                generator.destinationPath(
                                    generator.Settings[TSProjectPackageKey.Destination],
                                    workspaceConfig[TSProjectPackageKey.Destination]));
                        });
                });

            suite(
                nameof<WorkspaceSettings<any, any>>((settings) => settings.GetMetadata),
                () =>
                {
                    test(
                        "Checking whether the metadata is extracted properly…",
                        () =>
                        {
                            strictEqual(settings.GetMetadata(generator.Settings), workspaceConfig);
                        });
                });
        });
}
