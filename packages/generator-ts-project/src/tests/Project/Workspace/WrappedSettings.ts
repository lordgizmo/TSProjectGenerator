import { ITSProjectPackage } from "../../../Project/Settings/ITSProjectPackage.js";
import { ITSProjectSettings } from "../../../Project/Settings/ITSProjectSettings.js";
import { ICreationContextSettings } from "../../../Project/Workspace/ICreationContextSettings.js";

/**
 * Provides a wrapper for dynamically changing the represented context settings.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TAnswers
 * The type of the answers of the context.
 */
export class WrappedSettings<TSettings extends ITSProjectSettings, TAnswers extends ITSProjectPackage> implements ICreationContextSettings<TSettings, TAnswers>
{
    /**
     * The wrapped settings.
     */
    private settings: ICreationContextSettings<TSettings, TAnswers>;

    /**
     * Initializes a new instance of the {@linkcode WrappedSettings} class.
     *
     * @param settings
     * The settings to represent.
     */
    public constructor(settings: ICreationContextSettings<TSettings, TAnswers>)
    {
        this.settings = settings;
    }

    /**
     * Gets or sets the wrapped settings.
     */
    public get Settings(): ICreationContextSettings<TSettings, TAnswers>
    {
        return this.settings;
    }

    /**
     * @inheritdoc
     */
    public set Settings(value: ICreationContextSettings<TSettings, TAnswers>)
    {
        this.settings = value;
    }

    /**
     * @inheritdoc
     */
    public get DestinationKey(): keyof TAnswers
    {
        return this.Settings.DestinationKey;
    }

    /**
     * @inheritdoc
     */
    public get DisplayNameKey(): keyof TAnswers
    {
        return this.Settings.DisplayNameKey;
    }

    /**
     * @inheritdoc
     */
    public get ModuleNameKey(): keyof TAnswers
    {
        return this.Settings.ModuleNameKey;
    }

    /**
     * @inheritdoc
     */
    public get DescriptionKey(): keyof TAnswers
    {
        return this.Settings.DescriptionKey;
    }

    /**
     * @inheritdoc
     */
    public get AllowOutside(): boolean
    {
        return this.Settings.AllowOutside;
    }

    /**
     * @inheritdoc
     */
    public get IsWorkspaceRoot(): boolean
    {
        return this.Settings.IsWorkspaceRoot;
    }

    /**
     * @inheritdoc
     */
    public get BannedDirectories(): string[]
    {
        return this.Settings.BannedDirectories;
    }

    /**
     * @inheritdoc
     */
    public get BannedDisplayNames(): string[]
    {
        return this.Settings.BannedDisplayNames;
    }

    /**
     * @inheritdoc
     */
    public get BannedModuleNames(): string[]
    {
        return this.Settings.BannedModuleNames;
    }

    /**
     * @inheritdoc
     *
     * @param answers
     * The answers provided by the user up to this point.
     *
     * @returns
     * The root of the package to create.
     */
    public GetPackageRoot(answers: TAnswers): Promise<string>
    {
        return this.Settings.GetPackageRoot(answers);
    }

    /**
     * @inheritdoc
     *
     * @param answers
     * The answers provided by the user.
     *
     * @returns
     * The destination path of the generator.
     */
    public GetDestination(answers: TAnswers): string
    {
        return this.Settings.GetDestination(answers);
    }

    /**
     * @inheritdoc
     *
     * @param settings
     * The settings of the generator.
     *
     * @returns
     * The metadata of the package.
     */
    public GetMetadata(settings: TSettings): TAnswers
    {
        return this.Settings.GetMetadata(settings);
    }
}
