import { basename } from "node:path";
import { CreationPromptResolverTests } from "./CreationPromptResolver.test.js";
import { PackageCreationContextTests } from "./PackageCreationContext.test.js";
import { ProjectCreationContextTests } from "./ProjectCreationContext.test.js";
import { RootSettingTests } from "./RootSettings.test.js";
import { RunningWorkspaceSettingTests } from "./RunningWorkspaceSettings.test.js";
import { WorkspaceSettingTests } from "./WorkspaceSettings.test.js";
import { TSProjectGenerator } from "../../../Project/TSProjectGenerator.js";
import { TestContext } from "../../TestContext.js";

/**
 * Registers tests for workspace components.
 *
 * @param context
 * The test context.
 */
export function WorkspaceTests(context: TestContext<TSProjectGenerator>): void
{
    suite(
        basename(new URL(".", import.meta.url).pathname),
        () =>
        {
            RootSettingTests(context);
            WorkspaceSettingTests(context);
            RunningWorkspaceSettingTests(context);
            CreationPromptResolverTests(context);
            PackageCreationContextTests(context);
            ProjectCreationContextTests(context);
        });
}
