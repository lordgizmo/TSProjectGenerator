import { GeneratorOptions, IGeneratorSettings } from "@manuth/extended-yo-generator";
import { JSONProcessor } from "../../Serialization/JSONProcessor.js";

/**
 * Provides an implementation of the {@linkcode JSONProcessor} class for testing.
 *
 * @template TSettings
 * The type of the settings of the generator.
 *
 * @template TOptions
 * The type of the options of the generator.
 */
export class TestJSONProcessor<TSettings extends IGeneratorSettings, TOptions extends GeneratorOptions> extends JSONProcessor<TSettings, TOptions, any>
{
    /**
     * The data to return from the {@linkcode Process} method.
     */
    private data: any;

    /**
     * Initializes a new instance of the {@linkcode TestJSONProcessor} class.
     *
     * @param data
     * The data to return from the {@linkcode Process} method.
     */
    public constructor(data: any)
    {
        super(undefined as any);
        this.data = data;
    }

    /**
     * @inheritdoc
     *
     * @returns
     * The predefined {@linkcode data}.
     */
    public override async Process(): Promise<any>
    {
        return this.data;
    }
}
