import { spawnSync, SpawnSyncOptions, SpawnSyncReturns } from "node:child_process";
import { fileURLToPath } from "node:url";
import npmWhich from "npm-which";

/**
 * Spawns an npm command.
 *
 * @param args
 * The arguments to pass to npm.
 *
 * @param options
 * The options for spawning npm.
 *
 * @returns
 * The result of the process.
 */
export function spawnNPM(args?: string[], options?: SpawnSyncOptions): SpawnSyncReturns<string | Buffer>
{
    return spawnSync(
        npmWhich(fileURLToPath(new URL(".", import.meta.url))).sync("npm"),
        args ?? [],
        options ?? {});
}
