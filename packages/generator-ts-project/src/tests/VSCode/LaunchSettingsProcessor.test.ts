import { deepStrictEqual, ok, strictEqual } from "node:assert";
import { GeneratorOptions, IGeneratorSettings } from "@manuth/extended-yo-generator";
import { TestCodeWorkspaceComponent } from "./Components/TestCodeWorkspaceComponent.js";
import { IDebugConfiguration } from "../../VSCode/IDebugConfiguration.js";
import { ILaunchSettings } from "../../VSCode/ILaunchSettings.js";
import { LaunchSettingsProcessor } from "../../VSCode/LaunchSettingsProcessor.js";
import { TestContext } from "../TestContext.js";

/**
 * Registers tests for the {@linkcode LaunchSettingsProcessor} class.
 */
export function LaunchSettingsProcessorTests(): void
{
    suite(
        nameof(LaunchSettingsProcessor),
        () =>
        {
            let context = TestContext.Default;
            let launchSettings: ILaunchSettings;
            let includedDebugConfig: IDebugConfiguration;
            let excludedDebugConfig: IDebugConfiguration;
            let mutatedDebugConfig: IDebugConfiguration;
            let newName: string;
            let processor: TestLaunchSettingsProcessor;

            /**
             * Provides an implementation of the {@linkcode LaunchSettingsProcessor} class for testing.
             */
            class TestLaunchSettingsProcessor extends LaunchSettingsProcessor<IGeneratorSettings, GeneratorOptions>
            {
                /**
                 * @inheritdoc
                 *
                 * @param debugConfig
                 * The debug-configuration to filter.
                 *
                 * @returns
                 * A value indicating whether the debug-configuration should be included.
                 */
                public override async FilterDebugConfig(debugConfig: IDebugConfiguration): Promise<boolean>
                {
                    return debugConfig !== excludedDebugConfig;
                }

                /**
                 * @inheritdoc
                 *
                 * @param debugConfig
                 * The debug-configuration to process.
                 *
                 * @returns
                 * The processed debug-configuration.
                 */
                public override async ProcessDebugConfig(debugConfig: IDebugConfiguration): Promise<IDebugConfiguration>
                {
                    if (debugConfig === mutatedDebugConfig)
                    {
                        debugConfig.name = newName;
                    }

                    return debugConfig;
                }

                /**
                 * @inheritdoc
                 *
                 * @param config
                 * The configuration to replace the workspace directives in.
                 *
                 * @param newName
                 * The new name of the workspace.
                 */
                public override ReplaceWorkspaceDirectives(config: IDebugConfiguration, newName?: string): void
                {
                    super.ReplaceWorkspaceDirectives(config, newName);
                }
            }

            suiteSetup(
                async function()
                {
                    this.timeout(30 * 1000);
                    processor = new TestLaunchSettingsProcessor(new TestCodeWorkspaceComponent(await context.Generator));
                });

            setup(
                () =>
                {
                    includedDebugConfig = {
                        name: context.Random.string(9),
                        type: context.Random.string(9),
                        request: context.Random.string(9)
                    };

                    excludedDebugConfig = {
                        name: context.Random.string(10),
                        type: context.Random.string(10),
                        request: context.Random.string(10)
                    };

                    mutatedDebugConfig = {
                        name: context.Random.string(11),
                        type: context.Random.string(11),
                        request: context.Random.string(11)
                    };

                    launchSettings = {
                        version: "",
                        configurations: [
                            includedDebugConfig,
                            excludedDebugConfig,
                            mutatedDebugConfig
                        ]
                    };

                    newName = context.Random.string(12);
                });

            suite(
                nameof<LaunchSettingsProcessor<any, any>>((processor) => processor.Process),
                () =>
                {
                    test(
                        "Checking whether debug-configurations are only processed if existent…",
                        async () =>
                        {
                            strictEqual(
                                (await processor.Process({ version: "", configurations: undefined })).configurations,
                                undefined);
                        });
                });

            suite(
                nameof<TestLaunchSettingsProcessor>((processor) => processor.FilterDebugConfig),
                () =>
                {
                    test(
                        "Checking whether debug-configurations can be excluded…",
                        async () =>
                        {
                            ok((await processor.Process(launchSettings)).configurations?.includes(includedDebugConfig));
                            ok(!(await processor.Process(launchSettings)).configurations?.includes(excludedDebugConfig));
                        });
                });

            suite(
                nameof<TestLaunchSettingsProcessor>((processor) => processor.ProcessDebugConfig),
                () =>
                {
                    test(
                        "Checking whether debug-configurations can be mutated…",
                        async () =>
                        {
                            ok(
                                (await processor.Process(launchSettings)).configurations?.some(
                                    (configuration) => configuration.name === newName));
                        });
                });

            suite(
                nameof<TestLaunchSettingsProcessor>((processor) => processor.ReplaceWorkspaceDirectives),
                () =>
                {
                    const destinationSetting = "outFiles";

                    test(
                        "Checking whether all workspace directives are replaced properly…",
                        () =>
                        {
                            let workspaceDirective = "${workspaceFolder:Project}";

                            let configurations: Array<[newName: string | undefined, replacement: string]> = [
                                [undefined, "${workspaceFolder}"],
                                ["Test", "${workspaceFolder:Test}"]
                            ];

                            for (let configuration of configurations)
                            {
                                let [newName, replacement] = configuration;

                                let debugConfig: IDebugConfiguration = {
                                    ...includedDebugConfig,
                                    program: workspaceDirective,
                                    args: [
                                        workspaceDirective,
                                        workspaceDirective
                                    ],
                                    [destinationSetting]: [
                                        workspaceDirective
                                    ],
                                    cwd: `${workspaceDirective}/test`
                                };

                                processor.ReplaceWorkspaceDirectives(debugConfig, newName);

                                deepStrictEqual(
                                    debugConfig,
                                    {
                                        ...includedDebugConfig,
                                        program: replacement,
                                        args: [replacement, replacement],
                                        [destinationSetting]: [replacement],
                                        cwd: `${replacement}/test`
                                    });
                            }
                        });

                    test(
                        `Checking whether duplicate \`${destinationSetting}\` entries are prevented…`,
                        () =>
                        {
                            for (let newName of [undefined, "Project"])
                            {
                                let debugConfig = {
                                    ...includedDebugConfig,
                                    [destinationSetting]: [
                                        "${workspaceFolder:Client}",
                                        "${workspaceFolder:Server}",
                                        "${workspaceFolder:Extras}"
                                    ]
                                };

                                processor.ReplaceWorkspaceDirectives(debugConfig, newName);
                                strictEqual(debugConfig[destinationSetting].length, 1);
                                strictEqual(debugConfig[destinationSetting][0], processor.GetWorkspaceFolderDirective(newName));
                            }
                        });

                    test(
                        `Checking whether the \`${nameof<IDebugConfiguration>((config) => config.cwd)}\` config is removed if unnecessary…`,
                        () =>
                        {
                            let debugConfig: IDebugConfiguration = {
                                ...includedDebugConfig,
                                cwd: "${workspaceFolder:Test}"
                            };

                            for (let newName of [undefined, "Project"])
                            {
                                processor.ReplaceWorkspaceDirectives(debugConfig, newName);
                                strictEqual(Boolean(newName), "cwd" in debugConfig);
                            }
                        });

                    test(
                        `Checking whether the \`${nameof<IDebugConfiguration>((config) => config.cwd)}\` config is added when creating a named workspace folder…`,
                        () =>
                        {
                            let newName = "Project";
                            let debugConfig: IDebugConfiguration = { ...includedDebugConfig };
                            delete debugConfig.cwd;
                            processor.ReplaceWorkspaceDirectives(debugConfig, newName);
                            strictEqual(debugConfig.cwd, processor.GetWorkspaceFolderDirective(newName));
                        });
                });
        });
}
