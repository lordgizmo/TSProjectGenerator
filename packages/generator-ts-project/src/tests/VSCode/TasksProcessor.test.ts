import { deepStrictEqual, ok, strictEqual } from "node:assert";
import { GeneratorOptions, IGeneratorSettings } from "@manuth/extended-yo-generator";
import { TestCodeWorkspaceComponent } from "./Components/TestCodeWorkspaceComponent.js";
import { ITaskDefinition } from "../../VSCode/ITaskDefinition.js";
import { ITaskSettings } from "../../VSCode/ITaskSettings.js";
import { TasksProcessor } from "../../VSCode/TasksProcessor.js";
import { TestContext } from "../TestContext.js";

/**
 * Registers tests for the {@linkcode TasksProcessor} class.
 */
export function TasksProcessorTests(): void
{
    suite(
        nameof(TasksProcessor),
        () =>
        {
            let context = TestContext.Default;
            let includedTask: ITaskDefinition;
            let excludedTask: ITaskDefinition;
            let mutatedTask: ITaskDefinition;
            let newType: string;
            let taskMeta: ITaskSettings;
            let processor: TestTasksProcessor;

            /**
             * Provides an implementation of the {@linkcode TasksProcessor} class for testing.
             */
            class TestTasksProcessor extends TasksProcessor<IGeneratorSettings, GeneratorOptions>
            {
                /**
                 * @inheritdoc
                 *
                 * @param task
                 * The task to filter.
                 *
                 * @returns
                 * A value indicating whether the task should be included.
                 */
                public override async FilterTask(task: ITaskDefinition): Promise<boolean>
                {
                    return task !== excludedTask;
                }

                /**
                 * @inheritdoc
                 *
                 * @param task
                 * The task to process.
                 *
                 * @returns
                 * The processed task.
                 */
                public override async ProcessTask(task: ITaskDefinition): Promise<ITaskDefinition>
                {
                    if (task === mutatedTask)
                    {
                        return {
                            type: newType
                        };
                    }
                    else
                    {
                        return task;
                    }
                }

                /**
                 * @inheritdoc
                 *
                 * @param task
                 * The task to replace the workspace directives in.
                 *
                 * @param newName
                 * The name of the new workspace.
                 */
                public override ReplaceWorkspaceDirectives(task: ITaskDefinition, newName?: string | undefined): void
                {
                    super.ReplaceWorkspaceDirectives(task, newName);
                }
            }

            suiteSetup(
                async function()
                {
                    this.timeout(30 * 1000);
                    processor = new TestTasksProcessor(new TestCodeWorkspaceComponent(await context.Generator));
                });

            setup(
                () =>
                {
                    includedTask = {
                        type: context.Random.string(8)
                    };

                    excludedTask = {
                        type: context.Random.string(9)
                    };

                    mutatedTask = {
                        type: context.Random.string(10)
                    };

                    newType = context.Random.string(11);

                    taskMeta = {
                        version: "",
                        tasks: [
                            includedTask,
                            excludedTask,
                            mutatedTask
                        ]
                    };
                });

            suite(
                nameof<TasksProcessor<any, any>>((processor) => processor.ReplaceWorkspaceFolder),
                () =>
                {
                    test(
                        "Checking whether workspace folder directives are replaced properly…",
                        () =>
                        {
                            let expectations: Array<[string | undefined, string]> = [
                                [undefined, "${workspaceFolder}"],
                                ["Project", "${workspaceFolder:Project}"]
                            ];

                            for (let original of ["${workspaceFolder}", "${workspaceFolder:Named}"])
                            {
                                for (let expectation of expectations)
                                {
                                    let [newName, replacement] = expectation;

                                    let task = {
                                        command: original,
                                        script: original,
                                        args: [
                                            original,
                                            `${original}2`
                                        ]
                                    } as ITaskDefinition;

                                    processor.ReplaceWorkspaceDirectives(task, newName);

                                    deepStrictEqual(
                                        task,
                                        {
                                            command: replacement,
                                            script: replacement,
                                            args: [
                                                replacement,
                                                `${replacement}2`
                                            ]
                                        } as ITaskDefinition);
                                }
                            }
                        });

                    test(
                        "Checking whether workspace folder directives persist in the `cwd` option when renaming the workspace folder…",
                        () =>
                        {
                            let config = {
                                options: {
                                    cwd: "${workspaceFolder}"
                                }
                            } as ITaskDefinition;

                            processor.ReplaceWorkspaceDirectives(config, "Project");
                            strictEqual(config.options?.cwd, "${workspaceFolder:Project}");
                        });

                    test(
                        "Checking whether unnecessary `fileLocation` options are deleted…",
                        async () =>
                        {
                            let config = {
                                problemMatcher: {
                                    fileLocation: [
                                        "relative",
                                        "${workspaceFolder:Named}"
                                    ]
                                }
                            } as ITaskDefinition;

                            processor.ReplaceWorkspaceDirectives(config);
                            ok(typeof config.problemMatcher === "object");
                            ok(!("fileLocation" in config.problemMatcher));
                        });

                    test(
                        "Checking whether unnecessary problemMatcher-arrays are simplified…",
                        async () =>
                        {
                            let problemMatcher = "$eslint-compact";

                            let config = {
                                problemMatcher: [
                                    problemMatcher
                                ]
                            } as ITaskDefinition;

                            processor.ReplaceWorkspaceDirectives(config);
                            strictEqual(config.problemMatcher, problemMatcher);
                        });
                });

            suite(
                nameof<TasksProcessor<any, any>>((processor) => processor.Process),
                () =>
                {
                    test(
                        "Checking whether tasks are only processed if existent…",
                        async () =>
                        {
                            strictEqual(
                                (await processor.Process({ version: "", tasks: undefined })).tasks,
                                undefined);
                        });
                });

            suite(
                nameof<TestTasksProcessor>((processor) => processor.FilterTask),
                () =>
                {
                    test(
                        "Checking whether tasks can be filtered…",
                        async () =>
                        {
                            ok(taskMeta.tasks?.includes(excludedTask));
                            ok((await processor.Process(taskMeta)).tasks?.includes(includedTask));
                            ok(!(await processor.Process(taskMeta)).tasks?.includes(excludedTask));
                        });
                });

            suite(
                nameof<TestTasksProcessor>((processor) => processor.ProcessTask),
                () =>
                {
                    test(
                        "Checking whether tasks can be pre-processed…",
                        async () =>
                        {
                            /**
                             * Checks whether the specified {@linkcode taskMeta} contains the predefined mutation.
                             *
                             * @param taskMetadata
                             * The task-metadata.
                             *
                             * @param expected
                             * A value indicating whether a mutation is expected to exist.
                             */
                            function AssertMutation(taskMetadata: ITaskSettings, expected = true): void
                            {
                                strictEqual(
                                    taskMetadata.tasks?.some(
                                        (task) => task.type === newType), expected);
                            }

                            AssertMutation(taskMeta, false);
                            AssertMutation(await processor.Process(taskMeta), true);
                        });
                });
        });
}
